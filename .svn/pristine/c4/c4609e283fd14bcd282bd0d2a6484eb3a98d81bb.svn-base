//
//  ReminderTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 09/10/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol ReminderListNavigationDelegate {
    func navigateToView(indexPath: IndexPath)
}

class ReminderTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView?
    var reminderListDataModel: FollowAdsReminderListDataModel?
    var delegate: ReminderListNavigationDelegate?
    
    init(reminderListData: FollowAdsReminderListDataModel?) {
        self.reminderListDataModel = reminderListData
        super.init()
    }
    
    // MARK:- TableView Delegates and Datasources

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (reminderListDataModel?.user_reminder_ads.count) != nil {
            return (reminderListDataModel?.user_reminder_ads.count)!
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "ReminderCellId", for: indexPath) as! ReminderTableViewCell
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let reminderDate = timeFormatter.date(from: reminderListDataModel?.user_reminder_ads[indexPath.row].reminder_time ?? "")
        
        if reminderDate != nil {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "dd"
            let stringDay = dateFormat.string(from: reminderDate!)
            
            let monthFormat = DateFormatter()
            monthFormat.dateFormat = "MMM"
            if Localize.currentLanguage() == "ta-IN"{
                monthFormat.locale = Locale(identifier: "ta")
            }
            else {
                monthFormat.locale = Locale(identifier: "en")
            }
            let stringMonth = monthFormat.string(from: reminderDate!)
            
            let yearFormat = DateFormatter()
            yearFormat.dateFormat = "yyyy"
            let numYear = yearFormat.string(from: reminderDate!)
            
            let hourFormat = DateFormatter()
            hourFormat.dateFormat = "HH"
            let numHour = hourFormat.string(from: reminderDate!)
            
            let minFormat = DateFormatter()
            minFormat.dateFormat = "mm"
            let numMin = minFormat.string(from: reminderDate!)
            
            let validDate = timeFormatter.date(from: reminderListDataModel?.user_reminder_ads[indexPath.row].expiry_date ?? "")
            
            let validDateFormat = DateFormatter()
            validDateFormat.dateFormat = "dd"
            let validDay = validDateFormat.string(from: validDate!)
            
            let validMonthFormat = DateFormatter()
            validMonthFormat.dateFormat = "MMM"
            if Localize.currentLanguage() == "ta-IN"{
                validMonthFormat.locale = Locale(identifier: "ta")
            }
            else {
                validMonthFormat.locale = Locale(identifier: "en")
            }
            let validMonth = validMonthFormat.string(from: validDate!)
            
            let validYearFormat = DateFormatter()
            validYearFormat.dateFormat = "yyyy"
            let validYear = validYearFormat.string(from: validDate!)
            
            cell.offerCodeLabel.text = reminderListDataModel?.user_reminder_ads[indexPath.row].advertisement_caption
            cell.businessNameLabel.text = reminderListDataModel?.user_reminder_ads[indexPath.row].business_name
            cell.dateLabel.text = stringDay
            cell.monthLabel.text = stringMonth
            cell.yearLabel.text = numYear
            cell.timeLabel.text = numHour + ":" + numMin
            
            if Localize.currentLanguage() == "ta-IN"{
                cell.validOfferLabel.text = validMonth + " " + validDay + ", " + validYear + " வரை செல்லுபடியாகும்"
            }
            else {
                cell.validOfferLabel.text = "Valid till ".localized() + validMonth + " " + validDay + ", " + validYear
            }
            
        }
        else {
          
            
            cell.offerCodeLabel.text = reminderListDataModel?.user_reminder_ads[indexPath.row].advertisement_caption
            cell.businessNameLabel.text = reminderListDataModel?.user_reminder_ads[indexPath.row].business_name
            cell.dateLabel.text = "00"
            cell.monthLabel.text = "00"
            cell.yearLabel.text = "0000"
            cell.timeLabel.text = "00:00"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
