//
//  UPIPinViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 09/05/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseDynamicLinks

class UPIPinViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var logoImgVw: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var upiPinVw: UIView!
    @IBOutlet weak var upiPinTextField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var followAdsRequestManager = FollowAdsRequestManager()
    var upiDetailSaveDataModel: FollowAdsUPIDetailSaveDataModel?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var emptyFieldAlert = "Please enter your bank details to proceed"
    var upiPinValue: String?
    var mobileNumAlert = "Please Enter Your UPI Pin"
    var validMobileNumAlert = "Please Enter Valid Mobile Number"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = "UPI Pin".localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]

        self.upiPinTextField.becomeFirstResponder()
        confirmBtn.layer.cornerRadius = 5.0
        logoImgVw?.layer.cornerRadius = 4.0
        logoImgVw?.clipsToBounds = true
        self.upiPinTextField.text = upiPinValue
        self.titleLabel.text = "UPI Pin".localized()
        self.confirmBtn.setTitle("Confirm".localized(using: buttonTitles), for: .normal)
        upiPinTextField.placeholder = "UPI Pin".localized()
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }

    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to show the activity indicator.
     @param - notification.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform UPI Pin service call.
     @param -.
     @return -.
     */
    func upiPinUpdate() {
        self.followAdsRequestManager.upiDetailSaveRequestManager = FollowAdsUPIDetailSaveRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.upiDetailSaveRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.upiDetailSaveRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.upiDetailSaveRequestManager?.lang_id = langId
        self.followAdsRequestManager.upiDetailSaveRequestManager?.channel_code = "upi_pin"
        var arrayValue = [String : Array<[String:String]>]()
        arrayValue = ["bank_details" : [["detail_key":"UPI Pin","detail_value": self.upiPinValue]]] as! [String : Array<[String : String]>]
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: arrayValue,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            self.followAdsRequestManager.upiDetailSaveRequestManager?.bank_details = theJSONText!
        }
        else {
            self.followAdsRequestManager.upiDetailSaveRequestManager?.bank_details = ""
        }
        
        
        let upiDetailSaveCompletion: UPIDetailSaveCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.upiDetailSaveDataModel = response!
                        print(self.upiDetailSaveDataModel as Any)
                        
                        if self.upiDetailSaveDataModel?.payment_detail.isEmpty == false {
                            if self.upiDetailSaveDataModel?.payment_detail[0].payment_id != nil {
                        UserDefaults.standard.set(self.upiPinValue, forKey: "UPIPinValue")
                        UserDefaults.standard.set(self.upiDetailSaveDataModel?.payment_detail[0].payment_id, forKey: "PaymentIdUPI")
                        UserDefaults.standard.synchronize()
                            }
                        }
                  //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "")
                        if response?.response_code == "019" {

                        var title = self.alertTitle.localized()
                        var message = "Your UPI details has been saved".localized()
                        title = NSLocalizedString(title, comment: "")
                            message = NSLocalizedString(message , comment: "")
                        let alert = UIAlertController(
                            title: title,
                            message: message,
                            preferredStyle: UIAlertControllerStyle.alert)
                        let okButton = UIAlertAction(
                            title:"OK".localized(using: buttonTitles),
                            style: UIAlertActionStyle.default,
                            handler:
                            {
                                (alert: UIAlertAction!)  in
                                self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(okButton)
                        self.present(alert, animated: true, completion: nil)
                        
                        }
                        else if response?.response_code == "020" {
                            var title = self.alertTitle.localized()
                            var message = "Your UPI details has not been saved".localized()
                            title = NSLocalizedString(title, comment: "")
                            message = NSLocalizedString(message , comment: "")
                            let alert = UIAlertController(
                                title: title,
                                message: message,
                                preferredStyle: UIAlertControllerStyle.alert)
                            let okButton = UIAlertAction(
                                title:"OK".localized(using: buttonTitles),
                                style: UIAlertActionStyle.default,
                                handler:
                                {
                                    (alert: UIAlertAction!)  in
                                    self.navigationController?.popViewController(animated: true)
                            })
                            alert.addAction(okButton)
                            self.present(alert, animated: true, completion: nil)
                        }

                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callUPIDetailSaveServiceCall(requestObject: self.followAdsRequestManager, upiDetailSaveCompletion)
    }
    
    
    @IBAction func confirmBtnAction(_ sender: UIButton) {
        if upiPinTextField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: mobileNumAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        else {
            self.upiPinValue = upiPinTextField.text
            if Reachability.isConnectedToNetwork() == true {
                self.upiPinUpdate()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
            }
        }
        
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UPIPinViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case textField:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
            
        default:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
        }
        return true
    }
}
