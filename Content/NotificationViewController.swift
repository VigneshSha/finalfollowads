//
//  NotificationViewController.swift
//  Content
//
//  Created by Vijayarajan Suresh on 09/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import SDWebImageFLPlugin


class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var imageView: FLAnimatedImageView!
    @IBOutlet var label: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        let content = notification.request.content
        
        if let urlImageString = content.userInfo["urlImageString"] as? String {
            if let url = URL(string: urlImageString) {
                self.imageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))

                URLSession.downloadImage(atURL: url) { [weak self] (data, error) in
                    if let _ = error {
                        return
                    }
                    guard let data = data else {
                        return
                    }
                    DispatchQueue.main.async {
                        if (try? Data(contentsOf: Bundle.main.url(forResource: "play", withExtension: "gif")!)) != nil {
                            self?.imageView.image = UIImage.gifImageWithData(data)

                        }
                        else {
                            self?.imageView.image = UIImage(data: data)
                        }
                    }
                }
                
                
            }
        }
    }

}

extension URLSession {
    
    class func downloadImage(atURL url: URL, withCompletionHandler completionHandler: @escaping (Data?, NSError?) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            completionHandler(data, nil)
        }
        dataTask.resume()
    }
}

