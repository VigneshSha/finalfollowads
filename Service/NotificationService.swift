//
//  NotificationService.swift
//  Service
//
//  Created by Vijayarajan Suresh on 09/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
          //  bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
//            bestAttemptContent.title = "\(String(describing: bestAttemptContent.userInfo["alert"]))"
//            bestAttemptContent.title = "\(String(describing: request.content.title))"
//            bestAttemptContent.subtitle = "\(String(describing: request.content.body))"
//            bestAttemptContent.subtitle = "\(String(describing: request.content.userInfo))"
//            bestAttemptContent.title = "\(String(describing: request.content.body))"
//response.notification.request.content.userInfo
            
            let userInfo = request.content.userInfo
            let apss = userInfo["aps"] as? [AnyHashable : Any]
            if let aps = apss {
                if aps["title"] != nil {
                    bestAttemptContent.body =  aps["description"] as! String
                    bestAttemptContent.title = aps["title"] as! String
                 }
            }

            var urlString:String? = nil
            if let urlImageString = request.content.userInfo["urlImageString"] as? String {
                urlString = urlImageString
            }
            bestAttemptContent.userInfo["urlImageString"] = urlString
            let imageExtensions = ["gif"]
            let pathExtention = URL(string: urlString ?? "")?.pathExtension
            
            if imageExtensions.contains(pathExtention ?? "") {
                if urlString != nil, let fileUrl = URL(string: urlString!) {
                    print("fileUrl: \(fileUrl)")
                    
                    guard let imageData = NSData(contentsOf: fileUrl) else {
                        contentHandler(bestAttemptContent)
                        return
                    }
                    guard let attachment = UNNotificationAttachment.saveImageToDisk(fileIdentifier: "image.gif", data: imageData, options: nil) else {
                        print("error in UNNotificationAttachment.saveImageToDisk()")
                        contentHandler(bestAttemptContent)
                        return
                    }
                    
                    
                    bestAttemptContent.attachments = [ attachment ]
                }
            }
            else {
                if urlString != nil, let fileUrl = URL(string: urlString!) {
                    print("fileUrl: \(fileUrl)")
                    
                    guard let imageData = NSData(contentsOf: fileUrl) else {
                        contentHandler(bestAttemptContent)
                        return
                    }
                    guard let attachment = UNNotificationAttachment.saveImageToDisk(fileIdentifier: "image.jpg", data: imageData, options: nil) else {
                        print("error in UNNotificationAttachment.saveImageToDisk()")
                        contentHandler(bestAttemptContent)
                        return
                    }
                    
                    
                    bestAttemptContent.attachments = [ attachment ]
                }
                
            }
            
           
            
            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}

@available(iOSApplicationExtension 10.0, *)
extension UNNotificationAttachment {
    
    static func saveImageToDisk(fileIdentifier: String, data: NSData, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let folderName = ProcessInfo.processInfo.globallyUniqueString
        let folderURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(folderName, isDirectory: true)
        
        do {
            try fileManager.createDirectory(at: folderURL!, withIntermediateDirectories: true, attributes: nil)
            let fileURL = folderURL?.appendingPathComponent(fileIdentifier)
            try data.write(to: fileURL!, options: [])
            let attachment = try UNNotificationAttachment(identifier: fileIdentifier, url: fileURL!, options: options)
            return attachment
        } catch let error {
            print("error \(error)")
        }
        
        return nil
    }
}
