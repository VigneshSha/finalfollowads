//
//  FollowAdsUIModel.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import EVReflection

class FollowAdsDataModel: EVObject {
    var langListDataModel: FollowAdsLangListDataModel?
    var registerDataModel: FollowAdsRegisterDataModel?
    var otpVerificationDataModel: FollowAdsOtpVerificationDataModel?
    var profileDataModel: FollowAdsProfileDataModel?
    var homeDataModel: [FollowAdsHomeDataModel]?
    var popupadslistDataModel: FollowAdsPopupAdsDataModel?
    var homePromotionsDataModel: [FollowAdsHomePromotionsDataModel]?
    var offerDetailDataModel: FollowAdsOfferDetailDataModel?
    var businessDetailDataModel: FollowAdsBusinessDetailDataModel?
    var allPromotionsDataModel: FollowAdsAllPromotionsDataModel?
    var writeAdvertisementReviewDataModel: FollowAdsWriteAdvertisementReviewDataModel?
    var popupOfferlistDetailDataModel: FollowAdspopupOfferlistDataModel?
    var likeAdvertisementDataModel: FollowAdsLikeAdvertisementDataModel?
    var followBusinessDataModel: FollowAdsFollowBusinessDataModel?
    var bankListDataModel: FollowAdsBankListDataModel?
    var usedOfferCodeDataModel: FollowAdsUsedOfferCodeDataModel?
    var transferMoneyDataModel: FollowAdsTransferMoneyDataModel?
    var likedOffersListDataModel: FollowAdsLikedOffersListDataModel?
    var followedBusinessListDataModel: FollowAdsFollowedBusinessListDataModel?
    var usedCouponListDataModel: FollowAdsUsedCouponListDataModel?
    var searchFilterDataModel: FollowAdsSearchFilterDataModel?
    var notificationListDataModel: FollowAdsNotificationListDataModel?
    var nearByBussinessDataModel : FollowAdsNearByBussinessDataModel?
    var changeLangDataModel: FollowAdsChangeLangDataModel?
    var reminderListDataModel: FollowAdsReminderListDataModel?
    var savedReminderDataModel: FollowAdsSavedReminderDataModel?
    var editBankDetailsDataModel: FollowAdsEditBankDetailsDataModel?
    var categoryListDataModel: FollowAdsCategoryListDataModel?
    var detailCatListDataModel: FollowAdsDetailCatListDataModel?
    var giftCouponDataModel: FollowAdsGiftCouponDataModel?
    var scannedCouponDataModel: FollowAdsScannedCouponDataModel?
    var areaListDataModel: FollowAdsAreaListDataModel?
    var mobTransferDataModel: FollowAdsMobTransferDataModel?
    var upiDetailSaveDataModel: FollowAdsUPIDetailSaveDataModel?
}

class FollowAdsLangListDataModel: EVObject {
    var error: String?
    var information: [FollowAdsLangListDataModel]?
    
    class FollowAdsLangListDataModel: EVObject {
        var language_id: String?
        var name_in_english: String?
        var name_in_native: String?
    }
}



class FollowAdsOtpVerificationDataModel: EVObject {
    var error: String?
    var message: String?
    var user_reminder_count: String?
    var response_code: String?
    var information: [FollowAdsOtpVerificationDataModel]?
    
    class FollowAdsOtpVerificationDataModel: EVObject {
        var user_id: String?
        var email_id: String?
        var lang: String?
        var device_name: String?
        var device_token: String?
        var user_name: String?
        var user_phone_no: String?
        var profile_img: String?
        var followed_stores_count: String?
        var wallet_amt: String?
        var used_coupon_count: String?
        var liked_offers_count: String?
        var scanned_coupon_count: String?
        var response_code: String?
        var notification: String?

    }
}

class FollowAdsRegisterDataModel: EVObject {
    var error: String?
    var message: String?
    var response_code: String?
    var information: [FollowAdsRegisterDataModel]?
    
    class FollowAdsRegisterDataModel: EVObject {
        var otp: String?
        var user_id: String?
        var is_already_registered: String?
    }
}

class FollowAdsProfileDataModel: EVObject {
    var error: String?
    var message: String?
    var information: [FollowAdsProfileDataModel]?
    
    class FollowAdsProfileDataModel: EVObject {
        var email_id: String?
        var profile_img: String?
        var user_id: String?
        var user_name: String?
        var response_code: String?
    }
}

class FollowAdsHomeDataModel: EVObject {
    
    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
        if let value = value as? String, value.count == 0 || value == "null" {
            print("Ignoring empty string for key \(key)")
            return true
        } else if let value = value as? NSArray, value.count == 0 {
            print("Ignoring empty NSArray for key \(key)")
            return true
        } else if value is NSNull {
            print("Ignoring NSNull for key \(key)")
            return true
        }
        return false
    }
    
    var error: String?
    var notification: String?
    var error_code: String?
    var wallet_balance: String?
    var liked_offer_count: String?
    var follow_business_count: String?
    var user_used_coupon_count: String?
    var user_reminder_count: String?
    var scanned_coupon_count: String?
    var welecome_area: String?
    var promotions = FollowAdsHomeDataModel()
    var banner : [FollowAdsHomeBannersDataModel]? = []
    
    class FollowAdsHomeDataModel: EVObject {
        
        var promotion2 = [HomePromotionsListData]()
        var promotion3 = [HomePromotionsListData]()
        var promotion4 = [HomePromotionsListData]()
        var promotion5 = [HomePromotionsListData]()
        var promotion6 = [HomePromotionsListData]()
        var promotion7 = [HomePromotionsListData]()
        var promotion8 = [HomePromotionsListData]()
        
        class HomePromotionsListData : EVObject {
            var advertisement_id: String?
            var advertisment_img: String?
            var business_addr: String?
            var business_id: String?
            var business_lan: String?
            var business_lat: String?
            var business_name: String?
            var promotion_id: String?
            var promotion_name: String?
            var advertisement_caption: String?
            var advertisement_description: String?
            var business_caption: String?
            var business_image_icon_url: String?
            var promotion_image_url: String?
            var business_area: String?
           


        }
        
    }
    
    class FollowAdsHomeBannersDataModel : EVObject {
        var banner_img: String?
        var off_id: String?
        var advertisment_img: String?

    }
}

class FollowAdsHomePromotionsDataModel: EVObject {
    
    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
        if let value = value as? String, value.count == 0 || value == "null" {
            print("Ignoring empty string for key \(key)")
            return true
        } else if let value = value as? NSArray, value.count == 0 {
            print("Ignoring empty NSArray for key \(key)")
            return true
        } else if value is NSNull {
            print("Ignoring NSNull for key \(key)")
            return true
        }
        return false
    }
    
    var error: String?
    var error_code: String?

    var promotions = FollowAdsHomePromotionsDataModel()

    class FollowAdsHomePromotionsDataModel: EVObject {

        var promotion3 = [HomePromotionsListData]()
        var promotion4 = [HomePromotionsListData]()

        class HomePromotionsListData : EVObject {
            var advertisement_id: String?
            var advertisment_img: String?
            var business_addr: String?
            var business_id: String?
            var business_lan: String?
            var business_lat: String?
            var business_name: String?
            var promotion_id: String?
            var promotion_name: String?
            var advertisement_caption: String?
            var advertisement_description: String?
            var business_caption: String?
            var business_image_icon_url: String?
            var promotion_image_url: String?
            var business_area: String?

        }
    }

}
//class FollowAdsPopupAdsDataModel: EVObject {
//
//    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
//        if let value = value as? String, value.count == 0 || value == "null" {
//            print("Ignoring empty string for key \(key)")
//            return true
//        } else if let value = value as? NSArray, value.count == 0 {
//            print("Ignoring empty NSArray for key \(key)")
//            return true
//        } else if value is NSNull {
//            print("Ignoring NSNull for key \(key)")
//            return true
//        }
//        return false
//    }
//
//    var error: String?
//    var error_code: String?
//
//    var promotions = FollowAdsPopupAdsDataModel()
//
//    class FollowAdsPopupAdsDataModel: EVObject {
//
//        var promotion3 = [PopupadslistData]()
//        var promotion4 = [PopupadslistData]()
//
//        class PopupadslistData : EVObject {
//            var full_file_name: String?
//            var is_active: String?
//            var end_date: String?
//            var start_date: String?
//
//
//        }
//    }
//
//}

class FollowAdsPopupAdsDataModel: EVObject {
    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
        if let value = value as? String, value.count == 0 || value == "null" {
            print("Ignoring empty string for key \(key)")
            return true
        } else if let value = value as? NSArray, value.count == 0 {
            print("Ignoring empty NSArray for key \(key)")
            return true
        } else if value is NSNull {
            print("Ignoring NSNull for key \(key)")
            return true
        }
        return false
    }
    var error: String?
    var images: [FollowAdsPopupAdsDataModel] = []
    class FollowAdsPopupAdsDataModel: EVObject {
        var full_file_name: String?
        var advertisement_name: String?
        var start_date: String?
        var end_date: String?
        var _id: String?
        var ads_list = [FollowAdsPopupAdsDataModel]()
        
        
        class FollowAdsPopupAdsDataModel: EVObject {

            var full_file_name: String?
            var id:String?
            var file_name:String?
            
        }
    }
}
class FollowAdspopupOfferlistDataModel: EVObject {
    override func skipPropertyValue(_ value: Any, key: String) -> Bool {
        if let value = value as? String, value.count == 0 || value == "null" {
            print("Ignoring empty string for key \(key)")
            return true
        } else if let value = value as? NSArray, value.count == 0 {
            print("Ignoring empty NSArray for key \(key)")
            return true
        } else if value is NSNull {
            print("Ignoring NSNull for key \(key)")
            return true
        }
        return false
    }
    var error: String?
    var popAdsListData: [FollowAdspopupOfferlistDataModel] = []
    class FollowAdspopupOfferlistDataModel: EVObject {
        var advertisement_id: String?
        var ads_active: String?
        var business_name: String?
        var business_addrs_id: String?
        var business_address: String?
        var advertisement_area: String?
        var advertisement_caption: String?
        var advertisement_desc: String?
        var adsname: String?
        var adsvalid_from: String?
        var adsvalid_to: String?
        var advertisement_icon: String?
        var distance_in_km: String?
    }
}
class FollowAdsOfferDetailDataModel: EVObject {
    var error: String?
    var error_code: String?
    var notification: String?
    var advertisement_detail = FollowAdsOfferDetailDataModel()
    
    class FollowAdsOfferDetailDataModel: EVObject {
        var advertisement_caption: String?
        var advertisement_desc: String?
        var ads_reminder_status: String?
        var ads_reminder_time: String?
        var advertisement_offer_code: String?
        var advertisement_offer_type: String?
        var advertisement_valid_from: String?
        var advertisement_valid_to: String?
        var business_addr: String?
        var business_dist: String?
        var business_address_id: String?
        var business_id: String?
        var business_img: String?
        var business_interest_count: String?
        var business_lan: String?
        var business_lat: String?
        var business_name: String?
        var business_phone_num: String?
        var business_landline_number: String?
        var business_reviews_count: String?
        var is_liked: String?
        var rating_count: String?
        var user_used_coupon: String?
        var map_url: String?
        var advertisement_img = [OfferDetailAdvertisementImgData]()
        var advertisement_reviews = [OfferDetailAdvertisementReviewsData]()
        
        class OfferDetailAdvertisementImgData: EVObject {
            var advertisement_img: String?
        }
        
        class OfferDetailAdvertisementReviewsData: EVObject {
            var advertisement_id: String?
            var business_id: String?
            var current_server_time: String?
            var feedback: String?
            var posted_time: String?
            var rating: String?
            var title: String?
            var user_id: String?
            var user_name: String?
        }
    }
}

class FollowAdsBusinessDetailDataModel: EVObject {
    var error: String?
    var error_code: String?
    var information = FollowAdsBusinessDetailDataModel()
    
    class FollowAdsBusinessDetailDataModel: EVObject {
        var business_banner_img: String?
        var business_followed_count: String?
        var business_reviews_count: String?
        var is_followed: String?
        var logo: String?
        var name: String?
        var business_description: String?
        var category = [String]()
        
        var advertisement = [AdvertisementData]()
        var business_images = [BusinessImageData]()
        var business_video = [BusinessVideoData]()
        var busines_address = BusinessAddressData()
        
        class AdvertisementData: EVObject {
            var advertisement_id: String?
            var advertisement_img: String?
        }
        
        class BusinessImageData: EVObject {
            var business_id: String?
            var business_img: String?
        }
        
        class BusinessVideoData: EVObject {
            var business_id: String?
            var business_vid: String?
        }
        
        class BusinessAddressData: EVObject {
            var address: String?
            var area: String?
            var business_distance: String?
            var business_id: String?
            var business_phone_no: String?
            var business_landline_number: String?
            var city: String?
            var latitude: String?
            var longitude: String?
            var map_url: String?
            var state: String?
        }
    }
}

class FollowAdsAllPromotionsDataModel: EVObject {
    var error: String?
    var promotions = [AllPromotionSectionData]()
    
    class AllPromotionSectionData: EVObject {
        var advertisement_id: String?
        var advertisement_name: String?
        var advertisment_img: String?
        var business_addr: String?
        var business_dist: String?
        var business_id: String?
        var business_name: String?
        var promotion_id: String?
        var promotion_name: String?
        var business_area: String?
        var business_state: String?
        var promotion_image_url: String?


    }
}

class FollowAdsWriteAdvertisementReviewDataModel: EVObject {
    var error: String?
    var avg_advertisement_rating_counts: String?
    var message: String?
    var response_code: String?

}

class FollowAdsLikeAdvertisementDataModel: EVObject {
    var error: String?
    var count: String?
    var message: String?
    var status: String?
}

class FollowAdsFollowBusinessDataModel: EVObject {
    var error: String?
    var count: String?
    var message: String?
    var status: String?
    var error_code: String?
    
}

class FollowAdsBankListDataModel: EVObject {
    var error: String?
    var message: String?
    var bank_detail = [BankDetailList]()
    
    class BankDetailList: EVObject {
        var bank_id: String?
        var bank_name: String?
        var acc_no: String?
        var ifsc_code: String?
        var acc_name: String?
    }
}

class FollowAdsUsedOfferCodeDataModel: EVObject {
    var error: String?
    var message: String?
    var error_code: String?
    var user_ads_save_count: String?
    var response_code: String?
}

class FollowAdsTransferMoneyDataModel: EVObject {
    var error: String?
    var message: String?
    var error_code: String?
    var wallet_amount: String?
    var response_code: String?
}

class FollowAdsLikedOffersListDataModel: EVObject {
    var error: String?
    var response_code: String?
    var Liked_advertisements = [LikedAdvertisementsList]()
    
    class LikedAdvertisementsList: EVObject {
        var advertisement_id: String?
        var business_id: String?
        var business_name: String?
        var business_img: String?
        var advertisement_name: String?
        var advertisement_addr: String?
        var advertisement_desc: String?
        var advertisement_img: String?
        var advertisement_exp_date: String?
        var advertisement_area: String?
        var advertisement_caption: String?
        var is_active: String?
        var business_active: String?

    }
}

class FollowAdsFollowedBusinessListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var response_code: String?
    var followed_business = [FollowedBusinessList]()
    
    class FollowedBusinessList: EVObject {
        var business_id: String?
        var business_name: String?
        var business_img: String?
        var business_addr: String?
        var business_interested_count: String?
        var business_review_count: String?
        var business_dist: String?
        var business_address_id: String?
        var is_active: String?

    }
}

class FollowAdsUsedCouponListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var user_used_coupon_list = [UserUsedCouponListData]()
    
    class UserUsedCouponListData: EVObject {
        var user_id: String?
        var advertisement_id: String?
        var advertisement_name: String?
        var business_id: String?
        var advertisement_valid_from: String?
        var advertisement_valid_to: String?
        var used_coupon_date: String?
        var offer_code: String?
        var offer_type_code: String?
        var ads_description: String?
        var user_redeemed_advertisement_code_id: String?
        var business_name: String?
        var advertisement_caption: String?


    }
}

class FollowAdsSaveBankDetailsDataModel: EVObject {
    var error: String?
    var error_code: String?
    var message : String?
    var response_code: String?
    var bank_detail = [FollowAdsBankDetailsDataModel]()
    class FollowAdsBankDetailsDataModel : EVObject {
        var acc_name : String?
        var acc_no : String?
        var bank_id : String?
        var bank_name : String?
        var ifsc_code : String?
    }
}

class FollowAdsEditBankDetailsDataModel: EVObject {
}

class FollowAdsSearchSuggestionDataModel: EVObject {
    var error: String?
    var searchname = [FollowAdsSearchNameModel]()
    
    class FollowAdsSearchNameModel : EVObject {
        var  category_name : String?
    }
}

class FollowAdsSearchOffersDataModel: EVObject {
    var error: String?

    var All_ads = [FollowAdsSearchKeyValueDataModel]()
    var Coming = [FollowAdsSearchKeyValueDataModel]()
    var Expiring = [FollowAdsSearchKeyValueDataModel]()
    var Live = [FollowAdsSearchKeyValueDataModel]()
    var Nearby = [FollowAdsSearchKeyValueDataModel]()

    class FollowAdsSearchKeyValueDataModel : EVObject {

        var ads_id : String?
        var ads_name : String?
        var advertisement_caption : String?
        var advertisement_desc : String?
        var advertisement_valid_from : String?
        var advertisement_valid_to : String?
        var advertisment_icon : String?
        var business_name : String?
        var business_addr : String?
        var business_area : String?
        var business_dist : String?
        var business_lan : String?
        var business_lat : String?
        var promotion_image_url : String?
        var advertisment_img: String?
        var is_active: String?

    }
}

class FollowAdsSearchFilterDataModel: EVObject {
    var error: String?
    var filter_list = [FilterListData]()
    
    class FilterListData: EVObject {
        var ads_caption: String?
        var ads_id: String?
    }
}

class FollowAdsNearByBussinessDataModel : EVObject {
    
    var business_list = [FollowAdsNearByBussinessList]()
    var error : String?
    class FollowAdsNearByBussinessList : EVObject {
        var business_id : String?
        var business_img: String?
        var business_lat: String?
        var business_lng: String?
        var business_name: String?
        var advertisement_img: String?
        var bus_address: String?
        var bus_area: String?
        var bus_city: String?
        var bus_state: String?
        var advertisement_caption: String?
        var advertisement_id: String?
        var business_address_id: String?
        var map_url: String?
        var advertisement_offer_code: String?



    }
}

class FollowAdsNotificationListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var information = [NotificationInfoData]()
    
    class NotificationInfoData: EVObject {
//        var business_id: String?
//        var business_name: String?
//        var business_img: String?
//        var advertisement_id: String?
//        var advertisement_name: String?
//        var advertisement_valid_from: String?
//        var advertisement_valid_to: String?
//        var is_read: String?
//        var notification_count: String?
//        var notification_id: String?
//        var pushnotification_description : String?
//        var pushnotification_id : String?
//        var pushnotification_image : String?
//        var title : String?

        
        var advertisement_id: String?
        var ad_description: String?
        var image_Url: String?
        var pushnotification_id: String?
        var sound_url: String?
        var title: String?
        var valid_from: String?
        var valid_to: String?
        var is_active: String?
        var business_active: String?
        var read_unread: String?

//        var advertisement_id: String?
//        var created_at: String?
//        var descriptiosn: String?
//        var is_active: String?
//        var notification_id: String?
//        var pushnotification_id: String?
//        var title : String?
//        var updated_at: String?
//        var user_id: String?

    }
}

class FollowAdsChangeLangDataModel: EVObject {
    var error: String?
    var message: String?
    var response_code: String?
}


class FollowAdsReminderListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var user_reminder_ads = [UserReminderAds]()
    
    class UserReminderAds: EVObject {
        var advertisement_id: String?
        var advertisement_name: String?
        var business_id: String?
        var business_name: String?
        var created_at: String?
        var ads_image_icon: String?
        var ads_description: String?
        var advertisement_caption: String?
        var reminder_status: String?
        var reminder_time: String?
        var expiry_date: String?
        var is_active: String?
        var business_active: String?
    }
}

class FollowAdsSavedReminderDataModel: EVObject {
    var error: String?
    var error_code: String?
    var message: String?
    var user_ads_save_count: String?
    var response_code: String?
    
}

class FollowAdsCategoryListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var categoryList = [category_list]()

    class category_list: EVObject {
        var category_icon: String?
        var category_id: String?
        var category_name: String?
        var languge_name: String?
    }
}

class FollowAdsDetailCatListDataModel: EVObject {
    var error: String?
    var error_code: String?
    var category_advertisement_list = [CategoryListDataModel]()
    
    class CategoryListDataModel: EVObject {
        var advertisement_id: String?
        var advertisement_name: String?
        var business_id: String?
        var advertisement_desc: String?
        var advertisement_caption: String?
        var advertisement_address: String?
        var advertisement_area: String?
        var advertisement_city: String?
        var advertisement_icon: String?
        var business_name: String?
        var is_active: String?
    }
}

class FollowAdsGiftCouponDataModel: EVObject {
    var error: String?
    var giftCoupon = [gift_coupon]()

    class gift_coupon: EVObject {
        var activated_by: String?
        var activated_on: String?
        var business_id: String?
        var code: String?
        var created_at: String?
        var created_by: String?
        var generated_by: String?
        var generated_on: String?
        var gift_coupon_id: String?
        var is_active: String?
        var max_redemption_count: String?
        var updated_at: String?
        var updated_by: String?
        var value: String?

    }
}


class FollowAdsScannedCouponDataModel: EVObject {
    var error: String?
    var error_code: String?
    var scanned_coupon_count: String?
    var wallet_amount: String?
    var is_redeem: String?
    var response_code: String?

}

class FollowAdsAreaListDataModel: EVObject {
    var error: String?
    var area = [AreaList]()

    class AreaList: EVObject {
        var Description: String?
        var PostalCode: String?
        var area_id: String?
        var area_lang_name: String?
        var area_language_list_id: String?
        var area_name: String?
        var created_at: String?
        var created_by: String?
        var id: String?
        var language_id: String?
        var updated_at: String?
        var updated_by: String?

    }

    }

class FollowAdsMobTransferDataModel: EVObject {
    var error: String?
    var error_code: String?
    var message: String?
    var payment_detail = [PaymentDetail]()
    var response_code: String?

    class PaymentDetail: EVObject {
        var mobile_number: String?
        var payment_id: String?

    }

}

class FollowAdsUPIDetailSaveDataModel: EVObject {
    var error: String?
    var error_code: String?
    var message: String?
    var payment_detail = [PaymentDetail]()
    var response_code: String?

    class PaymentDetail: EVObject {
        var upi_pin: String?
        var payment_id: String?

    }
    
}

class FollowAdsUIModel: NSObject {
    
    /**
     This method is used for update language list model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateLangListWith(response: Data?, err error: NSError?, updateUIModel updateUI: LangListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let langList = FollowAdsLangListDataModel(json: responseString)
                print("fbffgfdg",langList)
                updateUI(langList,nil)
            }
        }
    }
    /**
     This method is used for update register model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateRegisterWith(response: Data?, err error: NSError?, updateUIModel updateUI: RegisterCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let register = FollowAdsRegisterDataModel(json: responseString)
                print(register)
                updateUI(register,nil)
            }
        }
    }
    
    /**
     This method is used for update otp verification model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateOtpVerificationWith(response: Data?, err error: NSError?, updateUIModel updateUI: OtpVerificationCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let otpVerification = FollowAdsOtpVerificationDataModel(json: responseString)
                print(otpVerification)
                updateUI(otpVerification,nil)
            }
        }
    }
    
    
    /**
     This method is used for update profile model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateProfileWith(response: Data?, err error: NSError?, updateUIModel updateUI: ProfileCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let profile = FollowAdsProfileDataModel(json: responseString)
                print(profile)
                updateUI(profile,nil)
            }
        }
    }
    
    /**
     This method is used for update home model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateHomeWith(response: Data?, err error: NSError?, updateUIModel updateUI: HomeCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                
                
                let data = responseString!.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    let promotions = json["promotions"]! as Any
                    print(promotions)
                    
                    if let names = json["promotions"] as? String {
                        print(names)
                        updateUI(nil,error)
                    }
                    else {
                        let home = FollowAdsHomeDataModel(json: responseString)
                        print(home)
                        updateUI(home,nil)
                    }
                    
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
                
            }
        }
    }
    
    class func updatePopupAdsWith(response: Data?, err error: NSError?, updateUIModel updateUI: PopupAdsCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let popuplist = FollowAdsPopupAdsDataModel(json: responseString)
                print("fbffegfdg",popuplist)
                updateUI(popuplist,nil)
            }
        }
    }
    class func updatePopupOfferlistWith(response: Data?, err error: NSError?, updateUIModel updateUI: popupOfferlistCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let popupOfferlist = FollowAdspopupOfferlistDataModel(json: responseString)
                print("fbffegfdg",popupOfferlist)
                updateUI(popupOfferlist,nil)
            }
        }
    }

//    class func updatePopupAdsWith(response: Data?, err error: NSError?, updateUIModel updateUI: PopupAdsCompletionBlock) {
//        if (error != nil) {
//            updateUI(nil,error)
//        }
//        else{
//            let responseString = String(data: response!, encoding: String.Encoding.utf8)
//            print("responseString = \(String(describing: responseString))")
//            do {
//
//
//                let data = responseString!.data(using: String.Encoding.utf8, allowLossyConversion: false)!
//
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
//                    let promotions = json["images"]! as Any
//                    print("sfksfknksfk",promotions)
//
//                    if let names = json["promotions"] as? String {
//                        print(names)
//                        updateUI(nil,error)
//                    }
//                    else {
//                        let home = FollowAdsPopupAdsDataModel(json: responseString)
//                        print("jggu",home)
//                        updateUI(home,nil)
//                    }
//
//                } catch let error as NSError {
//                    print("Failed to load: \(error.localizedDescription)")
//                }
//
//
//            }
//        }
//    }
    
    /**
     This method is used for update home promotions model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateHomePromotionsWith(response: Data?, err error: NSError?, updateUIModel updateUI: HomeCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                
                
                let data = responseString!.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    let promotions = json["promotions"]! as Any
                    print(promotions)
                    
                    if let names = json["promotions"] as? String {
                        print(names)
                        updateUI(nil,error)
                    }
                    else {
                        let home = FollowAdsHomeDataModel(json: responseString)
                        print(home)
                        updateUI(home,nil)
                    }
                    
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
                
            }
        }
    }
    
    /**
     This method is used for update offer detail model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateOfferDetailWith(response: Data?, err error: NSError?, updateUIModel updateUI: OfferDetailCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let offerDetail = FollowAdsOfferDetailDataModel(json: responseString)
                print(offerDetail)
                updateUI(offerDetail,nil)
            }
        }
    }
    
    /**
     This method is used for update business detail model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateBusinessDetailWith(response: Data?, err error: NSError?, updateUIModel updateUI: BusinessDetailCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let businessDetail = FollowAdsBusinessDetailDataModel(json: responseString)
                print(businessDetail)
                updateUI(businessDetail,nil)
            }
        }
    }
    
    /**
     This method is used for update all promotions model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateAllPromotionsWith(response: Data?, err error: NSError?, updateUIModel updateUI: AllPromotionsCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let allPromotions = FollowAdsAllPromotionsDataModel(json: responseString)
                print(allPromotions)
                updateUI(allPromotions,nil)
            }
        }
    }
    
    /**
     This method is used for update write advertisement review model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateWriteAdvertisementReviewWith(response: Data?, err error: NSError?, updateUIModel updateUI: WriteAdvertisementReviewCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let writeAdvertisementReview = FollowAdsWriteAdvertisementReviewDataModel(json: responseString)
                print(writeAdvertisementReview)
                updateUI(writeAdvertisementReview,nil)
            }
        }
    }
    
    /**
     This method is used for update like advertisement model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateLikeAdvertisementWith(response: Data?, err error: NSError?, updateUIModel updateUI: LikeAdvertisementCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let likeAdvertisement = FollowAdsLikeAdvertisementDataModel(json: responseString)
                print(likeAdvertisement)
                updateUI(likeAdvertisement,nil)
            }
        }
    }
    
    /**
     This method is used for update follow business model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateFollowBusinessWith(response: Data?, err error: NSError?, updateUIModel updateUI: FollowBusinessCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let followBusiness = FollowAdsFollowBusinessDataModel(json: responseString)
                print(followBusiness)
                updateUI(followBusiness,nil)
            }
        }
    }
    
    /**
     This method is used for update bank list model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateBankListWith(response: Data?, err error: NSError?, updateUIModel updateUI: BankListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let bankList = FollowAdsBankListDataModel(json: responseString)
                print(bankList)
                updateUI(bankList,nil)
            }
        }
    }
    
    /**
     This method is used for update used offer code model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateUsedOfferCodeWith(response: Data?, err error: NSError?, updateUIModel updateUI: UsedOfferCodeCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let usedOfferCode = FollowAdsUsedOfferCodeDataModel(json: responseString)
                print(usedOfferCode)
                updateUI(usedOfferCode,nil)
            }
        }
    }
    
    /**
     This method is used for update transfer money model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateTransferMoneyWith(response: Data?, err error: NSError?, updateUIModel updateUI: TransferMoneyCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let transferMoney = FollowAdsTransferMoneyDataModel(json: responseString)
                print(transferMoney)
                updateUI(transferMoney,nil)
            }
        }
    }
    
    /**
     This method is used for update liked offers list model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateLikedOffersListWith(response: Data?, err error: NSError?, updateUIModel updateUI: LikedOffersListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let likedOffersList = FollowAdsLikedOffersListDataModel(json: responseString)
                print(likedOffersList)
                updateUI(likedOffersList,nil)
            }
        }
    }
    
    /**
     This method is used for update followed business list model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateFollowedBusinessListWith(response: Data?, err error: NSError?, updateUIModel updateUI: FollowedBusinessListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let followedBusinessList = FollowAdsFollowedBusinessListDataModel(json: responseString)
                print(followedBusinessList)
                updateUI(followedBusinessList,nil)
            }
        }
    }
    
    /**
     This method is used for update used coupon list model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateUsedCouponListWith(response: Data?, err error: NSError?, updateUIModel updateUI: UsedCouponListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let usedCouponList = FollowAdsUsedCouponListDataModel(json: responseString)
                print(usedCouponList)
                updateUI(usedCouponList,nil)
            }
        }
    }
    
    /**
     This method is used for save bank details model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func saveBankDetailsWith(response: Data?, err error: NSError?, updateUIModel updateUI: SaveBankDetailsCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let usedCouponList = FollowAdsSaveBankDetailsDataModel(json: responseString)
                print(usedCouponList)
                updateUI(usedCouponList,nil)
            }
        }
    }
    
    /**
     This method is used for edit bank details model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func editBankDetailsWith(response: Data?, err error: NSError?, updateUIModel updateUI: EditBankDetailsCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let editBankDetails = FollowAdsEditBankDetailsDataModel(json: responseString)
                print(editBankDetails)
                updateUI(editBankDetails,nil)
            }
        }
    }
    
    /**
     This method is used for Search Suggestion UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateSearchSuggestionWith(response: Data?, err error: NSError?, updateUIModel updateUI: SearchSuggestionCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let usedCouponList = FollowAdsSearchSuggestionDataModel(json: responseString)
                print(usedCouponList)
                updateUI(usedCouponList,nil)
            }
        }
    }
    /**
     This method is used for Search offers UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateSearchOffersWith(response: Data?, err error: NSError?, updateUIModel updateUI: SearchOffersCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let usedCouponList = FollowAdsSearchOffersDataModel(json: responseString)
                print(usedCouponList)
                updateUI(usedCouponList,nil)
            }
        }
    }
    
    /**
     This method is used for Search Filter UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateSearchFilterWith(response: Data?, err error: NSError?, updateUIModel updateUI: SearchFilterCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let searchFilter = FollowAdsSearchFilterDataModel(json: responseString)
                print(searchFilter)
                updateUI(searchFilter,nil)
            }
        }
    }
    
    /**
     This method is used for Notification List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateNotificationListWith(response: Data?, err error: NSError?, updateUIModel updateUI: NotificationListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let notificationList = FollowAdsNotificationListDataModel(json: responseString)
                print(notificationList)
                updateUI(notificationList,nil)
            }
        }
    }
    /**
     This method is used for Notification List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateNearByBussinessWith(response: Data?, err error: NSError?, updateUIModel updateUI: NearByBussinessCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let notificationList = FollowAdsNearByBussinessDataModel(json: responseString)
                print(notificationList)
                updateUI(notificationList,nil)
            }
        }
    }
    
    /**
     This method is used for Change Lang UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateChangeLangWith(response: Data?, err error: NSError?, updateUIModel updateUI: ChangeLangCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let changeLang = FollowAdsChangeLangDataModel(json: responseString)
                print(changeLang)
                updateUI(changeLang,nil)
            }
        }
    }
    
    /**
     This method is used for Reminder List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateReminderListWith(response: Data?, err error: NSError?, updateUIModel updateUI: ReminderListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let reminderList = FollowAdsReminderListDataModel(json: responseString)
                print(reminderList)
                updateUI(reminderList,nil)
            }
        }
    }
    
    /**
     This method is used for Saved Reminder UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateSavedReminderWith(response: Data?, err error: NSError?, updateUIModel updateUI: SavedReminderCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let savedReminder = FollowAdsSavedReminderDataModel(json: responseString)
                print(savedReminder)
                updateUI(savedReminder,nil)
            }
        }
    }
    
    /**
     This method is used for Category List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateCategoryListWith(response: Data?, err error: NSError?, updateUIModel updateUI: CategoryListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let categoryList = FollowAdsCategoryListDataModel(json: responseString)
                print(categoryList)
                updateUI(categoryList,nil)
            }
        }
    }
    
    /**
     This method is used for Category List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateDetailCatListWith(response: Data?, err error: NSError?, updateUIModel updateUI: DetailCatListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let detailCatList = FollowAdsDetailCatListDataModel(json: responseString)
                print(detailCatList)
                updateUI(detailCatList,nil)
            }
        }
    }
    
    /**
     This method is used for Gift Coupon UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateGiftCouponWith(response: Data?, err error: NSError?, updateUIModel updateUI: GiftCouponCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let giftCoupon = FollowAdsGiftCouponDataModel(json: responseString)
                print(giftCoupon)
                updateUI(giftCoupon,nil)
            }
        }
    }
    
    /**
     This method is used for Gift Coupon UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateScannedCouponWith(response: Data?, err error: NSError?, updateUIModel updateUI: ScannedCouponCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let scannedCoupon = FollowAdsScannedCouponDataModel(json: responseString)
                print(scannedCoupon)
                updateUI(scannedCoupon,nil)
            }
        }
    }
    
    /**
     This method is used for Area List UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateAreaListWith(response: Data?, err error: NSError?, updateUIModel updateUI: AreaListCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let areaList = FollowAdsAreaListDataModel(json: responseString)
                print(areaList)
                updateUI(areaList,nil)
            }
        }
    }
    
    /**
     This method is used for Mobile Transfer UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateMobTransferWith(response: Data?, err error: NSError?, updateUIModel updateUI: MobTransferCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let mobTransfer = FollowAdsMobTransferDataModel(json: responseString)
                print(mobTransfer)
                updateUI(mobTransfer,nil)
            }
        }
    }
    
    /**
     This method is used for UPI Detail Save UI model with response from the service manager.
     @param Compltion haldler from handler.
     @return response.
     */
    
    class func updateUPIDetailSaveWith(response: Data?, err error: NSError?, updateUIModel updateUI: UPIDetailSaveCompletionBlock) {
        if (error != nil) {
            updateUI(nil,error)
        }
        else{
            let responseString = String(data: response!, encoding: String.Encoding.utf8)
            print("responseString = \(String(describing: responseString))")
            do {
                let upiDetailSave = FollowAdsUPIDetailSaveDataModel(json: responseString)
                print(upiDetailSave)
                updateUI(upiDetailSave,nil)
            }
        }
    }
}
