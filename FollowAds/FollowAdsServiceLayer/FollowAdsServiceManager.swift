//
//  FollowAdsServiceManager.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import Alamofire

class FollowAdsServiceManager: NSObject {
    
    /**
     This method is used for Language list service call.
     @param request parameters.
     @return response.
     */
    func LangListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/lang_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypeGet
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            print("sfsfsfsf",responseData)
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Register service call.
     @param request parameters.
     @return response.
     */
    func registerServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/register"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "full_name=\(String(describing: requestObject.registerRequestManager!.full_name!))&phone_number=\(String(describing: requestObject.registerRequestManager!.phone_number!))&device_token=\(String(describing: requestObject.registerRequestManager!.device_token!))&device_name=\(String(describing: requestObject.registerRequestManager!.device_name!))&email_id=\(String(describing: requestObject.registerRequestManager!.email_id!))&lang=\(String(describing: requestObject.registerRequestManager!.lang!))&default_location=\(String(describing: requestObject.registerRequestManager!.default_location!))&postal_code=\(String(describing: requestObject.registerRequestManager!.postal_code!))"
        print("Register :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Otp Verification service call.
     @param request parameters.
     @return response.
     */
    func otpVerificationServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/otp_verification"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.otpVerificationRequestManager!.user_id!))&otp=\(String(describing: requestObject.otpVerificationRequestManager!.otp!))"
        print("Otp Verification :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Profile service call.
     @param request parameters.
     @return response.
     */
    func profileServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/profile"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.profileRequestManager!.user_id!))&prof_img=\(String(describing: requestObject.profileRequestManager!.prof_img!))&user_name=\(String(describing: requestObject.profileRequestManager!.user_name!))"
        
        print("Profile :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print("iknknb",responseData.result.value as Any)
                if responseData.data != nil {
                    print("kjdbdf")
                    completion(responseData.data,responseData.error)
                }
                else {
                      print("kjdbdf")
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Home service call.
     @param request parameters.
     @return response.
     */
    func homeServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        //     let baseURL: String = kBaseURL + "/sampleapi"
        let baseURL: String = kBaseURL + "/home"
        var request = URLRequest(url: URL(string: baseURL)!)
       
        let postString = "user_id=\(String(describing: requestObject.homeRequestManager!.user_id!))&device_token=\(String(describing: requestObject.homeRequestManager!.device_token!))&device_name=\(String(describing: requestObject.homeRequestManager!.device_name!))&user_lat=\(String(describing: requestObject.homeRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.homeRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.homeRequestManager!.lang_id!))&area=\(String(describing: requestObject.homeRequestManager!.area!))&postal_code=\(String(describing: requestObject.homeRequestManager!.postal_code!))"
        print("sfknsf",postString)
        print("Home :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            print("gfwffw",responseData)
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    func PopupOfferlistServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
          
          //     let baseURL: String = kBaseURL + "/sampleapi"
             let baseURL: String = NodeBaseUrl + "/get_popup_ads_list"
          var request = URLRequest(url: URL(string: baseURL)!)
         
          let postString = "user_lat=\(String(describing: requestObject.PopupOfferlistRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.PopupOfferlistRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.PopupOfferlistRequestManager!.lang_id!))&postal_code=\(String(describing: requestObject.PopupOfferlistRequestManager!.postal_code!))&image_id=\(String(describing: requestObject.PopupOfferlistRequestManager!.image_id!))"
          print("sfknsf",postString)
          print("Home :",postString)
      let user = "admin"
            let password = "123456"
            let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
            let base64Credentials = credentialData.base64EncodedString(options: [])
            let headers = ["Authorization": "Basic \(base64Credentials)"]
            let parameters: Parameters = [
                "user_lat": String(describing: requestObject.PopupOfferlistRequestManager!.user_lat!),
                "user_lng": String(describing: requestObject.PopupOfferlistRequestManager!.user_lng!),
                "lang_id":  String(describing: requestObject.PopupOfferlistRequestManager!.lang_id!),
                "postal_code": String(describing: requestObject.PopupOfferlistRequestManager!.postal_code!),
                "image_id": String(describing: requestObject.PopupOfferlistRequestManager!.image_id!)
            ]
//          request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//          request.httpMethod = requestTypePost
//          request.httpBody = postString.data(using: .utf8)
      //   let addd =  baseURL+"?"+postString
  
    Alamofire.request(baseURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers:headers).responseJSON { (responseData ) in
                    print("shisbsfbus",responseData)
                    if((responseData.data) != nil) {
                        print(responseData.result.value as Any)
                        
                        if responseData.data != nil {
                            completion(responseData.data,responseData.error)
                        }
                        else {
                            completion(responseData.data,responseData.error)
                        }
                    }
            }
        
                   
      }
    
    
    
    func PopupAdsServiceCallWithCompilence(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
         
         //     let baseURL: String = kBaseURL + "/sampleapi"
        let user = "admin"
                 let password = "123456"
                 let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
                 let base64Credentials = credentialData.base64EncodedString(options: [])
                 let headers = ["Authorization": "Basic \(base64Credentials)"]

         let baseURL: String = NodeBaseUrl + "/pop_ads_home"
         var request = URLRequest(url: URL(string: baseURL)!)

         let postString = "device_token=\(String(describing: requestObject.popupRequestManager!.device_token!))&device_name=\(String(describing: requestObject.popupRequestManager!.device_name!))&user_lat=\(String(describing: requestObject.popupRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.popupRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.popupRequestManager!.lang_id!))&area=\(String(describing: requestObject.popupRequestManager!.area!))&postal_code=\(String(describing: requestObject.popupRequestManager!.postal_code!))"
         let addd =  baseURL+"?"+postString
              print("dgdgdg",addd)
         print("pop_ads_home :",postString)
         request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         request.httpMethod = requestTypePost
         request.httpBody = postString.data(using: .utf8)
         
        Alamofire.request(addd,
                                  method: .get,
                                  parameters: nil,
                                  encoding: URLEncoding.default,
                                  headers:headers) 
                    .validate()
                    .responseJSON { (responseData) -> Void in
            print("dgdsr",responseData)
             if((responseData.data) != nil) {
                 print(responseData.result.value as Any)
                 
                 if responseData.data != nil {
                     completion(responseData.data,responseData.error)
                 }
                 else {
                     completion(responseData.data,responseData.error)
                 }
             }
         }
     }
    
    
    /**
     This method is used for Home promotions service call.
     @param request parameters.
     @return response.
     */
    func homePromotionsServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        //     let baseURL: String = kBaseURL + "/sampleapi"
        let baseURL: String = kBaseURL + "/home_promo"
        var request = URLRequest(url: URL(string: baseURL)!)
        
        let postString = "user_id=\(String(describing: requestObject.homePromotionsRequestManager!.user_id!))&device_token=\(String(describing: requestObject.homePromotionsRequestManager!.device_token!))&device_name=\(String(describing: requestObject.homePromotionsRequestManager!.device_name!))&user_lat=\(String(describing: requestObject.homePromotionsRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.homePromotionsRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.homePromotionsRequestManager!.lang_id!))&area=\(String(describing: requestObject.homePromotionsRequestManager!.area!))&limit=\(String(describing: requestObject.homePromotionsRequestManager!.limit!))&postal_code=\(String(describing: requestObject.homePromotionsRequestManager!.postal_code!))"
        
        print("Home Promotions :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Offer Detail service call.
     @param request parameters.
     @return response.
     */
    func offerDetailServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/offer_details"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.offerDetailRequestManager!.user_id!))&advertisement_id=\(String(describing: requestObject.offerDetailRequestManager!.advertisement_id!))&user_lat=\(String(describing: requestObject.offerDetailRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.offerDetailRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.offerDetailRequestManager!.lang_id!))&business_address_id=\(String(describing: requestObject.offerDetailRequestManager!.business_address_id!))&notify_id=\(String(describing: requestObject.offerDetailRequestManager!.notify_id!))&pushnotification_id=\(String(describing: requestObject.offerDetailRequestManager!.pushnotification_id!))"
        print("Offer Detail :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Business Detail service call.
     @param request parameters.
     @return response.
     */
    func businessDetailServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/business_detail"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.businessDetailRequestManager!.user_id!))&business_id=\(String(describing: requestObject.businessDetailRequestManager!.business_id!))&user_lat=\(String(describing: requestObject.businessDetailRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.businessDetailRequestManager!.user_lng!))&lang_id=\(String(describing: requestObject.businessDetailRequestManager!.lang_id!))&business_address_id=\(String(describing: requestObject.businessDetailRequestManager!.business_address_id!))"
        
        print("Business Detail :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for All Promotions service call.
     @param request parameters.
     @return response.
     */
    func allPromotionsServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/promotion_section_data"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.promotionSectionRequestManager!.user_id!))&promotion_id=\(String(describing: requestObject.promotionSectionRequestManager!.promotion_id!))&lang_id=\(String(describing: requestObject.promotionSectionRequestManager!.lang_id!))&user_lat=\(String(describing: requestObject.promotionSectionRequestManager!.user_lat!))&user_lng=\(String(describing: requestObject.promotionSectionRequestManager!.user_lng!))&area=\(String(describing: requestObject.promotionSectionRequestManager!.area!))&postal_code=\(String(describing: requestObject.promotionSectionRequestManager!.postal_code!))"
     
        print("All Promotions Data :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Write Advertisement Review service call.
     @param request parameters.
     @return response.
     */
    func writeAdvertisementReviewServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/write_advertisement_review"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.user_id!))&business_id=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.business_id!))&advertisement_id=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.advertisement_id!))&comments=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.comments!))&rating=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.rating!))&title=\(String(describing: requestObject.writeAdvertisementReviewRequestManager!.title!))"
        print("Write Advertisement Review :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Like Advertisement service call.
     @param request parameters.
     @return response.
     */
    func likeAdvertisementServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/like_advertisement"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.likeAdvertisementRequestManager!.user_id!))&advertisement_id=\(String(describing: requestObject.likeAdvertisementRequestManager!.advertisement_id!))&fav_status=\(String(describing: requestObject.likeAdvertisementRequestManager!.fav_status!))&lang_id=\(String(describing: requestObject.likeAdvertisementRequestManager!.lang_id!))&business_address_id=\(String(describing: requestObject.likeAdvertisementRequestManager!.business_address_id!))"
        print("Like Advertisement :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Follow Business service call.
     @param request parameters.
     @return response.
     */
    func followBusinessServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/follow_business"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.followBusinessRequestManager!.user_id!))&business_id=\(String(describing: requestObject.followBusinessRequestManager!.business_id!))&followed_status=\(String(describing: requestObject.followBusinessRequestManager!.followed_status!))&lang_id=\(String(describing: requestObject.followBusinessRequestManager!.lang_id!))&business_address_id=\(String(describing: requestObject.followBusinessRequestManager!.business_address_id!))"
        print("Follow Business :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Bank List service call.
     @param request parameters.
     @return response.
     */
    func bankListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/bank_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.bankDetailsListRequestManager!.user_id!))&lang_id=\(String(describing: requestObject.bankDetailsListRequestManager!.lang_id!))"
        print("Bank List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Used Offer Code service call.
     @param request parameters.
     @return response.
     */
    func usedOfferCodeServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/User_used_offer_code"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.usedOfferCodeRequestManager!.user_id!))&advertisement_id=\(String(describing: requestObject.usedOfferCodeRequestManager!.advertisement_id!))&business_id=\(String(describing: requestObject.usedOfferCodeRequestManager!.business_id!))&offer_code=\(String(describing: requestObject.usedOfferCodeRequestManager!.offer_code!))&lang_id=\(String(describing: requestObject.usedOfferCodeRequestManager!.lang_id!))"
        print("Used Offer Code :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Transfer Money service call.
     @param request parameters.
     @return response.
     */
    func transferMoneyServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/transfer_money"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.transferMoneyRequestManager!.user_id!))&user_payment_method_id=\(String(describing: requestObject.transferMoneyRequestManager!.user_payment_method_id!))"
        print("Transfer Money :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Liked Offers List service call.
     @param request parameters.
     @return response.
     */
    func likedOffersListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/liked_advertisements_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.likedOffersListRequestManager!.user_id!))&lang_id=\(String(describing: requestObject.likedOffersListRequestManager!.lang_id!))"
        print("Liked Offers List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Followed Business List service call.
     @param request parameters.
     @return response.
     */
    func followedBusinessListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/followed_business_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.followedBusinessListRequestManager!.user_id!))&lang_id=\(String(describing: requestObject.followedBusinessListRequestManager!.lang_id!))"
        print("Followed Business List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Used Coupon List service call.
     @param request parameters.
     @return response.
     */
    func usedCouponListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/User_used_offer_code_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: requestObject.usedCouponsListRequestManager!.user_id!))&lang=\(String(describing: requestObject.usedCouponsListRequestManager!.lang!))"
        print("Lang :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Save Bank details service call.
     @param request parameters.
     @return response.
     */
    func saveBankDetailsServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/save_bank_details"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.saveBankDetailsRequestManager?.user_id)!))&bank_details=\(String(describing: (requestObject.saveBankDetailsRequestManager?.bank_details)!))&channel_code=\(String(describing: (requestObject.saveBankDetailsRequestManager?.channel_code)!))&lang_id=\(String(describing: (requestObject.saveBankDetailsRequestManager?.lang_id)!))"
        print("Used Coupons List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Edit Bank details service call.
     @param request parameters.
     @return response.
     */
    func editBankDetailsServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/bank_account_edit"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.editBankDetailsRequestManager?.user_id)!))&bank_details=\(String(describing: (requestObject.editBankDetailsRequestManager?.bank_details)!))&channel_code=\(String(describing: (requestObject.editBankDetailsRequestManager?.channel_code)!))&lang_id=\(String(describing: (requestObject.editBankDetailsRequestManager?.lang_id)!))"
        print("Edit Bank Details :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Save Bank details service call.
     @param request parameters.
     @return response.
     */
    func searchSuggestionServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/search_suggestion"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.serachSuggestionsRequestManager?.user_id)!))&user_lat=\(String(describing: (requestObject.serachSuggestionsRequestManager?.user_lat)!))&user_lng=\(String(describing: (requestObject.serachSuggestionsRequestManager?.user_lng)!))"
        print("Used Coupons List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Search Offers service call.
     @param request parameters.
     @return response.
     */
    func searchOffersServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/searchdata"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.searchOffersRequestManager?.user_id)!))&user_lat=\(String(describing: (requestObject.searchOffersRequestManager?.user_lat)!))&user_lng=\(String(describing: (requestObject.searchOffersRequestManager?.user_lng)!))&search_key=\(String(describing: (requestObject.searchOffersRequestManager?.search_key)!))&limit=\(String(describing: (requestObject.searchOffersRequestManager?.limit)!))&lang_id=\(String(describing: (requestObject.searchOffersRequestManager?.lang_id)!))&area=\(String(describing: (requestObject.searchOffersRequestManager?.area)!))&postal_code=\(String(describing: (requestObject.searchOffersRequestManager?.postal_code)!))"
        print("Search Offers :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
    
   
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }


        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
//                if responseData.error?.localizedDescription != "cancelled" {
                    if responseData.data != nil {
                        completion(responseData.data,responseData.error)
                    }
                    else {
                        completion(responseData.data,responseData.error)
//                    }
                }
             }
        }
    }
    
    /**
     This method is used for Search Filter service call.
     @param request parameters.
     @return response.
     */
    func searchFilterServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/searchfilter"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.searchFiltersRequestManager?.user_id)!))&user_lat=\(String(describing: (requestObject.searchFiltersRequestManager?.user_lat)!))&user_lng=\(String(describing: (requestObject.searchFiltersRequestManager?.user_lng)!))&ads_ids=\(String(describing: (requestObject.searchFiltersRequestManager?.ads_ids)!))&lang_id=\(String(describing: (requestObject.searchFiltersRequestManager?.lang_id)!))"
        print("Search Filtert :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Search Filter service call.
     @param request parameters.
     @return response.
     */
    func nearByBussinessServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/nearby_business_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.nearByBussinessRequestManager?.user_id)!))&user_lat=\(String(describing: (requestObject.nearByBussinessRequestManager?.user_lat)!))&user_lng=\(String(describing: (requestObject.nearByBussinessRequestManager?.user_lng)!))&ads_ids=\(String(describing: (requestObject.nearByBussinessRequestManager?.ads_ids)!))&lang_id=\(String(describing: (requestObject.nearByBussinessRequestManager?.lang_id)!))&postal_code=\(String(describing: requestObject.nearByBussinessRequestManager!.postal_code!))&area=\(String(describing: requestObject.nearByBussinessRequestManager!.area!))&limit=\(String(describing: requestObject.nearByBussinessRequestManager!.limit!))&category_id=\(String(describing: requestObject.nearByBussinessRequestManager!.category_id!))"
        
        print("Search Filtert :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Notification List service call.
     @param request parameters.
     @return response.
     */
    func notificationListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/notification_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.notificationListRequestManager?.user_id)!))&lang_id=\(String(describing: (requestObject.notificationListRequestManager?.lang_id)!))"
        print("Notification List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Change Lang service call.
     @param request parameters.
     @return response.
     */
    func changeLangServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/change_lang"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.changeLangRequestManager?.user_id)!))&lang_id=\(String(describing: (requestObject.changeLangRequestManager?.lang_id)!))"
        print("Change Lang :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Reminder List service call.
     @param request parameters.
     @return response.
     */
    func reminderListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/user_saved_reminder_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.reminderListRequestManager?.user_id)!))&lang=\(String(describing: (requestObject.reminderListRequestManager?.lang)!))"
        print("Reminder List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Saved Reminder service call.
     @param request parameters.
     @return response.
     */
    func savedReminderServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/user_saved_ads_reminder"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.savedReminderRequestManager?.user_id)!))&advertisement_id=\(String(describing: (requestObject.savedReminderRequestManager?.advertisement_id)!))&remind_status=\(String(describing: (requestObject.savedReminderRequestManager?.remind_status)!))&remind_time=\(String(describing: (requestObject.savedReminderRequestManager?.remind_time)!))&business_address_id=\(String(describing: (requestObject.savedReminderRequestManager?.business_address_id)!))"
        
        print("Saved Reminder :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Category List service call.
     @param request parameters.
     @return response.
     */
    func categoryListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/get_category"
        var request = URLRequest(url: URL(string: baseURL)!)
        print("efefwer",request)
        let postString = "lang_id=\(String(describing: (requestObject.categoryListRequestManager?.lang_id)!))"
        print("Category List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Detail Category List service call.
     @param request parameters.
     @return response.
     */
    func detailCatListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/get_category_ad_list"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "category_id=\(String(describing: (requestObject.detailCatListRequestManager?.category_id)!))&lang_id=\(String(describing: (requestObject.detailCatListRequestManager?.lang_id)!))&user_lat=\(String(describing: (requestObject.detailCatListRequestManager?.user_lat)!))&user_lng=\(String(describing: (requestObject.detailCatListRequestManager?.user_lng)!))&postal_code=\(String(describing: (requestObject.detailCatListRequestManager?.postal_code)!))"
        print("Detail Category List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Gift Coupon service call.
     @param request parameters.
     @return response.
     */
    func giftCouponServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/get_gift_coupon"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.giftCouponRequestManager?.user_id)!))"
        print("Gift Coupon :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Scanned Coupon service call.
     @param request parameters.
     @return response.
     */
    func scannedCouponServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/get_scan_coupon"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "user_id=\(String(describing: (requestObject.scannedCouponRequestManager?.user_id)!))&coupon_code=\(String(describing: (requestObject.scannedCouponRequestManager?.coupon_code)!))"
        print("Scanned Coupon :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Area List service call.
     @param request parameters.
     @return response.
     */
    func areaListServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/arealist"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "lang_id=\(String(describing: (requestObject.areaListRequestManager?.lang_id)!))"
        print("Area List :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for Mobile Transfer service call.
     @param request parameters.
     @return response.
     */
    func mobTransferServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/mobiletransfer"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "lang_id=\(String(describing: (requestObject.mobTransferRequestManager?.lang_id)!))&user_id=\(String(describing: (requestObject.mobTransferRequestManager?.user_id)!))&channel_code=\(String(describing: (requestObject.mobTransferRequestManager?.channel_code)!))&bank_details=\(String(describing: (requestObject.mobTransferRequestManager?.bank_details)!))"
        print("Mobile Transfer :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
    
    /**
     This method is used for UPI Detail Save service call.
     @param request parameters.
     @return response.
     */
    func upiDetailSaveServiceCallWithCompletion(requestObject: FollowAdsRequestManager, _ completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let baseURL: String = kBaseURL + "/upidetailsave"
        var request = URLRequest(url: URL(string: baseURL)!)
        let postString = "lang_id=\(String(describing: (requestObject.upiDetailSaveRequestManager?.lang_id)!))&user_id=\(String(describing: (requestObject.upiDetailSaveRequestManager?.user_id)!))&channel_code=\(String(describing: (requestObject.upiDetailSaveRequestManager?.channel_code)!))&bank_details=\(String(describing: (requestObject.upiDetailSaveRequestManager?.bank_details)!))"
        print("UPI Detail Save :",postString)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = requestTypePost
        request.httpBody = postString.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (responseData) -> Void in
            if((responseData.data) != nil) {
                print(responseData.result.value as Any)
                
                if responseData.data != nil {
                    completion(responseData.data,responseData.error)
                }
                else {
                    completion(responseData.data,responseData.error)
                }
            }
        }
    }
}
