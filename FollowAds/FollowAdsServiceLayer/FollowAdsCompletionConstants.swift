//
//  FollowAdsCompletionConstants.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation

typealias LangListCompletionBlock = (_ data: FollowAdsLangListDataModel?, _ error: Error?) -> Void
typealias RegisterCompletionBlock = (_ data: FollowAdsRegisterDataModel?, _ error: Error?) -> Void
typealias OtpVerificationCompletionBlock = (_ data: FollowAdsOtpVerificationDataModel?, _ error: Error?) -> Void
typealias ProfileCompletionBlock = (_ data: FollowAdsProfileDataModel?, _ error: Error?) -> Void
typealias HomeCompletionBlock = (_ data: FollowAdsHomeDataModel?, _ error: Error?) -> Void
typealias PopupAdsCompletionBlock = (_ data: FollowAdsPopupAdsDataModel?, _ error: Error?) -> Void
typealias popupOfferlistCompletionBlock = (_ data: FollowAdspopupOfferlistDataModel?, _ error: Error?) -> Void
typealias OfferDetailCompletionBlock = (_ data: FollowAdsOfferDetailDataModel?, _ error: Error?) -> Void
typealias BusinessDetailCompletionBlock = (_ data: FollowAdsBusinessDetailDataModel?, _ error: Error?) -> Void
typealias AllPromotionsCompletionBlock = (_ data: FollowAdsAllPromotionsDataModel?, _ error: Error?) -> Void
typealias WriteAdvertisementReviewCompletionBlock = (_ data: FollowAdsWriteAdvertisementReviewDataModel?, _ error: Error?) -> Void
typealias LikeAdvertisementCompletionBlock = (_ data: FollowAdsLikeAdvertisementDataModel?, _ error: Error?) -> Void
typealias FollowBusinessCompletionBlock = (_ data: FollowAdsFollowBusinessDataModel?, _ error: Error?) -> Void
typealias BankListCompletionBlock = (_ data: FollowAdsBankListDataModel?, _ error: Error?) -> Void
typealias UsedOfferCodeCompletionBlock = (_ data: FollowAdsUsedOfferCodeDataModel?, _ error: Error?) -> Void
typealias TransferMoneyCompletionBlock = (_ data: FollowAdsTransferMoneyDataModel?, _ error: Error?) -> Void
typealias LikedOffersListCompletionBlock = (_ data: FollowAdsLikedOffersListDataModel?, _ error: Error?) -> Void
typealias FollowedBusinessListCompletionBlock = (_ data: FollowAdsFollowedBusinessListDataModel?, _ error: Error?) -> Void
typealias UsedCouponListCompletionBlock = (_ data: FollowAdsUsedCouponListDataModel?, _ error: Error?) -> Void
typealias SaveBankDetailsCompletionBlock = (_ data: FollowAdsSaveBankDetailsDataModel?, _ error: Error?) -> Void
typealias SearchSuggestionCompletionBlock = (_ data: FollowAdsSearchSuggestionDataModel?, _ error: Error?) -> Void
typealias SearchOffersCompletionBlock = (_ data: FollowAdsSearchOffersDataModel?, _ error: Error?) -> Void
typealias SearchFilterCompletionBlock = (_ data: FollowAdsSearchFilterDataModel?, _ error: Error?) -> Void
typealias NotificationListCompletionBlock = (_ data: FollowAdsNotificationListDataModel?, _ error: Error?) -> Void
typealias NearByBussinessCompletionBlock = (_ data: FollowAdsNearByBussinessDataModel?, _ error: Error?) -> Void
typealias ChangeLangCompletionBlock = (_ data: FollowAdsChangeLangDataModel?, _ error: Error?) -> Void
typealias ReminderListCompletionBlock = (_ data: FollowAdsReminderListDataModel?, _ error: Error?) -> Void
typealias SavedReminderCompletionBlock = (_ data: FollowAdsSavedReminderDataModel?, _ error: Error?) -> Void
typealias EditBankDetailsCompletionBlock = (_ data: FollowAdsEditBankDetailsDataModel?, _ error: Error?) -> Void
typealias CategoryListCompletionBlock = (_ data: FollowAdsCategoryListDataModel?, _ error: Error?) -> Void
typealias DetailCatListCompletionBlock = (_ data: FollowAdsDetailCatListDataModel?, _ error: Error?) -> Void
typealias GiftCouponCompletionBlock = (_ data: FollowAdsGiftCouponDataModel?, _ error: Error?) -> Void
typealias ScannedCouponCompletionBlock = (_ data: FollowAdsScannedCouponDataModel?, _ error: Error?) -> Void

typealias HomePromotionsCompletionBlock = (_ data: FollowAdsHomePromotionsDataModel?, _ error: Error?) -> Void
typealias AreaListCompletionBlock = (_ data: FollowAdsAreaListDataModel?, _ error: Error?) -> Void
typealias MobTransferCompletionBlock = (_ data: FollowAdsMobTransferDataModel?, _ error: Error?) -> Void
typealias UPIDetailSaveCompletionBlock = (_ data: FollowAdsUPIDetailSaveDataModel?, _ error: Error?) -> Void



