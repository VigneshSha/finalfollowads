//
//  FollowAdsRequestManager.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class FollowAdsRequestManager: NSObject {

    var registerRequestManager: FollowAdsRegisterRequestManager?
    var otpVerificationRequestManager: FollowAdsOtpVerificationRequestManager?
    var profileRequestManager: FollowAdsProfileRequestManager?
    var homeRequestManager: FollowAdsHomeRequestManager?
    var popupRequestManager: FollowAdspopupadsRequestManager?
    var PopupOfferlistRequestManager:FollowAdsPopupOfferlistRequestManager?
    var ShowpopupOfferlistRequestManager: FollowAdsShowpopupOfferlistRequestManager?
    var offerDetailRequestManager: FollowAdsOfferDetailRequestManager?
    var businessDetailRequestManager: FollowAdsBusinessDetailRequestManager?
    var promotionSectionRequestManager: FollowAdsPromotionSectionRequestManager?
    var writeAdvertisementReviewRequestManager: FollowAdsWriteAdvertisementReviewRequestManager?
    var likeAdvertisementRequestManager: FollowAdsLikeAdvertisementRequestManager?
    var followBusinessRequestManager: FollowAdsFollowBusinessRequestManager?
    var bankDetailsListRequestManager: FollowAdsBankDetailsListRequestManager?
    var usedOfferCodeRequestManager: FollowAdsUsedOfferCodeRequestManager?
    var transferMoneyRequestManager: FollowAdsTransferMoneyRequestManager?
    var likedOffersListRequestManager: FollowAdsLikedOffersListRequestManager?
    var followedBusinessListRequestManager: FollowAdsFollowedBusinessListRequestManager?
    var usedCouponsListRequestManager: FollowAdsUsedCouponsListRequestManager?
    var saveBankDetailsRequestManager: FollowAdsSaveBankDetailsRequestManager?
    var serachSuggestionsRequestManager : FollowAdsSearchSuggestionRequestManager?
    var searchOffersRequestManager : FollowAdsSearchOffersRequestManager?
    var searchFiltersRequestManager : FollowAdsSearchFiltersRequestManager?
    var nearByBussinessRequestManager : FollowAdsNearBySearchRequestManager?
    var notificationListRequestManager: FollowAdsNotificationsListRequestManager?
    var changeLangRequestManager: FollowAdsChangeLangRequestManager?
    var reminderListRequestManager: FollowAdsReminderListRequestManager?
    var savedReminderRequestManager: FollowAdsSavedReminderRequestManager?
    var editBankDetailsRequestManager: FollowAdsEditBankDetailsRequestManager?
    var categoryListRequestManager: FollowAdsCategoryListRequestManager?
    var detailCatListRequestManager: FollowAdsDetailCatListRequestManager?
    var giftCouponRequestManager: FollowAdsGiftCouponRequestManager?
    var scannedCouponRequestManager: FollowAdsScannedCouponRequestManager?
    var homePromotionsRequestManager: FollowAdsHomePromotionsRequestManager?
    var areaListRequestManager: FollowAdsAreaListRequestManager?
    var mobTransferRequestManager: FollowAdsMobTransferRequestManager?
    var upiDetailSaveRequestManager: FollowAdsUPIDetailSaveRequestManager?
}

class FollowAdsUPIDetailSaveRequestManager : NSObject {
    var lang_id: String?
    var user_id: String?
    var bank_details : String?
    var channel_code : String?

}
class FollowAdsMobTransferRequestManager : NSObject {
    var lang_id: String?
    var user_id: String?
    var bank_details : String?
    var channel_code : String?

}

class FollowAdsAreaListRequestManager : NSObject {
    var lang_id: String?
}

class FollowAdsNearBySearchRequestManager : NSObject {
    var user_id: String?
    var user_lat: String?
    var user_lng: String?
    var ads_ids: String?
    var lang_id: String?
    var postal_code: String?
    var area: String?
    var limit: String?
    var category_id: String?

 }

class FollowAdsSearchSuggestionRequestManager : NSObject {
    var user_id : String?
    var user_lat : String?
    var user_lng : String?
    var lang_id: String?

}

class  FollowAdsSearchOffersRequestManager : NSObject {
    var user_id  : String?
    var user_lat  : String?
    var user_lng  : String?
    var search_key  : String?
    var limit  : String?
    var lang_id: String?
    var area: String?
    var postal_code: String?



}

class FollowAdsRegisterRequestManager: NSObject {
    var full_name: String?
    var phone_number: String?
    var device_token: String?
    var device_name: String?
    var is_already_registered: String?
    var email_id: String?
    var app_version: String?
    var lang: String?
    var default_location: String?
    var default_language: String?
    var lang_id: String?
    var postal_code: String?


}

class FollowAdsOtpVerificationRequestManager: NSObject {
    var user_id: String?
    var otp: String?
    var lang_id: String?

}

class FollowAdsProfileRequestManager: NSObject {
    var user_id: String?
    var prof_img: String?
    var user_name: String?
    var email_id: String?
    var lang_id: String?
}

class FollowAdsHomeRequestManager: NSObject {
    var user_id: String?
    var device_token: String?
    var device_name: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var area: String?
    var postal_code: String?

}
class FollowAdspopupadsRequestManager: NSObject {
    var user_id: String?
    var device_token: String?
    var device_name: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var area: String?
    var postal_code: String?

}
class FollowAdsPopupOfferlistRequestManager: NSObject {
   
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var postal_code: String?
    var image_id:String?
}

class FollowAdsShowpopupOfferlistRequestManager: NSObject {
    var user_id: String?
    var device_token: String?
    var device_name: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var area: String?
    var postal_code: String?

}
class FollowAdsHomePromotionsRequestManager: NSObject {
    var user_id: String?
    var device_token: String?
    var device_name: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var area: String?
    var limit: String?
    var postal_code: String?


}

class FollowAdsOfferDetailRequestManager: NSObject {
    var user_id: String?
    var advertisement_id: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var business_address_id: String?
    var notify_id: String?
    var pushnotification_id: String?

}

class FollowAdsBusinessDetailRequestManager: NSObject {
    var user_id: String?
    var business_id: String?
    var user_lat: String?
    var user_lng: String?
    var lang_id: String?
    var business_address_id: String?
}

class FollowAdsPromotionSectionRequestManager: NSObject {
    var user_id: String?
    var promotion_id: String?
    var lang_id: String?
    var user_lat: String?
    var user_lng: String?
    var area: String?
    var postal_code: String?

}

class FollowAdsWriteAdvertisementReviewRequestManager: NSObject {
    var user_id: String?
    var business_id: String?
    var advertisement_id: String?
    var comments: String?
    var rating: String?
    var title: String?
    var lang_id: String?

}

class FollowAdsLikeAdvertisementRequestManager: NSObject {
    var user_id: String?
    var advertisement_id: String?
    var fav_status: String?
    var lang_id: String?
    var business_address_id: String?
}

class FollowAdsFollowBusinessRequestManager: NSObject {
    var user_id: String?
    var business_id: String?
    var followed_status: String?
    var lang_id: String?
    var business_address_id: String?

}

class FollowAdsBankDetailsListRequestManager: NSObject {
    var user_id: String?
    var lang_id: String?

}

class FollowAdsUsedOfferCodeRequestManager: NSObject {
    var user_id: String?
    var advertisement_id: String?
    var business_id: String?
    var offer_code: String?
    var lang_id: String?

}

class FollowAdsTransferMoneyRequestManager: NSObject {
    var user_id: String?
    var bank_id: String?
    var lang_id: String?
    var user_payment_method_id: String?

}

class FollowAdsLikedOffersListRequestManager: NSObject {
    var user_id: String?
    var lang_id: String?
    var postal_code: String?
    var user_lat: String?
    var user_lng: String?

}

class FollowAdsFollowedBusinessListRequestManager: NSObject {
    var user_id: String?
    var lang_id: String?

}

class FollowAdsSaveBankDetailsRequestManager: NSObject {
    var user_id : String?
    var bank_details : String?
    var lang_id: String?
    var channel_code: String?


}

class FollowAdsEditBankDetailsRequestManager: NSObject {
    var bank_details : String?
    var user_id : String?
    var lang_id: String?
    var channel_code: String?

}

class FollowAdsUsedCouponsListRequestManager: NSObject {
    var user_id: String?
    var lang: String?
    var advertisement_id: String?
    var business_id: String?
    var offer_code: String?
    var lang_id: String?

}

class FollowAdsSearchFiltersRequestManager: NSObject {
    var user_id: String?
    var user_lat: String?
    var user_lng: String?
    var ads_ids: String?
    var lang_id: String?

}

class FollowAdsNotificationsListRequestManager: NSObject {
    var user_id: String?
    var lang_id: String?

}

class FollowAdsChangeLangRequestManager: NSObject {
    var user_id: String?
    var lang_id: String?
}

class FollowAdsReminderListRequestManager: NSObject {
    var user_id: String?
    var lang: String?
    var lang_id: String?
}

class FollowAdsSavedReminderRequestManager: NSObject {
    var user_id: String?
    var advertisement_id: String?
    var remind_status: String?
    var remind_time: String?
    var lang_id: String?
    var business_address_id: String?
}

class FollowAdsCategoryListRequestManager: NSObject {
    var lang_id: String?
}

class FollowAdsDetailCatListRequestManager: NSObject {
    var category_id: String?
    var lang_id: String?
    var user_lat: String?
    var user_lng: String?
    var postal_code: String?

}

class FollowAdsGiftCouponRequestManager: NSObject {
    var user_id: String?

}

class FollowAdsScannedCouponRequestManager: NSObject {
    var user_id: String?
    var gift_coupon_id: String?
    var coupon_value: String?
    var coupon_code: String?

}
