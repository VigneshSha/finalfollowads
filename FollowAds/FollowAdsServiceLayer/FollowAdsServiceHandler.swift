//
//  FollowAdsServiceHandler.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class FollowAdsServiceHandler: NSObject {
    
    /**
     This method is used for Language List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callLanguageListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping LangListCompletionBlock) {
        FollowAdsServiceManager().LangListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateLangListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateLangListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Register Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callRegisterServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping RegisterCompletionBlock) {
        FollowAdsServiceManager().registerServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateRegisterWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateRegisterWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Otp Verification Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callOtpVerificationServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping OtpVerificationCompletionBlock) {
        FollowAdsServiceManager().otpVerificationServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateOtpVerificationWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateOtpVerificationWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Profile Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callProfileServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping ProfileCompletionBlock) {
        FollowAdsServiceManager().profileServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateProfileWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateProfileWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Home Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callHomeServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping HomeCompletionBlock) {
        FollowAdsServiceManager().homeServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateHomeWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateHomeWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    class func callpopupadsservicecalls(requestObject: FollowAdsRequestManager,_ completion: @escaping PopupAdsCompletionBlock) {
          FollowAdsServiceManager().PopupAdsServiceCallWithCompilence(requestObject: requestObject) { (data, error) -> Void in
              if error != nil {
                  if (error) != nil {
                      FollowAdsUIModel.updatePopupAdsWith(response: data, err: error as NSError?, updateUIModel: completion)
                  }
              }
              else{
                  FollowAdsUIModel.updatePopupAdsWith(response: data, err: error as NSError?, updateUIModel: completion)
              }
          }
      }
    class func callPopupOfferlistservicecalls(requestObject: FollowAdsRequestManager,_ completion: @escaping popupOfferlistCompletionBlock) {
            FollowAdsServiceManager().PopupOfferlistServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
                if error != nil {
                    if (error) != nil {
                        FollowAdsUIModel.updatePopupOfferlistWith(response: data, err: error as NSError?, updateUIModel: completion)
                    }
                }
                else{
                    FollowAdsUIModel.updatePopupOfferlistWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
        }
    
    /**
     This method is used for Home Promotions Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callHomePromotionsServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping HomeCompletionBlock) {
        FollowAdsServiceManager().homePromotionsServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateHomePromotionsWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateHomePromotionsWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }

    /**
     This method is used for Offer Detail Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callOfferDetailServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping OfferDetailCompletionBlock) {
        FollowAdsServiceManager().offerDetailServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateOfferDetailWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateOfferDetailWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Business Detail Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callBusinessDetailServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping BusinessDetailCompletionBlock) {
        FollowAdsServiceManager().businessDetailServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateBusinessDetailWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateBusinessDetailWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for All Promotions Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callAllPromotionsServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping AllPromotionsCompletionBlock) {
        FollowAdsServiceManager().allPromotionsServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateAllPromotionsWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateAllPromotionsWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Write Advertisement Review Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callWriteAdvertisementReviewServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping WriteAdvertisementReviewCompletionBlock) {
        FollowAdsServiceManager().writeAdvertisementReviewServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateWriteAdvertisementReviewWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateWriteAdvertisementReviewWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Like Advertisement Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callLikeAdvertisementServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping LikeAdvertisementCompletionBlock) {
        FollowAdsServiceManager().likeAdvertisementServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateLikeAdvertisementWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateLikeAdvertisementWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Follow Business Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callFollowBusinessServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping FollowBusinessCompletionBlock) {
        FollowAdsServiceManager().followBusinessServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateFollowBusinessWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateFollowBusinessWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Bank List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callBankListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping BankListCompletionBlock) {
        FollowAdsServiceManager().bankListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateBankListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateBankListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Used Offer Code Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callUsedOfferCodeServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping UsedOfferCodeCompletionBlock) {
        FollowAdsServiceManager().usedOfferCodeServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateUsedOfferCodeWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateUsedOfferCodeWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Transfer Money Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callTransferMoneyServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping TransferMoneyCompletionBlock) {
        FollowAdsServiceManager().transferMoneyServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateTransferMoneyWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateTransferMoneyWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Liked Offers List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callLikedOffersListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping LikedOffersListCompletionBlock) {
        FollowAdsServiceManager().likedOffersListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateLikedOffersListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateLikedOffersListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Followed Business List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callFollowedBusinessListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping FollowedBusinessListCompletionBlock) {
        FollowAdsServiceManager().followedBusinessListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateFollowedBusinessListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateFollowedBusinessListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Used Coupon List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callUsedCouponListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping UsedCouponListCompletionBlock) {
        FollowAdsServiceManager().usedCouponListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateUsedCouponListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateUsedCouponListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Save Bank details Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callSaveBankDetailsServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping SaveBankDetailsCompletionBlock) {
        FollowAdsServiceManager().saveBankDetailsServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.saveBankDetailsWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.saveBankDetailsWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Edit Bank details Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callEditBankDetailsServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping EditBankDetailsCompletionBlock) {
        FollowAdsServiceManager().editBankDetailsServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.editBankDetailsWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.editBankDetailsWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Search suggestions Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callSearchSuggestionServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping SearchSuggestionCompletionBlock) {
        FollowAdsServiceManager().searchSuggestionServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateSearchSuggestionWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateSearchSuggestionWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Search offers Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callSearchOffersServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping SearchOffersCompletionBlock) {
        FollowAdsServiceManager().searchOffersServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateSearchOffersWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateSearchOffersWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Search filter Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callSearchFilterServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping SearchFilterCompletionBlock) {
        FollowAdsServiceManager().searchFilterServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateSearchFilterWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateSearchFilterWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for NearBy Bussiness Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callNearByBussinessServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping NearByBussinessCompletionBlock) {
        FollowAdsServiceManager().nearByBussinessServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateNearByBussinessWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateNearByBussinessWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Notification List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callNotificationListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping NotificationListCompletionBlock) {
        FollowAdsServiceManager().notificationListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateNotificationListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateNotificationListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Change Lang Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callChangeLangServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping ChangeLangCompletionBlock) {
        FollowAdsServiceManager().changeLangServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateChangeLangWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateChangeLangWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
        
    }
    
    /**
     This method is used for Reminder List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callReminderListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping ReminderListCompletionBlock) {
        FollowAdsServiceManager().reminderListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateReminderListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateReminderListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Saved Reminder Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callSavedReminderServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping SavedReminderCompletionBlock) {
        FollowAdsServiceManager().savedReminderServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateSavedReminderWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateSavedReminderWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Category List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callCategoryListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping CategoryListCompletionBlock) {
        FollowAdsServiceManager().categoryListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateCategoryListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateCategoryListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Detail Category List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callDetailCatListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping DetailCatListCompletionBlock) {
        FollowAdsServiceManager().detailCatListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateDetailCatListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateDetailCatListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Gift Coupon Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callGiftCouponServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping GiftCouponCompletionBlock) {
        FollowAdsServiceManager().giftCouponServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateGiftCouponWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateGiftCouponWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Scanned Coupon Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callScannedCouponServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping ScannedCouponCompletionBlock) {
        FollowAdsServiceManager().scannedCouponServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateScannedCouponWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateScannedCouponWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Area List Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callAreaListServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping AreaListCompletionBlock) {
        FollowAdsServiceManager().areaListServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateAreaListWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateAreaListWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for Mobile Transfer Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callMobTransferServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping MobTransferCompletionBlock) {
        FollowAdsServiceManager().mobTransferServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateMobTransferWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateMobTransferWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
    
    /**
     This method is used for UPI Detail Save Service handler to handle service call from viewcontroller to UIModel class.
     @param Requset object.
     @return completion block.
     */
    class func callUPIDetailSaveServiceCall(requestObject: FollowAdsRequestManager,_ completion: @escaping UPIDetailSaveCompletionBlock) {
        FollowAdsServiceManager().upiDetailSaveServiceCallWithCompletion(requestObject: requestObject) { (data, error) -> Void in
            if error != nil {
                if (error) != nil {
                    FollowAdsUIModel.updateUPIDetailSaveWith(response: data, err: error as NSError?, updateUIModel: completion)
                }
            }
            else{
                FollowAdsUIModel.updateUPIDetailSaveWith(response: data, err: error as NSError?, updateUIModel: completion)
            }
        }
    }
}
