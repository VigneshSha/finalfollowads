//
//  OfferListTableViewController.swift
//  FollowAds
//
//  Created by IOS Developer on 11/02/20.
//  Copyright © 2020 com.vijai.appname. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
protocol OfferlistDetailDelegate {
    func navigateToView(indexPath: IndexPath)
}
class OfferListTableViewController: UITableViewController {
    var logoImages: [UIImage] = []
     var colVwCell: UITableViewCell?
    var passingoffertitle:String = ""
    var followAdsRequestManager = FollowAdsRequestManager()
    var popupOfferlistDetailDataModel: FollowAdspopupOfferlistDataModel?
    var delegate: OfferlistDetailDelegate?
    @IBOutlet var tableview: UITableView!
    override func viewDidLoad() {

        print("dgss",passingoffertitle)
        // All three of these calls are equivalent
        displaypopup()
    }
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
         self.title = passingoffertitle
    }
    func displaypopup(){
               if UserDefaults.standard.value(forKey: City) != nil{
                          self.followAdsRequestManager.PopupOfferlistRequestManager = FollowAdsPopupOfferlistRequestManager()
                   
                          if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                              let userLatText = String()
                              let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.user_lat = userLat
                          }
                          if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                              let userLngText = String()
                              let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.user_lng = userLng
                          
                          }
                          let langidText = String()
                          let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
                          self.followAdsRequestManager.PopupOfferlistRequestManager?.lang_id = langId
                          if UserDefaults.standard.value(forKey: City) != nil{
                              let cityName = String()
                              let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.image_id = city
                          }
                          else {
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.image_id = ""
                              
                          }
                          if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                              let postalCodeName = String()
                              let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.postal_code = postalCode
                          }
                          else {
                              self.followAdsRequestManager.PopupOfferlistRequestManager?.postal_code = ""
                              
                          }
                if UserDefaults.standard.value(forKey: "image_id") != nil{
                               let ImageIdCode = String()
                               let ImageId: String = ImageIdCode.passedString((UserDefaults.standard.object(forKey: "image_id") as? String))
                               self.followAdsRequestManager.PopupOfferlistRequestManager?.image_id = ImageId
                    print("gdsfsf",ImageId)
                           }
                           else {
                               self.followAdsRequestManager.PopupOfferlistRequestManager?.image_id = ""
                           }
               let PopupOfferlist: popupOfferlistCompletionBlock = {(response, error) in
                   if let _ = error {
                    print("fsafs",error)
                       MBProgressHUD.hide(for: self.view, animated: true)
                   }
                   else
                   {
                       if response != nil {
                           DispatchQueue.main.async(execute: {
                           self.popupOfferlistDetailDataModel = response!
                                   print("sjsfjfsjs",self.popupOfferlistDetailDataModel)
                            self.tableview.reloadData()
                        //       print("dgdgdgdg",self.popupOfferlistDetailDataModel?.images[0].ads_list.count)
                         //      self.CollectionView.reloadData()
       //                        self.imageview()
                             //  self.genicontroller?.popupadslistDataModel = self.popupadslistDataModel
                             
                                           //  self.collectionView.reloadData()
                               MBProgressHUD.hide(for: self.view, animated: true)
                           })
                       }
                   }
               }
                   
               FollowAdsServiceHandler.callPopupOfferlistservicecalls(requestObject: self.followAdsRequestManager, PopupOfferlist)
               }

             }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if section == 0 {
            print("fssfsf",self.popupOfferlistDetailDataModel?.popAdsListData.count)
              if (self.popupOfferlistDetailDataModel?.popAdsListData.count) != nil {
                if (((self.popupOfferlistDetailDataModel?.popAdsListData.count)!) ) >= 15 {
                           return 4
                       }
                       else {
                      return (((self.popupOfferlistDetailDataModel?.popAdsListData.count)!))
                       }
                   }
                   else {
                       return 0
                   }
               }
               else {
                   return 0
               }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
       if indexPath.section == 0 {
                                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OfferListTableViewCell
                                   
           
           //                       cell.offerImageVw.layer.cornerRadius = cell.offerImageVw.frame.size.width / 2
           //                       cell.offerImageVw.clipsToBounds = true
           
                       if (self.popupOfferlistDetailDataModel?.popAdsListData.count) != nil {
                      
                            let advertisement_icon = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_icon
                                                                let advertisement_id = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_id
                                                                let ImageId = ImageIdUrl
                                                              
                                                              let joinstring  = ImageId + "/" + advertisement_id! +  "/" + advertisement_icon!
                                                              print("sfsfsfdfssf",joinstring)
           
                                      if joinstring != nil {
                                        if let encodedString  = joinstring.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                                            cell.offerfirstlabel.text = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].business_name
                                            cell.offersecondlabel.text = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_area
                                              cell.offerthirdlabel.text = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_caption
                                            cell.arrowimage.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                                           // cell.image.sizeToFit()
                                          }
                                      }
                                      else {
                                        cell.arrowimage.image = UIImage(named: "home_ads_placeholder")
                                      }
           
                                  }
                                  else {
           
                                      cell.arrowimage.image = UIImage(named: "home_ads_placeholder")
                              }
           
                                  return cell
                              }
                   else {
                       return colVwCell!
                   }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].ads_active != nil {
                       if self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].ads_active == "0" {
           //                self.banner = Banner(title: "This Offer has expired.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
           //                self.banner.dismissesOnTap = true
           //                self.banner.show(duration: 3.0)
                           self.view.makeToast("This Offer has expired.".localized(), duration: 3.0, position: .center)

                       }
                       else {
                           let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                           let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                           if self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_id != nil {
                               vc.offerId = self.popupOfferlistDetailDataModel?.popAdsListData[indexPath.row].advertisement_id ?? ""
                           }
                           else {
                               vc.offerId = ""
                           }
                           self.navigationController?.pushViewController(vc, animated: true)
                       }
                   }       }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
