//
//  VideosCollectionViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class VideosCollectionViewController: FollowAdsBaseViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    var videosCollectionDatasource: VideosCollectionDatasource?
    var videoLink: String?
    var visibleIP : IndexPath = IndexPath.init(row: 0, section: 0)
    var aboutToBecomeInvisibleCell = -1
    var refreshControl: UIRefreshControl!
    var followAdsRequestManager = FollowAdsRequestManager()
    var businessDetailDataModel = FollowAdsBusinessDetailDataModel()
    var businessId: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var navigationTitle = "Videos"
    var businessAddrId: String?
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        videoLink = "https://www.youtube.com/watch?v=n1gs_zh8zuM"
        collectionView.register(UINib(nibName: "VideosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "videosCellId")
        self.setupCollectionViewDataSource()
        self.setUpNavigationBarButton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if Reachability.isConnectedToNetwork() == true {
            self.shopDetailUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.layer.backgroundColor = UIColor(red: 183.0 / 255.0, green: 29.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0).cgColor
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.setupRefresh()
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to show the activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform shop detail service call.
     @param -.
     @return -.
     */
    func shopDetailUpdate() {
        self.followAdsRequestManager.businessDetailRequestManager = FollowAdsBusinessDetailRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.businessDetailRequestManager?.business_id = businessId
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lng = userLng
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.lang_id = langId
        self.followAdsRequestManager.businessDetailRequestManager?.business_address_id = businessAddrId

        let businessDetailCompletion: BusinessDetailCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.businessDetailDataModel = response!
                        print(self.businessDetailDataModel)
                        self.videosCollectionDatasource?.businessDetailsDataModel = self.businessDetailDataModel
                        self.collectionView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callBusinessDetailServiceCall(requestObject: self.followAdsRequestManager, businessDetailCompletion)
    }
    
    /*
     This method is used to setup refresh control.
     @param -.
     @return -.
     */
    func setupRefresh () {
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: (NSLocalizedString(pullToRefrest, comment: emptyString)))
        refreshControl.addTarget(self, action:#selector(refresh), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    /*
     This method is used to setup refresh control action.
     @param -.
     @return -.
     */
    @objc func refresh() {
        collectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    /*
     This method is used to setup collection view datasource.
     @param -.
     @return -.
     */
    func setupCollectionViewDataSource() {
        videosCollectionDatasource = VideosCollectionDatasource(VideoUrlData: videoLink)
        self.collectionView.dataSource = videosCollectionDatasource
        self.collectionView.delegate = videosCollectionDatasource
        videosCollectionDatasource?.collectionView = self.collectionView
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
        // Set font, color and size for navigation title
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
