//
//  PhotoZoomViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 18/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import AppsFlyerLib

class PhotoZoomViewController: FollowAdsBaseViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    var imageStr: String?
    var imageArray : [String]?
    var newImageArray : [UIImage] = []
    let scrollView = UIScrollView(frame: CGRect(x: 0, y: 100, width: UIScreen.main.bounds.size.width , height: (UIScreen.main.bounds.size.height - 140)))
    var frame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var subView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for image in (imageArray?.enumerated())! {
            let imageView = UIImageView()
            imageView.sd_setImage(with: URL(string: image.element), placeholderImage: UIImage(named: "Store_Placeholder"))
            newImageArray.append(imageView.image!)
        }
        self.configureScrollView()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    /*
     This method is used to configure the scroll view.
     @param -.
     @return -.
     */
    func configureScrollView(){
        scrollView.backgroundColor = UIColor.black
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isUserInteractionEnabled = true
        scrollView.clipsToBounds = true
        scrollView.bouncesZoom = true
        self.view.addSubview(scrollView)
        var pinchGesture = UIPinchGestureRecognizer()
        for index in 0..<newImageArray.count {
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            frame.size = self.scrollView.frame.size
            subView = UIImageView(frame: frame)
            subView.tag = index
            subView.contentMode = .scaleToFill
            subView.isUserInteractionEnabled = true
            subView.image = newImageArray[index]
            pinchGesture.delegate = self
            pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.zoom(sender:)))
            self.subView.addGestureRecognizer(pinchGesture)
            self.scrollView.addSubview(subView)
        }
        self.scrollView.isPagingEnabled = true
        self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.size.width *  CGFloat(newImageArray.count)), height: self.scrollView.frame.size.height)
    }
    
    /*
     This method is used to zoom and scale the image.
     @param -.
     @return -.
     */
    @objc func zoom(sender:UIPinchGestureRecognizer) {
        if sender.state == .ended || sender.state == .changed {
            let currentScale = sender.view!.frame.size.width / sender.view!.bounds.size.width
            var newScale = currentScale*sender.scale
            if newScale < 1 {
                newScale = 1
            }
            if newScale > 9 {
                newScale = 9
            }
            let transform = CGAffineTransform(scaleX: newScale, y: newScale)
            self.subView.transform = transform
            sender.scale = 1
        }
    }
    
    /*
     This method is used to dismiss the view when using swipe down gesture.
     @param -.
     @return -.
     */
    @objc func respondToSwipeGesture() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Scroll view delegates
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.subView
    }
    
}
