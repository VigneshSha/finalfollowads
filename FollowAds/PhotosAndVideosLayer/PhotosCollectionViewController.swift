//
//  PhotosCollectionViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import SKPhotoBrowser
import FirebaseDynamicLinks
import AppsFlyerLib

class PhotosCollectionViewController: FollowAdsBaseViewController, PhotoZoomViewDelegate {
    
    @IBOutlet var collectionView: UICollectionView!
    var photosArray: [String]?
    var photosDatasource: PhotosCollectionDatasource?
    var followAdsRequestManager = FollowAdsRequestManager()
    var businessDetailDataModel = FollowAdsBusinessDetailDataModel()
    var businessId: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var navigationTitle = "Photos"
    var businessAddrId: String?
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        photosArray = ["BigBagOffer1", "BigBagOffer2", "BigBagOffer3", "BigBagOffer4", "BigBagOffer1", "BigBagOffer2", "BigBagOffer3", "BigBagOffer4"]
        collectionView.register(UINib(nibName: "PhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photosCellId")
        self.setupCollectionViewDataSource()
        self.setUpNavigationBarButton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.shopDetailUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
     This method is used to show the activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform shop detail service call.
     @param -.
     @return -.
     */
    func shopDetailUpdate() {
        self.followAdsRequestManager.businessDetailRequestManager = FollowAdsBusinessDetailRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.businessDetailRequestManager?.business_id = businessId
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lat = userLat
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lng = userLng
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.lang_id = langId
        self.followAdsRequestManager.businessDetailRequestManager?.business_address_id = businessAddrId

        
        let businessDetailCompletion: BusinessDetailCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.businessDetailDataModel = response!
                        print(self.businessDetailDataModel)
                        self.photosDatasource?.businessDetailsDataModel = self.businessDetailDataModel
                        self.collectionView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callBusinessDetailServiceCall(requestObject: self.followAdsRequestManager, businessDetailCompletion)
    }
    
    /*
     This method is used to setup collection view datasouce.
     @param -.
     @return -.
     */
    func setupCollectionViewDataSource() {
        photosDatasource = PhotosCollectionDatasource(photosArrData: photosArray!)
        self.collectionView.dataSource = photosDatasource
        self.collectionView.delegate = photosDatasource
        photosDatasource?.collectionView = self.collectionView
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        photosDatasource?.delegate = self
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
        // Set font, color and size for navigation title
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to navigate to the photo view.
     @param -.
     @return -.
     */
    func navigateToPhotoView(imageArray : [String]?, selectedIndex : Int)
    {
        var images = [SKPhoto]()
        for image in (imageArray!.enumerated()) {
            let imageView = UIImageView()
            if let encodedString  = image.element.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))
            }
            let photo = SKPhoto.photoWithImage(imageView.image!)// add some UIImage
            images.append(photo)
        }
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
