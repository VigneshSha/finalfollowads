//
//  VideosCollectionDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class VideosCollectionDatasource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    var collectionView: UICollectionView?
    var videoUrlStr: String?
    var cell : VideosCollectionViewCell?
    var businessDetailsDataModel: FollowAdsBusinessDetailDataModel?
    
    init(VideoUrlData: String?){
        self.videoUrlStr = VideoUrlData
        super.init()
    }
    
    // MARK:- COLLECTION VIEW DELEGATES AND DATASOURCES

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if businessDetailsDataModel?.information.business_video.count != nil {
            return (businessDetailsDataModel?.information.business_video.count)!
        }
        else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videosCellId", for: indexPath) as? VideosCollectionViewCell
        cell?.videoUrl = self.businessDetailsDataModel?.information.business_video[indexPath.row].business_vid
        cell?.playBtn.tag = indexPath.row
        cell?.playBtn.isHidden = false
        if cell?.videoUrl == "" {
            cell?.videoImageView.image = UIImage(named: "business_Placeholder")
        }
        cell?.videoImageView.isHidden = false
        cell?.VideoPlayerView.isHidden = true
        cell?.layer.shouldRasterize = true
        cell?.layer.rasterizationScale = UIScreen.main.scale
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView!.frame.width/2.3, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cellfds : VideosCollectionViewCell  = (collectionView.cellForItem(at: indexPath) as? VideosCollectionViewCell) else { return }
        DispatchQueue.main.async(execute: {
            cellfds.playBtn.isHidden = true
            cellfds.videoImageView.isHidden = true
            cellfds.VideoPlayerView.isHidden = false
            cellfds.VideoPlayerView.stop()
            cellfds.VideoPlayerView.clear()
            cellfds.VideoPlayerView.play()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cellfds : VideosCollectionViewCell  = (collectionView.cellForItem(at: indexPath) as? VideosCollectionViewCell) else { return }
        DispatchQueue.main.async(execute: {
            cellfds.playBtn.isHidden = false
            cellfds.videoImageView.isHidden = false
            cellfds.VideoPlayerView.isHidden = true
            cellfds.VideoPlayerView.stop()
            cellfds.VideoPlayerView.clear()
        })
    }
    
    // Scroll View Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
}
