//
//  PhotosCollectionDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoZoomViewDelegate {
    func navigateToPhotoView(imageArray : [String]?, selectedIndex : Int)
}

class PhotosCollectionDatasource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var collectionView: UICollectionView?
    var photosArray: [String?]
    var delegate: PhotoZoomViewDelegate?
    var businessDetailsDataModel: FollowAdsBusinessDetailDataModel?
    
    init(photosArrData: [String?]) {
        self.photosArray = photosArrData
        super.init()
    }
    
    // MARK:- COLLECTION VIEW DELEGATES AND DATASOURCES

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if businessDetailsDataModel?.information.business_images.count != nil {
            return (businessDetailsDataModel?.information.business_images.count)!
        }
        else {
            return 1
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photosCellId", for: indexPath) as? PhotosCollectionViewCell
        let urlstring = self.businessDetailsDataModel?.information.business_images[indexPath.row].business_img
        
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {

            cell?.shopImagesImgView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "business_Placeholder"))
            }
        }
        else {
            cell?.shopImagesImgView.image = UIImage(named: "business_Placeholder")
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            return CGSize(width: self.collectionView!.frame.width/2.7, height: 110)

        }
        else {
            return CGSize(width: self.collectionView!.frame.width/3.2, height: 110)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var imagesString : [String] = []
        for images in (businessDetailsDataModel?.information.business_images.enumerated())! {
            imagesString.append(images.element.business_img!)
        }
        if delegate != nil {
            delegate?.navigateToPhotoView(imageArray: imagesString, selectedIndex: indexPath.row)
        }
    }
    
}
