//
//  CollectionVwHeaderReusableView.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 18/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class CollectionVwHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet var offerNameLabel: UILabel!
    
    @IBOutlet var seeAllBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        offerNameLabel.text = "Offer"
    }
}
