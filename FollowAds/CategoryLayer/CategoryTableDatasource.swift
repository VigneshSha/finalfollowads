//
//  CategoryTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryDelegate {
    func navigateToIndexpath(indexPath: IndexPath)
}
class CategoryTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var delegate: CategoryDelegate?
    
    init(categoryListModel: FollowAdsCategoryListDataModel){
        self.categoryListDataModel = categoryListModel
        super.init()
    }
    
    // MARK: TableView Delegates and Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryListDataModel.categoryList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let categoryCell: CategoryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "CategoryCellId") as? CategoryTableViewCell
        let urlstring = categoryListDataModel.categoryList[indexPath.row].category_icon

        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                categoryCell?.categoryIconsImgVw.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            categoryCell?.categoryIconsImgVw.image = UIImage(named: "home_ads_placeholder")
        }
        categoryCell.labelCategoryName.text = categoryListDataModel.categoryList[indexPath.row].category_name
        return categoryCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToIndexpath(indexPath: indexPath)
        }
    }
}
