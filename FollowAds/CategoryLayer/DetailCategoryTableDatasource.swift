//
//  DetailCategoryTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 08/02/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

protocol DetailCatDelegate {
    func navigateToView(indexPath: IndexPath)
}

class DetailCategoryTableDatasource: NSObject, UITableViewDataSource, UITableViewDelegate {

    var tableView: UITableView?
    var detailCatListDataModel = FollowAdsDetailCatListDataModel()
    var delegate: DetailCatDelegate?
    
    init(detailCategoryListModel: FollowAdsDetailCatListDataModel){
        self.detailCatListDataModel = detailCategoryListModel
        super.init()
    }
    
    // MARK: TableView Delegates and Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailCatListDataModel.category_advertisement_list.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detailCatCell: DetailCategoryTableVwCell! = tableView.dequeueReusableCell(withIdentifier: "DetailCategoryCellId") as? DetailCategoryTableVwCell
        detailCatCell.storeImgVw.layer.cornerRadius = 10
        detailCatCell.storeImgVw.clipsToBounds = true
        detailCatCell.storeNameLabel.text = detailCatListDataModel.category_advertisement_list[indexPath.row].business_name
        detailCatCell.storeAreaLabel.text = detailCatListDataModel.category_advertisement_list[indexPath.row].advertisement_area
        detailCatCell.storeCaptionLabel.text = detailCatListDataModel.category_advertisement_list[indexPath.row].advertisement_caption

        let urlstring = detailCatListDataModel.category_advertisement_list[indexPath.row].advertisement_icon

        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                detailCatCell?.storeImgVw.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            detailCatCell?.storeImgVw.image = UIImage(named: "home_ads_placeholder")
        }
        return detailCatCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
