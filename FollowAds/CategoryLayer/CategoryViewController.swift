//
//  CategoryViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class CategoryViewController: FollowAdsBaseViewController, CategoryDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    var categoryTableDatasource: CategoryTableDatasource?
    let categoryListArray = ["Apparel & Accessories", "Electronics & HomeAppliances", "Events & Activities", "Food & Beverages", "Gym & Fitness", "Mobile & Gadgets", "Provisions & Groceries", "Salon & Spa", "Services", "Others"]
    let categoryImgsArray = ["google-maps (4)", "google-maps (4)", "google-maps (4)", "google-maps (4)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)"]
    var navigationTitle = "Categories".localized()
    var followAdsRequestManager = FollowAdsRequestManager()
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableDatasource()
        self.trackViewcontroller(name: "View_Category", screenClass: "CategoryViewController")
        AppsFlyerTracker.shared().trackEvent("View_Category",
                                             withValues: [
                                                AFEventAdView: "View_Category",
                                                ]);
         // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.isConnectedToNetwork() == true {
            self.categoryList()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.navigationItem.title = navigationTitle
        isAlerdayNav = false
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
        
        if let pushId = notification.userInfo?["pushnotification_id"] as? String {
            vc.pushnotificationId = pushId
            vc.notifiId = "1"
        }
        
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to setup TableView Datasource.
     @param ~~.
     @return ~~.
     */
    func setupTableDatasource() {
        categoryTableDatasource = CategoryTableDatasource(categoryListModel: categoryListDataModel)
        self.tableView.delegate = categoryTableDatasource
        self.tableView.dataSource = categoryTableDatasource
        categoryTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
        categoryTableDatasource?.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))

    }
    
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func categoryList() {
        self.followAdsRequestManager.categoryListRequestManager = FollowAdsCategoryListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = ""
        }
        
        let categoryListCompletion: CategoryListCompletionBlock = {(response, error) in
            print("fddvdf",response)
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.categoryListDataModel = response!
                        print(self.categoryListDataModel)
                        self.categoryTableDatasource?.categoryListDataModel = self.categoryListDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callCategoryListServiceCall(requestObject: self.followAdsRequestManager, categoryListCompletion)
    }
    
    func navigateToIndexpath(indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.trackViewcontroller(name: "Select_Apparels", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_Apparels",
                                                 withValues: [
                                                    AFEventAdView: "Select_Apparels",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "Select_electronics", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_electronics",
                                                 withValues: [
                                                    AFEventAdView: "Select_electronics",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "Select_events", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_events",
                                                 withValues: [
                                                    AFEventAdView: "Select_events",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "Select_Food", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_Food",
                                                 withValues: [
                                                    AFEventAdView: "Select_Food",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "Select_gym", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_gym",
                                                 withValues: [
                                                    AFEventAdView: "Select_gym",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "Select_mobile", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_mobile",
                                                 withValues: [
                                                    AFEventAdView: "Select_mobile",
                                                    ]);
        case 6:
            self.trackViewcontroller(name: "Select_provisions", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_provisions",
                                                 withValues: [
                                                    AFEventAdView: "Select_provisions",
                                                    ]);
        case 7:
            self.trackViewcontroller(name: "Select_salon", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_salon",
                                                 withValues: [
                                                    AFEventAdView: "Select_salon",
                                                    ]);
        case 8:
            self.trackViewcontroller(name: "Select_services", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_services",
                                                 withValues: [
                                                    AFEventAdView: "Select_services",
                                                    ]);
        case 9:
            self.trackViewcontroller(name: "Select_others", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_others",
                                                 withValues: [
                                                    AFEventAdView: "Select_others",
                                                    ]);
        default:
            self.trackViewcontroller(name: "Select_others", screenClass: "CategoryViewController")
            AppsFlyerTracker.shared().trackEvent("Select_others",
                                                 withValues: [
                                                    AFEventAdView: "Select_others",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailVc") as! DetailCategoryVC
        if self.categoryListDataModel.categoryList[indexPath.row].category_id != nil {
        vc.catId = self.categoryListDataModel.categoryList[indexPath.row].category_id
            vc.navigationTitle = self.categoryListDataModel.categoryList[indexPath.row].category_name
        }
        else {
            vc.catId = ""

        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used for back button action.
     @param ~~.
     @return ~~.
     */
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
