//
//  DetailCategoryVC.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 08/02/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import MBProgressHUD
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

class DetailCategoryVC: FollowAdsBaseViewController, DetailCatDelegate {
   

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var detailCatgryDatasource: DetailCategoryTableDatasource?
    var detailCatListDataModel = FollowAdsDetailCatListDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    var catId: String?
    var navigationTitle: String?
    var banner = Banner()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        // Do any additional setup after loading the view.
        self.setupTableDatasource()
       
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        noDataLabel.text = "No Data Available".localized()
        if Reachability.isConnectedToNetwork() == true {
            self.detailCategoryList()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.navigationItem.title = navigationTitle
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
    //    }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to add the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    func navigateToView(indexPath: IndexPath) {
        if self.self.detailCatListDataModel.category_advertisement_list[indexPath.row].is_active != nil {
            if self.detailCatListDataModel.category_advertisement_list[indexPath.row].is_active == "0" {
//                self.banner = Banner(title: "This Offer has expired.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                self.banner.dismissesOnTap = true
//                self.banner.show(duration: 3.0)
                self.view.makeToast("This Offer has expired.".localized(), duration: 3.0, position: .center)

            }
            else {
                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                if self.detailCatListDataModel.category_advertisement_list[indexPath.row].advertisement_id != nil {
                    vc.offerId = self.detailCatListDataModel.category_advertisement_list[indexPath.row].advertisement_id ?? ""
                }
                else {
                    vc.offerId = ""
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
      
    }
    
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func detailCategoryList() {
        self.followAdsRequestManager.detailCatListRequestManager = FollowAdsDetailCatListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.detailCatListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.detailCatListRequestManager?.lang_id = ""
        }
        
        self.followAdsRequestManager.detailCatListRequestManager?.category_id = catId
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        
        self.followAdsRequestManager.detailCatListRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        
        self.followAdsRequestManager.detailCatListRequestManager?.user_lng = userLng
        
        if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
            let postalCodeName = String()
            let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
            self.followAdsRequestManager.detailCatListRequestManager?.postal_code = postalCode
        }
        else {
            self.followAdsRequestManager.detailCatListRequestManager?.postal_code = ""
            
        }

        let detailCatListCompletion: DetailCatListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        
                        self.detailCatListDataModel = response!
                        print(self.detailCatListDataModel)
                        if self.detailCatListDataModel.category_advertisement_list.isEmpty == true {
                            self.tableView.isHidden = true
                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.tableView.isHidden = false
                            self.noDataLabel.isHidden = true
                            
                        }
                        self.detailCatgryDatasource?.detailCatListDataModel = self.detailCatListDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)

                    })
                }
            }
        }
        FollowAdsServiceHandler.callDetailCatListServiceCall(requestObject: self.followAdsRequestManager, detailCatListCompletion)
    }
    
    /*
     This method is used to setup TableView Datasource.
     @param ~~.
     @return ~~.
     */
    func setupTableDatasource() {
        detailCatgryDatasource = DetailCategoryTableDatasource(detailCategoryListModel: detailCatListDataModel)
        self.tableView.delegate = detailCatgryDatasource
        self.tableView.dataSource = detailCatgryDatasource
        detailCatgryDatasource?.tableView = self.tableView
        detailCatgryDatasource?.delegate = self
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
