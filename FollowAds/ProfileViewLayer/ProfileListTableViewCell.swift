//
//  ProfileListTableViewCell.swift
//  FollowAds
//
//  Created by openwave on 21/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class ProfileListTableViewCell: UITableViewCell {
    @IBOutlet weak var ProfileListImage: UIImageView!
    @IBOutlet weak var ProfileListLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
