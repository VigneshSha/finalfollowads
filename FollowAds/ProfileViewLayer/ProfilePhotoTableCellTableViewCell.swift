//
//  ProfilePhotoTableCellTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 17/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class ProfilePhotoTableCellTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet var profileNameTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var activityView: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityView.isHidden = true
        self.profileNameTextField.isUserInteractionEnabled = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK:- TextField Delegates
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
}
