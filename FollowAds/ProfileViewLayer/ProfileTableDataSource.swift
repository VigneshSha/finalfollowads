//
//  ProfileTableDataSource.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import Foundation

protocol ProfileNavigationsDelegate {
    func ReminderPassing(indexPath: IndexPath)
}

class ProfileTableDataSource: NSObject,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var tableView: UITableView!
    var profileListData : [String: [String]]?
    var text = String()
    var delegate: ProfileNavigationsDelegate!
    var header: UITableViewHeaderFooterView? = nil
    var labelHeader: UILabel!
    var selection: Bool?
    var nameText = String()
    var versionString = "Version "
    
    /*
     This method is used for init tableview datasource.
     @param ~~.
     @return ~~.
     */
    init(profileListData: [String: [String]]?, selectionData: Bool?, nameData: String) {
        self.profileListData = profileListData
        self.selection = selectionData
        self.nameText = nameData
        super.init()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.selection != nil {
            if self.selection == false {
            }
            else {
                textField.resignFirstResponder()
            }
        }
        else {
        }
    }
    
    /*
     This method is used to create a label
     @param - UILabel.
     @return - UILabel.
     */
    func creatingLabel() -> UILabel {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: myLabel.center.x, y: myLabel.center.y, width: tableView.frame.width, height: 35.0)
        myLabel.font = UIFont.boldSystemFont(ofSize: 14)
        myLabel.textColor = UIColor(red: 124/255, green: 126/255, blue: 125/255, alpha: 1.0)
        myLabel.textAlignment = .center
        if let dict = Bundle.main.infoDictionary {
            if let version = dict["CFBundleShortVersionString"] as? String,
                let bundleVersion = dict["CFBundleVersion"] as? String,
                let appName = dict["CFBundleName"] as? String {
                print("You're using \(appName) v\(version) (Build \(bundleVersion)).")
                myLabel.text = "FollowAds " + "\(version)" + " Build " + "\(bundleVersion)"
            //    myLabel.text = "FollowAds " + "1.0.4" + " Build" + " 1"

                let versionBuildNum : String = "\(version)" + "." + "\(bundleVersion)"
                UserDefaults.standard.set(versionBuildNum, forKey: "AppVersion")
            }
        }
        return myLabel
    }
    
    // MARK: - TABLEVIEW DELEGATE & DATASOURCE
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return (profileListData![SettingsList1]?.count)!
        } else if section == 2 {
            return (profileListData![SettingsList2]?.count)!
        } else if section == 3 {
            return (profileListData![SettingsList3]?.count)!
        } else if section == 4 {
            return (profileListData![SettingsList4]?.count)!
        } else {
            return 1
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        } else if indexPath.section == 5 {
            return 0.0
        } else {
            return 44
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let profilePhotoCell: ProfilePhotoTableCellTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfilePhotoCellId) as? ProfilePhotoTableCellTableViewCell
            profilePhotoCell.profileNameTextField.delegate = self
            profilePhotoCell.profileImageView.layer.cornerRadius =  profilePhotoCell.profileImageView.frame.size.width / 2
            profilePhotoCell.profileImageView.clipsToBounds = true
            profilePhotoCell.backgroundColor = profilePhotoCellBgColor
            if UserDefaults.standard.value(forKey: UserName) != nil {
                let usernameText = String()
                let username: String = usernameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
                profilePhotoCell.profileNameTextField.text = username
            }
            else {
                profilePhotoCell.profileNameTextField.text = emptyString
            }
            if UserDefaults.standard.value(forKey: ProfImg) != nil {
                let urlstring = UserDefaults.standard.value(forKey: ProfImg) as? String
                
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        
                        profilePhotoCell.profileImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "Man_Avatar"))
                    }
                }
                else {
                    profilePhotoCell.profileImageView.image = UIImage(named: PlaceholderImg)
                }
            }
            return profilePhotoCell
        } else if indexPath.section == 1 {
            
            let ProfileDataCell: ProfileDefaultTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId) as? ProfileDefaultTableViewCell

            
//            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            ProfileDataCell.textLabel?.text = profileListData?[SettingsList1]![indexPath.row]
//            ProfileDataCell.textLabel?.font = UIFont(name: "Helvetica", size: 17)
//            ProfileDataCell.detailTextLabel?.font = UIFont(name: "Helvetica", size: 17)
            
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)

            
            ProfileDataCell.detailTextLabel?.text =  profileListData?[SettingsDisclosure1]![indexPath.row]
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 10, y: 5, width: 34, height: 34)
            let redImage200x200 = UIImage(named: imageArrayFirstSection[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = imageViewColorsSetFour[indexPath.row]
            if indexPath.row == 2 {
                ProfileDataCell.detailTextLabel?.textColor = UIColor.lightGray
//                ProfileDataCell.accessoryType = .none
//                let label = UILabel.init(frame: CGRect(x:ProfileDataCell.frame.size.width - 30,y:0,width:20,height:20))
//                ProfileDataCell.accessoryView = label
//                let cellAudioButton = UIButton(type: .custom)
//                cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
////                cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
//                //            cellAudioButton.setImage(UIImage(named: "blueSpeaker.png"), for: .normal)
//                cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
//
////                cellAudioButton.backgroundColor = UIColor.orange
//                cellAudioButton.contentMode = .scaleAspectFit
//                cellAudioButton.tag = indexPath.row
                ProfileDataCell.accessoryView = UIView()

            }
                
            else {
                ProfileDataCell.detailTextLabel?.textColor = UIColor(red: 93.0/255.0, green: 93.0/255.0, blue: 93.0/255.0, alpha: 1.0)
                
//                ProfileDataCell.accessoryType = .disclosureIndicator
                
                let cellAudioButton = UIButton(type: .custom)
                cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//                cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
                cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
//                cellAudioButton.backgroundColor = UIColor.red
                cellAudioButton.contentMode = .scaleAspectFit
                cellAudioButton.tag = indexPath.row
                ProfileDataCell.accessoryView = cellAudioButton as UIView

                
            }
            return ProfileDataCell
            
        } else if indexPath.section == 2 {
//            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            let ProfileDataCell: ProfileDefaultTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId) as? ProfileDefaultTableViewCell

            ProfileDataCell.textLabel?.text = profileListData?[SettingsList2]![indexPath.row]
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.text =  profileListData?[SettingsDisclosure2]![indexPath.row]
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 10, y: 5, width: 34, height: 34)
            let redImage200x200 = UIImage(named: imageArraySecondSection[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = imageViewColorsSetThree[indexPath.row]
          
            let cellAudioButton = UIButton(type: .custom)
            cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//            cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
            cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
//            cellAudioButton.backgroundColor = UIColor.red
            cellAudioButton.contentMode = .scaleAspectFit
            cellAudioButton.tag = indexPath.row
            ProfileDataCell.accessoryView = cellAudioButton as UIView

            
            return ProfileDataCell
        } else if indexPath.section == 3 {
//            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            let ProfileDataCell: ProfileDefaultTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId) as? ProfileDefaultTableViewCell

            ProfileDataCell.textLabel?.text = profileListData?[SettingsList3]![indexPath.row]
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.text =  profileListData?[SettingsDisclosure3]![indexPath.row]
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 15, y: 12.5, width: 25, height: 25)
            let redImage200x200 = UIImage(named: imageArrayThirdSection[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = imageViewColors[indexPath.row]
            if indexPath.row == 2 {
//                ProfileDataCell.accessoryType = .none
//                let label = UILabel.init(frame: CGRect(x:ProfileDataCell.frame.size.width - 20,y:0,width:10,height:20))
//                ProfileDataCell.accessoryView = label
//                let cellAudioButton = UIButton(type: .custom)
//                cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
////                cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
//                cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
////                cellAudioButton.backgroundColor = UIColor.orange
//                cellAudioButton.contentMode = .scaleAspectFit
//                cellAudioButton.tag = indexPath.row
                ProfileDataCell.accessoryView = UIView()

            }
            else {
                ProfileDataCell.detailTextLabel?.textColor = UIColor(red: 93.0/255.0, green: 93.0/255.0, blue: 93.0/255.0, alpha: 1.0)
                
                let cellAudioButton = UIButton(type: .custom)
                cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//                cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
                cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
//                cellAudioButton.backgroundColor = UIColor.red
//                cellAudioButton.contentMode = .scaleAspectFit
                cellAudioButton.tag = indexPath.row
                ProfileDataCell.accessoryView = cellAudioButton as UIView

//                ProfileDataCell.accessoryType = .disclosureIndicator
            }
            
            

            return ProfileDataCell
        } else {
//            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            let ProfileDataCell: ProfileDefaultTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId) as? ProfileDefaultTableViewCell

            ProfileDataCell.textLabel?.text = profileListData?[SettingsList4]![indexPath.row]
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.text =  profileListData?[SettingsDisclosure4]![indexPath.row]
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 15, y: 12.5, width: 25, height: 25)
            let redImage200x200 = UIImage(named: imageArrayFourthSection[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = imageViewColorsSetTwo[indexPath.row]
//            ProfileDataCell.accessoryType = .disclosureIndicator
            let cellAudioButton = UIButton(type: .custom)
            cellAudioButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//            cellAudioButton.addTarget(self, action: Selector(("accessoryButtonTapped:")), for: .touchUpInside)
            cellAudioButton.setImage(UIImage(named: "Right_Arrow"), for: .normal)
//            cellAudioButton.backgroundColor = UIColor.red
            cellAudioButton.contentMode = .scaleAspectFit
            cellAudioButton.tag = indexPath.row
            ProfileDataCell.accessoryView = cellAudioButton as UIView

            return ProfileDataCell
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 || section == 3 || section == 4 {
            let headerView = UIView()
            let headerCell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderCellId) as! ProfileHeaderTableViewCell
            headerView.addSubview(headerCell)
            headerView.backgroundColor = profilePhotoCellBgColor
            return headerView
        } else if section == 5 {
            let headerView = UIView()
            headerView.addSubview(creatingLabel())
            return headerView
        } else {
            let headerView = UIView()
            return headerView
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        _ = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderCellId) as! ProfileHeaderTableViewCell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 || section == 3 || section == 4 || section == 5 {
            return 35.0
        } else {
            return 0.0
        }
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil{
            delegate?.ReminderPassing(indexPath: indexPath)
        }
    }
}
