//
//  ProfileViewController.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import MBProgressHUD
import FirebaseAnalytics
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

class ProfileViewController: FollowAdsBaseViewController, ProfileNavigationsDelegate, NotificationPassing, LikedOffersPassing, FollowedStores, UsedCoupons, MFMailComposeViewControllerDelegate,customNavigationViewUpdate, updateEmail, customProfileViewUpdate {
    
    var profileTableDataSource: ProfileTableDataSource!
    @IBOutlet var editBtn: UIButton!
    var PhoneNumber = String()
    var Name = String()
    var EmailStr = String()
    var Notification = String()
    var Reminder  = String()
    var LikedOffers = String()
    var UsedCoupons = String()
    var FollowedStores = String()
    var SavedCoupons = String()
    var isSelected: Bool?
    @IBOutlet weak var profileTable: UITableView!
    var langStr = String()
    var SelectLanguageTitle = "Select Language"
    let settingsList1Array = ["Reminders".localized(), "Location".localized(), "Phone Number".localized(), "Email ".localized()]
    let settingsList2Array = ["Language".localized(), "Redeem Money".localized()]
    let settingsList3Array = ["Liked Offers".localized(),"Followed Stores".localized(),"Scanned Coupons".localized(),"Used Offers".localized()]
    let settingsList4Array = ["Share the App".localized(),"Rate the App".localized(),"Send FeedBack".localized(),"About FollowAds".localized()]
    var likedOffCount = String()
    var followedStoreCount = String()
    var locationStr = String()
    var userCouponCountStr = String()
    var likedOffersString = String()
    var followedStoresString = String()
    var scannedCouponString = String()
    var emailString = String()
    var langString = String()
    var mobileString = String()
    var navigationTitle = "Me".localized()
    var notificationSwitchStatus = String()
    var reminderCountStr = String()
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var mailCheckAlert = "Mail services are not available"
    var bankListDataModel: FollowAdsBankListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    private var loginWindow = LoginView()
    var banner = Banner()
    var isAlerdayNav : Bool = false

    @IBOutlet weak var loginVw: UIView!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var logoBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginWindow = loadNiB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isAlerdayNav = false

        super.viewWillAppear(animated)
        self.viewWillAppearUpdate()
        print("********localize check*******\("See All".localized())")
        print("********localize check*******\("Language".localized())")
        self.trackViewcontroller(name: "View_Profile", screenClass: "ProfileViewController")
        AppsFlyerTracker.shared().trackEvent("View_Profile",
                                             withValues: [
                                                AFEventAdView: "View_Profile",
                                                ]);
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isAlerdayNav = false

    }
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    /*
     This method is used to update the viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {

        if UserDefaults.standard.value(forKey: "User_Id") != nil {
            loginWindow.removeFromSuperview()
            loginWindow.isHidden = true
            
//            if let tabItems = self.tabBarController?.tabBar.items {
//                if UserDefaults.standard.value(forKey: UserId) != nil {
//
//                        if UserDefaults.standard.value(forKey: "Notification_Count") != nil {
//                            let notificationCountText = String()
//                            let notificationCount: String = notificationCountText.passedString((UserDefaults.standard.object(forKey: "Notification_Count") as? String))
//                            let notifyCountInt: Int = Int(notificationCount) ?? 0
//                            if notificationCount != "" {
//                                if notificationCount == "0" {
//                                } else {
//                                    if notifyCountInt <= 0 {
//                                    }
//                                    else {
//                                        let tabItem = tabItems[1]
//                                        tabItem.badgeValue = notificationCount
//                                    }
//                                }
//                            }
//                            else {
//                                if notifyCountInt <= 0 {
//                                }
//                                else {
//                                    let tabItem = tabItems[1]
//                                    tabItem.badgeValue = notificationCount
//                                }
//                            }
//                        }
//                }
//            }
        }
        else {
            if let signInView = Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)?.first as? LoginView {
                self.view.addSubview(signInView)
                loginWindow = signInView
                signInView.isHidden = false
                signInView.frame = self.view.bounds
                signInView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                
              //  signInView.logoBtn.addTarget(self, action: #selector(logoBtnPressed), for: UIControlEvents.touchUpInside)
                
                signInView.signInBtn.addTarget(self, action: #selector(signInPressed), for: UIControlEvents.touchUpInside)
                
                signInView.signInBtn.setTitle("Sign In".localized(using: buttonTitles), for: .normal)
                signInView.commentsLabel.text = "To access all features of the app".localized()
                
            }
            
        }
        
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.navigationItem.title = navigationTitle
        if UserDefaults.standard.value(forKey: "LIKED_OFFERS_COUNT") != nil {
            let likedOffersCountText = String()
            let likedOffersCount: String = likedOffersCountText.passedString((UserDefaults.standard.object(forKey: "LIKED_OFFERS_COUNT") as? String))
            likedOffCount = likedOffersCount
        }
        if UserDefaults.standard.value(forKey: "FOLLOWED_STORES_COUNT") != nil {
            let followedStoresCountText = String()
            let followedStoresCount: String = followedStoresCountText.passedString((UserDefaults.standard.object(forKey: "FOLLOWED_STORES_COUNT") as? String))
            followedStoreCount = followedStoresCount
        }

        self.tabBarController?.tabBar.isHidden = false
 /*       if UserDefaults.standard.value(forKey: UserId) != nil {
//                loginWindow.isHidden = true
//                loginWindow.removeFromSuperview()
            loginVw.isHidden = true
            
        }
        else {
            loginVw.isHidden = false

       /*
            if let signInView = Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)?.first as? LoginView {
            self.view.addSubview(signInView)
            loginWindow = signInView
            signInView.isHidden = false
            signInView.frame = self.view.bounds
            signInView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            signInView.logoBtn.addTarget(self, action: #selector(logoBtnPressed), for: UIControlEvents.touchUpInside)
            signInView.signInBtn.addTarget(self, action: #selector(signInPressed), for: UIControlEvents.touchUpInside)
            } */
        } */
        UpdateProfileDetails()
        print(PhoneNumber)
        print(EmailStr)
        self.setupTableViewDataSource()
        tabBarController?.tabBar.isHidden = false
        self.profileTable.reloadData()
        
        // Navigates to offerDetailView by tapping on push notification
        if isFromNotifi == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            vc.offerId = off_Notifi_Id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)

    }
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> LoginView{
        let loginWindow = LoginView.instanceFromNib() as! LoginView
        return loginWindow
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
                
                if let pushId = aps["pushnotification_id"] as? Int {
                    vc.pushnotificationId = String(pushId)
                    vc.notifiId = "1"
                }
            }
            else {
                vc.offerId = "0"
            }
        }
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)


        isFromNotifi = false

    }
    
    /*
     This method is used to update the delegate method.
     @param --.
     @return --.
     */
    func updateUI() {
        self.isSelected = false
        self.editBtn.setTitle(saveName, for: .normal)
    }
    
    /*
     This method is used to update the delegate method.
     @param --.
     @return --.
     */
    func updateInfo(string: String,alertString: String) {
        Name = string
        print(Name)
        UserDefaults.standard.set(Name, forKey: UserName)
     
        self.view.makeToast(alertString, duration: 3.0, position: .center)
     //   Toast.show(message: alertString, controller: self)
        //UserDefaults.standard.synchronize()
    }
    
    /*
     This method is used to update the phone number.
     @param --.
     @return --.
     */
    func PhoneNumberData(string: String){
        PhoneNumber = string
        print(PhoneNumber)
    }
    
    /*
     This method is used to update the Email.
     @param --.
     @return --.
     */
    func EmailData(string: String) {
        EmailStr = string
        print(EmailStr)
    }
    
    /*
     This method is used to update the used coupons count.
     @param --.
     @return --.
     */
    func UsedCoupons(string: String) {
        UsedCoupons = string
        print(UsedCoupons)
    }
    
    /*
     This method is used to update the followed stores count.
     @param --.
     @return --.
     */
    func FollowesStores(string: String) {
        FollowedStores = string
        print(FollowedStores)
    }
    
    /*
     This method is used to update the saved coupons count.
     @param --.
     @return --.
     */
    func SavedCoupons(string: String) {
        SavedCoupons = string
        print(SavedCoupons)
    }
    
    /*
     This method is used to update the liked offers count.
     @param --.
     @return --.
     */
    func LikedOffers(string: String) {
        LikedOffers = string
        print(LikedOffers)
    }
    
    /*
     This method is used to update the notification status.
     @param --.
     @return --.
     */
    func NotificationPassing(string: String) {
        Notification = string
        print(Notification)
    }
    
    /*
     This method is used to update the reminders count.
     @param --.
     @return --.
     */
    func ReminderPassing(string: String) {
        Reminder = string
        print(Reminder)
    }
    
    lazy var info : [String: [String]] = {
        var dictionary = [String: [String]]()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            if UserDefaults.standard.value(forKey: "Welcome_Area") != nil {
                let locationText = String()
                let location: String = locationText.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
                locationStr = location
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "USER_COUPON_COUNT") != nil {
                let userCouponCountText = String()
                let userCouponCount: String = userCouponCountText.passedString((UserDefaults.standard.object(forKey: "USER_COUPON_COUNT") as? String))
                userCouponCountStr = userCouponCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "LIKED_OFFERS_COUNT") != nil {
                let likedOffersCountText = String()
                let likedOffersCount: String = likedOffersCountText.passedString((UserDefaults.standard.object(forKey: "LIKED_OFFERS_COUNT") as? String))
                likedOffersString = likedOffersCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "FOLLOWED_STORES_COUNT") != nil {
                let followedStoresCountText = String()
                let followedStoresCount: String = followedStoresCountText.passedString((UserDefaults.standard.object(forKey: "FOLLOWED_STORES_COUNT") as? String))
                followedStoresString = followedStoresCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "Scanned_Coupon_Count") != nil {
                let scannedCouponCountText = String()
                let scannedCouponCount: String = scannedCouponCountText.passedString((UserDefaults.standard.object(forKey: "Scanned_Coupon_Count") as? String))
                scannedCouponString = scannedCouponCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
                let phoneNumberText = String()
                let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
                mobileString = phoneNumberCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "EmailId") != nil {
                let emailText = String()
                let emailCount: String = emailText.passedString((UserDefaults.standard.object(forKey: "EmailId") as? String))
                emailString = emailCount
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "LangName") != nil {
                let langNameText = String()
                let langName: String = langNameText.passedString((UserDefaults.standard.object(forKey: "LangName") as? String))
                langString = langName
            }
            else {
            }
            if UserDefaults.standard.value(forKey: "UserReminderCount") != nil {
                let reminderCountText = String()
                let reminderCount: String = reminderCountText.passedString((UserDefaults.standard.object(forKey: "UserReminderCount") as? String))
                reminderCountStr = reminderCount
            }
            else {
                reminderCountStr = ""
            }
            if UserDefaults.standard.value(forKey: "NotificationStatus") != nil {
                let notificationStatusText = String()
                let notificationStatus: String = notificationStatusText.passedString((UserDefaults.standard.object(forKey: "NotificationStatus") as? String))
                notificationSwitchStatus = notificationStatus
            }
            else {
                notificationSwitchStatus = ""
            }
            dictionary[SettingsList1] = settingsList1Array
            dictionary[SettingsList2] = settingsList2Array
            dictionary[SettingsList3] = settingsList3Array
            dictionary[SettingsList4] = settingsList4Array
            dictionary[SettingsDisclosure1] = [reminderCountStr, locationStr, mobileString, emailString]
            dictionary[SettingsDisclosure2] = [langString, emptyString]
            dictionary[SettingsDisclosure3] = [likedOffersString, followedStoresString, scannedCouponString, userCouponCountStr]
            dictionary[SettingsDisclosure4] = [emptyString, emptyString, emptyString, emptyString, emptyString]
        }
        else {
            dictionary[SettingsList1] = settingsList1Array
            dictionary[SettingsList2] = settingsList2Array
            dictionary[SettingsList3] = settingsList3Array
            dictionary[SettingsList4] = settingsList4Array
            
            if UserDefaults.standard.value(forKey: "Welcome_Area") != nil {
                let locationText = String()
                let location: String = locationText.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
                locationStr = location
                dictionary[SettingsDisclosure1] = ["", locationStr, "Phone Number", ""]

            }
            else {
                dictionary[SettingsDisclosure1] = ["", "Chennai", "Phone Number", ""]

            }
            
            if UserDefaults.standard.value(forKey: "LangName") != nil {
                let langNameText = String()
                let langName: String = langNameText.passedString((UserDefaults.standard.object(forKey: "LangName") as? String))
                langString = langName
                dictionary[SettingsDisclosure2] = [langString, emptyString]
             }
            else {
                dictionary[SettingsDisclosure2] = ["English", emptyString]
             }
            
            dictionary[SettingsDisclosure3] = ["", "", "", ""]
            dictionary[SettingsDisclosure4] = [emptyString, emptyString, emptyString, emptyString, emptyString]
        }
        return dictionary
    }()
    
    /*
     This method is used to update the profile details.
     @param --.
     @return --.
     */
    func UpdateProfileDetails() {
        var list1 = info[SettingsDisclosure1] ?? []
        var list3 = info[SettingsDisclosure3] ?? []
        if UserDefaults.standard.value(forKey: "UserReminderCount") != nil {
            let savedRemindersCountText = String()
            let savedRemindersCount: String = savedRemindersCountText.passedString((UserDefaults.standard.object(forKey: "UserReminderCount") as? String))
            list1.remove(at: 0)
            list1.insert(("\(savedRemindersCount)"), at: 0)
        }
      /*  if UserDefaults.standard.value(forKey: "NotificationStatus") != nil {
            let notificationStatusText = String()
            let notificationStatus: String = notificationStatusText.passedString((UserDefaults.standard.object(forKey: "NotificationStatus") as? String))
            list1.remove(at: 1)
            list1.insert(("\(notificationStatus)"), at: 1)
        } */
        if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
            let phoneNumberText = String()
            let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
            list1.remove(at: 2)
            list1.insert(("\(phoneNumberCount)"), at: 2)
        }
        
        if UserDefaults.standard.value(forKey: "EmailId") != nil {
            let emailText = String()
            let email: String = emailText.passedString((UserDefaults.standard.object(forKey: "EmailId") as? String))
            list1.remove(at: 3)
            list1.insert(("\(email)"), at: 3)
        }
        if UserDefaults.standard.value(forKey: "LIKED_OFFERS_COUNT") != nil {
            let likedOffersCountText = String()
            let likedOffersCount: String = likedOffersCountText.passedString((UserDefaults.standard.object(forKey: "LIKED_OFFERS_COUNT") as? String))
            list3.remove(at: 0)
            list3.insert(("\(likedOffersCount)"), at: 0)
        }
        if UserDefaults.standard.value(forKey: "FOLLOWED_STORES_COUNT") != nil {
            let followedStoresCountText = String()
            let followedStoresCount: String = followedStoresCountText.passedString((UserDefaults.standard.object(forKey: "FOLLOWED_STORES_COUNT") as? String))
            list3.remove(at: 1)
            list3.insert(("\(followedStoresCount)"), at: 1)
        }
        if UserDefaults.standard.value(forKey: "Scanned_Coupon_Count") != nil {
            let scannedCouponCountText = String()
            let scannedCouponCount: String = scannedCouponCountText.passedString((UserDefaults.standard.object(forKey: "Scanned_Coupon_Count") as? String))
            list3.remove(at: 2)
            list3.insert(("\(scannedCouponCount)"), at: 2)
            
        }
        if UserDefaults.standard.value(forKey: "USER_COUPON_COUNT") != nil {
            let usedOffersCountText = String()
            let usedOffersCount: String = usedOffersCountText.passedString((UserDefaults.standard.object(forKey: "USER_COUPON_COUNT") as? String))
            
            list3.remove(at: 3)
            list3.insert(("\(usedOffersCount)"), at: 3)
        }
        print(list1)
        print(list3)
        info[SettingsDisclosure1] = list1
        info[SettingsDisclosure3] = list3
    }
    
    /*
     This method is used to perform bank list service call.
     @param -.
     @return -.
     */
    func bankListUpdate() {
        self.followAdsRequestManager.bankDetailsListRequestManager = FollowAdsBankDetailsListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.bankDetailsListRequestManager?.lang_id = langId
        
        let bankListCompletion: BankListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            else
            {
                if response != nil {
                    self.bankListDataModel = response!
                    if response?.bank_detail.isEmpty == false {
                        
                        if (self.bankListDataModel?.bank_detail.count)! > 0 {
                            self.performSegue(withIdentifier: "BankDetailsView", sender: self)
                        }
                        else {
                            self.performSegue(withIdentifier: "AddBankDetailsView", sender: self)
                        }
                    }
                    else {
                        self.performSegue(withIdentifier: "AddBankDetailsView", sender: self)
                    }
                }
            }
        }
        FollowAdsServiceHandler.callBankListServiceCall(requestObject: self.followAdsRequestManager, bankListCompletion)
    }
    
    /*
     This method is used to pass the data to the destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if #available(iOS 10.0, *) {
            if let destination = segue.destination as? NotificationViewController {
                destination.delegate = self
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    /*
     This method is used for Setup tableview data source.
     @param --.
     @return --.
     */
    func setupTableViewDataSource() {
        profileTableDataSource = ProfileTableDataSource(profileListData: info, selectionData: isSelected, nameData: Name)
        self.profileTable.dataSource = profileTableDataSource
        self.profileTable.delegate = profileTableDataSource
        profileTableDataSource?.tableView = self.profileTable
        self.profileTable.tableFooterView =  UIView()
        profileTableDataSource.delegate = self
    }
    
    /*
     This method is used to perform didSelect delegate method action.
     @param --.
     @return --.
     */
    func ReminderPassing(indexPath: IndexPath){
        if indexPath.section == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: EditVCStoryBoardId) as! EditViewController
            self.navigationController?.pushViewController(vc, animated: true)
            vc.delegate = self
            vc.emailStr = self.EmailStr
            vc.nameStr = self.Name
        }
        else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: RemainderViewId) as! ReminderViewController
                self.navigationController?.pushViewController(vc, animated: true)
                vc.navigationItem.title = ReminderTitle
            }
//            else if indexPath.row == 1 {
//                self.performSegue(withIdentifier: NotificationsTitle, sender: self)
//            }
            else if indexPath.row == 1 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 2 {
                
            }
            else  if indexPath.row == 3 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterEmailViewId") as! EnterEmailViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: LanguageSelectionViewId) as! LanguageSelectionViewController
                self.navigationController?.pushViewController(vc, animated: true)
                vc.navigationItem.title = SelectLanguageTitle.localized()
                vc.tabBarController?.tabBar.isHidden = true
            } else if indexPath.row == 1 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RedeemMoneyViewId") as! RedeemMoneyViewController
                self.navigationController?.pushViewController(vc, animated: true)
          //      vc.navigationItem.title = SelectLanguageTitle.localized()
                
//                if Reachability.isConnectedToNetwork() == true {
//                    self.bankListUpdate()
//                }
//                else {
//                    MBProgressHUD.hide(for: self.view, animated: true)
//                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
//                }
            }
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: "LikedOffers", sender: self)
            } else if indexPath.row == 1 {
                self.performSegue(withIdentifier: "FollowedStores", sender: self)
            } else if indexPath.row == 2 {
              //  self.performSegue(withIdentifier: "SavedCoupons", sender: self)
            } else if indexPath.row == 3 {
                self.performSegue(withIdentifier: "UsedCouponsId", sender: self)
            }
        } else {
            if indexPath.row == 0 {
                Analytics.logEvent("Share_FA_app", parameters: [
                    "name": "Share FollowAds" as NSObject
                    ])
                
          //      let shareUrl = "FollowAds helps users discover deals & offers around Chennai.".localized() + "\n\n" + "Download Now in your Android or iOS by clicking the link below!".localized() + "\n\n" + "http://followads.in/dl"
//                let shareUrl = "FollowAds helps users discover deals & offers around Chennai.".localized() + "\n\n" + "Download Now in your Android or iOS by clicking the link below!\n\nhttp://followads.in/dl".localized()
//                let textToShare = shareUrl
//                let objectsToShare = [textToShare]
//                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                let activityVC = UIActivityViewController(activityItems: [MyStringItemSource()], applicationActivities: nil)

                //New Excluded Activities Code
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                
                
                
             
                self.present(activityVC, animated: true, completion: nil)
                
                
//                activityVC.completionWithItemsHandler = { activity, success, items, error in
//                    if !success{
//                        print("cancelled")
//                        return
//                    }
//
//                    if activity == .postToTwitter {
//                        print("twitter")
//                    }
//
//                    if activity == .mail {
//                        print("mail")
//                        activityVC.setValue("FollowAds", forKey: "Subject")
//
//                    }
//                }
//
            } else if indexPath.row == 1 {
                Analytics.logEvent("Rate_FA_app", parameters: [
                    "name": "Rate FollowAds" as NSObject
                    ])
                AppsFlyerTracker.shared().trackEvent("Rate_FA_app",
                                                     withValues: [
                                                        AFEventAdClick: "Rate FollowAds",
                                                        ]);
                UIApplication.shared.openURL(NSURL(string: appShareUrl)! as URL)
            } else if indexPath.row == 2 {
                Analytics.logEvent("Feedback_FA_app", parameters: [
                    "name": "Feedback FollowAds" as NSObject
                    ])
                AppsFlyerTracker.shared().trackEvent("Feedback_FA_app",
                                                     withValues: [
                                                        AFEventAdClick: "Feedback FollowAds",
                                                        ]);

                if !MFMailComposeViewController.canSendMail() {
                    let email = ReceipentEmailId
                    if let url = URL(string: "mailto:\(email)") {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    return
                } else {
                    MailComposing()
                }
            } else if indexPath.row == 3 {
                self.performSegue(withIdentifier: AboutFollowAdsId, sender: true)
            } 
        }
    }
   
    
    /*
     This method is used to get the mobile Os version.
     @param --.
     @return --.
     */
    func getOSInfo()->String {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    /*
     This method is used for mail composing.
     @param --.
     @return --.
     */
    func MailComposing() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([ReceipentEmailId])
        composeVC.setSubject(SubjectForEmail)
        let userNameText = String()
        let userName: String = userNameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
        let mobileText = String()
        let mobile: String = mobileText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
        let appVersionText = String()
        let appVersion: String = appVersionText.passedString((UserDefaults.standard.object(forKey: "AppVersion") as? String))
        let LocationText = String()
        let Location: String = LocationText.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
        composeVC.setMessageBody("Username: \(userName)\nPhone: \(mobile)\nDevice name: \(kGetDeviceName)\nMobile OS: \(getOSInfo())\nApp version: \(appVersion)\nMobile Version: \(UIDevice.current.name)\nLocation: \(Location)", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    /*
     This method is used for mailcomposer delegate method.
     @param --.
     @return --.
     */
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    } 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used for logo button action.
     @param --.
     @return --.
     */
    @objc func logoBtnPressed() {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    /*
     This method is used for sign in button action.
     @param --.
     @return --.
     */
    @objc func signInPressed() {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
//    @IBAction func logoBtnPressed(_ sender: UIButton) {
//        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
   
    @IBAction func signInBtnPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
}



class MyStringItemSource: NSObject, UIActivityItemSource {
    
    public func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType?) -> Any? {
//        if activityType == UIActivityType.message {
//            return "FollowAds"
//        } else if activityType == UIActivityType.mail {
//            return "FollowAds"
//        } else if activityType == UIActivityType.postToTwitter {
//            return "FollowAds"
//        } else if activityType == UIActivityType.postToFacebook {
//            return "FollowAds"
//        }
        return  "FollowAds helps users discover deals & offers around Chennai.".localized() + "\n\n" + "Download Now in your Android or iOS by clicking the link below!\n\nhttp://followads.in/dl".localized()

//        return nil
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
//        if activityType == UIActivityType.message {
//            return "Subject for message"
//        } else if activityType == UIActivityType.mail {
//            return "Subject for mail"
//        } else if activityType == UIActivityType.postToTwitter {
//            return "Subject for twitter"
//        } else if activityType == UIActivityType.postToFacebook {
//            return "Subject for facebook"
//        }
//        let textToShare = shareUrl
        return "FollowAds"
    }
    
//    public func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivityType?, suggestedSize size: CGSize) -> UIImage? {
//        if activityType == UIActivityType.message {
//            return UIImage(named: "thumbnail-for-message")
//        } else if activityType == UIActivityType.mail {
//            return UIImage(named: "thumbnail-for-mail")
//        } else if activityType == UIActivityType.postToTwitter {
//            return UIImage(named: "thumbnail-for-twitter")
//        } else if activityType == UIActivityType.postToFacebook {
//            return UIImage(named: "thumbnail-for-facebook")
//        }
//        return UIImage(named: "some-default-thumbnail")
//    }
    
}
