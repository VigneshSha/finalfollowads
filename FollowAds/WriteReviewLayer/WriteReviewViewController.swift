//
//  WriteReviewViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 18/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseAnalytics
import FirebaseDynamicLinks
import AppsFlyerLib

class WriteReviewViewController: FollowAdsBaseViewController, UITextViewDelegate, FloatRatingViewDelegate {
    
    @IBOutlet var tapToRateLabel: UILabel!
    @IBOutlet var floatingRatingView: FloatRatingView!
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var reviewTextView: UITextView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var sendBtn: UIButton!
    var followAdsRequestManager = FollowAdsRequestManager()
    var writeReviewDataModel = FollowAdsWriteAdvertisementReviewDataModel()
    var businessIdValue: String?
    var advertisementIdValue: String?
    var userRatingValue : Double?
    var userRatingStr = String()
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var titleAlert = "Title should not be empty"
    var navigationTitle = "Write a Review"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setText()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleTextField.becomeFirstResponder()
        titleTextField.placeholder = "Title".localized()
        tapToRateLabel.text = "Tap to Rate".localized()
        reviewTextView.delegate = self
        self.floatingRatingView.delegate = self
        reviewTextView.text = "Review (Optional)".localized()
        reviewTextView.textColor = UIColor.lightGray
        self.setUpNavigationBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    
    /*
     This method is used to set text for localization.
     @param -.
     @return -.
     */
    @objc func setText(){
        cancelBtn.setTitle(cancelText.localized(using: buttonTitles), for: UIControlState.normal)
        sendBtn.setTitle(sendText.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to show the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform write advertisement review service call.
     @param -.
     @return -.
     */
    func writeReviewUpdate() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.writeAdvertisementReviewRequestManager = FollowAdsWriteAdvertisementReviewRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.user_id = ""
            }
            self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.business_id = businessIdValue
            self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.advertisement_id = advertisementIdValue
            if reviewTextView.text == "Review (Optional)".localized() {
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.comments = ""
            }
            else {
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.comments = reviewTextView.text
            }
            
            if userRatingStr != nil {
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.rating = userRatingStr
            }
            else {
                self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.rating = "4"
            }
            self.followAdsRequestManager.writeAdvertisementReviewRequestManager?.title = titleTextField.text
            
            let writeReviewCompletion: WriteAdvertisementReviewCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
                }
                else
                {
                    if response != nil {
                        DispatchQueue.main.async(execute: {
                            self.writeReviewDataModel = response!
                            print(self.writeReviewDataModel)
                            if response?.response_code == "013" {
                                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "You have rated successfully".localized())
                            }
                            else if response?.response_code == "014" {
                                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Your rating is unsuccessful".localized())
                            }
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
            FollowAdsServiceHandler.callWriteAdvertisementReviewServiceCall(requestObject: self.followAdsRequestManager, writeReviewCompletion)
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: locationAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 16))]
        
        // Set constraints for the navigation bar button. Fix for iOS 11 issue
        let widthConstraintBack = cancelBtn.widthAnchor.constraint(equalToConstant: 60)
        let heightConstraintBack = cancelBtn.heightAnchor.constraint(equalToConstant: 25)
        heightConstraintBack.isActive = true
        widthConstraintBack.isActive = true
        
        // Set constraints for the navigation bar button. Fix for iOS 11 issue
        let widthConstraintSend = sendBtn.widthAnchor.constraint(equalToConstant: 70)
        let heightConstraintSend = sendBtn.heightAnchor.constraint(equalToConstant: 25)
        heightConstraintSend.isActive = true
        widthConstraintSend.isActive = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to update the rating values in float rating view.
     @param -.
     @return -.
     */
    @objc func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double)
    {
        self.userRatingValue = rating
        userRatingStr = String(format:"%f", userRatingValue!)
        UserDefaults.standard.set(userRatingStr, forKey: "RatingValue")
        //UserDefaults.standard.synchronize()
    }
    
    // TextField Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if reviewTextView.textColor == UIColor.lightGray {
            reviewTextView.text = nil
            reviewTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if reviewTextView.textColor == UIColor.lightGray {
              reviewTextView.text = nil
            reviewTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if reviewTextView.text.isEmpty {
            reviewTextView.text = "Review (Optional)".localized()
            reviewTextView.textColor = UIColor.lightGray
        }
    }
    
    /*
     This method is used for cancel button action.
     @param -.
     @return -.
     */
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used for send button action.
     @param -.
     @return -.
     */
    @IBAction func sendBtnAction(_ sender: UIButton) {
        if userRatingStr == "" {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please rate it before you proceed.".localized())
            
        }
        else if titleTextField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: titleAlert.localized())
        }
        else {
            self.addLoadingView()
            if Reachability.isConnectedToNetwork() == true {
                Analytics.logEvent("Review Offer", parameters: [
                    "name": "Write Review" as NSObject
                    ])
                AppsFlyerTracker.shared().trackEvent("Review Offer",
                                                     withValues: [
                                                        AFEventAdClick: "Write Review",
                                                        ]);
                self.writeReviewUpdate()
                self.navigationController?.popViewController(animated: true)
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
        }
    }
    
}
