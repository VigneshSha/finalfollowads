//
//  LanguageSelectionViewController.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 25/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import BRYXBanner
import AppsFlyerLib

class LanguageSelectionViewController: FollowAdsBaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    let availableLanguages = Localize.availableLanguages()
    @IBOutlet var labelLangSelection: UILabel!
    var langListDataModel = FollowAdsLangListDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    var changeLangDataModel = FollowAdsChangeLangDataModel()
    var cell: UITableViewCell?
    var langIdStr: String?
    var langName = String()
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var langSelectionText = "Choose your Language"
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var areaListDataModel = FollowAdsAreaListDataModel()
    var banner = Banner()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.setNeedsLayout()
            }
        }
        self.viewWillAppearUpdate()
        self.trackViewcontroller(name: "Language", screenClass: "LanguageSelectionViewController")
        AppsFlyerTracker.shared().trackEvent("Language",
                                             withValues: [
                                                AFEventAdView: "Language",
                                                ]);
    }
    
    /*
     This method is used to update the vieWDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
        tableView.delegate = self
        tableView.dataSource = self
        self.addLoadingView()
        self.setText()
    }
    
    /*
     This method is used to update the viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {
        setNeedsStatusBarAppearanceUpdate()
        labelLangSelection.text = langSelectionText.localized()
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is ProfileViewController {
                // create a new button
                let leftButton = UIButton.init(type: .custom)
                // set image for button
                leftButton.setImage(UIImage(named: "backIcon"), for: UIControlState.normal)
                // add function for button
                leftButton.addTarget(self, action: #selector(backButtonPressed), for: UIControlEvents.touchUpInside)
                // set frame
                leftButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
                let leftBarButton = UIBarButtonItem(customView: leftButton)
                // assign button to navigationbar
                self.navigationItem.leftBarButtonItem = leftBarButton
                // Set constraints for the navigation bar button. Fix for iOS 11 issue
                let widthConstraintLeft = leftButton.widthAnchor.constraint(equalToConstant: 25)
                let heightConstraintLeft = leftButton.heightAnchor.constraint(equalToConstant: 25)
                heightConstraintLeft.isActive = true
                widthConstraintLeft.isActive = true
            }
            else {
            }
        }
        
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
        }
        else {
            topConstraint.constant = 200
            heightConstraint.constant = 300
        }
        self.tableView.isHidden = true
        if Reachability.isConnectedToNetwork() == true {
            self.getLangListData()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        self.tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @objc func setText(){
        labelLangSelection.text = chooseLangText.localized()
    }
    
    /*
     This method is used to create activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = emptyString
    }
    
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func categoryList() {
        self.followAdsRequestManager.categoryListRequestManager = FollowAdsCategoryListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = ""
        }
        
        let categoryListCompletion: CategoryListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.categoryListDataModel = response!
                        print(self.categoryListDataModel)
                        sharedInstance.categoryListDataModel = self.categoryListDataModel
                     //   FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.changeLangDataModel.message!)

                        self.areaList()
                        
                    })
                }
            }
        }
        FollowAdsServiceHandler.callCategoryListServiceCall(requestObject: self.followAdsRequestManager, categoryListCompletion)
    }
    
    /*
     This method is used to perform area list service call.
     @param -.
     @return -.
     */
    func areaList() {
        self.followAdsRequestManager.areaListRequestManager = FollowAdsAreaListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.areaListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.areaListRequestManager?.lang_id = ""
        }
        
        let areaListCompletion: AreaListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.areaListDataModel = response!
                        print(self.areaListDataModel)
                        sharedInstance.areaListDataModel = self.areaListDataModel
                        
                        //Navigate to Home View
                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                        nextViewController.modalPresentationStyle = .fullScreen
                        self.present(nextViewController, animated: true, completion: nil)
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callAreaListServiceCall(requestObject: self.followAdsRequestManager, areaListCompletion)
    }
    
    /*
     This method is used to perform lang list service call.
     @param --.
     @return --.
     */
    func getLangListData() {
        let langListCompletion: LangListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                DispatchQueue.main.async(execute: {
                    if response?.information?.count != nil {
                        if (response?.information?.count)! >= 0 {
                            self.langListDataModel = response!
                            self.tableView.isHidden = false
                            if (response?.information?.count)! > 3 {
                                self.tableView.isScrollEnabled = true
                            }
                            else {
                                self.tableView.isScrollEnabled = false
                            }
                        }
                    }
                    self.tableView.reloadData()
                    print(self.langListDataModel as Any)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tableView.isHidden = false
                })
            }
        }
        FollowAdsServiceHandler.callLanguageListServiceCall(requestObject: self.followAdsRequestManager, langListCompletion)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform change lang service call.
     @param -.
     @return -.
     */
    func changeLang() {
        self.followAdsRequestManager.changeLangRequestManager = FollowAdsChangeLangRequestManager()
        let userIdText = String()
        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
        self.followAdsRequestManager.changeLangRequestManager?.user_id = userId
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.changeLangRequestManager?.lang_id = langId
        
        let changeLangCompletion: ChangeLangCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.changeLangDataModel = response!
                        print(self.changeLangDataModel)
                        
//                        self.banner = Banner(title: "Language has been changed successfully".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                        self.banner.position = .bottom
//                        self.banner.dismissesOnTap = true
//                        self.banner.show(duration: 2.0)

                        self.view.makeToast("Language has been changed successfully".localized(), duration: 3.0, position: .center)

                    //    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.changeLangDataModel.message!)
                        sharedInstance.nearbyListDataModel.business_list = []
                        self.categoryList()

                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callChangeLangServiceCall(requestObject: self.followAdsRequestManager, changeLangCompletion)
    }
    
    /*
     This method is used to perform lang list button action.
     @param --.
     @return --.
     */
    @objc func langBtnPressed(sender: UIButton) {
        switch sender.tag {
        case 0:
            Localize.setCurrentLanguage("en")
            langIdStr = langListDataModel.information![sender.tag].language_id
            langName = self.langListDataModel.information![sender.tag].name_in_native!
            UserDefaults.standard.set(langName, forKey: "LangName")
            UserDefaults.standard.set(langIdStr, forKey: "Language_Id")
            
            if let vcs = self.navigationController?.viewControllers {
                let previousVC = vcs[vcs.count - 2]
                if previousVC is ProfileViewController {
                    // ... and so on
                    if Reachability.isConnectedToNetwork() == true {
                        self.changeLang()
//                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
//                        self.present(nextViewController, animated: true, completion: nil)

                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                    }
                }
                else {
                    self.categoryList()
                }
            }
            else {
                self.categoryList()
            }
            break;
        case 1:
            Localize.setCurrentLanguage("ta-IN")
            langIdStr = langListDataModel.information![sender.tag].language_id
            langName = self.langListDataModel.information![sender.tag].name_in_native!
            UserDefaults.standard.set(langName, forKey: "LangName")
            UserDefaults.standard.set(langIdStr, forKey: "Language_Id")
            //UserDefaults.standard.synchronize()
            if let vcs = self.navigationController?.viewControllers {
                let previousVC = vcs[vcs.count - 2]
                if previousVC is ProfileViewController {
                    // ... and so on
                    if Reachability.isConnectedToNetwork() == true {
                        self.changeLang()

//                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
//                        self.present(nextViewController, animated: true, completion: nil)
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                    }
                }
                else {
                    self.categoryList()
                }
            }
            else {
                self.categoryList()
            }
            break;
        default:
            break;
        }
    }
    
    // MARK : - TABLEVIEW DELEGATES AND DATASOURCE
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (langListDataModel.information?.count) != nil {
            return (langListDataModel.information?.count)!
        }
        else {
            return 5
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.langListDataModel.information?.count != nil {
            let langSelectionCell: LangSelectionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: LanguageSelectionCellId) as? LangSelectionTableViewCell
            langSelectionCell.langBtn.addTarget(self, action: #selector(langBtnPressed(sender:)), for: UIControlEvents.touchUpInside)
            if self.langListDataModel.information![indexPath.row].name_in_native != nil {
                langSelectionCell.langBtn.tag = indexPath.row
                langSelectionCell.langBtn.setTitle(langListDataModel.information![indexPath.row].name_in_native, for: UIControlState.normal)
            }
            return langSelectionCell
        }
        else {
            let langSelectionCell: LangSelectionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: LanguageSelectionCellId) as? LangSelectionTableViewCell
            return langSelectionCell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
