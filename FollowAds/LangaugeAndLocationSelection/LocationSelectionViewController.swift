//
//  LocationSelectionViewController.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 25/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GooglePlaces
import AppsFlyerLib

class LocationSelectionViewController: FollowAdsBaseViewController, CLLocationManagerDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet weak var setLocationLbl: UILabel!
    @IBOutlet weak var DetectLocationBtn: UIButton!
    @IBOutlet weak var SelectLocationBtn: UIButton!
    var locationManager = CLLocationManager()
    var location: CLLocation?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var city: String?
    var country: String?
    var countryShortName: String?
    var subAdministrativeAre : String?
    var address : String?
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var searchBar: UISearchBar?
    var tableDataSource: GMSAutocompleteTableDataSource?
    let GoogleMapsAPIServerKey = APIKey
    var loc_lat: String?
    var loc_lng: String?
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
    }
    
    // Add an observer for LCLLanguageChangeNotification on viewWillAppear. This is posted whenever a language changes and allows the viewcontroller to make the necessary UI updated. Very useful for places in your app when a language change might happen.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.determineCurrentLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        self.tabBarController?.tabBar.isHidden = true
       
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     This method is used to update the viewDidLoad method.
     @param -.
     @return -.
     */
    func viewDidLoadUpdate() {
        self.DetectLocationBtn.layer.cornerRadius = 20.0
        self.SelectLocationBtn.layer.cornerRadius = 20.0
        // Do any additional setup after loading the view.
        self.setText()
    }
    
    // MARK: Localized Text
    
    @objc func setText(){
        orLbl.text = orText.localized()
        setLocationLbl.text = SetYourLocationText.localized()
        DetectLocationBtn.setTitle(DetextLocation.localized(using: buttonTitles), for: UIControlState.normal)
        SelectLocationBtn.setTitle(EnterLocation.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to determine current location.
     @param -.
     @return -.
     */
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: CLLOCATION MANAGER DELEGATES
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.startUpdatingLocation()
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        loc_lat = String(describing: userLocation.coordinate.latitude)
        loc_lng = String(describing: userLocation.coordinate.longitude)
        UserDefaults.standard.set(loc_lat, forKey: "User_Lat")
        UserDefaults.standard.set(loc_lng, forKey: "User_Lng")
        //UserDefaults.standard.synchronize()
        
        let lat: Double = Double(userLocation.coordinate.latitude)
        let longt: Double = Double(userLocation.coordinate.longitude)
        let latString:String = String(format:"%3f", lat)
        print("latString: \(latString)") // b: 1.500000
        let lonString:String = String(format:"%3f", longt)
        print("lonString: \(lonString)") // b: 1.500000
        
        let latestLocation = locations.last!
        // here check if no need to continue just return still in the same place
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        // if it location is nil or it has been moved
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need
                self.parsePlacemarks()
                if self.city != nil {
                    if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                        let deviceTokenText = String()
                        let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: deviceToken), object: nil, userInfo: nil)
                    }
                }
            })
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.subLocality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                    UserDefaults.standard.set(city, forKey: City)
                    print(city)
                }
                if let subAdministrativeAre = placemark.name, !subAdministrativeAre.isEmpty {
                    self.subAdministrativeAre = subAdministrativeAre
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    self.country = country
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    self.countryShortName = countryShortName
                }
                self.address = self.getAddressString(placemark: placemark)
            }
        } else {
            // add some more check's if for some reason location manager is nil
        }
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func getAddressString(placemark: CLPlacemark) -> String? {
        var originAddress : String?
        if let addrList = placemark.addressDictionary?[FormattedAddressLines] as? [String]
        {
            originAddress =  addrList.joined(separator: ", ")
        }
        return originAddress
    }
    
    /*
     This method is used to perform detect location button action.
     @param -.
     @return -.
     */
    @IBAction func detectLocationBtnAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    /*
     This method is used to perform select location button action.
     @param -.
     @return -.
     */
    @IBAction func selectLocationBtnAction(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    /*
     This method is used to perform back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension LocationSelectionViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place latitude: \(String(describing: place.coordinate.latitude))")
        print("Place longtitude: \(String(describing: place.coordinate.longitude))")
        var lat = String()
        var lng = String()
        lat = String(describing: place.coordinate.latitude)
        lng = String(describing: place.coordinate.longitude)
        UserDefaults.standard.set(lat, forKey: "User_Lat")
        UserDefaults.standard.set(lng, forKey: "User_Lng")
        UserDefaults.standard.set(place.name, forKey: "City")
        dismiss(animated: true, completion: nil)
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

// Handle the user's selection.
extension LocationSelectionViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
}

