//
//  LangSelectionTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class LangSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet var langBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.langBtn.layer.cornerRadius = 20.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
