//
//  ViewController.swift
//  SJSegmentedScrollView
//
//  Created by Subins Jose on 06/10/2016.
//  Copyright © 2016 Subins Jose. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import MBProgressHUD

class ViewController: SJSegmentedViewController, UISearchBarDelegate {
    
    var selectedSegment: SJSegmentTab?
    lazy var searchBar:UISearchBar = UISearchBar()
    var followAdsRequestManager = FollowAdsRequestManager()
    var searchOffersDataModel : FollowAdsSearchOffersDataModel?
    var firstViewController : FirstTableViewController?
    var secondViewController : FirstTableViewController?
    var thirdViewController : FirstTableViewController?
    var fourthViewController : FirstTableViewController?
    var fivthViewController : FirstTableViewController?
    var segment : Int = 0
    var selectedString : String?
    var searchBarText : String?
    var suggestedSearchDataModel : FollowAdsSearchSuggestionDataModel?
    
    override func viewDidLoad() {
        if let storyboard = self.storyboard {
            firstViewController = storyboard
                .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController
            firstViewController?.title = "All".localized()
            
            secondViewController = storyboard
                .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController
            secondViewController?.title = "Expiring".localized()
            
            thirdViewController = storyboard
                .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController
            thirdViewController?.title = "Live".localized()
            
            fourthViewController = storyboard
                .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController
            fourthViewController?.title = "Coming".localized()
            
            fivthViewController = storyboard
                .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController
            fivthViewController?.title = "Near by".localized()
            
            segmentControllers = [firstViewController!,
                                  secondViewController!,
                                  thirdViewController!,
                                  fourthViewController!,fivthViewController!]
            
            headerViewHeight = 200
            selectedSegmentViewHeight = 5.0
            headerViewOffsetHeight = 31.0
            selectedSegmentViewColor = .red
            if firstViewController?.segment == 0 {
                segmentTitleColor = .red
            }
            segmentShadow = SJShadow.light()
            showsHorizontalScrollIndicator = false
            showsVerticalScrollIndicator = false
            segmentBounces = false
            delegate = self
        }
        searchBar.searchBarStyle = UISearchBarStyle.prominent
        if searchBarText != "" {
            searchBar.text = searchBarText
        }
        else {
            searchBar.placeholder = "Search for Offers".localized()
        }
        
        let view: UIView = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self){
                subView.tintColor = UIColor.black
            }
        }
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width - 30, height: 44)
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        self.suggestedSearchDataModel = FollowAdsSearchSuggestionDataModel()
        self.searchOffersDataModel = FollowAdsSearchOffersDataModel()
        self.searchOffersDataModel?.All_ads = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.searchOffersDataModel?.Expiring = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.searchOffersDataModel?.Live = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.searchOffersDataModel?.Nearby = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.searchOffersDataModel?.Coming = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.firstViewController?.searchOffersDataModel = FollowAdsSearchOffersDataModel()
        self.firstViewController?.searchOffersDataModel?.All_ads = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.firstViewController?.searchOffersDataModel?.Expiring = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.firstViewController?.searchOffersDataModel?.Live = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.firstViewController?.searchOffersDataModel?.Nearby = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.firstViewController?.searchOffersDataModel?.Coming = [FollowAdsSearchOffersDataModel.FollowAdsSearchKeyValueDataModel()]
        self.callSearchOffersServiceCall()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.becomeFirstResponder()
        if Reachability.isConnectedToNetwork() == true {
            //            self.callSearchSuggestionData()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
        }
        
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.isEnabled = true
            cancelButton.setTitle("Cancel".localized(using: buttonTitles), for: .normal)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        
        if let aps = notification.userInfo?["aps"] as? [AnyHashable:String]{
            if let ad_id = aps["advertisement_id"] {
                vc.offerId = ad_id
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.selectedString = searchBar.text
        self.callSearchOffersServiceCall()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.selectedString = "#"
        self.selectedString?.append(searchBar.text ?? "")
        self.searchBar.resignFirstResponder()
        self.callSearchOffersServiceCall()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        // Hide the cancel button
        searchBar.resignFirstResponder()
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.isEnabled = true
        }
        self.navigationController?.popViewController(animated: true)
        // You could also change the position, frame etc of the searchBar
    }
    
    func callSearchOffersServiceCall() {
        self.followAdsRequestManager.searchOffersRequestManager = FollowAdsSearchOffersRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String!))
            self.followAdsRequestManager.searchOffersRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.searchOffersRequestManager?.user_id = ""
        }
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String!))
        self.followAdsRequestManager.searchOffersRequestManager?.user_lat = userLat
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String!))
        self.followAdsRequestManager.searchOffersRequestManager?.user_lng = userLng
        self.followAdsRequestManager.searchOffersRequestManager?.search_key = selectedString
        self.followAdsRequestManager.searchOffersRequestManager?.limit = "1"
        
        let searchCompletion: SearchOffersCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: ErrorMsg)
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.searchOffersDataModel = response
                        self.firstViewController?.searchOffersDataModel = self.searchOffersDataModel
                        self.firstViewController?.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callSearchOffersServiceCall(requestObject: self.followAdsRequestManager, searchCompletion)
    }
    
    func getSegmentTabWithImage(_ imageName: String) -> UIView {
        let view = UIImageView()
        view.frame.size.width = 100
        view.image = UIImage(named: imageName)
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .white
        return view
    }
}

extension ViewController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        let segmentedViewController = SJSegmentedViewController()
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(.black)
        }
        
        if segments.count > 0 {
            selectedSegment = segments[index]
            selectedSegment?.titleColor(.red)
        }
        
        switch index {
        case 0:
            firstViewController?.segment = 0
            self.firstViewController?.searchOffersDataModel = self.searchOffersDataModel
            firstViewController?.tableView.reloadData()
            break
        case 1:
            secondViewController?.segment = 1
            self.secondViewController?.searchOffersDataModel = self.searchOffersDataModel
            secondViewController?.tableView.reloadData()
            break
        case 2:
            thirdViewController?.segment = 2
            self.thirdViewController?.searchOffersDataModel = self.searchOffersDataModel
            thirdViewController?.tableView.reloadData()
            break
        case 3:
            fourthViewController?.segment = 3
            self.fourthViewController?.searchOffersDataModel = self.searchOffersDataModel
            fourthViewController?.tableView.reloadData()
            
            break
        case 4:
            fivthViewController?.segment = 4
            self.fivthViewController?.searchOffersDataModel = self.searchOffersDataModel
            fivthViewController?.tableView.reloadData()
            break
        default:
            break
            
        }
    }
}

