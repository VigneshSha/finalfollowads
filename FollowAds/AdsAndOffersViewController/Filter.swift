//
//  Filter.swift
//  FollowAds
//
//  Created by openwave on 14/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class CustomCell : UITableViewCell {
    
}


class Filter: UIViewController {
    
    var profileTableDataSource: FilterTableDataSource!
    
    @IBOutlet weak var tableView: UITableView!
    let data = ["Bags", "Caps", "Chappels", "Caskets", "Desks", "Shirts For Men", "Shirts for Women", "Shoes for Men", "Shoes for Women", "Mobiles", "Vadais", "Shoes for Kids", "Shoes", "Shirts"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableViewDataSource()
        
        // Do any additional setup after loading the view.
    }
    
    func setupTableViewDataSource() {
        
        profileTableDataSource = FilterTableDataSource(data: data)
        self.tableView.dataSource = profileTableDataSource
        self.tableView.delegate = profileTableDataSource
        profileTableDataSource.tableView = self.tableView
        self.tableView.tableFooterView =  UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func doneBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

