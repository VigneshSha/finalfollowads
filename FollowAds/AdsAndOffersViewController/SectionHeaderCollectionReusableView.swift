//
//  SectionHeaderCollectionReusableView.swift
//  FollowAds
//
//  Created by openwave on 10/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class SectionHeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var header: UILabel!
     @IBOutlet weak var seeAllBtn: UIButton!
    
}
