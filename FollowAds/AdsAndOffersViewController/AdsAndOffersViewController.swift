//
//  ViewController.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 09/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class AdsAndOffersViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    let arrImages = ["img1.png", "img2.png", "img3.png", "img6.png"]
    let offerImage = [UIImage (named: "image11.png"), UIImage(named: "image12.png"), UIImage(named: "image13.jpg"), UIImage(named: "image14.jpg")]
    let shopTitle = ["Roshan bags", "American tourister", "Peter england", "Bata shoes"]
    let shopAddress = ["T.nagar, Chennai", "Velachery, Chennai", "Nungambakkam, Chennai", "Mogappair, Chennai"]
    override func viewDidLoad() {
        super.viewDidLoad()
        loadScrollView()
        scrollVw.delegate = self
        collectionView.delegate = self
                self.navigationItem.title = "Ads & Offers"
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        self.collectionView?.register(SectionHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
 //        layout.sectionInset = UIEdgeInsets(top: 20, left: 2, bottom: 10, right: 2)
//        // Do any additional setup after loading the view, typically from a nib.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offerImage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
         let img = offerImage[indexPath.row]
         let title = shopTitle[indexPath.row]
         let address = shopAddress[indexPath.row]
         cell.shopImg.image = img
         cell.shopTilte.text = title
         cell.shopAddress.text = address
         cell.shopImg.layer.cornerRadius = 10.0
         cell.shopImg.layer.borderColor = UIColor.black.cgColor
         cell.view.layer.borderWidth = 1
         cell.view.layer.cornerRadius = 10.0
         cell.view.layer.borderColor = UIColor.black.cgColor
         return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("sssss")
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId", for: indexPath) as! SectionHeaderCollectionReusableView
//        let title = "Big Bag Offers"
//        header.header.text = "Big Bag Offers"
        return header
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
         return CGSize(width: collectionView.bounds.width, height: 35)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: self.collectionView.frame.width/2, height: 235)
     }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 20.0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
   
   
    @IBAction func next(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Filter") as! Filter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    func loadScrollView() {
        let pageCount : CGFloat = CGFloat(arrImages.count)
        
        scrollVw.backgroundColor = UIColor.clear
        scrollVw.delegate = self
        scrollVw.isPagingEnabled = true
        scrollVw.contentSize = CGSize(width: scrollVw.frame.size.width * pageCount, height:  scrollVw.frame.size.height)
        scrollVw.showsHorizontalScrollIndicator = false
         pageControl.numberOfPages = Int(pageCount)
        pageControl.addTarget(self, action: #selector(self.pageChanged), for: .valueChanged)
        scrollVw.isPagingEnabled = true
        for i in 0..<Int(pageCount) {
            print(self.scrollVw.frame.size.width)
            print(self.scrollVw.frame.size.height)
            let image = UIImageView(frame: CGRect(x: self.scrollVw.frame.size.width * CGFloat(i), y: 0, width: self.scrollVw.frame.size.width, height: self.scrollVw.frame.size.height))
            image.image = UIImage(named: arrImages[i])!
            //It fix the image's size to current frame size
            image.contentMode = UIViewContentMode.scaleAspectFit
            // If we did not add this the images does not load into the views
            self.scrollVw.addSubview(image)
        }
    }
    // this function is used for scroll the UIPage control. if we did not fix it the page control does not load but the pages can scroll.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = floor((scrollView.contentOffset.x - (pageWidth/2)) / pageWidth) + 1
        pageControl?.currentPage = Int(page - 1)
        
    }
    
    @objc func pageChanged() {
        let pageNumber = pageControl.currentPage
        var frame = scrollVw.frame
        frame.origin.x = frame.size.width * CGFloat(pageNumber)
        frame.origin.y = 0
        scrollVw.scrollRectToVisible(frame, animated: true)
    }
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }


