//
//  FilterTableDataSource.swift
//  FollowAds
//
//  Created by openwave on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class FilterTableDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var data : [String]!
    
    init(data : [String]!) {
        self.data = data
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "customCell", for: indexPath)
        cell?.textLabel?.text = self.data[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath as IndexPath){
            if cell.accessoryType == .none{
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
        }
    }
    
}

