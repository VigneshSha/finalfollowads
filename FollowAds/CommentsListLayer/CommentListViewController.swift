//
//  CommentListViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 02/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDynamicLinks
import AppsFlyerLib

class CommentListViewController: FollowAdsBaseViewController {
    
    @IBOutlet var tableView: UITableView!
    var commentTitle: [String]?
    var commentListDatasource: CommentListTableDatasource?
    var offerDetailDataModel: FollowAdsOfferDetailDataModel?
    var nodataView: NoDataView?
    var navigationTitle = "Reviews"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        commentTitle = ["Awesome Experience!", "Great!!!", "Nice", "Good Quality.."]
        self.setupTableViewDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        noDataLabel.text = "No Data Available".localized()
        self.setUpNavigationBarButton()
        if offerDetailDataModel?.advertisement_detail.advertisement_reviews.isEmpty == false{
            self.tableView.isHidden = false
            self.nodataView?.isHidden = true
            self.nodataView?.removeFromSuperview()
        }
        else {
            self.tableView.isHidden = true
        }
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        self.tableView.reloadData()
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
  //      if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
     This method is used to display the nodataView if no records are available.
     @param --.
     @return --.
     */
    func setupNoDataView() {
        self.navigationController?.isNavigationBarHidden = false
        let noDataViewFrame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        nodataView = NoDataView.init(frame: noDataViewFrame)
        nodataView?.isHidden = false
        self.view.addSubview(nodataView!)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = "Reviews".localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    /*
     This method is used for Setup tableview data source.
     @param --.
     @return --.
     */
    func setupTableViewDataSource() {
        self.commentListDatasource = CommentListTableDatasource(commentTitlesList: commentTitle, offerDetailData: offerDetailDataModel)
        self.tableView.dataSource = commentListDatasource
        self.tableView.delegate = commentListDatasource
        commentListDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used for back button action.
     @param --.
     @return --.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
