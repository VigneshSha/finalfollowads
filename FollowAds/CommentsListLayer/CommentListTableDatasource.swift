//
//  CommentListTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 02/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class CommentListTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView?
    var commentTitlesArr : [String]!
    var header: UITableViewHeaderFooterView? = nil
    var headerLabel: UILabel?
    var offerDetailsDataModel: FollowAdsOfferDetailDataModel?
    var ratingValue = Double()
    
    init(commentTitlesList : [String]!, offerDetailData: FollowAdsOfferDetailDataModel?) {
        self.commentTitlesArr = commentTitlesList
        self.offerDetailsDataModel = offerDetailData
        super.init()
    }
    
    // MARK:- TABLEVIEW DELEGATES AND DATASOURCES

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews.count != nil {
            return (offerDetailsDataModel?.advertisement_detail.advertisement_reviews.count)!
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopReviewCellId", for: indexPath) as! OfferDetailShopReviewTableViewCell
        cell.commentTitleLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_reviews[indexPath.row].title
        cell.commentsLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_reviews[indexPath.row].feedback
        cell.userNameLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_reviews[indexPath.row].user_name
        
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews[indexPath.row].rating != nil {
            
            let ratingString = offerDetailsDataModel?.advertisement_detail.advertisement_reviews[indexPath.row].rating?.toDouble
            cell.floatRatingView.rating = ratingString!
        }
        
        let dateStr = offerDetailsDataModel!.advertisement_detail.advertisement_reviews[indexPath.row].posted_time!
        
        let dateString = offerDetailsDataModel!.advertisement_detail.advertisement_reviews[indexPath.row].current_server_time!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let serverDate = dateFormatter.date(from: dateString)!
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let serverSeperatedDateString = dateFormatter.string(from: serverDate)
        
        dateFormatter.dateFormat = "HH:mm"
        let serverSeperatedTimeString = dateFormatter.string(from: serverDate)
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let postedDate = dateFormatter.date(from: dateStr)!
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let postedSeperatedDateString = dateFormatter.string(from: postedDate)
        
        dateFormatter.dateFormat = "HH:mm"
        let postedSeperatedTimeString = dateFormatter.string(from: postedDate)
        
        print("********serverSeperatedDateString****\n\(serverSeperatedDateString)")
        print("********serverSeperatedTimeString****\n\(serverSeperatedTimeString)")
        print("********postedSeperatedDateString****\n\(postedSeperatedDateString)")
        print("********postedSeperatedTimeString****\n\(postedSeperatedTimeString)")
        
        let timeAgo: String = ReviewsTimeAgo.timeAgoSinceDate(postedDate, currentDate: serverDate, numericDates: true)
        cell.timeLabel.text = timeAgo
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews.isEmpty == false {
            return 50
        }
        else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        header = view as? UITableViewHeaderFooterView
        // create a label for section header.
        headerLabel = UILabel(frame: CGRect(x: 15, y: 15, width: 180, height: 20))
        header?.addSubview(headerLabel!)
        if Int(reviewsCount) ?? 0 > 1 {
            headerLabel?.text = reviewsCount + " Reviews".localized()
        }
        else {
            headerLabel?.text = reviewsCount + " Review".localized()
            
        }
        headerLabel?.textColor = UIColor.darkGray
        headerLabel?.font = (UIFont.mediumSystemFont(ofSize: 17))
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: tableView.sectionHeaderHeight))
        view.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view.contentView.backgroundColor = UIColor.white
        return view
    }
}

extension String {
    var toDouble: Double {
        return Double(self) ?? 0.0
    }
}

extension Date {
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
}
