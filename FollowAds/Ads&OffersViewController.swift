//
//  Ads&OffersViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class Ads_OffersViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var collectionView: UICollectionView!
    var colVwCell: UICollectionViewCell?
    
    @IBOutlet var locationBarButtonItem: UIBarButtonItem!
    
    @IBOutlet var searchBarButtonItem: UIBarButtonItem!
    
    //   let arrImages = ["img1.png", "img2.png", "img3.png", "img6.png"]
    let arrImages = [UIImage (named: "img1.png"), UIImage(named: "img2.png"), UIImage(named: "img3.png"), UIImage(named: "img6.png")]
    let offerImage = [UIImage (named: "image11.png"), UIImage(named: "image12.png"), UIImage(named: "image13.jpg"), UIImage(named: "image14.jpg")]
    let shopTitle = ["Roshan bags", "American tourister", "Peter england", "Bata shoes"]
    let shopAddress = ["T.nagar, Chennai", "Velachery, Chennai", "Nungambakkam, Chennai", "Mogappair, Chennai"]
    
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    var pageControl : UIPageControl?
        //= UIPageControl(frame: CGRect(x:50,y: 175, width:200, height:50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "FirstTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FirstCellId")
        collectionView.register(UINib(nibName: "SecondTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SecondCellId")
        collectionView.register(UINib(nibName: "ThirdTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "ThirdCellId")
        collectionView.register(UINib(nibName: "FourthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "FourthCellId")
        collectionView.register(UINib(nibName: "FifthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "FifthCellId")
        collectionView.register(UINib(nibName: "SixthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SixthCellId")

        collectionView.register(CollectionVwHeaderReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionVwHeaderId")
      
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        scrollView.addGestureRecognizer(tap)
        
        let rupee = "\u{20B9}"
        print(rupee)
      
        
        collectionView.dataSource = self
        collectionView.delegate = self
        scrollView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        
     

        
      //  self.collectionView.register(UINib(nibName: "FirstTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FirstCellId")
      //  self.collectionView.register(UINib(nibName: "SecondTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SecondCellId")

        
        pageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        pageControl?.center = CGPoint(x: self.view.frame.width/2, y: self.scrollView.frame.size.height-2)
        configurePageControl()
        
        for index in 0..<4 {
            
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            frame.size = self.scrollView.frame.size
            
            let subView = UIImageView(frame: frame)
            subView.image = arrImages[index]
            self.scrollView .addSubview(subView)
        }
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 4,height: self.scrollView.frame.size.height)
        pageControl?.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }

    
    
    @objc override func dismissKeyboard() {
        
        self.performSegue(withIdentifier: "OfferDetailViewId", sender: self)

    }
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl?.numberOfPages = arrImages.count
        self.pageControl?.currentPage = 0
        self.pageControl?.tintColor = UIColor.red
        self.pageControl?.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl?.currentPageIndicatorTintColor = UIColor.darkGray
        self.view.addSubview(pageControl!)
        
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat((pageControl?.currentPage)!) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
  
    
   
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl?.currentPage = Int(pageNumber)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return offerImage.count
        }
        else if section == 1 {
            return offerImage.count
        }
        else if section == 2 {
            return offerImage.count
        }
        else if section == 3 {
            return offerImage.count
        }
        else if section == 4 {
            return offerImage.count
        }
        else if section == 5 {
            return offerImage.count
        }
        else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : FirstTypeCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstCellId", for: indexPath) as? FirstTypeCollectionViewCell
            cell?.offerImageView.image = offerImage[indexPath.row]
            cell?.titleLabel.text = shopTitle[indexPath.row]
            cell?.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell!
        }
        else if indexPath.section == 1 {
            let cell : SecondTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondCellId", for: indexPath) as! SecondTypeCollectionViewCell
            cell.offerImageView.image = offerImage[indexPath.row]
            cell.titleLabel.text = shopTitle[indexPath.row]
            cell.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell
        }
        else if indexPath.section == 2 {
            let cell : ThirdTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThirdCellId", for: indexPath) as! ThirdTypeCollectionViewCell
            cell.offerImageView.image = offerImage[indexPath.row]
            cell.titleLabel.text = shopTitle[indexPath.row]
            cell.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell
        }
        else if indexPath.section == 3 {
            let cell : FourthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FourthCellId", for: indexPath) as! FourthTypeCollectionViewCell
            cell.offerImageView.image = offerImage[indexPath.row]
            cell.titleLabel.text = shopTitle[indexPath.row]
            cell.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell
        }
        else if indexPath.section == 4 {
            let cell : FifthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FifthCellId", for: indexPath) as! FifthTypeCollectionViewCell
            cell.OfferImageView.image = offerImage[indexPath.row]
            cell.titleLabel.text = shopTitle[indexPath.row]
            cell.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell
        }
        else if indexPath.section == 5 {
            let cell : SixthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SixthCellId", for: indexPath) as! SixthTypeCollectionViewCell
            cell.offerImageView.image = offerImage[indexPath.row]
            cell.titleLabel.text = shopTitle[indexPath.row]
            cell.descriptionLabel.text = shopAddress[indexPath.row]
            
            return cell
        }
        else {
            return colVwCell!
        }
    }

  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: self.collectionView.frame.width/2, height: 235)

        }
        else if indexPath.section == 1 {
            return CGSize(width: self.collectionView.frame.width/3, height: 170)
        }
        else if indexPath.section == 2 {
            return CGSize(width: self.collectionView.frame.width/4, height: 200)
        }
        else if indexPath.section == 3 {
            return CGSize(width: self.collectionView.frame.width/2, height: 140)
        }
        else if indexPath.section == 4
            {
            return CGSize(width: self.collectionView.frame.width/4, height: 170)
        }
        else if indexPath.section == 5 {
            return CGSize(width: self.collectionView.frame.width, height: 100)
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionVwHeaderId", for: indexPath) as? CollectionVwHeaderReusableView
     //   headerCell?.offerNameLabel.text = "Big Bag Offers"
        return headerCell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 35)
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        if indexPath.section == 0 {
//            switch kind {
//            //2
//            case UICollectionElementKindSectionHeader:
//                //3
//                let headerView : CollectionVwHeaderReusableView? = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionVwHeaderId", for: indexPath) as? CollectionVwHeaderReusableView
//                //   headerView?.offerNameLabel.text = "hello"
//                return headerView!
//            default:
//                //4
//                assert(false, "Unexpected element kind")
//            }
//        }
//        let headerView : CollectionVwHeaderReusableView? = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionVwHeaderId", for: indexPath) as? CollectionVwHeaderReusableView
//        //   headerView?.offerNameLabel.text = "hello"
//        return headerView!
//
//    }
    
    @IBAction func locationBarBtnAction(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func searchBarBtnAction(_ sender: UIBarButtonItem) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
