//
//  ShopDetailPhotosTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoViewDelegate {
    func navigateToPhotoView(indexPath: IndexPath)
}

class ShopDetailPhotosTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    var imageArray = [String] ()
    let offerImage = [UIImage (named: "BigBagOffer1"), UIImage(named: "BigBagOffer2"), UIImage(named: "BigBagOffer3"), UIImage(named: "BigBagOffer4")]
    var colVwCell: UICollectionViewCell?
    var businessDetailDataModel = FollowAdsBusinessDetailDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    var delegate: PhotoViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "PhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photosCellId")
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if businessDetailDataModel.information.business_images.count != nil {
                return businessDetailDataModel.information.business_images.count
            }
            else {
                return 1
            }
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : PhotosCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "photosCellId", for: indexPath) as? PhotosCollectionViewCell
            
            let urlstring = self.businessDetailDataModel.information.business_images[indexPath.row].business_img
            
            if urlstring != nil {
                if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    cell?.shopImagesImgView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "business_Placeholder"))
                }
            }
            else {
                cell?.shopImagesImgView.image = UIImage(named: "business_Placeholder")
            }
            return cell!
        }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            let itemsPerRow:CGFloat = 2.7
            let hardCodedPadding:CGFloat = 0
            let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
            let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else {
            let itemsPerRow:CGFloat = 3.2
            let hardCodedPadding:CGFloat = 0
            let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
            let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if delegate != nil {
            delegate?.navigateToPhotoView(indexPath: indexPath)
        }
    }
    
}
