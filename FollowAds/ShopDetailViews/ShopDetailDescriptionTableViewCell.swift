//
//  ShopDetailDescriptionTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 31/08/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class ShopDetailDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet var descriptionLabel: ExpandableLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
