//
//  ShopDetailTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol NavigationsDelegate {
    func CallBtnAction()
    func WhatsAppBtnAction()
    func MessageBtnAction()
    func LocationBtnAction()
    func photoViewNavigation()
    func videoViewNavigation()
    func followBtnNavigation()
    func followBtnServiceCall()
    func navigateToPhotoView(imageArray : [String]?, selectedIndex : Int)
}

class ShopDetailTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource, PhotoViewDelegate, ExpandableLabelDelegate{
    
    var tableView: UITableView?
    var bannerImg: String?
    var tableVwCell: UITableViewCell?
    var delegate: NavigationsDelegate?
    var header: UITableViewHeaderFooterView? = nil
    var labelHeader: UILabel!
    var seeAllBtn: UIButton!
    var businessDetailsDataModel: FollowAdsBusinessDetailDataModel?
    var allPromotionsDataModel: FollowAdsAllPromotionsDataModel?
    var imageStr: String?
    var bool: String?
    var shopDetailVC: ShopDetailViewController?
    var landLineNum: String?
    var mobNum: String?
    var states : Bool = true

    /*
     This method is used for init tableview datasource.
     @param ~~.
     @return ~~.
     */
    init(bannerImgData: String?, businessDetailData: FollowAdsBusinessDetailDataModel?) {
        self.bannerImg = bannerImgData
        self.businessDetailsDataModel = businessDetailData
        super.init()
    }
    
    func willExpandLabel(_ label: ExpandableLabel) {
        
        tableView?.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: point) as IndexPath? {
            states = false
            DispatchQueue.main.async { [weak self] in
                //                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                self?.tableView?.scrollToRow(at: indexPath, at: .none, animated: false)
            }
        }
        tableView?.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableView?.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView?.indexPathForRow(at: point) as IndexPath? {
            states = true
            DispatchQueue.main.async { [weak self] in
                //                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableView?.endUpdates()
    }
    
    // MARK: - TABLEVIEW DELEGATE & DATASOURCE
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let bannerCell: ShopBannerImgTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "bannerImgCellId") as? ShopBannerImgTableViewCell
            let urlstring = self.businessDetailsDataModel?.information.business_banner_img
            if urlstring != nil {
                if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    let imageExtensions = ["gif"]
                    let pathExtention = URL(string: encodedString)?.pathExtension
                    
                    if imageExtensions.contains(pathExtention ?? "") {
                        bannerCell?.bannerImgView.image = UIImage.gifImageWithURL(encodedString)
                    }
                    else {
                        bannerCell?.bannerImgView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))

                    }
                }
            }
            else {
                bannerCell?.bannerImgView.image = UIImage(named: "home_banner_placeholder")
            }
            return bannerCell
        }
        else if indexPath.section == 1 {
            let shopDetailCell: ShopDetailDescriptionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopDescriptionCellId") as? ShopDetailDescriptionTableViewCell
            
            shopDetailCell.descriptionLabel.delegate = self
            
            let currentSource = businessDetailsDataModel?.information.business_description
            
            
            //                offerDescriptionCell.offerDescriptionLabel.setLessLinkWith(lessLink: "Close", attributes: [.foregroundColor:UIColor.red], position: .left)
            
            shopDetailCell.layoutIfNeeded()
            
            shopDetailCell.descriptionLabel.shouldCollapse = true
            shopDetailCell.descriptionLabel.textReplacementType = .word
            shopDetailCell.descriptionLabel.numberOfLines = 3
            shopDetailCell.descriptionLabel.collapsed = states
            shopDetailCell.descriptionLabel.text = currentSource

            return shopDetailCell
        }
        else if indexPath.section == 2 {
            let shopDetailCell: ShopDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopDetailCellId") as? ShopDetailTableViewCell
            
            if businessDetailsDataModel?.information.is_followed == "1" {
                if (UserDefaults.standard.value(forKey: "Followed_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Followed_Status_Value") as? String))
                    if status == "1" {
                        shopDetailCell.followBtn.setTitleColor(UIColor.white, for: .normal)
                        shopDetailCell.followBtn.backgroundColor = UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                        shopDetailCell.followBtn.setTitle("Followed".localized(using: buttonTitles), for: UIControlState.normal)
                    }
                    else {
                        shopDetailCell.followBtn.setTitleColor(UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0), for: .normal)
                        shopDetailCell.followBtn.backgroundColor = UIColor.white
                        shopDetailCell.followBtn.setTitle("Follow".localized(using: buttonTitles), for: UIControlState.normal)
                    }
                }
                else {
                    shopDetailCell.followBtn.setTitleColor(UIColor.white, for: .normal)
                    shopDetailCell.followBtn.backgroundColor = UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                    shopDetailCell.followBtn.setTitle("Followed".localized(using: buttonTitles), for: UIControlState.normal)                }
            }
            else  {
                if (UserDefaults.standard.value(forKey: "Followed_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Followed_Status_Value") as? String))
                    if status == "1" {
                        shopDetailCell.followBtn.setTitleColor(UIColor.white, for: .normal)
                        shopDetailCell.followBtn.backgroundColor = UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                        shopDetailCell.followBtn.setTitle("Followed".localized(using: buttonTitles), for: UIControlState.normal)
                    }
                    else {
                        shopDetailCell.followBtn.setTitleColor(UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0), for: .normal)
                        shopDetailCell.followBtn.backgroundColor = UIColor.white
                        shopDetailCell.followBtn.setTitle("Follow".localized(using: buttonTitles), for: UIControlState.normal)
                    }
                }
                else {
                    shopDetailCell.followBtn.setTitleColor(UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0), for: .normal)
                    shopDetailCell.followBtn.backgroundColor = UIColor.white
                    shopDetailCell.followBtn.setTitle("Follow".localized(using: buttonTitles), for: UIControlState.normal)                }
                
            }
            shopDetailCell.shopImgView.layer.cornerRadius = shopDetailCell.shopImgView.frame.size.width / 2
            shopDetailCell.shopImgView.clipsToBounds = true
            shopDetailCell.followBtn.addTarget(self, action: #selector(FollowBtnBtnAction), for: UIControlEvents.touchUpInside)
            shopDetailCell.shopNameLabel.text = businessDetailsDataModel?.information.name
            shopDetailCell.shopAddrLabel.text = businessDetailsDataModel?.information.busines_address.address
            
            
            if Int(shopReviewsCount) ?? 0 > 1 && Int(shopInterestCount) ?? 0 > 1 {
                shopDetailCell.shopReviewsLabel.text = shopReviewsCount + " Reviews | ".localized() + shopInterestCount + " Interested".localized()
            }
            else if Int(shopReviewsCount) ?? 0 > 1 && Int(shopInterestCount) ?? 0 <= 1 {
                shopDetailCell.shopReviewsLabel.text = shopReviewsCount + " Reviews | ".localized() + shopInterestCount + " " + "Interested".localized()
            }
            else if Int(shopReviewsCount) ?? 0 <= 1 && Int(shopInterestCount) ?? 0 > 1 {
                shopDetailCell.shopReviewsLabel.text = shopReviewsCount + " Review | ".localized() + shopInterestCount + " Interested".localized()
            }
            else {
                shopDetailCell.shopReviewsLabel.text = shopReviewsCount + " Review | ".localized() + shopInterestCount + " " + "Interested".localized()
            }
            
            shopDetailCell.shopDistanceLabel.text = shopDistance + " away".localized()
            
            let urlstring = self.businessDetailsDataModel?.information.logo
            
            if urlstring != nil {
                if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    shopDetailCell?.shopImgView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "business_Placeholder"))
                }
            }
            else {
                shopDetailCell?.shopImgView.image = UIImage(named: "business_Placeholder")
            }
            return shopDetailCell
        }
        else if indexPath.section == 3 {
            let shopContactCell: OfferDetailShopContactTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopContactCellId") as? OfferDetailShopContactTableViewCell
            if landLineNum == "" {
                shopContactCell.callBtn.alpha = 0.5
            }
            else {
                shopContactCell.callBtn.alpha = 1
            }
            if mobNum == "" {
                shopContactCell.whatsappBtn.alpha = 0.5
                shopContactCell.messageBtn.alpha = 0.5
                
            }
            else {
                shopContactCell.whatsappBtn.alpha = 1
                shopContactCell.messageBtn.alpha = 1
            }
            shopContactCell.locationBtn.addTarget(self, action: #selector(locationBtnAction), for: UIControlEvents.touchUpInside)
            shopContactCell.whatsappBtn.addTarget(self, action: #selector(whatsAppBtnAction), for: UIControlEvents.touchUpInside)
            shopContactCell.messageBtn.addTarget(self, action: #selector(messageBtnAction), for: UIControlEvents.touchUpInside)
            shopContactCell.callBtn.addTarget(self, action: #selector(callBtnAction), for: UIControlEvents.touchUpInside)
            return shopContactCell
        }
//        else if indexPath.section == 4 {
//            let shopDescriptionCell: ShopDetailDescriptionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopDescriptionCellId") as? ShopDetailDescriptionTableViewCell
//            return shopDescriptionCell
//        }
            
        else if indexPath.section == 4 {
            let shopVideosCell: ShopDetailVideosTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "shopVideosCell") as? ShopDetailVideosTableViewCell
            shopVideosCell.businessDetailDataModel = self.businessDetailsDataModel!
            shopVideosCell.collectionView.reloadData()
            return shopVideosCell
        }
        else if indexPath.section == 5 {
            let shopPhotosCell: ShopDetailPhotosTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "shopPhotosCell") as? ShopDetailPhotosTableViewCell
            shopPhotosCell.businessDetailDataModel = self.businessDetailsDataModel!
            shopPhotosCell.collectionView.reloadData()
            shopPhotosCell.delegate = self
            return shopPhotosCell
        }
        else  {
            return tableVwCell!
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if FollowAdsDisplayType.typeIsLike == DisplayType.iphoneX || FollowAdsDisplayType.typeIsLike == DisplayType.unknown {
                return tableView.bounds.size.height / 2.7
            }
            else {
                return tableView.bounds.size.height / 2.3
            }
        }
        else if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        }
        else if indexPath.section == 2 {
          //  return 140
            return UITableViewAutomaticDimension

        }
        else if indexPath.section == 3 {
            return 90
        }
       
        else if indexPath.section == 4 {
            if businessDetailsDataModel?.information.business_video.isEmpty == true {
                return 0
            }
            else {
                return 110
            }
        }
        else if indexPath.section == 5 {
            if businessDetailsDataModel?.information.business_images.isEmpty == true {
                return 0
            }
            else {
                return 110
            }
        }
        else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: tableView.sectionHeaderHeight))
        view.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view.contentView.backgroundColor = UIColor.white
        return view
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        header = view as? UITableViewHeaderFooterView
        // create a label for section header.
        labelHeader = UILabel(frame: CGRect(x: 15, y: 12, width: 150, height: 20));
        header?.addSubview(labelHeader!)
        labelHeader?.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        labelHeader?.font = (UIFont.mediumSystemFont(ofSize: 17))
        
        let locStr = "See All".localized(using: buttonTitles)
        let stringsize = locStr.SizeOf(UIFont.mediumSystemFont(ofSize: 15))
        let button = UIButton(frame: CGRect(x: (self.header?.frame.size.width)! - stringsize.width - 15, y: 7, width: stringsize.width, height: 30))
        button.setTitle("See All".localized(using: buttonTitles), for: UIControlState.normal)
        print("******witdsads\(stringsize.width)")
        button.sizeToFit()
        button.addTarget(self, action: #selector(seeAllBtnClicked), for: UIControlEvents.touchUpInside)
        button.setTitleColor(appThemeColor, for: UIControlState.normal)
        button.titleLabel?.font =  (UIFont.mediumSystemFont(ofSize: 15))
        header?.addSubview(button)
        if section == 4 {
            if Int(shopVideosCount)! > 1 {
                labelHeader?.text = shopVideosCount + " Videos".localized()
            }
            else {
                labelHeader?.text = shopVideosCount + " Video".localized()
            }
            button.tag = 0
        }
        else if section == 5 {
            if Int(shopImagesCount)! > 1 {
                labelHeader?.text = shopImagesCount + " Photos".localized()
            }
            else {
                labelHeader?.text = shopImagesCount + " Photo".localized()
            }
            
            button.tag = 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 4 {
            if businessDetailsDataModel?.information.business_video.isEmpty == true {
                return 0.0
            }
            else {
                return 44.0
            }
        }
        else if section == 5 {
            if businessDetailsDataModel?.information.business_images.isEmpty == true {
                return 0.0
            }
            else {
                return 44.0
            }
        }
        else {
            return 0.0
        }
    }
    
    func navigateToPhotoView(indexPath: IndexPath) {
        imageStr = businessDetailsDataModel?.information.business_images[indexPath.row].business_img
        UserDefaults.standard.set(imageStr, forKey: "IndexValue")
        var imagesString : [String] = []
        for images in (businessDetailsDataModel?.information.business_images.enumerated())! {
            imagesString.append(images.element.business_img!)
        }
        delegate?.navigateToPhotoView(imageArray: imagesString, selectedIndex: indexPath.row)
    }
    
    @objc func seeAllBtnClicked(sender: UIButton!){
        if sender.tag == 0 {
            if delegate != nil {
                delegate?.videoViewNavigation()
            }
        }
        else if sender.tag == 1 {
            if delegate != nil {
                delegate?.photoViewNavigation()
            }
        }
    }
    
    @objc func FollowBtnBtnAction(sender: UIButton!){
        if UserDefaults.standard.value(forKey: "User_Id") != nil {
            if businessDetailsDataModel?.information.is_followed == "1" {
                if (UserDefaults.standard.value(forKey: "Followed_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Followed_Status_Value") as? String))
                    if status == "1" {
                        statusBool = "0"
                    }
                    else {
                        statusBool = "1"
                    }
                }
                else {
                    statusBool = "0"
                }
            }
            else  {
                if (UserDefaults.standard.value(forKey: "Followed_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Followed_Status_Value") as? String))
                    if status == "1" {
                        statusBool = "0"
                    }
                    else {
                        statusBool = "1"
                    }
                }else {
                    statusBool = "1"
                }
            }
            
            if delegate != nil {
                delegate?.followBtnServiceCall()
            }
        }
        else {
            if delegate != nil {
                delegate?.followBtnNavigation()
            }
        }
    }
    
    @objc func locationBtnAction() {
        delegate?.LocationBtnAction()
    }
    
    @objc func whatsAppBtnAction() {
        delegate?.WhatsAppBtnAction()
    }
    
    @objc func messageBtnAction() {
        delegate?.MessageBtnAction()
    }
    
    @objc func callBtnAction() {
        delegate?.CallBtnAction()
    }
}
