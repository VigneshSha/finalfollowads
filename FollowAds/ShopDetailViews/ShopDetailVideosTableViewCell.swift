
//
//  ShopDetailVideosTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import YouTubePlayer

class ShopDetailVideosTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var cell : VideosCollectionViewCell?
    
    @IBOutlet var collectionView: UICollectionView!
    var visibleIP : IndexPath = IndexPath.init(row: 0, section: 0)
    var aboutToBecomeInvisibleCell = -1
    var indexValue: Int?
    var businessDetailDataModel = FollowAdsBusinessDetailDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "VideosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "videosCellId")
        if let videoCell = collectionView.cellForItem(at: visibleIP) as? VideosCollectionViewCell
        {
        }
        // Initialization code
    }
    
    /*
     This method is used to perform shop detail service call.
     @param -.
     @return -.
     */
    func shopDetailUpdate() {
        self.followAdsRequestManager.businessDetailRequestManager = FollowAdsBusinessDetailRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = "297"
        }
        
        self.followAdsRequestManager.businessDetailRequestManager?.business_id = "1"
        
        let businessDetailCompletion: BusinessDetailCompletionBlock = {(response, error) in
            if let _ = error {
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.businessDetailDataModel = response!
                        print(self.businessDetailDataModel)
                        self.collectionView.reloadData()
                    })
                }
            }
        }
        FollowAdsServiceHandler.callBusinessDetailServiceCall(requestObject: self.followAdsRequestManager, businessDetailCompletion)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if businessDetailDataModel.information.business_video.count != nil {
                return businessDetailDataModel.information.business_video.count
            }
            else {
                return 1
            }
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        cell   = collectionView.dequeueReusableCell(withReuseIdentifier: "videosCellId", for: indexPath) as? VideosCollectionViewCell
        cell?.videoUrl = businessDetailDataModel.information.business_video[indexPath.row].business_vid
        cell?.playBtn.tag = indexPath.row
        cell?.playBtn.isHidden = false
        if cell?.videoUrl == "" {
            cell?.videoImageView.image = UIImage(named: "business_Placeholder")
        }
        cell?.videoImageView.isHidden = false
        cell?.VideoPlayerView.isHidden = true
        cell?.layer.shouldRasterize = true
        cell?.layer.rasterizationScale = UIScreen.main.scale
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 2.3
        let hardCodedPadding:CGFloat = 0
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cellfds : VideosCollectionViewCell  = (collectionView.cellForItem(at: indexPath) as! VideosCollectionViewCell) else { return }
        DispatchQueue.main.async(execute: {
            cellfds.playBtn.isHidden = false
            cellfds.videoImageView.isHidden = false
            cellfds.VideoPlayerView.isHidden = true
            cellfds.VideoPlayerView.stop()
            cellfds.VideoPlayerView.clear()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cellfds : VideosCollectionViewCell  = (collectionView.cellForItem(at: indexPath) as! VideosCollectionViewCell) else { return }
        indexValue = indexPath.row
        DispatchQueue.main.async(execute: {
            cellfds.playBtn.isHidden = true
            cellfds.videoImageView.isHidden = true
            cellfds.VideoPlayerView.isHidden = false
            cellfds.VideoPlayerView.stop()
            cellfds.VideoPlayerView.clear()
            cellfds.VideoPlayerView.play()
        })
    }
    
    @objc func playBtnAction(_ sender: UIButton) {
        cell?.playBtn.isHidden = true
        cell?.videoImageView.isHidden = true
        cell?.VideoPlayerView.isHidden = false
        cell?.VideoPlayerView.stop()
        cell?.VideoPlayerView.play()
    }
}
