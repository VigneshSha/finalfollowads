//
//  ShopDetailTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class ShopDetailTableViewCell: UITableViewCell {
    
    @IBOutlet var shopImgView: UIImageView!
    @IBOutlet var shopNameLabel: UILabel!
    @IBOutlet var shopAddrLabel: UILabel!
    @IBOutlet var shopReviewsLabel: UILabel!
    @IBOutlet var shopDistanceLabel: UILabel!
    @IBOutlet var followBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        followBtn.layer.borderWidth = 1
        followBtn.layer.borderColor = appThemeColor.cgColor
        followBtn.layer.cornerRadius = 5.0
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
