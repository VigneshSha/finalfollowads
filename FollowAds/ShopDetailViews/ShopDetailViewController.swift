//
//  ShopDetailViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import MBProgressHUD
import MessageUI
import SKPhotoBrowser
import FirebaseAnalytics
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

class ShopDetailViewController: FollowAdsBaseViewController, NavigationsDelegate, UIGestureRecognizerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet var tableView: UITableView!
    var header: UITableViewHeaderFooterView? = nil
    var labelHeader: UILabel!
    var seeAllBtn: UIButton!
    var shopDetailDatasource: ShopDetailTableDatasource?
    var img: String?
    var followAdsRequestManager = FollowAdsRequestManager()
    var businessDetailDataModel = FollowAdsBusinessDetailDataModel()
    var businessContactNum: String?
    var imageCell: ShopDetailPhotosTableViewCell?
    var followBusinessDataModel = FollowAdsFollowBusinessDataModel()
    var businessIdValue: String?
    var businessAddrIdValue: String?
    var selectedIndex: IndexPath?
    var contactNum = String()
    var landLineNum = String()
    var nodataView: NoDataView?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var banner = Banner()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        self.setupTableViewDataSource()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        if Reachability.isConnectedToNetwork() == true {
            self.tableView.isHidden = false
            self.shopDetailUpdate()
        }
        else {
            self.tableView.isHidden = true
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        UserDefaults.standard.removeObject(forKey: "Followed_Status_Value")
        noDataLabel.text = "No Data Available".localized()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.layer.backgroundColor = UIColor(red: 183.0 / 255.0, green: 29.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0).cgColor
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.setUpNavigationBarButton()
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
        if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    /*
     This method is used to display the nodataView if no records are available.
     @param --.
     @return --.
     */
    func setupNoDataView() {
        self.navigationController?.isNavigationBarHidden = false
        let noDataViewFrame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        nodataView = NoDataView.init(frame: noDataViewFrame)
        nodataView?.isHidden = false
        self.view.addSubview(nodataView!)
    }
    
    /*
     This method is used to perform shop detail service call.
     @param -.
     @return -.
     */
    func shopDetailUpdate() {
        self.followAdsRequestManager.businessDetailRequestManager = FollowAdsBusinessDetailRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.user_id = ""
        }
        if businessIdValue == nil {
            self.followAdsRequestManager.businessDetailRequestManager?.business_id = ""

        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.business_id = businessIdValue

        }
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.user_lng = userLng
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.businessDetailRequestManager?.lang_id = langId
       
        if businessAddrIdValue == nil {
            self.followAdsRequestManager.businessDetailRequestManager?.business_address_id = ""

        }
        else {
            self.followAdsRequestManager.businessDetailRequestManager?.business_address_id = businessAddrIdValue

        }
        
        let businessDetailCompletion: BusinessDetailCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.businessDetailDataModel = response!
                        print(self.businessDetailDataModel)
                        self.tableView.isHidden = false
                        self.nodataView?.isHidden = true
                        self.nodataView?.removeFromSuperview()
                        self.shopDetailDatasource?.businessDetailsDataModel = self.businessDetailDataModel
                        self.contactNum = self.businessDetailDataModel.information.busines_address.business_phone_no!
                         self.landLineNum = self.businessDetailDataModel.information.busines_address.business_landline_number!
                        shopReviewsCount = self.businessDetailDataModel.information.business_reviews_count!
                        shopInterestCount = self.businessDetailDataModel.information.business_followed_count!
                        shopDistance = self.businessDetailDataModel.information.busines_address.business_distance!
                        self.businessContactNum = self.businessDetailDataModel.information.busines_address.business_phone_no
                        shopImagesCount = String(describing: self.businessDetailDataModel.information.business_images.count)
                        shopVideosCount = String(describing: self.businessDetailDataModel.information.business_video.count)
                        
                        self.shopDetailDatasource?.landLineNum = self.landLineNum
                        self.shopDetailDatasource?.mobNum = self.contactNum
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callBusinessDetailServiceCall(requestObject: self.followAdsRequestManager, businessDetailCompletion)
    }
    
    /*
     This method is used to perform follow business service call.
     @param -.
     @return -.
     */
    func followBusinessUpdate() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.followBusinessRequestManager = FollowAdsFollowBusinessRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.followBusinessRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.followBusinessRequestManager?.user_id = ""
            }
           
            if businessIdValue == nil {
                self.followAdsRequestManager.followBusinessRequestManager?.business_id = ""

            }
            else {
                self.followAdsRequestManager.followBusinessRequestManager?.business_id = businessIdValue
            }
            
            if businessAddrIdValue == nil {
                self.followAdsRequestManager.followBusinessRequestManager?.business_address_id = ""
                
            }
            else {
                self.followAdsRequestManager.followBusinessRequestManager?.business_address_id = businessAddrIdValue
            }
            
            if statusBool == "" {
                self.followAdsRequestManager.followBusinessRequestManager?.followed_status = statusBool
            }
            else {
                self.followAdsRequestManager.followBusinessRequestManager?.followed_status = statusBool
            }
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.followBusinessRequestManager?.lang_id = langId
            
            let followBusinessCompletion: FollowBusinessCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                }
                else
                {
                    if response != nil {
                        self.followBusinessDataModel = response!
                        print(self.followBusinessDataModel)
                        UserDefaults.standard.set(self.followBusinessDataModel.count, forKey: "FOLLOWED_STORES_COUNT")
                        UserDefaults.standard.set(self.followBusinessDataModel.status, forKey: "Followed_Status_Value")
                        
                        let offset = self.tableView.contentOffset
                        DispatchQueue.main.async(execute: {
                            UIView.animate(withDuration: 0, animations: {
                                self.tableView.reloadData()
                            }, completion:{ _ in
                                self.tableView.endUpdates()
                                self.tableView.contentOffset = offset
                                self.tableView.beginUpdates()
                            })
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    }
                }
            }
            FollowAdsServiceHandler.callFollowBusinessServiceCall(requestObject: self.followAdsRequestManager, followBusinessCompletion)
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: locationAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        let navLabel = UILabel()
        navLabel.frame = CGRect(x: 0, y: 0, width: 0, height: 44)
        let navTitle = NSMutableAttributedString(string: "FollowAds", attributes:[
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!])
        
//        navTitle.append(NSMutableAttributedString(string: "™", attributes:[
//            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!,
//            NSAttributedStringKey.foregroundColor: UIColor.white]))
        
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        
//        self.navigationController?.navigationBar.titleTextAttributes =
//            [NSAttributedStringKey.foregroundColor: UIColor.white,
//             NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!]
    }
    
    /*
     This method is used for Setup tableview data source.
     @param --.
     @return --.
     */
    func setupTableViewDataSource() {
        self.shopDetailDatasource = ShopDetailTableDatasource(bannerImgData: img, businessDetailData: businessDetailDataModel)
        self.tableView.dataSource = shopDetailDatasource
        self.tableView.delegate = shopDetailDatasource
        shopDetailDatasource?.tableView = self.tableView
        shopDetailDatasource?.delegate = self
        self.tableView.tableFooterView = UIView()
        shopDetailDatasource?.landLineNum = self.landLineNum
        shopDetailDatasource?.mobNum = self.contactNum
    }
    
    func followBtnServiceCall() {
        self.addLoadingView()
        if Reachability.isConnectedToNetwork() == true {
            Analytics.logEvent("Follow_store", parameters: [
                "name": "Follow store" as NSObject
                ])
            AppsFlyerTracker.shared().trackEvent("Follow_store",
                                                 withValues: [
                                                    AFEventAdClick: "Follow store",
                                                    ]);
            self.followBusinessUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
    }
    
    func navigateToPhotoView(imageArray : [String]?, selectedIndex : Int)
    {
        var images = [SKPhoto]()
        for image in (imageArray?.enumerated())! {
            let imageView = UIImageView()
            if let encodedString  = image.element.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))
            }
            let photo = SKPhoto.photoWithImage(imageView.image!)// add some UIImage
            images.append(photo)
        }
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(selectedIndex)
        present(browser, animated: true, completion: {})
    }
    
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is FollowedStoresViewController {
                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                if self.businessDetailDataModel.information.advertisement[0].advertisement_id != nil {
                    nextViewController.offerId = self.businessDetailDataModel.information.advertisement[0].advertisement_id ?? ""
                }
                self.navigationController?.pushViewController(nextViewController, animated: true
                )
            }
            else {
                self.navigationController?.popViewController(animated: true)

            }
        }
    //    self.navigationController?.popViewController(animated: true)
    }
    
    func followBtnNavigation() {
//        self.navigationController?.isNavigationBarHidden = false
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewId") as! LoginViewController
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
//        self.banner = Banner(title: "Please Login to Follow this Business".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//        self.banner.dismissesOnTap = true
//        self.banner.show(duration: 3.0)
        self.view.makeToast("Please Login to Follow this Business".localized(), duration: 3.0, position: .center)

        
    }
    
    func MessageBtnAction() {
        if (MFMessageComposeViewController.canSendText()) {
            if contactNum == "" {
                
            }
            else {
                if UserDefaults.standard.value(forKey: UserName) != nil {
                    let controller = MFMessageComposeViewController()
                    let usernameText = String()
                    let username: String = usernameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
                    let bus_name: String = self.businessDetailDataModel.information.name ?? ""
                    let hiText = "Hi "
                    let ImText = "I'm "
                    let followadsText = ", from FollowAds..."
                    controller.body = "\(hiText.localized())\(String(describing: bus_name)).\n\(ImText.localized())\(username)\(followadsText.localized())"
                    controller.recipients = [contactNum]
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
                else {
//                    self.banner = Banner(title: "Please Login to start the conversation".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                    self.banner.dismissesOnTap = true
//                    self.banner.show(duration: 3.0)
                    self.view.makeToast("Please Login to start the conversation".localized(), duration: 3.0, position: .center)

                }
            }
            
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Messages cannot be able to send.".localized())
        }
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    func CallBtnAction() {
        if landLineNum == "" {
            
        }
        else {
            let trimmedString = landLineNum.trimmingCharacters(in: .whitespaces)

            let url: NSURL = URL(string: "tel://\(trimmedString)")! as NSURL
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
    
    func WhatsAppBtnAction() {
        if contactNum == "" {
            
        }
        else {
            if UserDefaults.standard.value(forKey: UserName) != nil {
                let usernameText = String()
                let username: String = usernameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
                let bus_name: String = self.businessDetailDataModel.information.name ?? ""
                let hiText = "Hi "
                let ImText = "I'm "
                let followadsText = ", from FollowAds..."
                let urlWhats = "https://wa.me/91\(contactNum)?text=\(hiText.localized())\(String(describing: bus_name)).\n\(ImText.localized())\(username)\(followadsText.localized())"
                if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                    if let whatsappURL = URL(string: urlString) {
                        if UIApplication.shared.canOpenURL(whatsappURL) {
                            UIApplication.shared.openURL(whatsappURL)
                        } else {
                            let alert = UIAlertController(title: alertTitle.localized(), message: "Please install the whatsapp".localized(), preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK".localized(), style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)                }
                    }
                }
            }
            else {
//                self.banner = Banner(title: "Please Login to start the conversation".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                self.banner.dismissesOnTap = true
//                self.banner.show(duration: 3.0)
                self.view.makeToast("Please Login to start the conversation".localized(), duration: 3.0, position: .center)

            }
        }
        
    }
    
    func LocationBtnAction() {
//        let lat: String = businessDetailDataModel.information.busines_address.latitude!
//        let lng: String = businessDetailDataModel.information.busines_address.latitude!
        
//        if let latd: Double = Double(lat) {
//            if let lngt: Double = Double(lng) {
        
//                let latitude: CLLocationDegrees = Double(latd)
//                let longitude: CLLocationDegrees = Double(lngt)
        
        let urlstring = self.businessDetailDataModel.information.busines_address.map_url
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    if #available(iOS 10.0, *) {
                        //                        UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitude),\(longitude)&zoom=14&q=\(String(describing: businessDetailDataModel.information.name!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!))")!, options: [:], completionHandler: nil)
                        UIApplication.shared.openURL(NSURL(string:"\(String(describing: encodedString))")! as URL)
                    } else {
                        // Fallback on earlier versions
                        if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
                        {
                            //                            UIApplication.shared.openURL(NSURL(string:"https://maps.google.com//?center=\(latitude),\(longitude)&zoom=14&q=\(String(describing: businessDetailDataModel.information.name!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!))")! as URL)
                            UIApplication.shared.openURL(NSURL(string:"\(String(describing: encodedString))")! as URL)
                        }
                    }
                }
                else {
                    if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL))
                    {
                        //                        UIApplication.shared.openURL(NSURL(string:"https://maps.google.com//?center=\(latitude),\(longitude)&zoom=14&q=\(String(describing: businessDetailDataModel.information.name!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!))")! as URL)
                        UIApplication.shared.openURL(NSURL(string:"\(String(describing: encodedString))")! as URL)
                    }
                }
                //      }
                //    }
                
            }
        }
        
    }
    
    func photoViewNavigation() {
        self.trackViewcontroller(name: "View_Store_Gallery", screenClass: "ShopDetailViewController")
        AppsFlyerTracker.shared().trackEvent("View_Store_Gallery",
                                             withValues: [
                                                AFEventAdView: "View_Store_Gallery",
                                                ]);
        self.performSegue(withIdentifier: "PhotosViewId", sender: self)
    }
    
    func videoViewNavigation() {
        self.trackViewcontroller(name: "View_Store_Gallery", screenClass: "ShopDetailViewController")
        
        self.performSegue(withIdentifier: "VideosViewId", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: PhotosCollectionViewController.self) {
            let vc = segue.destination as? PhotosCollectionViewController
            vc?.businessId = businessIdValue
            vc?.businessAddrId = businessAddrIdValue
        }
        else if segue.destination.isKind(of: VideosCollectionViewController.self) {
            let vc = segue.destination as? VideosCollectionViewController
            vc?.businessId = businessIdValue
            vc?.businessAddrId = businessAddrIdValue
        }
    }
    
}
