//
//  SearchLocationViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 28/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import Foundation
import GooglePlaces
import AppsFlyerLib

class SearchLocationViewController: FollowAdsBaseViewController, UISearchBarDelegate {
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var searchBar: UISearchBar?
    var tableDataSource: GMSAutocompleteTableDataSource?
 //   let GoogleMapsAPIServerKey = "AIzaSyAw1EUIPb7qT-yvkzjEspIEiPStYpY4GlU"
    let GoogleMapsAPIServerKey = "AIzaSyCiegoMGd23wvMrbM5mVHKlYzJ8TjKsmwM"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let autocompleteController = GMSAutocompleteViewController()
        //  autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
        resultsViewController = GMSAutocompleteResultsViewController()
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

