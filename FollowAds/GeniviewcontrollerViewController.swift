//
//  GeniviewcontrollerViewController.swift
//  FollowAds
//
//  Created by IOS Developer on 27/01/20.
//  Copyright © 2020 com.vijai.appname. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
protocol popupadsdelegate {
    func navigateTopopupads(indexPath: IndexPath)
}
class GeniviewcontrollerViewController:FollowAdsBaseViewController {
    var colVwCell: UICollectionViewCell?
      var imageid: String?
    var popupadslistDataModel: FollowAdsPopupAdsDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
      var delegate: popupadsdelegate?
    var categoryListDataModel = FollowAdsCategoryListDataModel()
        var categoryTableDatasource: CategoryTableDatasource?
    var images:[UIImage]  = [UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!,UIImage(named:"FollowAds_LoginLogo")!]
    @IBOutlet weak var CollectionView: UICollectionView!
    override func viewDidLoad() {
            super.viewDidLoad()
          
        CollectionView.reloadData()
        CollectionView.dataSource =  self
        CollectionView.delegate = self
       
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let backButton: UIBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = backButton;
        displaypopup()
        CollectionView.reloadData()
        CollectionView.dataSource =  self
        CollectionView.delegate = self
    }
    @objc func back() {
        self.dismiss(animated: true, completion: nil)
           }
     func displaypopup(){
            if UserDefaults.standard.value(forKey: City) != nil{
                       self.followAdsRequestManager.popupRequestManager = FollowAdspopupadsRequestManager()
                       if UserDefaults.standard.value(forKey: UserId) != nil {
                           let userIdText = String()
                           let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                           self.followAdsRequestManager.popupRequestManager?.user_id = userId
                           print("nksnsf",userId)
                       }
                       else {
                           self.followAdsRequestManager.popupRequestManager?.user_id = ""
                       }
                       if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                           let deviceTokenText = String()
                           let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                           self.followAdsRequestManager.popupRequestManager?.device_token = deviceToken
                       }
                       else {
                           self.followAdsRequestManager.popupRequestManager?.device_token = ""
                       }
                       self.followAdsRequestManager.popupRequestManager?.device_name = kGetDeviceName
                       if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                           let userLatText = String()
                           let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                           self.followAdsRequestManager.popupRequestManager?.user_lat = userLat
                       }
                       if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                           let userLngText = String()
                           let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                           self.followAdsRequestManager.popupRequestManager?.user_lng = userLng
                        
                       }
                       let langidText = String()
                       let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
                       self.followAdsRequestManager.popupRequestManager?.lang_id = langId
                       if UserDefaults.standard.value(forKey: City) != nil{
                           let cityName = String()
                           let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                           self.followAdsRequestManager.popupRequestManager?.area = city
                       }
                       else {
                           self.followAdsRequestManager.popupRequestManager?.area = ""
                           
                       }
                       if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                           let postalCodeName = String()
                           let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                           self.followAdsRequestManager.popupRequestManager?.postal_code = postalCode
                       }
                       else {
                           self.followAdsRequestManager.popupRequestManager?.postal_code = ""
                           
                       }
            let PopupAdsCompletion: PopupAdsCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
                   // FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                }
                else
                {
                    if response != nil {
                        DispatchQueue.main.async(execute: {
                        self.popupadslistDataModel = response!
                                print("sfjbjsfnb",self.popupadslistDataModel)
                            print("dgdgdgdg",self.popupadslistDataModel?.images[0].ads_list.count)
                            self.navigationController?.navigationBar.topItem?.title = self.popupadslistDataModel?.images[0].advertisement_name
                            self.CollectionView.reloadData()
                        
    //                        self.imageview()
                          //  self.genicontroller?.popupadslistDataModel = self.popupadslistDataModel
                          
                                        //  self.collectionView.reloadData()
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    }
                }
            }
                
            FollowAdsServiceHandler.callpopupadsservicecalls(requestObject: self.followAdsRequestManager, PopupAdsCompletion)
            }

          }
   
}

extension GeniviewcontrollerViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
            return 1
        }
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//            return images.count
    //        print("fff",self.popupadslistDataModel.images[0].ads_list.count)

    if section == 0 {
        if (self.popupadslistDataModel?.images[0].ads_list.count) != nil {
            if (((self.popupadslistDataModel?.images[0].ads_list.count)!) ) >= 1 {
                     return 4
                 }
                 else {
    
                return (((self.popupadslistDataModel?.images[0].ads_list.count)!))
                 }
             }
             else {
                 return 0
             }
         }
         else {
             return 0
         }
       
     }
        
        
       
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    print("egrget",images[indexPath.item])
//
//            let cell : GeniCollectionViewCell = CollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GeniCollectionViewCell
//            cell.image.image = images[indexPath.row]
            
    
            if indexPath.section == 0 {
                       let cell : GeniCollectionViewCell = CollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GeniCollectionViewCell

    //                       cell.offerImageVw.layer.cornerRadius = cell.offerImageVw.frame.size.width / 2
    //                       cell.offerImageVw.clipsToBounds = true

                if (self.popupadslistDataModel?.images[0].ads_list.count) != nil {
                    print("ddgsdsf",self.popupadslistDataModel?.images[0].ads_list[indexPath.row].full_file_name as Any)
                    let urlstring = self.popupadslistDataModel?.images[0].ads_list[indexPath.row].full_file_name

                               if urlstring != nil {
                                   if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {

                                       cell.image.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                                    // cell.image.sizeToFit()
                                   }
                               }
                               else {
                                   cell.image.image = UIImage(named: "home_ads_placeholder")
                               }

                           }
                           else {

                               cell.image.image = UIImage(named: "home_ads_placeholder")
                       }

                           return cell
                       }
            else {
                return colVwCell!
            }
//            return cell
    }
 
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferListTableViewController") as! OfferListTableViewController
                           self.navigationController?.pushViewController(vc, animated: true)
        vc.passingoffertitle = (self.popupadslistDataModel?.images[0].advertisement_name)!
        if let imageid = self.popupadslistDataModel?.images[0].ads_list[indexPath.row].id, !(self.popupadslistDataModel?.images[0].ads_list[indexPath.row].id!.isEmpty)! {
                                                  self.imageid = imageid
                                                  UserDefaults.standard.set(imageid, forKey: "image_id")
                                                  UserDefaults.standard.synchronize()
                                                  print("ssgsgfsfsf",imageid)
                                              }
        print("gdffx",images.count)
        for data in images{
            vc.logoImages.append(data)
            print("sfsfsf",  vc.logoImages.count)
        }
        
        }
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        let width = (self.view.frame.size.width - 12 * 3) / 3 //some width
//        let height = width * 1.5 //ratio
//        return CGSize(width: width, height: height)
//    }
}
 
