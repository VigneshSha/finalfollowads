//
//  CategoryTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class CategoryTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var catImg : [String]?
    var catName : [String]?
    
    init(catImgModel: [String]?, catNameModel: [String]?){
        self.catImg = catImgModel
        self.catName = catNameModel
        super.init()
    }
    
    // MARK: TableView Delegates and Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catName?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let categoryCell: CategoryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "CategoryCellId") as? CategoryTableViewCell
        categoryCell.labelCategoryName.text = catName?[indexPath.row]
        categoryCell.categoryIconsImgVw.image = UIImage(named: catImg?[indexPath.row] ?? "")
        return categoryCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
