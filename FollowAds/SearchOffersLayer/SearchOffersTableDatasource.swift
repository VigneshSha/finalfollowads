//
//  SearchOffersTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 29/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol customNavigationDelegate {
    func selctDataFromTable(value : IndexPath)
}

class SearchOffersTableDatasource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var header: UITableViewHeaderFooterView? = nil
    var labelHeader: UILabel!
    var customDelegate : customNavigationDelegate?
    var suggestedSearchDataModel : FollowAdsSearchSuggestionDataModel?
    var filteredData: [String]!

    init(data : FollowAdsSearchSuggestionDataModel!) {
        self.suggestedSearchDataModel = data
        super.init()
    }
    
    // MARK:- TABLEVIEW DELEGATES AND DATASOURCES

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (suggestedSearchDataModel?.searchname.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchOffersCellId", for: indexPath) as! SearchOffersTableViewCell
        cell.searchLabel.text = suggestedSearchDataModel?.searchname[indexPath.row].category_name
        cell.searchImageView.image = UIImage(named: "arrowLine")
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: tableView.sectionHeaderHeight))
        view.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view.contentView.backgroundColor = UIColor.white
        return view
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        header = view as? UITableViewHeaderFooterView
        // create a label for section header.
        labelHeader = UILabel(frame: CGRect(x: 15, y: 12, width: 150, height: 20));
        header?.addSubview(labelHeader!)
        labelHeader?.textColor = appThemeColor
        labelHeader?.font = (UIFont.mediumSystemFont(ofSize: 14))
        labelHeader?.text = "TRENDING".localized()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if suggestedSearchDataModel?.searchname[indexPath.row].category_name == "" {
            return 0
        }
        else {
            return 50
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = customDelegate {
            delegate.selctDataFromTable(value: indexPath)
        }
    }
}
