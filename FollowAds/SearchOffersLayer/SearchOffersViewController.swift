//
//  SearchOffersViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 29/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class SearchOffersViewController: FollowAdsBaseViewController, UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate,customNavigationDelegate, UIScrollViewDelegate {
    
    var followAdsRequestManager = FollowAdsRequestManager()
    @IBOutlet var tableView: UITableView!
    var searchController = UISearchController()
    var filteredData: [String]!
    var textField: UITextField?
    lazy var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 30, height: 20))
    var searchOffersTableDatasource: SearchOffersTableDatasource?
    var offerItems = ["Shoes", "Garments", "Gems", "Pants", "Lights", "Chappels", "Shirts", "Dresses"]
    var suggestedSearchDataModel : FollowAdsSearchSuggestionDataModel?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var cancelButton = UIButton()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = "Search for Offers".localized()
        searchBar.showsCancelButton = true
        let view: UIView = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self){
                subView.tintColor = UIColor.black
            }
        }
        searchBar.delegate = self
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        self.suggestedSearchDataModel = FollowAdsSearchSuggestionDataModel()
        self.suggestedSearchDataModel?.searchname = [FollowAdsSearchSuggestionDataModel.FollowAdsSearchNameModel()]
        setupTableViewDataSource()
        filteredData = offerItems
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.isHidden = true
        
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.isEnabled = true
            cancelButton.setTitle("Cancel".localized(using: buttonTitles), for: .normal)
        }
        searchBar.becomeFirstResponder()
        if Reachability.isConnectedToNetwork() == true {
            self.callSearchSuggestionData()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        
        if let pushId = notification.userInfo?["pushnotification_id"] as? String {
            vc.pushnotificationId = pushId
            vc.notifiId = "1"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }

    /*
     This method is used to call the search suggestions data service.
     @param -.
     @return -.
     */
    func callSearchSuggestionData() {
        self.followAdsRequestManager.serachSuggestionsRequestManager = FollowAdsSearchSuggestionRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.serachSuggestionsRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.serachSuggestionsRequestManager?.user_id = ""
        }
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.serachSuggestionsRequestManager?.user_lat = userLat
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.serachSuggestionsRequestManager?.user_lng = userLng
        
        let searchSuggestionCompletion: SearchSuggestionCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    self.tableView.isHidden = false
                    DispatchQueue.main.async(execute: {
                        self.suggestedSearchDataModel = response
                        self.searchOffersTableDatasource?.suggestedSearchDataModel = self.suggestedSearchDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callSearchSuggestionServiceCall(requestObject: self.followAdsRequestManager, searchSuggestionCompletion)
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.isEnabled = true
            cancelButton.setTitle("Cancel".localized(using: buttonTitles), for: .normal)
        }
        
    }
    
    // Searchbar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        // Hide the cancel button
        searchBar.resignFirstResponder()
        if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.isEnabled = true
            self.navigationController?.popViewController(animated: true)
        }
        // You could also change the position, frame etc of the searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchBar.text?.count ?? 0 > 3 {
            searchBar.resignFirstResponder()

            let VC = UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
            //to push that controller on the stack
            VC.selectedString = searchText
            VC.searchBarText = searchText
            self.searchBar.text = ""
            self.navigationController?.pushViewController(VC, animated: false)

//        }
        
    }
    
    /*
     This method is used to setup table view datasource.
     @param --.
     @return --.
     */
    func setupTableViewDataSource() {
        searchOffersTableDatasource = SearchOffersTableDatasource(data: self.suggestedSearchDataModel)
        self.tableView.dataSource = searchOffersTableDatasource
        self.tableView.delegate = searchOffersTableDatasource
        searchOffersTableDatasource?.tableView = self.tableView
        self.searchOffersTableDatasource?.customDelegate = self
        self.tableView.tableFooterView =  UIView()
    }
    
    /*
     This method is used to update the search results.
     @param --.
     @return --.
     */
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filteredData = searchText.isEmpty ? offerItems : offerItems.filter({(dataString: String) -> Bool in
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
        } else {
            filteredData = offerItems
        }
        tableView?.reloadData()
    }
    
    /*
     This method is used to perform didSelect delegate method action.
     @param --.
     @return --.
     */
    func selctDataFromTable(value : IndexPath)
    {
        searchBar.resignFirstResponder()
        let VC = UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        //to push that controller on the stack
        VC.selectedString = "$"
       
        
        
        VC.selectedString?.append(self.suggestedSearchDataModel?.searchname[value.row].category_name ?? "")
        
        VC.searchBarText = ""
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
