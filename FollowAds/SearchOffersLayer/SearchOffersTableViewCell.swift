//
//  SearchOffersTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 29/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class SearchOffersTableViewCell: UITableViewCell {
    
    @IBOutlet var searchImageView: UIImageView!
    @IBOutlet var searchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
