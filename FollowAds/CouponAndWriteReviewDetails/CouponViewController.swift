//
//  ViewController.swift
//  FollowAdsCouponCode
//
//  Created by openwave on 21/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

@available(iOS 10.0, *)
class CouponViewController: FollowAdsBaseViewController, ScratchCardImageViewDelegate {
    
    @IBOutlet weak var CouponCodeLbl: UILabel!
    @IBOutlet weak var RemindView: UIView!
    @IBOutlet weak var OfferLbl: UILabel!
    @IBOutlet weak var ReminderSwitch: UISwitch!
    @IBOutlet weak var ReminderLbl: UILabel!
    @IBOutlet weak var DatePicker: UIDatePicker!
    @IBOutlet weak var ToolBar: UIToolbar!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet var couponImageView: ScratchCardImageView!
    var advertiseId: String?
    var businessId: String?
    var isScratched: Bool? = true
    @IBOutlet weak var remindMeLbl: UILabel!
    @IBOutlet weak var validTillLabel: UILabel!
    let toolBar = UIToolbar()
    var offerCode: String?
    var usedOfferCodeDataModel: FollowAdsUsedOfferCodeDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var savedReminderDataModel: FollowAdsSavedReminderDataModel?
    let delegate = UIApplication.shared.delegate as? AppDelegate
    var adCaption: String?
    var expireDataStr: String?
    var datePickerDateValue: String?
    var adReminderTime: String?
    var adReminderStatus: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var used_coupon: String?
    var isAlerdayNav : Bool = false
    var bus_addr_id: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.DatePicker.isHidden = true
        self.DatePicker.datePickerMode = .dateAndTime
        self.DatePicker.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        self.ToolBar.isHidden = true
//        DatePicker.minimumDate = Date()
        let rupee = "\u{20B9}"
        print(rupee)
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = CouponCodeLbl.bounds
     //   yourViewBorder.frame = CouponCodeLbl.frame

        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: CouponCodeLbl.bounds).cgPath
        CouponCodeLbl.layer.addSublayer(yourViewBorder)
        self.RemindView.layer.borderColor = UIColor(red: 231.0 / 255.0, green: 231.0 / 255.0, blue: 231.0 / 255.0, alpha: 1.0).cgColor
        self.RemindView.layer.borderWidth = 1.0
        self.OfferLbl.text = ("Buy 2 polos @ \u{20B9} 699")
        self.tabBarController?.tabBar.isHidden = true
        if used_coupon != nil {
        if used_coupon == "0" {
            couponImageView.image = UIImage(color: appThemeColor, size: couponImageView.frame.size)
        }
        else {
            couponImageView.image = UIImage(color: .clear, size: couponImageView.frame.size)

        }
        }
        else {
            
        }
        couponImageView.lineType = .square
        couponImageView.lineWidth = 20
        couponImageView.delegate = self
        self.setText()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpNavigationBarButton()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd-MM-yyyy"
        
        
            if expireDataStr != "" {
                let validDate = timeFormatter.date(from: expireDataStr ?? "")
                
                let validDateFormat = DateFormatter()
                validDateFormat.dateFormat = "dd"
                let validDay = validDateFormat.string(from: validDate!)
                
                let validMonthFormat = DateFormatter()
                validMonthFormat.dateFormat = "MMM"
                if Localize.currentLanguage() == "ta-IN"{
                    validMonthFormat.locale = Locale(identifier: "ta")
                }
                else {
                    validMonthFormat.locale = Locale(identifier: "en")
                }
                let validMonth = validMonthFormat.string(from: validDate!)
                
                let validYearFormat = DateFormatter()
                validYearFormat.dateFormat = "yyyy"
                let validYear = validYearFormat.string(from: validDate!)
                
                if Localize.currentLanguage() == "ta-IN"{
                    validTillLabel.text = validMonth + " " + validDay + ", " + validYear + " வரை செல்லுபடியாகும்"
                }
                else {
                    validTillLabel.text = "Valid till ".localized() + validMonth + " " + validDay + ", " + validYear
                }
                
            }
            else {
                validTillLabel.text = ""
            }
            
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = NSTimeZone.local

        let date = dateFormatter.date(from: expireDataStr!)
        DatePicker.maximumDate = date
 
        CouponCodeLbl.text = offerCode
        OfferLbl.text = adCaption
     //   validTillLabel.text = "Valid till ".localized() + expireDataStr!
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        if adReminderStatus == "1" {
        ReminderSwitch.isOn = true
            
        let timeFormatter2 = DateFormatter()
        timeFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if adReminderTime != "" {
            let validDate = timeFormatter2.date(from: adReminderTime ?? "")
            
            let validDateFormat = DateFormatter()
            validDateFormat.dateFormat = "dd"
            let validDay = validDateFormat.string(from: validDate!)
            
            let validMonthFormat = DateFormatter()
            validMonthFormat.dateFormat = "MMM"
            if Localize.currentLanguage() == "ta-IN"{
                validMonthFormat.locale = Locale(identifier: "ta")
            }
            else {
                validMonthFormat.locale = Locale(identifier: "en")
            }
            let validMonth = validMonthFormat.string(from: validDate!)
            
            let validYearFormat = DateFormatter()
            validYearFormat.dateFormat = "yyyy"
            let validYear = validYearFormat.string(from: validDate!)
        
        let hourFormat = DateFormatter()
        hourFormat.dateFormat = "HH"
        let numHour = hourFormat.string(from: validDate!)
        
        let minFormat = DateFormatter()
        minFormat.dateFormat = "mm"
        let numMin = minFormat.string(from: validDate!)
        
            if Localize.currentLanguage() == "ta-IN"{
                ReminderLbl.text = validMonth + " " + validDay + ", " + validYear + " " + numHour + ":" + numMin
            }
            else {
                ReminderLbl.text = "on " + validMonth + " " + validDay + ", " + validYear + " " + numHour + ":" + numMin
            }
        }
        else {
              ReminderLbl.text = adReminderTime

        }
        
        }
        else {
            ReminderSwitch.isOn = false
            ReminderLbl.text = ""

        }
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

        }
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
        remindMeLbl.text = remindMeText.localized()
        doneBtn.title = doneBtnText.localized()
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform used offer code service call.
     @param -.
     @return -.
     */
    func usedOfferCodeUpdate() {
        self.followAdsRequestManager.usedOfferCodeRequestManager = FollowAdsUsedOfferCodeRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.usedOfferCodeRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.usedOfferCodeRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.usedOfferCodeRequestManager?.advertisement_id = advertiseId
        self.followAdsRequestManager.usedOfferCodeRequestManager?.business_id = businessId
        self.followAdsRequestManager.usedOfferCodeRequestManager?.offer_code = CouponCodeLbl.text
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.usedOfferCodeRequestManager?.lang_id = langId
        
        let usedOfferCodeCompletion: UsedOfferCodeCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.usedOfferCodeDataModel = response!
                        print(self.usedOfferCodeDataModel as Any)
                        UserDefaults.standard.set(self.usedOfferCodeDataModel!.user_ads_save_count, forKey: "USER_COUPON_COUNT")
                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Congratulations!".localized())
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callUsedOfferCodeServiceCall(requestObject: self.followAdsRequestManager, usedOfferCodeCompletion)
    }
    
    /*
     This method is used to perform saved reminder service call.
     @param -.
     @return -.
     */
    func savedReminderUpdate() {
        self.followAdsRequestManager.savedReminderRequestManager = FollowAdsSavedReminderRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.savedReminderRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.savedReminderRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.savedReminderRequestManager?.advertisement_id = advertiseId
        if remindStatusValue == "" {
            self.followAdsRequestManager.savedReminderRequestManager?.remind_status = "0"
        }
        else {
            self.followAdsRequestManager.savedReminderRequestManager?.remind_status = remindStatusValue
        }
        if datePickerDateValue != nil {
            self.followAdsRequestManager.savedReminderRequestManager?.remind_time = datePickerDateValue
        }
        else {
            if UserDefaults.standard.value(forKey: "UnremindDate") != nil {
                let unremindText = String()
                let unremind: String = unremindText.passedString((UserDefaults.standard.object(forKey: "UnremindDate") as? String))
                self.followAdsRequestManager.savedReminderRequestManager?.remind_time = unremind
            }
            else {
            self.followAdsRequestManager.savedReminderRequestManager?.remind_time = ""
            }
        }
        
        if bus_addr_id != nil {
            self.followAdsRequestManager.savedReminderRequestManager?.business_address_id = bus_addr_id
        }
        else {
            self.followAdsRequestManager.savedReminderRequestManager?.business_address_id = ""
        }
        
        let savedReminderCompletion: SavedReminderCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.savedReminderDataModel = response!
                        print(self.savedReminderDataModel as Any)
                        UserDefaults.standard.set(self.savedReminderDataModel?.user_ads_save_count, forKey: "UserReminderCount")
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.navigationController?.popViewController(animated: true)

                    })
                }
            }
        }
        FollowAdsServiceHandler.callSavedReminderServiceCall(requestObject: self.followAdsRequestManager, savedReminderCompletion)
    }
    
    func scratchCardImageViewDidEraseProgress(eraseProgress: Float) {
        print(eraseProgress)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            // Your code with delay
            if Reachability.isConnectedToNetwork() == true {
                if self.isScratched == true {
                    self.usedOfferCodeUpdate()
                    self.isScratched = false
                }
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
            }
        }
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    @IBAction func ActionTrigeered(_ sender: UISwitch) {
        let onState = ReminderSwitch.isOn
        if onState{
            DatePicker.isHidden = false
            ToolBar.isHidden = false
            DatePicker.backgroundColor = UIColor.white
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(CouponViewController.datePickerValueChanged))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let RightArrowButton = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
            let buttonIcon1 = UIImage(named: "right-arrow")
            RightArrowButton.image = buttonIcon1
            let LeftArrowButton = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
            let buttonIcon2 = UIImage(named: "left-arrow")
            LeftArrowButton.image = buttonIcon2
            ToolBar.setItems([LeftArrowButton, RightArrowButton, spaceButton, doneButton], animated: false)
//
            print("Switch is on")
            remindStatusValue = "1"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.timeZone = NSTimeZone.local
            
            let date = dateFormatter.date(from: expireDataStr!)
            DatePicker.maximumDate = date
 
            
        } else {
            DatePicker.isHidden = true
            ToolBar.isHidden = true
            print("Switch is Off")
            self.ReminderLbl.text = ""
            datePickerDateValue = unremindDate
            remindStatusValue = "0"
            delegate?.scheduleNotification(at: DatePicker.date,identifier: "",title: "",body: "", userInfo: advertiseId!)

        }
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date = dateFormatter.string(from: DatePicker.date)
        print(date)
        datePickerDateValue = ("\(dateFormatter.string(from: DatePicker.date))")
        unremindDate = ("\(dateFormatter.string(from: DatePicker.date))")
        UserDefaults.standard.set(unremindDate, forKey: "UnremindDate")
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd"
        let stringDay = dateFormat.string(from: DatePicker.date)
        
        let monthFormat = DateFormatter()
        monthFormat.dateFormat = "MMM"
        if Localize.currentLanguage() == "ta-IN"{
            monthFormat.locale = Locale(identifier: "ta")
        }
        else {
            monthFormat.locale = Locale(identifier: "en")
        }
        let stringMonth = monthFormat.string(from: DatePicker.date)

        let yearFormat = DateFormatter()
        yearFormat.dateFormat = "yyyy"
        let numYear = yearFormat.string(from: DatePicker.date)
        
        let hourFormat = DateFormatter()
        hourFormat.dateFormat = "HH"
        let numHour = hourFormat.string(from: DatePicker.date)
        
        let minFormat = DateFormatter()
        minFormat.dateFormat = "mm"
        let numMin = minFormat.string(from: DatePicker.date)
        if Localize.currentLanguage() == "ta-IN"{
            ReminderLbl.text = stringMonth + " " + stringDay + ", " + numYear + " " + numHour + ":" + numMin
        }
        else {
            ReminderLbl.text = "on " + stringMonth + " " + stringDay + ", " + numYear + " " + numHour + ":" + numMin
        }
        UserDefaults.standard.set(ReminderLbl.text, forKey: "ReminderText")
        UserDefaults.standard.synchronize()
        var notificationString = String()
        notificationString = "Avail your offer " + adCaption!
        delegate?.scheduleNotification(at: DatePicker.date,identifier: "Polo",title: "Reminder",body: notificationString, userInfo: advertiseId!)

        DatePicker.isHidden = true
        ToolBar.isHidden = true
        self.view.endEditing(true)
    }
    
    /*
     This method is used to add the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @IBAction func DoneBtn(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func doneBtnAction(_ sender: UIBarButtonItem) {
        self.addLoadingView()
        let onState = ReminderSwitch.isOn
        if onState {
            if datePickerDateValue == "" {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please select reminder date and time".localized())
                unremindDate = ""
                UserDefaults.standard.set(unremindDate, forKey: "UnremindDate")
                UserDefaults.standard.synchronize()
                MBProgressHUD.hide(for: self.view, animated: true)

            }
            else {
                if Reachability.isConnectedToNetwork() == true {
                    if ReminderLbl.text == "" {
                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please select reminder date and time".localized())
                        unremindDate = ""
                        UserDefaults.standard.set(unremindDate, forKey: "UnremindDate")
                        UserDefaults.standard.synchronize()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    } else {
                        self.savedReminderUpdate()
                    }
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                }
            }
        }
        else {
            if Reachability.isConnectedToNetwork() == true {
                
                self.savedReminderUpdate()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
        }
       
        
     //   self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


