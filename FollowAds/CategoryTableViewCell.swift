//
//  CategoryTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 14/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryIconsImgVw: UIImageView!
    @IBOutlet weak var labelCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
