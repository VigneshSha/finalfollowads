//
//  OfferListTableViewCell.swift
//  FollowAds
//
//  Created by IOS Developer on 11/02/20.
//  Copyright © 2020 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferListTableViewCell: UITableViewCell {

    @IBOutlet weak var arrowimage: UIImageView!
    @IBOutlet weak var offerbigimage: UIImageView!
    @IBOutlet weak var offerthirdlabel: UILabel!
    @IBOutlet weak var offersecondlabel: UILabel!
    @IBOutlet weak var offerfirstlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
