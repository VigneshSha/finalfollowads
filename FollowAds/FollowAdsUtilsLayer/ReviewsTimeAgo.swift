//
//  ReviewsTimeAgo.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/10/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class ReviewsTimeAgo: NSObject {
    
    class func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest != now) ? now : date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return  "\(components.year!)\(" years ago".localized())"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago".localized()
            } else {
                return "Last year".localized()
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)\(" months ago".localized())"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago".localized()
            } else {
                return "Last month".localized()
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)\(" weeks ago".localized())"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago".localized()
            } else {
                return "Last week".localized()
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)\(" days ago".localized())"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago".localized()
            } else {
                return "Yesterday".localized()
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)\(" hours ago".localized())"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago".localized()
            } else {
                return "An hour ago".localized()
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)\(" minutes ago".localized())"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago".localized()
            } else {
                return "A minute ago".localized()
            }
        } else if (components.second! >= 3) {
            return "\(components.second!)\(" seconds ago".localized())"
        } else {
            return  "Just now".localized()
        }
    }
    
    class func convertTodateFromString(_ fromString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: fromString)!
        return date
    }
}
