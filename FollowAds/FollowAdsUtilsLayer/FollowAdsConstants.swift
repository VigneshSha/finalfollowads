//
//  FollowAdsConstants.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 17/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit


let sharedInstance = FollowAdsSharedInstance.sharedInstance
// app url
//let kBaseURL = "http://192.9.200.10/followads/public/api"
var isFromNotifi = false
var off_Notifi_Id : String?

let kDeepLink = "deepLink"

// Live Phase 2 Url
let kBaseURL = "http://api.followads.in/v1_0/api"
let NodeBaseUrl = "http://followads.in:8081/api"
let PROFILEIMAGEURL = "http://api.followads.in/v1_0/api/profile"
let ImageIdUrl = "https://media.followads.in/images/advertisement_images"

// Dev Url
//let kBaseURL = "http://103.249.207.132/followads_live/api"
//let PROFILEIMAGEURL = "http://103.249.207.132/followads_live/api/profile"




// Dev New Url  - current
//let kBaseURL = "http://113.193.25.22/followads_live/api"
//let PROFILEIMAGEURL = "http://113.193.25.22/followads_live/api/profile"

// Live Url
//let kBaseURL = "http://113.193.25.20/followads/public/api"
//let PROFILEIMAGEURL = "http://113.193.25.20/followads/public/api/profile"

// Testing Url
//let kBaseURL = "http://113.193.25.20/followads_new/public/api"
//let PROFILEIMAGEURL = "http://113.193.25.20/followads_new/public/api/profile"

// New Url
//let kBaseURL = "http://103.249.207.132/followads_new/api"
//let PROFILEIMAGEURL = "http://103.249.207.132/followads_new/api/profile"

// Staging Url
//let kBaseURL = "http://api.followads.in/vNxt/api"
//let PROFILEIMAGEURL = "http://api.followads.in/vNxt/api/profile"

//let kBaseURL = "http://192.9.200.10/followads_live/public"
//let PROFILEIMAGEURL = "http://192.9.200.10/followads_live/public/api/profile"

//let kBaseURL = "http://113.193.25.20/followads_live/public/api"
//let PROFILEIMAGEURL = "http://113.193.25.20/followads_live/public/api/profile"

// app color code
let appThemeColor: UIColor = UIColor(red: 183.0/255.0, green: 28.0/255.0, blue: 28.0/255.0, alpha: 1.0)
let profileReminderColor: UIColor = UIColor(red: 229.0/255.0, green: 81.0/255.0, blue: 73.0/255.0, alpha: 1.0)
let profileNotificationColor: UIColor = UIColor(red: 120/255, green: 214/255, blue: 117/255, alpha: 1.0)
let profileLocationColor: UIColor = UIColor(red: 54/255, green: 131/255, blue: 246/255, alpha: 1.0)
let profilePhoneNumberColor: UIColor = UIColor(red: 90/255, green: 86/255, blue: 208/255, alpha: 1.0)
let profileEmailColor: UIColor = UIColor(red: 259/255, green: 79/255, blue: 55/255, alpha: 1.0)
let profileLanguageColor: UIColor = UIColor(red: 253/255, green: 65/255, blue: 251/255, alpha: 1.0)
let profileBankDetailsColor: UIColor = UIColor(red: 254/255, green: 36/255, blue: 4/255, alpha: 1.0)
let profileLikedOffersColor: UIColor = UIColor(red: 248/255, green: 25/255, blue: 66/255, alpha: 1.0)
let profileFollowedStoresColor: UIColor = UIColor(red: 147/255, green: 83/255, blue: 4/255, alpha: 1.0)
let profileSavedCouponsColor: UIColor = UIColor(red: 147/255, green: 56/255, blue: 254/255, alpha: 1.0)
let profileUsedCouponsColor: UIColor = UIColor(red: 36/255, green: 144/255, blue: 145/255, alpha: 1.0)
let profileShareAppColor: UIColor = UIColor(red: 36/255, green: 143/255, blue: 81/255, alpha: 1.0)
let profileRateAppColor: UIColor = UIColor(red: 255/255, green: 147/255, blue: 1/255, alpha: 1.0)
let profileSendFeedbackColor: UIColor = UIColor(red: 147/255, green: 22/255, blue: 78/255, alpha: 1.0)
let profileAboutFollowAdsColor: UIColor = UIColor(red: 249/255, green: 23/255, blue: 66/255, alpha: 1.0)
let profilePhotoCellBgColor: UIColor = UIColor(red: 246/255, green: 248/255, blue: 247/255, alpha: 1.0)
let greyColor: UIColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)
let mapPointColor: UIColor = UIColor(red: 54/255, green: 117/255, blue: 136/255, alpha: 1.0)


let shopImages = ["Shop_Img", "MochiImg", "AngiImg"]
let shopTitles = ["Classic Polo", "Mochi", "Angi Clothing"]
let shopDescriptions = ["Buy 2 POLOS @ \u{20B9}699 from Mar 20 to Mar 31, 2018", "Exclusive 50% on all shoes Only on Mar 25, 2018", "Tamil Tees 50% off This Summer"]
let offerImgs = ["vanHeusen", "Pepsi", "vanHeusen", "GRT", "vanHeusen"]
let offerTitle = ["GRT Jewellers", "Pepsi", "Star Vijay", "Royal Enfield", "Van Heusen"]
let requestTypePost = "POST"
let requestTypeGet = "GET"
let kPushNotificationKey = "PUSH_NOTIFY_KEY"
let kPushNotificationIsVeryFirst = "Push_Notificaiton_First"
let kGetDeviceToken = "deviceTokenNotify"
let kGetDeviceName = "iOS"
let kUserName = "admin"
let kPassword = "123456"
//let APIKey = "AIzaSyAw1EUIPb7qT-yvkzjEspIEiPStYpY4GlU"
let APIKey = "AIzaSyCiegoMGd23wvMrbM5mVHKlYzJ8TjKsmwM"

let imageArrayFirstSection = ["reminders", "location-2", "phone_number", "Email"]
let imageArraySecondSection = ["language", "redeem_money"]
let imageArrayThirdSection = ["liked_offers", "followed_stores", "saved_coupons", "used_coupons"]
let imageArrayFourthSection = ["share_app", "rate_app", "send_feedback", "about_follow_ads"]
let imageViewColors = [profileLikedOffersColor, profileFollowedStoresColor, profileSavedCouponsColor, profileUsedCouponsColor]
let imageViewColorsSetTwo = [profileShareAppColor, profileRateAppColor, profileSendFeedbackColor, profileAboutFollowAdsColor]
let imageViewColorsSetThree = [profileLanguageColor, profileBankDetailsColor]
let imageViewColorsSetFour = [profileReminderColor, profileLocationColor, profilePhoneNumberColor, profileRateAppColor]

let bankDetailsImgArr1 = ["bankDetail"]
let bankDetailsImgArr2 = ["bankDetail", "gPay", "payTM", "UPI"]

let videoUrls = ["https://www.youtube.com/watch?v=E5qZLg0ehCo", "https://www.youtube.com/watch?v=OF1V3UO_rGA", "https://www.youtube.com/watch?v=ayQLD5vXNJQ", "https://www.youtube.com/watch?v=GLSxbzfX6Dc", "https://www.youtube.com/watch?v=E5qZLg0ehCo", "https://www.youtube.com/watch?v=OF1V3UO_rGA", "https://www.youtube.com/watch?v=ayQLD5vXNJQ", "https://www.youtube.com/watch?v=GLSxbzfX6Dc"]

let internetConnectionAlert = "Please check your Internet Connection"
let AlertName = "Alert"

let codeInstructionText = "We have sent you a code \n to verify your number"
let identifyCommentText = "To identify you, \n we need your mobile number"
let rememberCommentText = "Remember - never sign up with \n another person's mobile number."
let addPhotoCommentText = "Add a photo so your friends \n can find you."
let phoneNumberPlaceholderText = "Mobile Number"
let logoutAlert = "Logout"
let logoutMsg = "Do you want to logout?"
let mailAlert = "Mail services are not available"
let appShareText = "https://itunes.apple.com/in/app/followads/id1453659816?mt=8"
let appShareUrl = "https://itunes.apple.com/in/app/followads/id1453659816?mt=8"
let ReceipentEmailId = "feedback@followads.in"
let SubjectForEmail = "Feedback"
let EmailBody = ""
let WarningAlertTitle = "Warning"
let WarningMsg = "You don't have camera"
let FullNamePlaceholderText = "Full Name"
let EnterNameAlertMsg = "Please Enter Your Full Name"
let CodePlaceholderText = "Code"
let PleaseWaitText = "Please wait... "
let EnterOtpAlertMsg = "Please Enter Your OTP Code"
let YourOtpMsg = "\nYour OTP is "
let OtpMsg = "OTP is sent to your registered mobile number "
let EnterMobNumAlert = "Please Enter Your Mobile Number"
let EnterValidMobNumAlert = "Please Enter Valid Mobile Number"
let EmailPlaceHolderText = "Email Id"
let EnterEmailIdMsg = "Please Enter Your Email Id"
let ErrorMsg = "Sorry, something went wrong"
let InvalidEmailIdAlert = "Please enter your valid email id"
let SetYourLocationText = "Set your location"
let DetextLocation = "Detect Location"
let EnterLocation = "Enter Location"
let orText = "or"
let WalletText = "is in your wallet. You can transfer this to your bank account to get this money."
let TransferText = "Money will be transfered to your bank account within 3 working days after requesting."
let RedeemMoney = "Redeem Money"
let ShowOfferCodeText = "Show Offer Code"
let fullNameText = "What's your full name?"
let NamePlaceholder = "Full Name"
let NextText = "Next"
let cancelText = "Cancel"
let sendText = "Send"
let chooseLangText = "Choose your Language"
let mobileNumText = "Mobile Number"
let sentTo = "Sent to "
let NumQueryText = "Didn't get it?"
let sendNewCode = "Send new code >"
let addProfilePic = "Add a profile picture"
let chooseAPhoto = "Choose a photo"
let takeAPhoto = "Take a photo"
let skip = "Skip"
let signInInstructionText = "To access all features of the app."
let signInText = "Sign In"
var notificationCount = ""
var walletAmt = ""
let cancelBtnTitle = "Cancel"
let saveBtnTitle = "Save"
let doneBtnText = "Done"
let remindMeText = "Remind Me"
let alertTitle = "Alert"
let reachabilityAlert = "Please check your Internet Connection"
let locationAlert = "Please allow access to your location to proceed further"
let errorMessage = "Sorry, something went wrong"


let FormattedAddressLines = "FormattedAddressLines"
let alertLocation = "Please allow access to your location to proceed further"
let versionStr = "Version "
var pullToRefrest = "Pull to refresh"


let SettingsList1 = "SettingsList1"
let SettingsList2 = "SettingsList2"
let SettingsList3 = "SettingsList3"
let SettingsList4 = "SettingsList4"

let SettingsDisclosure1 = "SettingsDisclosure1"
let SettingsDisclosure2 = "SettingsDisclosure2"
let SettingsDisclosure3 = "SettingsDisclosure3"
let SettingsDisclosure4 = "SettingsDisclosure4"

let RedeemMoneyList1 = "RedeemMoneyList1"
let RedeemMoneyList2 = "RedeemMoneyList2"


let storyBoardName = "Main"
let AppBundleVersionKey = "CFBundleShortVersionString"

let emptyString = ""
let editName = "Edit"
let buttonTitles = "ButtonTitles"
let saveName = "Save"
let yes = "Yes"
let no = "No"
let Reviews = " Reviews | "
let Interested = " Interested"
var reviewsCount = ""
var interestCount = ""
var distanceText = ""
var shopReviewsCount = ""
var shopInterestCount = ""
var shopDistance = ""
var titleText = ""
var feedbackText = ""
var usernameText = ""
var rating = ""
var shopImagesCount = ""
var shopVideosCount = ""
var followedReviewsCount = ""
var followedInterestCount = ""
var followedDistanceText = ""
var postedTime = ""
var currentTime = ""
var likeStatus = ""
var statusBool = ""
var remindStatusValue = ""


// Storyboard Id
let EnterFullNameViewId = "EnterFullNameViewController"
let AddProfilePicViewId = "AddProfilePictureViewController"
let RemainderViewId = "ReminderViewController"
let SelectLocationViewId = "SelectLocationViewController"
let ProfilePhoneNumViewId = "ProfilePhoneNumberViewController"
let EnterEmailViewId = "EnterEmailViewId"
let LanguageSelectionViewId = "LanguageSelectionViewId"
let BankDetailsViewId = "BankDetailsViewController"
let TabBarId = "tabBarId"
let SelectLocationViewStoryBoardId = "SelectLocationViewId"
let LanguageSelectionCellId = "langSelectionCellId"
let EditVCStoryBoardId = "EditProfileVCId"

// Segue Id
let NotificationsId = "Notifications"
let AboutFollowAdsId = "AboutFollowAds"
let PhoneNumberViewId = "phoneNumViewId"
let ProfileViewId = "profileViewId"
let verificationViewId = "verificationViewId"

// Navigation Title
let AddPhotoTitle = "Add Photo"
let ReminderTitle = "Reminder"
let NotificationsTitle = "Notifications"
let LikedOffersTitle = "Liked Offers"
let FollowedStoresTitle = "Followed Stores"
let SavedCouponsTitle = "Scanned Coupons"
let UsedCouponsTitle = "Used Coupons"

// Cell Identifiers
let ProfilePhotoCellId = "profilePhotoCell"
let ProfileCellId = "profileCell"
let ProfileHeaderCellId = "ProfileHeaderCellId"

// Image Name
let PlaceholderImg = "placeholder_img"

// font name
let fontHelveticaNeueMedium = "HelveticaNeue-Medium"
let fontHelveticaNeueCndnsdBold = "HelveticaNeue-CondensedBold"

var imageUrl = ""

// UserDefaults
let UserName = "Username"
let UserId = "User_Id"
let ProfImg = "PROF_IMG"
let ImgUrl = "ImgUrlStr"
let City = "City"
let LikedOffersCount = "LIKED_OFFERS_COUNT"
let FollowedStoresCount = "FOLLOWED_STORES_COUNT"

var isFromDeep = false
var unremindDate: String?
