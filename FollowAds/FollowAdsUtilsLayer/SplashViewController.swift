//
//  SplashViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 23/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class SplashViewController: FollowAdsBaseViewController, CLLocationManagerDelegate, BWWalkthroughViewControllerDelegate {
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var langListDataModel = FollowAdsLangListDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    @IBOutlet var splashImgView: UIImageView!
    @IBOutlet var followAdsLabel: UILabel!
    var locationManager = CLLocationManager()
    var location: CLLocation?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var city: String?
    var country: String?
    var countryShortName: String?
    var subAdministrativeAre : String?
    var address : String?
    var postalCode: String?
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var areaListDataModel = FollowAdsAreaListDataModel()
    var notificationNew:Notification?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
               DispatchQueue.main.async {
                   self.navigationController?.navigationBar.setNeedsLayout()
                   self.navigationController?.navigationBar.layoutIfNeeded()
               }
           }
        // logo mask
        setNeedsStatusBarAppearanceUpdate()

        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        self.splashImgView.layer.mask = CALayer()
        self.splashImgView.layer.mask?.contents = UIImage(named: "Key")!.cgImage
        self.splashImgView.layer.mask?.bounds = CGRect(x: 0, y: 0, width: splashImgView.frame.size.width, height: splashImgView.frame.size.height)
        self.splashImgView.layer.mask?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.splashImgView.layer.mask?.position = CGPoint(x: self.splashImgView.frame.width / 2, y: self.splashImgView.frame.height / 2)
        
        // logo mask background view
        let maskBgView = UIView(frame: self.splashImgView.frame)
        maskBgView.backgroundColor = UIColor.clear
        maskBgView.layer.mask?.contents = UIImage(named: "Key")!.cgImage
        self.view.addSubview(maskBgView)
        self.view.bringSubview(toFront: maskBgView)
        
        // logo mask animation
        let transformAnimation = CAKeyframeAnimation(keyPath: "bounds")
        //      transformAnimation.delegate = self as! CAAnimationDelegate
        transformAnimation.duration = 1
        transformAnimation.beginTime = CACurrentMediaTime() + 1 //add delay of 1 second
        let initalBounds = NSValue(cgRect: (self.splashImgView.layer.mask?.bounds)!)
        let secondBounds = NSValue(cgRect: CGRect(x: 0, y: 0, width: 50, height: 50))
        let finalBounds = NSValue(cgRect: CGRect(x: 0, y: 0, width: 2000, height: 2000))
        transformAnimation.values = [initalBounds, secondBounds, finalBounds]
        transformAnimation.keyTimes = [0, 0.5, 1]
        transformAnimation.timingFunctions = [CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut), CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)]
        transformAnimation.isRemovedOnCompletion = false
        transformAnimation.fillMode = kCAFillModeForwards
        self.view.layer.mask?.add(transformAnimation, forKey: "maskAnimation")
        
        // logo mask background view animation
        UIView.animate(withDuration: 0.1,
                       delay: 1.35,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        maskBgView.alpha = 0.0
        },
                       completion: { finished in
                        maskBgView.removeFromSuperview()
        })
        
        // root view animation
        UIView.animate(withDuration: 0.25,
                       delay: 1.3,
                       options: UIViewAnimationOptions.transitionFlipFromBottom,
                       animations: {
                        self.view.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },
                       completion: { finished in
                        UIView.animate(withDuration: 0.3,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.curveEaseInOut,
                                       animations: {
                                        self.view.transform = CGAffineTransform.identity
                        },
                                       completion: nil
                        )
        })
        perform(#selector(navigateToHomeView), with: nil, afterDelay: 2.0)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToHomeViewNew), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
   /* @objc func callDeepLinkService(notification:Notification) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
        if let aps = apss {
            if let ad_id = aps.url  {
                let off_Id = ad_id.valueOf("offerid")
                vc.offerId = off_Id!
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }*/
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        isFromNotifi = true
        if let aps = notification.userInfo?["aps"] as? [AnyHashable:Any]{
            if let ad_id = aps["advertisement_id"] as? Int {
                off_Notifi_Id = String(describing: ad_id)
            }
            else {
                off_Notifi_Id = "0"
            }
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
         nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func navigateToHomeViewNew(notification:Notification) {
        
        isFromDeep = true
        notificationNew = notification
         if Reachability.isConnectedToNetwork() == true {
            if UserDefaults.standard.value(forKey: "Language_Id") != nil {
                self.categoryList()
            }
            else {
                self.getLangListData()
            }
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDeepLink), object: nil, userInfo: notification.userInfo)
         }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
        }
    }
    
    @objc func navigateToHomeView() {
        if Reachability.isConnectedToNetwork() == true {
            if UserDefaults.standard.value(forKey: "Language_Id") != nil {
                self.categoryList()
            }
            else {
                self.getLangListData()
            }
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
        }
    }
    
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func categoryList() {
        self.followAdsRequestManager.categoryListRequestManager = FollowAdsCategoryListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = ""
        }
        
        let categoryListCompletion: CategoryListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
           //     FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                self.areaList()
            }
            else
            {
                if response != nil {
                    self.determineCurrentLocation()
                    DispatchQueue.main.async(execute: {
                        self.categoryListDataModel = response!
                        print(self.categoryListDataModel)
                        sharedInstance.categoryListDataModel = self.categoryListDataModel
                        self.areaList()
                       
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callCategoryListServiceCall(requestObject: self.followAdsRequestManager, categoryListCompletion)
    }
    
    /*
     This method is used to perform area list service call.
     @param -.
     @return -.
     */
    func areaList() {
        self.followAdsRequestManager.areaListRequestManager = FollowAdsAreaListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.areaListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.areaListRequestManager?.lang_id = ""
        }
        
        let areaListCompletion: AreaListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                //Navigate to Home View
                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                 nextViewController.modalPresentationStyle = .fullScreen
                self.present(nextViewController, animated: true, completion: nil)
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.areaListDataModel = response!
                        print(self.areaListDataModel)
                        sharedInstance.areaListDataModel = self.areaListDataModel

                        //Navigate to Home View
                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                         nextViewController.modalPresentationStyle = .fullScreen
                        self.present(nextViewController, animated: true, completion: nil)
                        if isFromDeep == true {
 
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDeepLink), object: self.notificationNew, userInfo: self.notificationNew?.userInfo)
                            
                            
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callAreaListServiceCall(requestObject: self.followAdsRequestManager, areaListCompletion)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    /*
     This method is used to perform lang list service call.
     @param --.
     @return --.
     */
    func getLangListData() {
        let langListCompletion: LangListCompletionBlock = {(response, error) in
            if let _ = error {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: ErrorMsg)
            }
            else
            {
                self.determineCurrentLocation()
                DispatchQueue.main.async(execute: {
                    if response?.information?.count != nil {
                        if (response?.information?.count)! >= 0 {
                            
                            
                            if UserDefaults.standard.value(forKey: "Language_Id") != nil {
                                
                              
                            }
                            else {
                                
                                // Get view controllers and build the walkthrough
                                let stb = UIStoryboard(name: storyBoardName, bundle: nil)
                                let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
                                let page_one = stb.instantiateViewController(withIdentifier: "walk1")
                                let page_two = stb.instantiateViewController(withIdentifier: "walk2")
                                let page_three = stb.instantiateViewController(withIdentifier: "walk3")
                                
                                // Attach the pages to the master
                                walkthrough.delegate = self
                                walkthrough.add(viewController:page_one)
                                walkthrough.add(viewController:page_two)
                                walkthrough.add(viewController:page_three)
                                 walkthrough.modalPresentationStyle = .fullScreen
                                self.present(walkthrough, animated: true, completion: nil)
                            }
                        }
                    }
                })
            }
        }
        print("ihsihihsf",langListCompletion)
        FollowAdsServiceHandler.callLanguageListServiceCall(requestObject: self.followAdsRequestManager, langListCompletion)
    }
    
    
    // MARK: - Walkthrough delegate -

    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }

    func walkthroughCloseButtonPressed() {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LanguageSelectionViewId") as! LanguageSelectionViewController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    /*
     This method is used to determine current location.
     @param -.
     @return -.
     */
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: CLLOCATION MANAGER DELEGATES
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        let lat: Double = Double(userLocation.coordinate.latitude)
        let longt: Double = Double(userLocation.coordinate.longitude)
        let latString:String = String(format:"%3f", lat)
        print("latString: \(latString)") // b: 1.500000
        let lonString:String = String(format:"%3f", longt)
        print("lonString: \(lonString)") // b: 1.500000
        
        let latestLocation = locations.last!
        // here check if no need to continue just return still in the same place
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        // if it location is nil or it has been moved
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need
                self.parsePlacemarks()
                if self.city != nil {
                    if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                        let deviceTokenText = String()
                        let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: deviceToken), object: nil, userInfo: nil)
                    }
                }
            })
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.subLocality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                    UserDefaults.standard.set(city, forKey: City)
                    UserDefaults.standard.synchronize()
                }
                if let subAdministrativeAre = placemark.name, !subAdministrativeAre.isEmpty {
                    self.subAdministrativeAre = subAdministrativeAre
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    self.country = country
                }
                if let postalCode = placemark.postalCode, !postalCode.isEmpty {
                    self.postalCode = postalCode
                    UserDefaults.standard.set(postalCode, forKey: "Postal_Code")
                    UserDefaults.standard.synchronize()
                    print(postalCode)
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    self.countryShortName = countryShortName
                }
                self.address = self.getAddressString(placemark: placemark)
            }
        } else {
            // add some more check's if for some reason location manager is nil
        }
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func getAddressString(placemark: CLPlacemark) -> String? {
        var originAddress : String?
        if let addrList = placemark.addressDictionary?[FormattedAddressLines] as? [String]
        {
            originAddress =  addrList.joined(separator: ", ")
        }
        return originAddress
    }
    
}
