//
//  FollowAdsSharedInstance.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/11/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation

class FollowAdsSharedInstance {
    
    static let sharedInstance : FollowAdsSharedInstance = {
        let instance = FollowAdsSharedInstance(array: [], catList: FollowAdsCategoryListDataModel(), areaList: FollowAdsAreaListDataModel(), nearbyList: FollowAdsNearByBussinessDataModel(), locEnabled: false, openNotification: false)
        return instance
    }()
    
    //MARK: Local Variable
    
    var filterDataArrray : [String]
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var areaListDataModel = FollowAdsAreaListDataModel()
    var nearbyListDataModel = FollowAdsNearByBussinessDataModel()
    var switchLocEnabled: Bool?
    var fromNotification: Bool?


    //MARK: Init
    
    init( array : [String], catList: FollowAdsCategoryListDataModel?, areaList: FollowAdsAreaListDataModel?, nearbyList: FollowAdsNearByBussinessDataModel?, locEnabled: Bool?, openNotification: Bool?) {
        filterDataArrray = array
        categoryListDataModel = catList!
        areaListDataModel = areaList!
        nearbyListDataModel = nearbyList!
        switchLocEnabled = locEnabled!
        fromNotification = openNotification
    }
    
}
