//
//  FollowAdsDisplayType.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 31/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

public enum DisplayType {
    case unknown
    case iphone4
    case iphone5
    case iphone6
    case iphone6plus
    case iphoneXR
    case iPadMini4
    case iPadPro10
    case iPadPro12
    case iPadPro9
    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus
    static let iPhoneXSMax = iphoneXR
    static let iPadAir2 = iPadMini4
    static let iPadPro2ndGen = iPadPro12
    case iphoneX
}

public final class FollowAdsDisplayType {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength:CGFloat { return max(width, height) }
    class var minLength:CGFloat { return min(width, height) }
    class var zoomed:Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina:Bool { return UIScreen.main.scale >= 2.0 }
    class var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    //    class var carplay:Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    //    class var tv:Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike:DisplayType {
        if phone && maxLength < 568 {
            return .iphone4
        }
        else if phone && maxLength == 568 {
            return .iphone5
        }
        else if phone && maxLength == 667 {
            return .iphone6
        }
        else if phone && maxLength == 736 {
            return .iphone6plus
        }
        else if phone && maxLength == 812 {
            return .iphoneX
        }
        else if phone && maxLength == 808 {
            return .iphoneXR
        }
        else if pad && maxLength == 1024 {
            return .iPadMini4
        }
        else if pad && maxLength == 1024 {
            return .iPadPro9
        }
        else if pad && maxLength == 834 {
            return .iPadPro10
        }
        else if pad && maxLength == 1366 {
            return .iPadPro12
        }
        return .unknown
    }
}

