//
//  FirstTableViewController.swift
//  SJSegmentedScrollView
//
//  Created by Subins Jose on 13/06/16.
//  Copyright © 2016 Subins Jose. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import MBProgressHUD
import BRYXBanner
import FirebaseDynamicLinks

class FirstTableViewController: UITableViewController {
    
    // MARK: - Table view data source
    var searchOffersDataModel : FollowAdsSearchOffersDataModel?
    var segment : Int?
    var selectedIndex: IndexPath?
    var followAdsRequestManager = FollowAdsRequestManager()
    var selectedString: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var banner = Banner()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl?.addTarget(self,
                                  action: #selector(handleRefresh(_:)),
                                  for: UIControlEvents.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.addLoadingView()
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
    //    }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to add the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    func callSearchOffersServiceCall() {
        self.followAdsRequestManager.searchOffersRequestManager = FollowAdsSearchOffersRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.searchOffersRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.searchOffersRequestManager?.user_id = ""
        }
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.searchOffersRequestManager?.user_lat = userLat
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.searchOffersRequestManager?.user_lng = userLng
        self.followAdsRequestManager.searchOffersRequestManager?.search_key = selectedString
        self.followAdsRequestManager.searchOffersRequestManager?.limit = "1"
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.searchOffersRequestManager?.lang_id = langId
        if UserDefaults.standard.value(forKey: City) != nil{
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
            self.followAdsRequestManager.searchOffersRequestManager?.area = city
        }
        else {
            self.followAdsRequestManager.searchOffersRequestManager?.area = ""
            
        }
        if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
            let postalCodeName = String()
            let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
            self.followAdsRequestManager.searchOffersRequestManager?.postal_code = postalCode
        }
        else {
            self.followAdsRequestManager.searchOffersRequestManager?.postal_code = ""
            
        }
        
        
        let searchCompletion: SearchOffersCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
          //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: ErrorMsg)
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
//                        self.searchOffersDataModel = response
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callSearchOffersServiceCall(requestObject: self.followAdsRequestManager, searchCompletion)
        
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.perform(#selector(self.endRefresh), with: nil, afterDelay: 1.0)
    }
    
    @objc func endRefresh() {
        refreshControl?.endRefreshing()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch segment {
//        case 0?:
        if self.searchOffersDataModel?.All_ads.isEmpty == false {
            if (self.searchOffersDataModel?.All_ads.count)! > 0 {
                return (self.searchOffersDataModel?.All_ads.count)!
            }
            else {
                return 1
            }
        }
        else {
            return 1
        }
//        case 1?:
//
//            if (self.searchOffersDataModel?.Expiring.count)! > 0 {
//                return (self.searchOffersDataModel?.Expiring.count)!
//            }
//            else {
//                return 1
//            }
//        case 2? :
//            if (self.searchOffersDataModel?.Live.count)! > 0 {
//                return (self.searchOffersDataModel?.Live.count)!
//            }
//            else {
//                return 1
//            }
//        case 3? :
//            if (self.searchOffersDataModel?.Coming.count)! > 0 {
//                return (self.searchOffersDataModel?.Coming.count)!
//            }
//            else {
//                return 1
//            }
//        case 4? :
//            if (self.searchOffersDataModel?.Nearby.count)! > 0 {
//                return (self.searchOffersDataModel?.Nearby.count)!
//            }
//            else {
//                return 1
//            }
//        default:
//            return 0
//        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        switch segment {
//        case 0?:
        if self.searchOffersDataModel?.All_ads.isEmpty == false {

            if (self.searchOffersDataModel?.All_ads.count)! > 0 {
                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
                cell.shopImageView.layer.cornerRadius = 10
                cell.shopImageView.clipsToBounds = true
                cell.shopTitleLabel.text = searchOffersDataModel?.All_ads[indexPath.row].business_name
                cell.shopDescLabel.text = searchOffersDataModel?.All_ads[indexPath.row].business_area
                
          /*      let timeFormatter = DateFormatter()
                timeFormatter.dateFormat = "dd-MM-yyyy"
                if searchOffersDataModel?.All_ads[indexPath.row].advertisement_valid_to != nil {
                    let validDate = timeFormatter.date(from: searchOffersDataModel?.All_ads[indexPath.row].advertisement_valid_to ?? "")
                    
                    let validDateFormat = DateFormatter()
                    validDateFormat.dateFormat = "dd"
                    let validDay = validDateFormat.string(from: validDate!)
                    
                    let validMonthFormat = DateFormatter()
                    validMonthFormat.dateFormat = "MMM"
                    let validMonth = validMonthFormat.string(from: validDate!)
                    
                    let validYearFormat = DateFormatter()
                    validYearFormat.dateFormat = "yyyy"
                    let validYear = validYearFormat.string(from: validDate!)
                    
                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
                } */
                
                cell.shopValidLabel.text = searchOffersDataModel?.All_ads[indexPath.row].advertisement_caption

                let urlstring = self.searchOffersDataModel?.All_ads[indexPath.row].promotion_image_url
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        
                        cell?.shopImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                    }
                }
                else {
                    cell?.shopImageView.image = UIImage(named: "home_ads_placeholder")
                }
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
                cell.textLabel?.text = "No Data Available".localized()
                cell.textLabel?.textAlignment = .center
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
                cell.tintColor = .clear
                tableView.isScrollEnabled = false
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
            cell.textLabel?.text = "No Data Available".localized()
            cell.textLabel?.textAlignment = .center
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
            cell.tintColor = .clear
            tableView.isScrollEnabled = false
            return cell
        }
//        case 1?:
//            if (self.searchOffersDataModel?.Expiring.count)! > 0 {
//                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
//                cell.shopImageView.layer.cornerRadius = cell.shopImageView.frame.size.width / 2
//                cell.shopImageView.clipsToBounds = true
//                cell.shopTitleLabel.text = searchOffersDataModel?.Expiring[indexPath.row].business_name
//                cell.shopDescLabel.text = searchOffersDataModel?.Expiring[indexPath.row].ads_name
//
//                let timeFormatter = DateFormatter()
//                timeFormatter.dateFormat = "dd-MM-yyyy"
//
//                if searchOffersDataModel?.Expiring[indexPath.row].advertisement_valid_to != nil {
//                    let validDate = timeFormatter.date(from: searchOffersDataModel?.Expiring[indexPath.row].advertisement_valid_to ?? "")
//
//                    let validDateFormat = DateFormatter()
//                    validDateFormat.dateFormat = "dd"
//                    let validDay = validDateFormat.string(from: validDate!)
//
//                    let validMonthFormat = DateFormatter()
//                    validMonthFormat.dateFormat = "MMM"
//                    let validMonth = validMonthFormat.string(from: validDate!)
//
//                    let validYearFormat = DateFormatter()
//                    validYearFormat.dateFormat = "yyyy"
//                    let validYear = validYearFormat.string(from: validDate!)
//
//                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
//                }
//
//                let urlstring = self.searchOffersDataModel?.Expiring[indexPath.row].advertisment_img
//                if urlstring != nil {
//                    cell?.shopImageView.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Store_Placeholder"))
//                }
//                else {
//                    cell?.shopImageView.image = UIImage(named: "Store_Placeholder")
//                }
//                return cell
//            }
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
//                cell.textLabel?.text = "No Data Available".localized()
//                cell.textLabel?.textAlignment = .center
//                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
//                cell.tintColor = .clear
//                tableView.isScrollEnabled = false
//                return cell
//            }
//        case 2? :
//            if (self.searchOffersDataModel?.Live.count)! > 0 {
//                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
//                cell.shopImageView.layer.cornerRadius = cell.shopImageView.frame.size.width / 2
//                cell.shopImageView.clipsToBounds = true
//                cell.shopTitleLabel.text = searchOffersDataModel?.Live[indexPath.row].business_name
//                cell.shopDescLabel.text = searchOffersDataModel?.Live[indexPath.row].ads_name
//
//                let timeFormatter = DateFormatter()
//                timeFormatter.dateFormat = "dd-MM-yyyy"
//
//                if searchOffersDataModel?.Live[indexPath.row].advertisement_valid_to != nil {
//                    let validDate = timeFormatter.date(from: searchOffersDataModel?.Live[indexPath.row].advertisement_valid_to ?? "")
//
//                    let validDateFormat = DateFormatter()
//                    validDateFormat.dateFormat = "dd"
//                    let validDay = validDateFormat.string(from: validDate!)
//
//                    let validMonthFormat = DateFormatter()
//                    validMonthFormat.dateFormat = "MMM"
//                    let validMonth = validMonthFormat.string(from: validDate!)
//
//                    let validYearFormat = DateFormatter()
//                    validYearFormat.dateFormat = "yyyy"
//                    let validYear = validYearFormat.string(from: validDate!)
//
//                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
//
//                }
//
//                let urlstring = self.searchOffersDataModel?.Live[indexPath.row].advertisment_img
//                if urlstring != nil {
//                    cell?.shopImageView.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Store_Placeholder"))
//                }
//                else {
//                    cell?.shopImageView.image = UIImage(named: "Store_Placeholder")
//                }
//                return cell
//            }
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
//                cell.textLabel?.text = "No Data Available".localized()
//                cell.textLabel?.textAlignment = .center
//                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
//                cell.tintColor = .clear
//                tableView.isScrollEnabled = false
//                return cell
//            }
//        case 3? :
//            if (self.searchOffersDataModel?.Coming.count)! > 0 {
//                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
//                cell.shopImageView.layer.cornerRadius = cell.shopImageView.frame.size.width / 2
//                cell.shopImageView.clipsToBounds = true
//                cell.shopTitleLabel.text = searchOffersDataModel?.Coming[indexPath.row].business_name
//                cell.shopDescLabel.text = searchOffersDataModel?.Coming[indexPath.row].ads_name
//
//                let timeFormatter = DateFormatter()
//                timeFormatter.dateFormat = "dd-MM-yyyy"
//
//                if searchOffersDataModel?.Coming[indexPath.row].advertisement_valid_to != nil {
//                    let validDate = timeFormatter.date(from: searchOffersDataModel?.Coming[indexPath.row].advertisement_valid_to ?? "")
//
//                    let validDateFormat = DateFormatter()
//                    validDateFormat.dateFormat = "dd"
//                    let validDay = validDateFormat.string(from: validDate!)
//
//                    let validMonthFormat = DateFormatter()
//                    validMonthFormat.dateFormat = "MMM"
//                    let validMonth = validMonthFormat.string(from: validDate!)
//
//                    let validYearFormat = DateFormatter()
//                    validYearFormat.dateFormat = "yyyy"
//                    let validYear = validYearFormat.string(from: validDate!)
//
//                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
//                }
//
//                let urlstring = self.searchOffersDataModel?.Coming[indexPath.row].advertisment_img
//                if urlstring != nil {
//                    cell?.shopImageView.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Store_Placeholder"))
//                }
//                else {
//                    cell?.shopImageView.image = UIImage(named: "Store_Placeholder")
//                }
//                return cell
//            }
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
//                cell.textLabel?.text = "No Data Available".localized()
//                cell.textLabel?.textAlignment = .center
//                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
//                cell.tintColor = .clear
//                tableView.isScrollEnabled = false
//                return cell
//            }
//        case 4? :
//            if (self.searchOffersDataModel?.Nearby.count)! > 0 {
//                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
//                cell.shopImageView.layer.cornerRadius = cell.shopImageView.frame.size.width / 2
//                cell.shopImageView.clipsToBounds = true
//                cell.shopTitleLabel.text = searchOffersDataModel?.Nearby[indexPath.row].business_name
//                cell.shopDescLabel.text = searchOffersDataModel?.Nearby[indexPath.row].ads_name
//
//                let timeFormatter = DateFormatter()
//                timeFormatter.dateFormat = "dd-MM-yyyy"
//
//                if searchOffersDataModel?.Nearby[indexPath.row].advertisement_valid_to != nil {
//                    let validDate = timeFormatter.date(from: searchOffersDataModel?.Nearby[indexPath.row].advertisement_valid_to ?? "")
//
//                    let validDateFormat = DateFormatter()
//                    validDateFormat.dateFormat = "dd"
//                    let validDay = validDateFormat.string(from: validDate!)
//
//                    let validMonthFormat = DateFormatter()
//                    validMonthFormat.dateFormat = "MMM"
//                    let validMonth = validMonthFormat.string(from: validDate!)
//
//                    let validYearFormat = DateFormatter()
//                    validYearFormat.dateFormat = "yyyy"
//                    let validYear = validYearFormat.string(from: validDate!)
//
//                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
//                }
//
//                let urlstring = self.searchOffersDataModel?.Nearby[indexPath.row].advertisment_img
//                if urlstring != nil {
//                    cell?.shopImageView.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Store_Placeholder"))
//                }
//                else {
//                    cell?.shopImageView.image = UIImage(named: "Store_Placeholder")
//                }
//                return cell
//            }
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
//                cell.textLabel?.text = "No Data Available".localized()
//                cell.textLabel?.textAlignment = .center
//                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
//                cell.tintColor = .clear
//                tableView.isScrollEnabled = false
//                return cell
//            }
//        default:
//            if (self.searchOffersDataModel?.All_ads.count)! > 0 {
//                let cell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell
//                cell.shopImageView.layer.cornerRadius = cell.shopImageView.frame.size.width / 2
//                cell.shopImageView.clipsToBounds = true
//                cell.shopTitleLabel.text = searchOffersDataModel?.All_ads[indexPath.row].business_name
//                cell.shopDescLabel.text = searchOffersDataModel?.All_ads[indexPath.row].ads_name
//
//                let timeFormatter = DateFormatter()
//                timeFormatter.dateFormat = "dd-MM-yyyy"
//
//                if searchOffersDataModel?.All_ads[indexPath.row].advertisement_valid_to != nil {
//                    let validDate = timeFormatter.date(from: searchOffersDataModel?.All_ads[indexPath.row].advertisement_valid_to ?? "")
//
//                    let validDateFormat = DateFormatter()
//                    validDateFormat.dateFormat = "dd"
//                    let validDay = validDateFormat.string(from: validDate!)
//
//                    let validMonthFormat = DateFormatter()
//                    validMonthFormat.dateFormat = "MMM"
//                    let validMonth = validMonthFormat.string(from: validDate!)
//
//                    let validYearFormat = DateFormatter()
//                    validYearFormat.dateFormat = "yyyy"
//                    let validYear = validYearFormat.string(from: validDate!)
//
//                    cell.shopValidLabel.text = "Valid till " + validMonth + " " + validDay + ", " + validYear
//                }
//
//                let urlstring = self.searchOffersDataModel?.All_ads[indexPath.row].advertisment_img
//                if urlstring != nil {
//                    cell?.shopImageView.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Store_Placeholder"))
//                }
//                else {
//                    cell?.shopImageView.image = UIImage(named: "Store_Placeholder")
//                }
//                return cell
//            }
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
//                cell.textLabel?.text = "No Data Available".localized()
//                cell.textLabel?.textAlignment = .center
//                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.tableView.frame.size.height)
//                cell.tintColor = .clear
//                tableView.isScrollEnabled = false
//                return cell
//            }
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
//            switch segment {
//            case 0?:
                selectedIndex = indexPath
                if (searchOffersDataModel?.All_ads[(selectedIndex?.row)!].business_name) != nil {
                    let businessName: String = searchOffersDataModel?.All_ads[(selectedIndex?.row)!].business_name ?? ""
                    selectedString = "$"
                    selectedString?.append(businessName)
                    if Reachability.isConnectedToNetwork() == true {
                        self.callSearchOffersServiceCall()
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                    }
                }
            
            if (searchOffersDataModel?.All_ads[(selectedIndex?.row)!].is_active) != nil {
                if searchOffersDataModel?.All_ads[(selectedIndex?.row)!].is_active == "0" {
//                    self.banner = Banner(title: "This Offer has expired.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                    self.banner.dismissesOnTap = true
//                    self.banner.show(duration: 3.0)
                    
                    self.view.makeToast("This Offer has expired.".localized(), duration: 3.0, position: .center)
                    
                    
                }
                else {
                    let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                    if (searchOffersDataModel?.All_ads[(selectedIndex?.row)!].ads_id) != nil {
                        vc.offerId = (searchOffersDataModel?.All_ads[(selectedIndex?.row)!].ads_id)!
                    }
                    else {
                        vc.offerId = ""
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
//            case 1?:
//                selectedIndex = indexPath
//                if (searchOffersDataModel?.Expiring[(selectedIndex?.row)!].business_name) != nil {
//                    let businessName: String = searchOffersDataModel?.Expiring[(selectedIndex?.row)!].business_name ?? ""
//                    selectedString = "#"
//                    selectedString?.append(businessName)
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.callSearchOffersServiceCall()
//                    }
//                    else {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
//                    }
//                }
//                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
//                if (searchOffersDataModel?.Expiring[(selectedIndex?.row)!].ads_id) != nil {
//                    vc.offerId = (searchOffersDataModel?.Expiring[(selectedIndex?.row)!].ads_id)!
//                }
//                else {
//                    vc.offerId = ""
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//            case 2? :
//                selectedIndex = indexPath
//                if (searchOffersDataModel?.Live[(selectedIndex?.row)!].business_name) != nil {
//                    let businessName: String = searchOffersDataModel?.Live[(selectedIndex?.row)!].business_name ?? ""
//                    selectedString = "#"
//                    selectedString?.append(businessName)
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.callSearchOffersServiceCall()
//                    }
//                    else {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
//                    }
//                }
//                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
//                if (searchOffersDataModel?.Live[(selectedIndex?.row)!].ads_id) != nil {
//                    vc.offerId = (searchOffersDataModel?.Live[(selectedIndex?.row)!].ads_id)!
//                }
//                else {
//                    vc.offerId = ""
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//            case 3? :
//                selectedIndex = indexPath
//                if (searchOffersDataModel?.Coming[(selectedIndex?.row)!].business_name) != nil {
//                    let businessName: String = searchOffersDataModel?.Coming[(selectedIndex?.row)!].business_name ?? ""
//                    selectedString = "#"
//                    selectedString?.append(businessName)
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.callSearchOffersServiceCall()
//                    }
//                    else {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
//                    }
//                }
//                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
//                if (searchOffersDataModel?.Coming[(selectedIndex?.row)!].ads_id) != nil {
//                    vc.offerId = (searchOffersDataModel?.Coming[(selectedIndex?.row)!].ads_id)!
//                }
//                else {
//                    vc.offerId = ""
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//            case 4? :
//                selectedIndex = indexPath
//                if (searchOffersDataModel?.Nearby[(selectedIndex?.row)!].business_name) != nil {
//                    let businessName: String = searchOffersDataModel?.Nearby[(selectedIndex?.row)!].business_name ?? ""
//                    selectedString = "#"
//                    selectedString?.append(businessName)
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.callSearchOffersServiceCall()
//                    }
//                    else {
//                        MBProgressHUD.hide(for: self.view, animated: true)
//                        FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
//                    }
//                }
//                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
//                if (searchOffersDataModel?.Nearby[(selectedIndex?.row)!].ads_id) != nil {
//                    vc.offerId = (searchOffersDataModel?.Nearby[(selectedIndex?.row)!].ads_id)!
//                }
//                else {
//                    vc.offerId = ""
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//            default:
//                break
//            }
        }
    }
    
    func viewForObserve() -> UIView{
        return self.tableView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch segment {
//        case 0? :
            if searchOffersDataModel?.All_ads.isEmpty == true {
                return self.view.frame.size.height
            }
            else {
                return 110
            }
//        case 1?:
//            if searchOffersDataModel?.Expiring.isEmpty == true {
//                return self.view.frame.size.height
//            }
//            else {
//                return 110
//            }
//        case 2?:
//            if searchOffersDataModel?.Live.isEmpty == true {
//                return self.view.frame.size.height
//            }
//            else {
//                return 110
//            }
//        case 3?:
//            if searchOffersDataModel?.Coming.isEmpty == true {
//                return self.view.frame.size.height
//            }
//            else {
//                return 110
//            }
//        case 4?:
//            if searchOffersDataModel?.Nearby.isEmpty == true {
//                return self.view.frame.size.height
//            }
//            else {
//                return 110
//            }
//        default:
//            break
//        }
//        return 0
    }
}
// added new dummy lines
