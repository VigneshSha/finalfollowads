//
//  SearchBarTableDataSource.swift
//  FollowAds
//
//  Created by openwave on 16/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class SearchBarTableDataSource: NSObject, UITableViewDataSource {
 
    var tableView: UITableView?
    var data : [String]!
    
    init(data : [String]!) {
        self.data = data
        super.init()
    }
    var filteredData: [String]!
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchBarTableViewCell
        cell.locationimage?.image = UIImage(named :"locationss.png")
        cell.lbl?.text = data[indexPath.row]
        return cell
    }
    
}
