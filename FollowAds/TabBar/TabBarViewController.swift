//
//  HomeScreen.swift
//  FollowAds
//
//  Created by openwave on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
protocol TabGeni {
    func openGeni()
}
@available(iOS 10.0, *)
class TabBarViewController: UITabBarController,UITabBarControllerDelegate,PopupDeleget {
    var genicontroller: GeniviewcontrollerViewController?
    var popupadslistDataModel = FollowAdsPopupAdsDataModel()
    var adsOfferTitle = "Offers".localized()
    var notificationTitle = "Notifications".localized()
    var scanTitle = "Scan".localized()
    var walletTitle = "Wallet".localized()
    var Openimage:TabGeni?
    var profileTitle = "Me".localized()
      var followAdsRequestManager = FollowAdsRequestManager()
    override func viewDidLoad() {
    super.viewDidLoad()
        self.tabBar.items?[0].title = adsOfferTitle
        self.tabBar.items?[1].title = notificationTitle
        self.tabBar.items?[2].title = scanTitle
        self.tabBar.items?[3].title = walletTitle
        self.tabBar.items?[4].title = profileTitle
               setupMiddleButton()
    }
    func
setupMiddleButton()
    {
         let middleBtn = UIButton(frame: CGRect(x: (self.view.bounds.width / 2)-25, y: -25, width: 50, height: 50))
                         
                         //STYLE THE BUTTON YOUR OWN WAY
                   middleBtn.setImage(UIImage(named:"Geni"), for: .normal)
                        middleBtn.layer.cornerRadius = 0.5 * middleBtn.bounds.size.width
                        middleBtn.clipsToBounds = true
                         middleBtn.backgroundColor = UIColor.red
                         //add to the tabbar and add click event
                         self.tabBar.addSubview(middleBtn)
                         middleBtn.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)

                         self.view.layoutIfNeeded()
    }
      
   func closeTapped() {
       self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
   }
   func openimage() {
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = storyboard.instantiateViewController(withIdentifier: "GeniviewcontrollerViewController")
//    self.navigationController?.pushViewController(vc, animated: true)
    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "GeniviewcontrollerViewController") as! GeniviewcontrollerViewController
    let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
    self.present(navController, animated:true, completion: nil)
      self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
   }
    
    func displaypopup(){
        if UserDefaults.standard.value(forKey: City) != nil{
                   self.followAdsRequestManager.popupRequestManager = FollowAdspopupadsRequestManager()
                   if UserDefaults.standard.value(forKey: UserId) != nil {
                       let userIdText = String()
                       let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                       self.followAdsRequestManager.popupRequestManager?.user_id = userId
                       print("nksnsf",userId)
                   }
                   else {
                       self.followAdsRequestManager.popupRequestManager?.user_id = ""
                   }
                   if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                       let deviceTokenText = String()
                       let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                       self.followAdsRequestManager.popupRequestManager?.device_token = deviceToken
                   }
                   else {
                       self.followAdsRequestManager.popupRequestManager?.device_token = ""
                   }
                   self.followAdsRequestManager.popupRequestManager?.device_name = kGetDeviceName
                   if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                       let userLatText = String()
                       let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                       self.followAdsRequestManager.popupRequestManager?.user_lat = userLat
                   }
                   if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                       let userLngText = String()
                       let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                       self.followAdsRequestManager.popupRequestManager?.user_lng = userLng
                    print("dgdfgdf", self.followAdsRequestManager.popupRequestManager?.user_lng)
                   }
                   let langidText = String()
                   let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
                   self.followAdsRequestManager.popupRequestManager?.lang_id = langId
                   if UserDefaults.standard.value(forKey: City) != nil{
                       let cityName = String()
                       let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                       self.followAdsRequestManager.popupRequestManager?.area = city
                   }
                   else {
                       self.followAdsRequestManager.popupRequestManager?.area = ""
                       
                   }
                   if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                       let postalCodeName = String()
                       let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                       self.followAdsRequestManager.popupRequestManager?.postal_code = postalCode
                   }
                   else {
                       self.followAdsRequestManager.popupRequestManager?.postal_code = ""
                       
                   }
                   
        
        let PopupAdsCompletion: PopupAdsCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
               // FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                    self.popupadslistDataModel = response!
                            print("sfjbjsfnb",self.popupadslistDataModel)
                        print("dgdgdgdg",self.popupadslistDataModel.images[0].ads_list.count)
                       
//                        self.imageview()
                        self.genicontroller?.popupadslistDataModel = self.popupadslistDataModel
                      
                                    //  self.collectionView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
            
        FollowAdsServiceHandler.callpopupadsservicecalls(requestObject: self.followAdsRequestManager, PopupAdsCompletion)
        }

      }
    func imageview(){
        
        let showAlert = UIAlertController(title: "Demo Alert", message: nil, preferredStyle: .alert)
        let imageView = UIImageView(frame: CGRect(x: 10, y: 50, width: 250, height: 230))
        for getimages in self.popupadslistDataModel.images{
            let urlstring = getimages.full_file_name!
                    print("kdbjkbndf",urlstring)
                    if urlstring != nil {
                        if let encodedString  = urlstring.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                        imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                    imageView.image  = UIImage(named: "home_ads_placeholder")
                    }

                } // Your image here...
        showAlert.view.addSubview(imageView)
        let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 320)
        let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        showAlert.view.addConstraint(height)
        showAlert.view.addConstraint(width)
        showAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            // your actions here...
            print("phsifhifh8sfisah")
           
        }))
        self.present(showAlert, animated: true, completion: nil)

      
        
    }
    
    
    
     // Menu Button Touch Action
     @objc func menuButtonAction(sender: UIButton) {
//        print("snknsfknsfnsfuhn")
      var MYpopupView:PopupViewController!
      MYpopupView = PopupViewController(nibName:"PopupViewController", bundle: nil)
      self.view.alpha = 1.0
      MYpopupView.closePopup = self
      self.presentpopupViewController(popupViewController: MYpopupView, animationType: .BottomTop, completion: {() -> Void in
      })
     }
   
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected item")        // do something
                
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
