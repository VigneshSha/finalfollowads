//
//  Search Bar.swift
//  FollowAds
//
//  Created by openwave on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import AppsFlyerLib

class SearchViewController: FollowAdsBaseViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchController = UISearchController()
    var filteredData: [String]!
    var SearchBarDataSource: SearchBarTableDataSource!
    let places = ["Nungambakkam, Chennai", "Thirumangalam, Madurai", "Choolaimedu, Chennai", "Mogappair, Chennai", "Koyambedu, Chennai"]
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableViewDataSource()
        filteredData = places
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
         // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
       
    }
    
    /*
     This method is used to setup tableview datasource.
     @param ~~.
     @return ~~.
     */
    func setupTableViewDataSource() {
         SearchBarDataSource = SearchBarTableDataSource(data: places)
        self.tableView.dataSource = SearchBarDataSource
        self.tableView.delegate = SearchBarDataSource as? UITableViewDelegate
        SearchBarDataSource.tableView = self.tableView
        self.tableView.tableFooterView =  UIView()
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filteredData = searchText.isEmpty ? places : places.filter({(dataString: String) -> Bool in
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
        } else {
            filteredData = places
        }
        tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
