//
//  LikedOffersTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 12/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class LikedOffersTableViewCell: UITableViewCell {
    
    @IBOutlet var offerImageView: UIImageView!
    @IBOutlet var labelOfferName: UILabel!
    @IBOutlet var labelShopName: UILabel!
    @IBOutlet var labelOfferExpDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
