//
//  NotificationViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks
import UserNotifications
import AppsFlyerLib

protocol NotificationPassing{
    func NotificationPassing(string: String)
}

@available(iOS 10.0, *)
class NotificationViewController: FollowAdsBaseViewController,UNUserNotificationCenterDelegate {
    var delegate: NotificationPassing?
    let APPDELEGATE =   (UIApplication.shared.delegate as? AppDelegate)

    @IBOutlet weak var UISwitch: UISwitch!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    var navigationTitle = "Notifications"
    var isAlerdayNav : Bool = false
    let appDelegate = AppDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
           self.trackViewcontroller(name: "View_notifications", screenClass: "NotificationViewController")
        AppsFlyerTracker.shared().trackEvent("View_notifications",
                                             withValues: [
                                                AFEventAdView: "View_notifications",
                                                ]);
        
        notificationLabel.text = "Notification".localized()
        self.navigationItem.title = navigationTitle.localized()
        if UserDefaults.standard.value(forKey: "NotificationStatus") != nil {
            let statusValue : String = UserDefaults.standard.value(forKey: "NotificationStatus") as? String ?? ""
            if statusValue == "On" {
                UISwitch.isOn = true
            }
            else {
                UISwitch.isOn = false
            }
        }
        self.setUpNavigationBarButton()
        isAlerdayNav = false

        
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToHome), name: NSNotification.Name( "ForNotificationNavigation"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    @objc func navigateToHome(notification: NSNotification) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
        
    }
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to show the alert controller.
     @param -.
     @return -.
     */
    func presentLocationSettings() {
        let alertController = UIAlertController(title: "Alert!".localized(),
                                                message: "Go to settings and enable your notification to proceed further.".localized(),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .default) { _ in
            self.UISwitch.isOn = false
        })
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                        // Handle
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        })
        present(alertController, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false

        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    /*
     This method is used to set the text for localization.
     @param -.
     @return -.
     */
    @objc func setText(){
        doneButton.title = "Done".localized()
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }

    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used for done button action.
     @param -.
     @return -.
     */
    @IBAction func doneBtnAction(_ sender: UIBarButtonItem) {
        let onState = UISwitch.isOn
        if onState{
//            if #available(iOS 10.0, *) {
//
//                // SETUP FOR NOTIFICATION FOR iOS >= 10.0
//                let center  = UNUserNotificationCenter.current()
//                center.delegate = self
//                center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
//                    if error == nil{
//                        DispatchQueue.main.async(execute: {
//                            UIApplication.shared.registerForRemoteNotifications()
//                        })
//                    }
//                }
//
//            } else {
//
//                // SETUP FOR NOTIFICATION FOR iOS < 10.0
//
//                let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
//                UIApplication.shared.registerUserNotificationSettings(settings)
//
//                // This is an asynchronous method to retrieve a Device Token
//                // Callbacks are in AppDelegate.swift
//                // Success = didRegisterForRemoteNotificationsWithDeviceToken
//                // Fail = didFailToRegisterForRemoteNotificationsWithError
//                UIApplication.shared.registerForRemoteNotifications()
//            }

            
            
            
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
//                if !accepted {
//                    print("Notification access denied.")
//                }
//                else {
//                    let action = UNNotificationAction(identifier: "remindLater", title: "Remind me later", options: [])
//                    let category = UNNotificationCategory(identifier: "myCategory", actions: [action], intentIdentifiers: [], options: [])
//                    UNUserNotificationCenter.current().setNotificationCategories([category])
//                    UNUserNotificationCenter.current().delegate = self
//
//                    let center = UNUserNotificationCenter.current()
//                    center.delegate = self
//                    center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(grant, error)  in
//                        if error == nil {
//                            if grant {
//                                DispatchQueue.main.async(execute: {
//                                    UIApplication.shared.registerForRemoteNotifications()
//                                    self.APPDELEGATE?.isWhenNotificationChanged = true
//
//                                })
//                            } else {
//                                //User didn't grant permission
//                            }
//                        } else {
//                            print("error: ",error!)
//                        }
//                    })
//
//                } }
            
            delegate?.NotificationPassing(string: "On")
            UserDefaults.standard.set("On", forKey: "NotificationStatus")
            //UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
            //Navigate to Home View
            
            
        } else {
        //    UIApplication.shared.unregisterForRemoteNotifications()

            delegate?.NotificationPassing(string: "Off")
            UserDefaults.standard.set("Off", forKey: "NotificationStatus")
        //     UserDefaults.standard.set("", forKey:"KEY_DEVICE_TOKEN")
            //UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
            //Navigate to Home View
//            let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
//            self.present(nextViewController, animated: true, completion: nil)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    
       @available(iOS 10.0, *)
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // ...register device token with our Time Entry API server via REST
    }
     @available(iOS 10.0, *)
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("DidFaildRegistration : Device token for push notifications: FAIL -- ")
        //print(error.localizedDescription)
    }
    /*
     This method is used for UISwitch action.
     @param -.
     @return -.
     */
    @IBAction func ActionTrigerred(_ sender: UISwitch) {
        if UserDefaults.standard.value(forKey: "NotificationStatus") != nil {
            let onState = UISwitch.isOn
            if onState{
                print("On")
                UserDefaults.standard.set(sender.isOn, forKey: "switchState")
            } else {
                print("Off")
                UserDefaults.standard.set(sender.isOn, forKey: "switchState")
            }
        }
        else {
            self.presentLocationSettings()
        }
    }
    
}
