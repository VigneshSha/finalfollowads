//
//  FollowedStoresTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 14/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class FollowedStoresTableViewCell: UITableViewCell {
    
    @IBOutlet var shopNameLabel: UILabel!
    @IBOutlet var shopAddrLabel: UILabel!
    @IBOutlet var shopReviewsLabel: UILabel!
    @IBOutlet var shopDistanceLabel: UILabel!
    @IBOutlet var shopImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
