//
//  GPayViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 09/05/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseDynamicLinks

class GPayViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var headingView: UIView!
    @IBOutlet weak var logoGPay: UIImageView!
    @IBOutlet weak var googlePayLabel: UILabel!
    @IBOutlet weak var phoneNumView: UIView!
    @IBOutlet weak var extensionLabel: UILabel!
    @IBOutlet weak var mapImgVw: UIImageView!
    @IBOutlet weak var phoneNumTextField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var mobNum: String?
    var followAdsRequestManager = FollowAdsRequestManager()
    var mobTransferDataModel: FollowAdsMobTransferDataModel?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var emptyFieldAlert = "Please enter your bank details to proceed"
    var mobileNumAlert = "Please Enter Your Mobile Number"
    var validMobileNumAlert = "Please Enter Valid Mobile Number"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        confirmBtn.layer.cornerRadius = 5.0
        logoGPay?.layer.cornerRadius = 4.0
        logoGPay?.clipsToBounds = true
        phoneNumTextField.text = mobNum
        phoneNumTextField.becomeFirstResponder()
        phoneNumTextField.delegate = self
        self.navigationItem.title = "Google Pay".localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.googlePayLabel.text = "Google Pay".localized()
        self.confirmBtn.setTitle("Confirm".localized(using: buttonTitles), for: .normal)
        phoneNumTextField.placeholder = "Phone#".localized()
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
     This method is used to show the activity indicator.
     @param - notification.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform mobile transfer service call.
     @param -.
     @return -.
     */
    func mobTransferUpdate() {
        self.followAdsRequestManager.mobTransferRequestManager = FollowAdsMobTransferRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.mobTransferRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.mobTransferRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.mobTransferRequestManager?.lang_id = langId
        self.followAdsRequestManager.mobTransferRequestManager?.channel_code = "g_pay"
        var arrayValue = [String : Array<[String:String]>]()
        arrayValue = ["bank_details" : [["detail_key":"Mobile Number","detail_value": self.mobNum]]] as! [String : Array<[String : String]>]
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: arrayValue,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            self.followAdsRequestManager.mobTransferRequestManager?.bank_details = theJSONText!
        }
        else {
            self.followAdsRequestManager.saveBankDetailsRequestManager?.bank_details = ""
        }
      
        
        let mobTransferCompletion: MobTransferCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.mobTransferDataModel = response!
                        print(self.mobTransferDataModel as Any)
                        if self.mobTransferDataModel?.payment_detail.isEmpty == false {
                            if self.mobTransferDataModel?.payment_detail[0].payment_id != nil {
                            UserDefaults.standard.set(self.mobNum, forKey: "GPAYMobNum")
                            UserDefaults.standard.set(self.mobTransferDataModel?.payment_detail[0].payment_id, forKey: "PaymentId")
                            UserDefaults.standard.synchronize()
                            }
                            }
                       
                   //     FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.mobTransferDataModel?.message ?? "")

                        if response?.response_code == "017" {
                            var title = self.alertTitle.localized()
                            var message = "Your Google Pay details has been saved".localized()
                            title = NSLocalizedString(title, comment: "")
                            message = NSLocalizedString(message , comment: "")
                            let alert = UIAlertController(
                                title: title,
                                message: message,
                                preferredStyle: UIAlertControllerStyle.alert)
                            let okButton = UIAlertAction(
                                title:"OK".localized(using: buttonTitles),
                                style: UIAlertActionStyle.default,
                                handler:
                                {
                                    (alert: UIAlertAction!)  in
                                    self.navigationController?.popViewController(animated: true)
                            })
                            alert.addAction(okButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callMobTransferServiceCall(requestObject: self.followAdsRequestManager, mobTransferCompletion)
    }
    
    
    @IBAction func confirmBtnAction(_ sender: UIButton) {
        if phoneNumTextField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: mobileNumAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        else if (phoneNumTextField.text?.characters.count)! <= 9 {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: validMobileNumAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        else {
            self.mobNum = phoneNumTextField.text
            if Reachability.isConnectedToNetwork() == true {
                self.mobTransferUpdate()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
            }
            
        }
       
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension GPayViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case textField:
            if ((phoneNumTextField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
            
        default:
            if ((phoneNumTextField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
        }
        return true
    }
}
