//
//  FollowedStoresTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 14/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol ShopDetailNavigationDelegate {
    func navigateToView(indexPath: IndexPath)
}

class FollowedStoresTableDatasource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var followedBusinessListDataModel: FollowAdsFollowedBusinessListDataModel?
    var delegate: ShopDetailNavigationDelegate?
    
    init(followedBusinessListData: FollowAdsFollowedBusinessListDataModel?) {
        self.followedBusinessListDataModel = followedBusinessListData
        super.init()
    }
    
    // MARK:- TABLEVIEW DELEGATES AND DATASOURCES

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (followedBusinessListDataModel?.followed_business.count) != nil {
            return (followedBusinessListDataModel?.followed_business.count)!
        }
        else {
            return 1
        }
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150.0
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "FollowedStoresCellId", for: indexPath) as! FollowedStoresTableViewCell
        cell.shopImageView.layer.cornerRadius = 10
        cell.shopImageView.clipsToBounds = true
        cell.shopNameLabel.text = followedBusinessListDataModel?.followed_business[indexPath.row].business_name
        cell.shopAddrLabel.text = followedBusinessListDataModel?.followed_business[indexPath.row].business_addr
        if self.followedBusinessListDataModel?.followed_business[indexPath.row].business_review_count != nil {
            let followreviewsCount = "\(String(describing: (self.followedBusinessListDataModel?.followed_business[indexPath.row].business_review_count)!)) \(" Reviews | ".localized()) \(String(describing: (self.followedBusinessListDataModel?.followed_business[indexPath.row].business_interested_count)!)) \(" Interested".localized())"
            cell.shopReviewsLabel.text = followreviewsCount
        }
        
        if self.followedBusinessListDataModel?.followed_business[indexPath.row].business_dist != nil {
            let followBusinessDist = "\(String(describing: (self.followedBusinessListDataModel?.followed_business[indexPath.row].business_dist)!)) \(" away".localized())"
            
            cell.shopDistanceLabel.text = followBusinessDist
        }
        
        let urlstring = self.followedBusinessListDataModel?.followed_business[indexPath.row].business_img
        
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                cell.shopImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            cell.shopImageView.image = UIImage(named: "home_ads_placeholder")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
