//
//  UsedCouponsTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class UsedCouponsTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView?
    var usedCouponListDataModel: FollowAdsUsedCouponListDataModel?
    
    init(usedCouponsListData: FollowAdsUsedCouponListDataModel?) {
        self.usedCouponListDataModel = usedCouponsListData
        super.init()
    }
    
    // MARK:- TABLEVIEW DELEGATES AND DATASOURCES

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (usedCouponListDataModel?.user_used_coupon_list.count) != nil {
            return (usedCouponListDataModel?.user_used_coupon_list.count)!
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "UsedCouponsCellId", for: indexPath) as! UsedCouponsTableViewCell
        cell.offerNameLabel.text = usedCouponListDataModel?.user_used_coupon_list[indexPath.row].business_name
        cell.offerCodeLabel.text = usedCouponListDataModel?.user_used_coupon_list[indexPath.row].offer_code
        cell.OfferDescriptionLabel.text = usedCouponListDataModel?.user_used_coupon_list[indexPath.row].advertisement_caption
    //    cell.offerValidToLabel.text = usedCouponListDataModel?.user_used_coupon_list[indexPath.row].advertisement_valid_to
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd-MM-yyyy"
        
        if usedCouponListDataModel?.user_used_coupon_list[indexPath.row].advertisement_id != nil {

            if usedCouponListDataModel?.user_used_coupon_list[indexPath.row].advertisement_valid_to != "" {
                let validDate = timeFormatter.date(from: usedCouponListDataModel?.user_used_coupon_list[indexPath.row].advertisement_valid_to ?? "")
                
                let validDateFormat = DateFormatter()
                validDateFormat.dateFormat = "dd"
                let validDay = validDateFormat.string(from: validDate!)
                
                let validMonthFormat = DateFormatter()
                validMonthFormat.dateFormat = "MMM"
                if Localize.currentLanguage() == "ta-IN"{
                    validMonthFormat.locale = Locale(identifier: "ta")
                }
                else {
                    validMonthFormat.locale = Locale(identifier: "en")
                }
                let validMonth = validMonthFormat.string(from: validDate!)
                
                let validYearFormat = DateFormatter()
                validYearFormat.dateFormat = "yyyy"
                let validYear = validYearFormat.string(from: validDate!)
                
                if Localize.currentLanguage() == "ta-IN"{
                    cell.offerValidToLabel.text = validMonth + " " + validDay + ", " + validYear + " வரை மட்டுமே இந்த ஆஃபர் கோட் செல்லுபடியாகும்"
                }
                else {
                    cell.offerValidToLabel.text = "This Offer Code is Valid till ".localized() + validMonth + " " + validDay + ", " + validYear
                }
            }
            else {
                cell.offerValidToLabel.text = ""
            }
        }
        else {
            cell.offerValidToLabel.text = ""

        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
