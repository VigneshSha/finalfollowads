//
//  RedeemMoneyTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 06/05/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

protocol RedeemMoneyDelegate {
    func navigateToView(indexPath: IndexPath)
}

class RedeemMoneyTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView?
    var walletDetailsData : [String: [String]]?
    var delegate: RedeemMoneyDelegate?

    /*
     This method is used for init tableview datasource.
     @param ~~.
     @return ~~.
     */
    init(walletDetailsData: [String: [String]]?) {
        self.walletDetailsData = walletDetailsData
        super.init()
    }
    
    // MARK: - TABLEVIEW DELEGATE & DATASOURCE
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
//            return (walletDetailsData?[RedeemMoneyList1]?.count ?? 0)
//        } else if section == 1 {
            return (walletDetailsData?[RedeemMoneyList2]?.count ?? 0)
        }
        else {
            return 1
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
//            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
//            ProfileDataCell.textLabel?.text = walletDetailsData?[RedeemMoneyList1]![indexPath.row]
//
//            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
//            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
//            ProfileDataCell.accessoryType = .disclosureIndicator
//
//            ProfileDataCell.detailTextLabel?.text =  ""
//            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
//            ProfileDataCell.imageView?.clipsToBounds = true
//            ProfileDataCell.imageView?.frame = CGRect( x: 10, y: 5, width: 34, height: 34)
//            let redImage200x200 = UIImage(named: bankDetailsImgArr1[indexPath.row])
//            ProfileDataCell.imageView?.image = redImage200x200
//            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
//            ProfileDataCell.imageView?.backgroundColor = profileBankDetailsColor
//
//            return ProfileDataCell
//        } else if indexPath.section == 1 {
            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            ProfileDataCell.textLabel?.text = walletDetailsData?[RedeemMoneyList2]![indexPath.row]
            
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.accessoryType = .disclosureIndicator

            
            ProfileDataCell.detailTextLabel?.text =  ""
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 10, y: 5, width: 34, height: 34)
            let redImage200x200 = UIImage(named: bankDetailsImgArr2[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = UIColor.clear
            
            return ProfileDataCell
            
        }  else {
            let ProfileDataCell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: ProfileCellId)
            ProfileDataCell.textLabel?.text = walletDetailsData?[RedeemMoneyList2]![indexPath.row]
            ProfileDataCell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.detailTextLabel?.font = UIFont.systemFont(ofSize: 17)
            ProfileDataCell.accessoryType = .disclosureIndicator
            ProfileDataCell.detailTextLabel?.text =  ""
            ProfileDataCell.imageView?.layer.cornerRadius = 4.0
            ProfileDataCell.imageView?.clipsToBounds = true
            ProfileDataCell.imageView?.frame = CGRect( x: 15, y: 12.5, width: 25, height: 25)
            let redImage200x200 = UIImage(named: bankDetailsImgArr2[indexPath.row])
            ProfileDataCell.imageView?.image = redImage200x200
            ProfileDataCell.addSubview(ProfileDataCell.imageView!)
            ProfileDataCell.imageView?.backgroundColor = UIColor.clear
            return ProfileDataCell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 44
    }
    
//    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == 1 {
//            let headerView = UIView()
//            let headerCell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderCellId) as! ProfileHeaderTableViewCell
//            headerView.addSubview(headerCell)
//            headerView.backgroundColor = profilePhotoCellBgColor
//            return headerView
//        } else {
//            let headerView = UIView()
//            return headerView
//        }
//    }
//
//    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        _ = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderCellId) as! ProfileHeaderTableViewCell
//    }
//
//    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 1 {
//            return 35.0
//        } else {
//            return 0.0
//        }
//    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
}
