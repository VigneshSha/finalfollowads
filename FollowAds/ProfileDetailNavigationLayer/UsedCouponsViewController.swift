//
//  UsedCouponsViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

protocol UsedCoupons {
    func UsedCoupons(string: String)
}

class UsedCouponsViewController: FollowAdsBaseViewController {
    
    @IBOutlet var tableView: UITableView!
    var usedCouponTableDatasource: UsedCouponsTableDatasource?
    var followAdsRequestManager = FollowAdsRequestManager()
    var usedCouponListDataModel: FollowAdsUsedCouponListDataModel?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var navigationTitle = "Used Offers"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        noDataLabel.isHidden = true
        noDataLabel.text = "No Used Offers".localized()
        self.tableView.isHidden = true
        self.SetUpTableDataSource()
        self.setUpNavigationBarButton()
        self.trackViewcontroller(name: "View_UsedOffers", screenClass: "UsedCouponsViewController")
        AppsFlyerTracker.shared().trackEvent("View_UsedOffers",
                                             withValues: [
                                                AFEventAdView: "View_UsedOffers",
                                                ]);
        if Reachability.isConnectedToNetwork() == true {
            self.UsedCouponListUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: internetConnectionAlert)
            noDataLabel.isHidden = false
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }

    
    /*
     This method is used to display the activity indicator.
     @param - .
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    /*
     This method is used to perform used coupon list service call.
     @param -.
     @return -.
     */
    func UsedCouponListUpdate() {
        self.followAdsRequestManager.usedCouponsListRequestManager = FollowAdsUsedCouponsListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.usedCouponsListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.usedCouponsListRequestManager?.user_id = ""
        }
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.usedCouponsListRequestManager?.lang = langId
        
        let usedCoupoonListCompletion: UsedCouponListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                self.tableView.isHidden = true
                self.noDataLabel.isHidden = false
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.tableView.isHidden = false
                        self.noDataLabel.isHidden = true
                        
                        self.usedCouponListDataModel = response!
                        print(self.usedCouponListDataModel as Any)
                        if (self.usedCouponListDataModel?.user_used_coupon_list.count)! > 0 {
                            self.tableView.isHidden = false
                            self.noDataLabel.isHidden = true
                        }
                        else {
                            self.tableView.isHidden = true
                            self.noDataLabel.isHidden = false
                        }
                        self.usedCouponTableDatasource?.usedCouponListDataModel = self.usedCouponListDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callUsedCouponListServiceCall(requestObject: self.followAdsRequestManager, usedCoupoonListCompletion)
    }
    
    /*
     This method is used to setup table view datasource.
     @param -.
     @return -.
     */
    func SetUpTableDataSource(){
        usedCouponTableDatasource = UsedCouponsTableDatasource(usedCouponsListData: usedCouponListDataModel)
        self.tableView.dataSource = usedCouponTableDatasource
        self.tableView.delegate = usedCouponTableDatasource
        usedCouponTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
