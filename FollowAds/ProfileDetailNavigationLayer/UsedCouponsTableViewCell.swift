//
//  UsedCouponsTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class UsedCouponsTableViewCell: UITableViewCell {
    
    @IBOutlet var offerNameLabel: UILabel!
    @IBOutlet var offerCodeLabel: UILabel!
    @IBOutlet var OfferDescriptionLabel: UILabel!
    @IBOutlet var offerValidToLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let borderLayer = CAShapeLayer()
        borderLayer.strokeColor = UIColor.black.cgColor
        borderLayer.lineDashPattern = [2, 2]
        borderLayer.frame = CGRect(x: offerCodeLabel.bounds.minX, y: offerCodeLabel.bounds.minY, width: offerCodeLabel.frame.size.width, height: offerCodeLabel.frame.size.height)
        borderLayer.fillColor = nil
    //    if FollowAdsDisplayType.typeIsLike == DisplayType.iphone6 {
            borderLayer.path = UIBezierPath(rect: CGRect(x: offerCodeLabel.bounds.minX, y: offerCodeLabel.bounds.minY, width: offerCodeLabel.bounds.width, height: offerCodeLabel.bounds.height)).cgPath
//        }
//        else if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
//            borderLayer.path = UIBezierPath(rect: CGRect(x: offerCodeLabel.bounds.minX, y: offerCodeLabel.bounds.minY, width: offerCodeLabel.bounds.width, height: offerCodeLabel.bounds.height)).cgPath
//        }
//        else {
//        }
        offerCodeLabel.layer.addSublayer(borderLayer)
        offerCodeLabel.backgroundColor = UIColor.lightGray
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
