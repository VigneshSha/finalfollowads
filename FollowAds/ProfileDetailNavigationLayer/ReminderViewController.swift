//
//  ReminderViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

class ReminderViewController: FollowAdsBaseViewController, ReminderListNavigationDelegate {
    
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var noDataLabel: UILabel!
    var reminderListTableDatasource: ReminderTableDatasource?
    var reminderListDataModel: FollowAdsReminderListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var selectedIndex: IndexPath?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var navigationTitle = "Reminders"
    var banner = Banner()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        noDataLabel.text = "No Reminders".localized()
        self.SetUpTableDataSource()
        self.setUpNavigationBarButton()
        self.trackViewcontroller(name: "Remainders", screenClass: "ReminderViewController")
        AppsFlyerTracker.shared().trackEvent("Remainders",
                                             withValues: [
                                                AFEventAdView: "Remainders",
                                                ]);
        if Reachability.isConnectedToNetwork() == true {
            self.reminderListUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    /*
     This method is used to show the activity indicator.
     @param - notification.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
    //    }
        
    }

    /*
     This method is used to perform didSelect delegate method action.
     @param --.
     @return --.
     */
    func navigateToView(indexPath: IndexPath) {
        selectedIndex = indexPath
        if self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].is_active != nil {
            if self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].is_active == "0" {
//                self.banner = Banner(title: "This Offer has expired.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                self.banner.dismissesOnTap = true
//                self.banner.show(duration: 3.0)
                self.view.makeToast("This Offer has expired.".localized(), duration: 3.0, position: .center)

            }
                else if self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].business_active != nil {
                if self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].business_active == "0" {
//                    self.banner = Banner(title: "This Business is currently not available.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                    self.banner.dismissesOnTap = true
//                    self.banner.show(duration: 3.0)
                    self.view.makeToast("This Business is currently not available.".localized(), duration: 3.0, position: .center)

                }
                else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                    vc.offerId = (self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].advertisement_id)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
                vc.offerId = (self.reminderListDataModel?.user_reminder_ads[(selectedIndex?.row)!].advertisement_id)!
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
       
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform reminder list service call.
     @param -.
     @return -.
     */
    func reminderListUpdate() {
        self.followAdsRequestManager.reminderListRequestManager = FollowAdsReminderListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.reminderListRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.reminderListRequestManager?.user_id = ""
        }
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.reminderListRequestManager?.lang = langId
        
        let reminderListCompletion: ReminderListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.reminderListDataModel = response!
                        print(self.reminderListDataModel as Any)
                        if (self.reminderListDataModel?.user_reminder_ads.count)! > 0 {
                            self.tableView.isHidden = false
                        }
                        else {
                            self.tableView.isHidden = true
                        }
                        self.reminderListTableDatasource?.reminderListDataModel = self.reminderListDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callReminderListServiceCall(requestObject: self.followAdsRequestManager, reminderListCompletion)
    }
    
    /*
     This method is used to setup table view datasource.
     @param -.
     @return -.
     */
    func SetUpTableDataSource(){
        reminderListTableDatasource = ReminderTableDatasource(reminderListData: reminderListDataModel)
        self.tableView.dataSource = reminderListTableDatasource
        self.tableView.delegate = reminderListTableDatasource
        reminderListTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
        reminderListTableDatasource?.delegate = self
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used to pass the value to destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
}
