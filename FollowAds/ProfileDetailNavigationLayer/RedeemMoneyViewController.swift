//
//  RedeemMoneyViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 06/05/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import FirebaseAnalytics

class RedeemMoneyViewController: UIViewController, RedeemMoneyDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var redeemMoneyList1Arr = [String]()
    var redeemMonwyList2Arr = [String]()
    
    var redeemMoneyTableDatasource: RedeemMoneyTableDatasource?
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var bankListDataModel: FollowAdsBankListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var transferMoneyDataModel: FollowAdsTransferMoneyDataModel?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Analytics.setScreenName("Payment_details", screenClass: "RedeemMoneyViewController")

        redeemMoneyList1Arr = ["Bank Account".localized()]
        redeemMonwyList2Arr = ["Bank Account".localized(), "Google Pay".localized(), "Paytm".localized(), "UPI Pin".localized()]
        self.setupTableViewDataSource()
        self.navigationItem.title = "Redeem Money".localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    lazy var info: [String: [String]] = {
        var dictionary = [String: [String]]()
        dictionary[RedeemMoneyList1] = redeemMoneyList1Arr
        dictionary[RedeemMoneyList2] = redeemMonwyList2Arr
        return dictionary
    }()

    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
     This method is used for Setup tableview data source.
     @param --.
     @return --.
     */
    func setupTableViewDataSource() {
        redeemMoneyTableDatasource = RedeemMoneyTableDatasource(walletDetailsData: info)
        self.tableView.dataSource = redeemMoneyTableDatasource
        self.tableView.delegate = redeemMoneyTableDatasource
        redeemMoneyTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView =  UIView()
        redeemMoneyTableDatasource?.delegate = self
    }
    
    /*
     This method is used to perform bank list service call.
     @param -.
     @return -.
     */
    func bankListUpdate() {
        self.followAdsRequestManager.bankDetailsListRequestManager = FollowAdsBankDetailsListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.bankDetailsListRequestManager?.lang_id = langId
        
        let bankListCompletion: BankListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            else
            {
                if response != nil {
                    self.bankListDataModel = response!
                    if response?.bank_detail.isEmpty == false {
                        
                        if (self.bankListDataModel?.bank_detail.count)! > 0 {
                            self.performSegue(withIdentifier: "BankDetailsView", sender: self)
                        }
                        else {
                            self.performSegue(withIdentifier: "AddBankDetailsView", sender: self)
                        }
                    }
                    else {
                        self.performSegue(withIdentifier: "AddBankDetailsView", sender: self)
                    }
                }
            }
        }
        FollowAdsServiceHandler.callBankListServiceCall(requestObject: self.followAdsRequestManager, bankListCompletion)
    }
    
    
    func navigateToView(indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if Reachability.isConnectedToNetwork() == true {
                    self.bankListUpdate()
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                }
            }
            if indexPath.row == 1 {
                if UserDefaults.standard.value(forKey: "GPAYMobNum") != nil {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "GPayViewId") as! GPayViewController
                    let phoneNumberText = String()
                    let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "GPAYMobNum") as? String))
                    vc.mobNum = phoneNumberCount
                    self.navigationController?.pushViewController(vc, animated: true)
                }

                else if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GPayViewId") as! GPayViewController
                        let phoneNumberText = String()
                        let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
                        vc.mobNum = phoneNumberCount
                        self.navigationController?.pushViewController(vc, animated: true)
                    }

            }
            else if indexPath.row == 2 {
            
                if UserDefaults.standard.value(forKey: "PaytmMobNum") != nil {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayTMViewId") as! PayTMViewController
                    let phoneNumberText = String()
                    let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PaytmMobNum") as? String))
                    vc.mobNum = phoneNumberCount
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayTMViewId") as! PayTMViewController
                        let phoneNumberText = String()
                        let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
                        vc.mobNum = phoneNumberCount
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
              

            }
            else if indexPath.row == 3 {
                if UserDefaults.standard.value(forKey: "UPIPinValue") != nil {

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "UPIPinViewId") as! UPIPinViewController
                let upiPinText = String()
                let upiPinCount: String = upiPinText.passedString((UserDefaults.standard.object(forKey: "UPIPinValue") as? String))
                vc.upiPinValue = upiPinCount
                        self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "UPIPinViewId") as! UPIPinViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
              
            }
            else {
                
            }
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
