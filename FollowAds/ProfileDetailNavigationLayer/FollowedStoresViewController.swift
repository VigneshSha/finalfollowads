//
//  FollowedStoresViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

protocol FollowedStores {
    func FollowesStores(string: String)
}
class FollowedStoresViewController: FollowAdsBaseViewController, ShopDetailNavigationDelegate {
    
    @IBOutlet var tableView: UITableView!
    var followedStoresTableDatasource: FollowedStoresTableDatasource?
    var followedBusinessListDataModel: FollowAdsFollowedBusinessListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var index: Int?
    var selectedIndex: IndexPath?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var navigationTitle = "Followed Stores"
    var banner = Banner()
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.trackViewcontroller(name: "View_followedStores", screenClass: "FollowedStoresViewController")
        AppsFlyerTracker.shared().trackEvent("View_followedStores",
                                             withValues: [
                                                AFEventAdView: "View_followedStores",
                                                ]);
        self.tableView.isHidden = true
        self.noDataLabel.isHidden = true
        noDataLabel.text = "No Followed Stores".localized()
        self.navigationItem.title = navigationTitle.localized()
        self.SetUpTableDataSource()
        self.setUpNavigationBarButton()
        if Reachability.isConnectedToNetwork() == true {
            self.followedBusinessListUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false

        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

//        tableView.estimatedRowHeight = 150.0
//        tableView.rowHeight = UITableViewAutomaticDimension

////        self.tableView.reloadData()

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }

    
    /*
     This method is used to display the activity indicator.
     @param - .
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform didSelect delegate method action.
     @param --.
     @return --.
     */
    func navigateToView(indexPath: IndexPath) {
        selectedIndex = indexPath
        if self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].is_active != nil {
            if self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].is_active == "0" {
//                self.banner = Banner(title: "This Business is currently not available.".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                self.banner.dismissesOnTap = true
//                self.banner.show(duration: 3.0)
                self.view.makeToast("This Business is currently not available.".localized(), duration: 3.0, position: .center)

            }
            else {
                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ShopDetailId") as! ShopDetailViewController
                if self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_id != nil {
                    vc.businessIdValue = self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_id
                    vc.businessAddrIdValue = self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_address_id
                }
                else {
                    vc.businessIdValue = ""
                    vc.businessAddrIdValue = ""
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
      //  self.performSegue(withIdentifier: "FollowedStoresViewId", sender: self)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    /*
     This method is used to perform followed business list service call.
     @param -.
     @return -.
     */
    func followedBusinessListUpdate() {
        self.followAdsRequestManager.followedBusinessListRequestManager = FollowAdsFollowedBusinessListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.followedBusinessListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.followedBusinessListRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.followedBusinessListRequestManager?.lang_id = langId
        
        let followedBusinessListCompletion: FollowedBusinessListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.followedBusinessListDataModel = response!
                        print(self.followedBusinessListDataModel as Any)
                        if (self.followedBusinessListDataModel?.followed_business.count)! > 0 {
                            self.tableView.isHidden = false
                            self.noDataLabel.isHidden = true
                        }
                        else {
                            self.tableView.isHidden = true
                            self.noDataLabel.isHidden = false
                        }
                        self.followedStoresTableDatasource?.followedBusinessListDataModel = self.followedBusinessListDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callFollowedBusinessListServiceCall(requestObject: self.followAdsRequestManager, followedBusinessListCompletion)
    }
    
    /*
     This method is used to setup table view datasource.
     @param -.
     @return -.
     */
    func SetUpTableDataSource(){
        followedStoresTableDatasource = FollowedStoresTableDatasource(followedBusinessListData: followedBusinessListDataModel)
        self.tableView.dataSource = followedStoresTableDatasource
        self.tableView.delegate = followedStoresTableDatasource
        followedStoresTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
        followedStoresTableDatasource?.delegate = self
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used to pass the value to destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if #available(iOS 10.0, *) {
            if segue.destination.isKind(of: ShopDetailViewController.self) {
//                let vc = segue.destination as? ShopDetailViewController
//                if self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_id != nil {
//                    vc?.businessIdValue = self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_id
//                    vc?.businessAddrIdValue = self.followedBusinessListDataModel?.followed_business[(selectedIndex?.row)!].business_address_id
//                }
            }
        }
    }
    
}
