//
//  AboutFollowAdsViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class AboutFollowAdsViewController: FollowAdsBaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    var WebView: WKWebView!
    var navigationTitle = "About FollowAds"
    var isAlerdayNav : Bool = false

    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        WebView = WKWebView(frame: .zero, configuration: webConfiguration)
        WebView.uiDelegate = self
        view = WebView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
//        let myURL = URL(string: "http://103.249.207.132/followads_new/aboutus")
//        let myURLTamil = URL(string: "http://api.followads.in/aboutustamil")
//        let myURLTamil = URL(string: "http://103.249.207.132/followads_new/aboutustamil")
       
        // Staging Url
//        let myURL = URL(string: "http://api.followads.in/vNxt/aboutus")
//        let myURLTamil = URL(string: "http://api.followads.in/vNxt/aboutustamil")

        //Live Url

        let myURL = URL(string: "http://api.followads.in/v1_0/aboutus")
        let myURLTamil = URL(string: "http://api.followads.in/v1_0/aboutustamil")
        
        // 103 followads_live
        
//        let myURL = URL(string: "http://103.249.207.132/followads_live/aboutus")
//        let myURLTamil = URL(string: "http://103.249.207.132/followads_live/aboutustamil")
        
        // Dev New Url - current
//        let myURL = URL(string: "http://113.193.25.22/followads_live/aboutus")
//        let myURLTamil = URL(string: "http://113.193.25.22/followads_live/aboutustamil")
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        if langId == "1" {
            let myRequest = URLRequest(url: myURL!)
            WebView.load(myRequest)
        }
        else {
            let request = URLRequest(url: myURLTamil!)
            WebView.load(request)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpNavigationBarButton()
        self.trackViewcontroller(name: "About_us", screenClass: "AboutFollowAdsViewController")
        AppsFlyerTracker.shared().trackEvent("About_us",
                                             withValues: [
                                                AFEventAdView: "About_us",
                                                ]);
        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    /*
     This method is used to show the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
   //     if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
