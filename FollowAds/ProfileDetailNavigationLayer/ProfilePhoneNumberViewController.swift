//
//  ProfilePhoneNumberViewController.swift
//  FollowAds
//
//  Created by openwave on 04/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
protocol SendingDatasDelegate{
    func PhoneNumberData(string: String)
}

class ProfilePhoneNumberViewController: FollowAdsBaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var PhoneLblField: UILabel!
    @IBOutlet weak var TextField: UITextField!
    @IBOutlet var backBtn: UIButton!
    var delegate: SendingDatasDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TextField.delegate = self
        self.TextField.keyboardType = UIKeyboardType.decimalPad
        //        delegate = self as! SendingDatasDelegate
        // Do any additional setup after loading the view.
        self.trackViewcontroller(name: "Phonenumber Screen", screenClass: "ProfilePhoneNumberViewController")

    }
    
    // TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 9
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    /*
     This method is used for submit button action.
     @param -.
     @return -.
     */
    @IBAction func SubmitBtn(_ sender: UIButton) {
        let number: String? = TextField.text!
        if delegate != nil{
            delegate.PhoneNumberData(string: number!)
            print(TextField.text!)
            self.navigationController?.popViewController(animated: true)
        } else {
            print("Sorry you cant unwrap value")
        }
    }
    
    /*
     This method is used to dismiss the view editing.
     @param -.
     @return -.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used for back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
