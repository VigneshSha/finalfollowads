//
//  LikedOffersTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 12/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol LikedOffersDelegate {
    func navigateToView(indexPath: IndexPath)
}

class LikedOffersTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView?
    var likedOffersListDataModel: FollowAdsLikedOffersListDataModel?
    var delegate: LikedOffersDelegate?
    
    init(likedOffersData: FollowAdsLikedOffersListDataModel?) {
        self.likedOffersListDataModel = likedOffersData
        super.init()
    }
    
    // MARK:- TABLEVIEW DELEGATES AND DATASOURCES

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (likedOffersListDataModel?.Liked_advertisements.count) != nil {
            return (likedOffersListDataModel?.Liked_advertisements.count)!
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "likedOffersListCellId", for: indexPath) as! LikedOffersTableViewCell
        cell.labelOfferName.text = likedOffersListDataModel?.Liked_advertisements[indexPath.row].business_name
        cell.labelShopName.text = likedOffersListDataModel?.Liked_advertisements[indexPath.row].advertisement_caption
        cell.labelOfferExpDate.text = likedOffersListDataModel?.Liked_advertisements[indexPath.row].advertisement_desc
        
        cell.offerImageView.layer.cornerRadius = 10
        cell.offerImageView.clipsToBounds = true
        
        let urlstring = self.likedOffersListDataModel?.Liked_advertisements[indexPath.row].advertisement_img
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                
                cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            cell.offerImageView.image = UIImage(named: "home_ads_placeholder")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
