//
//  ViewController.swift
//  aaaa
//
//  Created by openwave on 25/06/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeDashedBorder()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func makeDashedBorder()  {
        
        let mViewBorder = CAShapeLayer()
        mViewBorder.strokeColor = UIColor.magenta.cgColor
        mViewBorder.lineDashPattern = [2, 2]
        mViewBorder.frame = Label.bounds
        mViewBorder.fillColor = nil
        mViewBorder.path = UIBezierPath(rect: Label.bounds).cgPath
        Label.layer.addSublayer(mViewBorder)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

