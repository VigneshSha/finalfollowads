//
//  OffersPagingTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import FLAnimatedImage

protocol BannerImageNavigationDelegate {
    func navigateToBannerDetailView(indexPath: IndexPath)
}

class OffersPagingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var pageControl : UIPageControl?
    var tapGesture = UITapGestureRecognizer()
    var timer: Timer?
    var offSet: CGFloat = 0
    @IBOutlet var imageViewWidthConstraint: NSLayoutConstraint!
    var followAdsRequestManager = FollowAdsRequestManager()
    var urlString: String?
    var tableViewObj : UITableView?
    var imageUrl: URL?
    var homeDataModel: FollowAdsHomeDataModel?
    var autoScrollTimer : Timer?
    var itemsCount: Int?
    var delegate: BannerImageNavigationDelegate?
    
    @IBOutlet weak var pageControls: UIPageControl! {
        didSet {
//            if (itemsCount ?? 0) >= 10 {
//                pageControls.numberOfPages = 10
//            }
//            else {
            pageControls.numberOfPages = itemsCount ?? 0
//            }
        }
    }
    
    @IBOutlet weak var collectionView: InfiniteCollectionView! {
        didSet {
         
            collectionView.infiniteDataSource = self
            collectionView.infiniteDelegate = self
            collectionView.register(ImageCollectionViewCell.nib, forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
        }
    }
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout! {
        didSet {
            layout.itemSize = UIScreen.main.bounds.size
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // Use this method for autoscroll initiation.
    func autoScrollHandler() {
        if autoScrollTimer != nil
        {
            if (autoScrollTimer?.isValid)!{
                autoScrollTimer?.invalidate()
                autoScrollTimer = nil
                self.startTimer()
            }
            else {
                self.startTimer()
            }
        }
        else {
            self.startTimer()
        }
    }
    
    // This method is used to start timer for autoscroll.
    func startTimer() {
        let indexPath = IndexPath(row: 0, section: 0)
//        let cell = tableViewObj.cellForRow(at: indexPath)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as! ImageCollectionViewCell
        
        cell.imageView.stopAnimating()


        autoScrollTimer =  Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    // This method is used for collectionview scroll automatically.
    @objc func scrollAutomatically(_ timer1: Timer) {
        if let coll  = collectionView  {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if (indexPath?.row) != nil {
                    if ((indexPath?.row)!  <  collectionView.numberOfItems(inSection: 0) - 1){
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                        pageControls.currentPage = (indexPath1?.row)!
                    }
                    else{
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                        pageControls.currentPage = (indexPath1?.row)!
                    }
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

// MARK: - InfiniteCollectionViewDataSource, InfiniteCollectionViewDelegate
extension OffersPagingTableViewCell: InfiniteCollectionViewDataSource, InfiniteCollectionViewDelegate {
    
    func number(ofItems collectionView: UICollectionView) -> Int {
//        if (itemsCount ?? 0) >= 10 {
//            return 10
//        }else {
        return itemsCount ?? 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dequeueForItemAt dequeueIndexPath: IndexPath, cellForItemAt usableIndexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: dequeueIndexPath) as! ImageCollectionViewCell
        
        // show upto 10 datas, if the data exceeds 10
//        if self.homeDataModel?.banner?.count ?? 0 > 10 {
//            for i in (0..<10) {
//
//        if self.homeDataModel?.banner?.count ?? 0 > 1 {
//            self.collectionView.isScrollEnabled = true
//            self.autoScrollHandler()
//            self.pageControls.isHidden = false
//        }
//        else {
//            self.collectionView.isScrollEnabled = false
//            self.pageControls.isHidden = true
//        }
//
//        let urlstring = self.homeDataModel?.banner?[i].advertisment_img
//        if urlstring != nil {
//            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
//                cell.imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))
//            }
//        }
//        else {
//            cell.imageView.image = UIImage(named: "home_banner_placeholder")
//        }
//            }
//        return cell
//        }
//            // if the data count is less than 10, show the entire datas.
//        else {
            if self.homeDataModel?.banner?.count ?? 0 > 1 {
                self.collectionView.isScrollEnabled = true
                self.autoScrollHandler()
                self.pageControls.isHidden = false
            }
            else {
                self.collectionView.isScrollEnabled = false
                self.pageControls.isHidden = true
            }
            
            let urlstring = self.homeDataModel?.banner?[usableIndexPath.row].advertisment_img
            if urlstring != nil {
                if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    cell.imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))

                    // To check gif of image
                    let imageExtensions = ["gif"]
                    let pathExtention = URL(string: encodedString)?.pathExtension
                    
                    
                    DispatchQueue.global().async { [weak self] in
                        do {
                            if imageExtensions.contains(pathExtention ?? "") {
                                let gifData = try Data(contentsOf:  URL(string: encodedString)!)
//                                DispatchQueue.main.async {
                                    cell.imageView.animatedImage = FLAnimatedImage(gifData: gifData)
 //                                }
                            }
                            else {
                                cell.imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
                             }
                            
                        } catch {
                            print(error)
                        }
                    }
                    
                    
//                    DispatchQueue.main.async {
//
//                    if imageExtensions.contains(pathExtention ?? "") {
//
////                        DispatchQueue.main.async {
//                         cell.imageView.image = UIImage.gifImageWithURL(encodedString)
//
//
//
////                        cell.imageView
////                        }
//                    }
//                    else {
//                        cell.imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
//
//                    }
//                    }

                    
                }
            }
            else {
                cell.imageView.image = UIImage(named: "home_banner_placeholder")
            }
            return cell
     //   }
    }
    
   
    func infiniteCollectionView(_ collectionView: UICollectionView, didSelectItemAt usableIndexPath: IndexPath) {
        // Here you will get the page control current page, pass this to next page id.
        print()
        print("didSelectItemAt: \(usableIndexPath.item)")
        print("current Page:\(pageControls.currentPage)")
        if delegate != nil {
            delegate?.navigateToBannerDetailView(indexPath: usableIndexPath)
        }
    }
    
    // MARK: - ScrollView Delegate
    func scrollView(_ scrollView: UIScrollView, pageIndex: Int) {
        pageControls.currentPage = pageIndex // This is for page control update
    }
}

extension UIImage {
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (self.size.equalTo(aSize)) {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0)
        self.draw(in: CGRect(x:0.0, y:0.0, width:aSize.width, height:aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
