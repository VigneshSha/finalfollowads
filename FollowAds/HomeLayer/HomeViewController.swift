//
//  HomeViewController.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 23/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import BRYXBanner
import SkeletonView
import CoreLocation
import FirebaseDynamicLinks
import AppsFlyerLib

extension String {
    func SizeOf(_ font: UIFont) -> CGSize {
        return self.size(withAttributes: [NSAttributedStringKey.font: font])
    }
}

@available(iOS 10.0, *)
class HomeViewController: FollowAdsBaseViewController, UITableViewDelegate, UITableViewDataSource, NavigationUpdateDelegate, NavigateDetailDelegate, FourthPromotionDelegate, FivthPromotionDelegate, SixthPromotionDelegate, SeventhPromotionDelegate, UIScrollViewDelegate, BannerImageNavigationDelegate, PremiumDelegate, CLLocationManagerDelegate,TabGeni {
    let APPDELEGATE =   (UIApplication.shared.delegate as? AppDelegate)
    var loacationdata:String = ""
    var tappedOfferId : Int = 0
    var offersPagingTableViewCell : OffersPagingTableViewCell?
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var welcomeoutlet: UIBarButtonItem!
    var header: UITableViewHeaderFooterView? = nil
    var labelHeader: UILabel!
    var seeAllBtn: UIButton!
    let SeeAllText = "See All".localized()
    var promotionHeaders: [String]?
    var bigBagCell: BigBagOfferTableViewCell?
    var notificationUesrInfo : String?
    var followAdsRequestManager = FollowAdsRequestManager()
    var homeDataModel = FollowAdsHomeDataModel()
    var homePromotionsDataModel = FollowAdsHomePromotionsDataModel()
    var selectedIndex: IndexPath?
    var button = UIButton()
    var Firstlabel = UILabel()
      var secondLabel = UILabel()
    var locationbutton = UIButton()
    var tableCell: UITableViewCell?
    var Promotion2Name: String? = emptyString
    var Promotion3Name: String? = emptyString
    var Promotion4Name: String? = emptyString
    var Promotion5Name: String? = emptyString
    var Promotion6Name: String? = emptyString
    var Promotion7Name: String? = emptyString
    var Promotion8Name: String? = emptyString
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var offSet: CGFloat = 0
    var pageControl : UIPageControl?
    var imageUrls: URL?
    var autoScrollTimer = Timer()
    var totalPossibleOffset = CGFloat()
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    var banner = Banner()
    var banner1 = Banner()
    var areaName: String?
    var locCheck: Bool? = false
    var nearbyBussinessDataModel : FollowAdsNearByBussinessDataModel?
    var filteredArray: [String] = []
    var locationManager = CLLocationManager()
    var location: CLLocation?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var city: String?
    var country: String?
    var countryShortName: String?
    var subAdministrativeAre : String?
    var address : String?
    var postalCode: String?
    var loc_lat: String?
    var loc_lng: String?
    var isAlerdayNav : Bool = false
    var showToastBool : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
//        let button1 = UIBarButtonItem(image: UIImage(named: "Geni"), style: .plain, target: self, action: #selector(logoutUser))
//     let logoutBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(logoutUser))
//     self.navigationItem.leftBarButtonItem  = logoutBarButtonItem
//         self.navigationItem.leftBarButtonItem  = button1
        self.addLoadingView()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chnagelocation))
        self.secondLabel.addGestureRecognizer(gestureRecognizer)
        tableView.estimatedRowHeight = 120
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showAnimatedGradientSkeleton()
        self.view.showAnimatedGradientSkeleton()
        self.setupRefresh()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.homeUpdate()
//                self.presentLocationSettings()
              //  MBProgressHUD.hide(for: self.view, animated: true)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                if Reachability.isConnectedToNetwork() == true {
                    self.homeUpdate()
                    print("Device_ID \(UIDevice.current.identifierForVendor!.uuidString)")
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                }
            }
        } else {
            print("Location services are not enabled")
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        
    }
   
    @IBAction func welcomebutton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
                     self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        Firstlabel.constraints.forEach { $0.isActive = false }
        Firstlabel.removeFromSuperview()
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
        if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            showToastBool = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
 
    }
    
    /*
     This method is used to display the welcome toast.
     @param - .
     @return -.
     */
  
    func displayWelcomeToast() {
        
        if UserDefaults.standard.value(forKey: "Welcome_Area") != nil{
            setUpNavigationBarButton()
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
            welcomeoutlet.title = city
         locationbutton.setTitle(city,for: .normal)
        print("efeff",city)
            self.banner = Banner(title: "Welcome to ".localized(), subtitle: "\(city)", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
            self.banner.dismissesOnTap = true
            
            self.banner.show(duration: 3.0)
        }
    }
    @objc func chnagelocation(){
        print("osbfjobsjbojbsf")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.setNeedsLayout()
            }
        }
        if showToastBool == true {
            if UserDefaults.standard.value(forKey: "Welcome_Area") != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
                self.banner1 = Banner(title: "Welcome to ".localized(), subtitle: "\(city)", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
                self.banner1.dismissesOnTap = true
                self.banner1.show(duration: 3.0)
                showToastBool = false
            }
        }
        //  tableView.register(HomeHeaderSectionView.self, forHeaderFooterViewReuseIdentifier: "Header")
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        isAlerdayNav = false
        
        self.noDataLabel.text = "No Data Available".localized()
        setNeedsStatusBarAppearanceUpdate()
        if UserDefaults.standard.value(forKey: "Welcome_Area") != nil{
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: "Welcome_Area") as? String))
            areaName = city
        }
        noDataLabel.isHidden = true
        self.setUpNavigationBarButton()
        //   self.tableView.isHidden = true
        // Navigates to offerDetailView by tapping on push notification
        if isFromNotifi == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            vc.offerId = off_Notifi_Id!
            self.showToastBool = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bannerViewNotification), name: Notification.Name("BannerNotification"), object: nil)
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Checking Reachability and doing Home service call.
        
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.banner.hideToast()
        self.banner.clearToastQueue()
        self.view.hideToast(self.banner)
        
        self.banner1.hideToast()
        self.banner1.clearToastQueue()
        self.view.hideToast(self.banner1)
       // self.showToastBool = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "BannerNotification"), object: nil)
        isFromNotifi = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        isAlerdayNav = false
        Firstlabel.constraints.forEach { $0.isActive = false }
        Firstlabel.removeFromSuperview()
    }
    
    
    
    /*
     This method is used for banner view notification action.
     @param - .
     @return -.
     */
    @objc func bannerViewNotification() {
     
        /*  if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.presentLocationSettings()
                MBProgressHUD.hide(for: self.view, animated: true)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                if Reachability.isConnectedToNetwork() == true {
                    self.homeUpdate()
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                }
            }
        } else {
            print("Location services are not enabled")
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        } */
        
    }
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.showToastBool = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
                
                if let pushId = aps["pushnotification_id"] as? Int {
                    vc.pushnotificationId = String(pushId)
                    vc.notifiId = "1"
                }
            }
            else {
                vc.offerId = "0"
            }
        }
       
        showToastBool = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /*
     This method is used to setup refresh control.
     @param -.
     @return -.
     */
    func setupRefresh () {
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: (NSLocalizedString(pullToRefrest, comment: emptyString)))
        refreshControl.addTarget(self, action:#selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        //    self.view.addSubview(refreshControl)
    }
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func categoryList() {
        self.followAdsRequestManager.categoryListRequestManager = FollowAdsCategoryListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = ""
        }
        
        let categoryListCompletion: CategoryListCompletionBlock = {(response, error) in
            if let _ = error {
              //  MBProgressHUD.hide(for: self.view, animated: true)
                //     FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
//                self.areaList()
            }
            else
            {
                if response != nil {
                    self.determineCurrentLocation()
                    DispatchQueue.main.async(execute: {
                        self.categoryListDataModel = response!
                        print(self.categoryListDataModel)
                        sharedInstance.categoryListDataModel = self.categoryListDataModel
//                        self.areaList()
                        
//                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callCategoryListServiceCall(requestObject: self.followAdsRequestManager, categoryListCompletion)
    }
  
    /*
     This method is used to setup refresh control action.
     @param -.
     @return -.
     */
    @objc func refresh() {
        if Reachability.isConnectedToNetwork() == true {
            
//            if sharedInstance.switchLocEnabled == true {
//                self.homeUpdate()
//            }
//            else {
            
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                self.categoryList()
                DispatchQueue.main.async {
                    print("This is run on the main queue, after the previous code in outer block")
                    self.determineCurrentLocation()
                    self.locCheck = true
                    self.Promotion2Name = emptyString
                    self.Promotion3Name = emptyString
                    self.Promotion4Name = emptyString
                    self.Promotion5Name = emptyString
                    self.Promotion6Name = emptyString
                    self.Promotion7Name = emptyString
                    self.Promotion8Name = emptyString
                    
                }
            }
            
    //        }

        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
    }
    
    @IBAction func MenuAction(_ sender: Any) {
        
    }
    /*
     This method is used to add the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToBannerDetailView(indexPath: IndexPath) {
        selectedIndex = indexPath
        
        self.trackViewcontroller(name: "View_Supertop", screenClass: "HomeViewController")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.banner?[(selectedIndex?.row)!].off_id != nil {
            vc.offerId = homeDataModel.banner?[(selectedIndex?.row)!].off_id! ?? ""
            UserDefaults.standard.set(homeDataModel.banner?[(selectedIndex?.row)!].off_id!, forKey: "Offer_Id")
            
            AppsFlyerTracker.shared().trackEvent("View_Supertop",
                                                 withValues: [
                                                    AFEventAdView: homeDataModel.banner?[(selectedIndex?.row)!].off_id! ?? "",
                                                    
                                                    ]);
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToView(indexPath: IndexPath)
    {
        selectedIndex = indexPath
        
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Square1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square1",
                                                 withValues: [
                                                    AFEventAdView: "View_Square1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Square2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square2",
                                                 withValues: [
                                                    AFEventAdView: "View_Square2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Square3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square3",
                                                 withValues: [
                                                    AFEventAdView: "View_Square3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Square4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square4",
                                                 withValues: [
                                                    AFEventAdView: "View_Square4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Square5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square5",
                                                 withValues: [
                                                    AFEventAdView: "View_Square5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Square6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square6",
                                                 withValues: [
                                                    AFEventAdView: "View_Square6",
                                                    ]);
        case 6:
            self.trackViewcontroller(name: "View_Square7", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square7",
                                                 withValues: [
                                                    AFEventAdView: "View_Square7",
                                                    ]);
        case 7:
            self.trackViewcontroller(name: "View_Square8", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square8",
                                                 withValues: [
                                                    AFEventAdView: "View_Square8",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Square8", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Square8",
                                                 withValues: [
                                                    AFEventAdView: "View_Square8",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion3.isEmpty == false {
        if homeDataModel.promotions.promotion3[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion3[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion3[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
            }
        
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToDetailView(indexPath: IndexPath) {
        selectedIndex = indexPath
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Six1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six1",
                                                 withValues: [
                                                    AFEventAdView: "View_Six1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Six2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six2",
                                                 withValues: [
                                                    AFEventAdView: "View_Six2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Six3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six3",
                                                 withValues: [
                                                    AFEventAdView: "View_Six3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Six4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six4",
                                                 withValues: [
                                                    AFEventAdView: "View_Six4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Six5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six5",
                                                 withValues: [
                                                    AFEventAdView: "View_Six5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Six6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six6",
                                                 withValues: [
                                                    AFEventAdView: "View_Six6",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Six6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Six6",
                                                 withValues: [
                                                    AFEventAdView: "View_Six6",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion4[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion4[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion4[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
            //UserDefaults.standard.synchronize()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToFourthPromoView(indexPath: IndexPath) {
        selectedIndex = indexPath
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Slim1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim1",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Slim2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim2",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Slim3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim3",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Slim4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim4",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Slim5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim5",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Slim6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim6",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim6",
                                                    ]);
        case 6:
            self.trackViewcontroller(name: "View_Slim7", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim7",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim7",
                                                    ]);
        case 7:
            self.trackViewcontroller(name: "View_Slim8", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim8",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim8",
                                                    ]);
        case 8:
            self.trackViewcontroller(name: "View_Slim9", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim9",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim9",
                                                    ]);
        case 9:
            self.trackViewcontroller(name: "View_Slim10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim10",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim10",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Slim10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slim10",
                                                 withValues: [
                                                    AFEventAdView: "View_Slim10",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion5[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion5[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion5[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
            //UserDefaults.standard.synchronize()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToFivthPromoView(indexPath: IndexPath) {
        selectedIndex = indexPath
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Bar1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar1",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Bar2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar2",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Bar3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar3",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Bar4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar4",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Bar5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar5",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Bar6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar6",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar6",
                                                    ]);
        case 6:
            self.trackViewcontroller(name: "View_Bar7", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar7",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar7",
                                                    ]);
        case 7:
            self.trackViewcontroller(name: "View_Bar8", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar8",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar8",
                                                    ]);
        case 8:
            self.trackViewcontroller(name: "View_Bar9", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar9",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar9",
                                                    ]);
        case 9:
            self.trackViewcontroller(name: "View_Bar10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar10",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar10",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Bar10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Bar10",
                                                 withValues: [
                                                    AFEventAdView: "View_Bar10",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion6[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion6[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion6[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToSixthPromoView(indexPath: IndexPath) {
        selectedIndex = indexPath
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Box1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box1",
                                                 withValues: [
                                                    AFEventAdView: "View_Box1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Box2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box2",
                                                 withValues: [
                                                    AFEventAdView: "View_Box2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Box3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box3",
                                                 withValues: [
                                                    AFEventAdView: "View_Box3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Box4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box4",
                                                 withValues: [
                                                    AFEventAdView: "View_Box4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Box5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box5",
                                                 withValues: [
                                                    AFEventAdView: "View_Box5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Box6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box6",
                                                 withValues: [
                                                    AFEventAdView: "View_Box6",
                                                    ]);
        case 6:
            self.trackViewcontroller(name: "View_Box7", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box7",
                                                 withValues: [
                                                    AFEventAdView: "View_Box7",
                                                    ]);
        case 7:
            self.trackViewcontroller(name: "View_Box8", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box8",
                                                 withValues: [
                                                    AFEventAdView: "View_Box8",
                                                    ]);
        case 8:
            self.trackViewcontroller(name: "View_Box9", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box9",
                                                 withValues: [
                                                    AFEventAdView: "View_Box9",
                                                    ]);
        case 9:
            self.trackViewcontroller(name: "View_Box10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box10",
                                                 withValues: [
                                                    AFEventAdView: "View_Box10",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Box10", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Box10",
                                                 withValues: [
                                                    AFEventAdView: "View_Box10",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion7[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion7[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion7[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToSeventhPromoView(indexPath: IndexPath) {
        selectedIndex = indexPath
        switch selectedIndex?.row {
        case 0:
            self.trackViewcontroller(name: "View_Slice1", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice1",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice1",
                                                    ]);
        case 1:
            self.trackViewcontroller(name: "View_Slice2", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice2",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice2",
                                                    ]);
        case 2:
            self.trackViewcontroller(name: "View_Slice3", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice3",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice3",
                                                    ]);
        case 3:
            self.trackViewcontroller(name: "View_Slice4", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice4",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice4",
                                                    ]);
        case 4:
            self.trackViewcontroller(name: "View_Slice5", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice5",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice5",
                                                    ]);
        case 5:
            self.trackViewcontroller(name: "View_Slice6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice6",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice6",
                                                    ]);
        default:
            self.trackViewcontroller(name: "View_Slice6", screenClass: "HomeViewController")
            AppsFlyerTracker.shared().trackEvent("View_Slice6",
                                                 withValues: [
                                                    AFEventAdView: "View_Slice6",
                                                    ]);
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if homeDataModel.promotions.promotion8[(selectedIndex?.row)!].advertisement_id != nil {
            vc.offerId = homeDataModel.promotions.promotion8[(selectedIndex?.row)!].advertisement_id!
            UserDefaults.standard.set(homeDataModel.promotions.promotion8[(selectedIndex?.row)!].advertisement_id!, forKey: "Offer_Id")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform didSelectRowAtIndexPath action.
     @param -.
     @return -.
     */
    func navigateToSuperPremiumView(indexPath: IndexPath) {
        // whatsapp like view
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuperPremiumViewId") as! SuperPremiumDetailVC
        if homeDataModel.promotions.promotion2[(selectedIndex?.row)!].advertisement_id != nil {
            vc.homeDataModel = self.homeDataModel
        }
        vc.indexValue = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
        //       self.present(vc, animated: true, completion: nil)
        vc.navigationController?.navigationBar.isHidden = true
        vc.tabBarController?.tabBar.isHidden = true
    }
    
    
    /*
     This method is used to perform home service call.
     @param -.
     @return -.
     */
    func homeUpdate() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.homeRequestManager = FollowAdsHomeRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.homeRequestManager?.user_id = userId
                print("nksnsf",userId)
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.user_id = ""
            }
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.homeRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.homeRequestManager?.device_name = kGetDeviceName
            if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                let userLatText = String()
                let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                self.followAdsRequestManager.homeRequestManager?.user_lat = userLat
            }
            if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                let userLngText = String()
                let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                self.followAdsRequestManager.homeRequestManager?.user_lng = userLng
            }
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.homeRequestManager?.lang_id = langId
            if UserDefaults.standard.value(forKey: City) != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                self.followAdsRequestManager.homeRequestManager?.area = city
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.area = ""
                
            }
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.homeRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.postal_code = ""
                
            }
            
            let homeCompletion: HomeCompletionBlock = {(response, error) in
                if let _ = error {
                    self.tableView.hideSkeleton()
                    self.view.hideSkeleton()
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tableView.isHidden = true
                    self.noDataLabel.isHidden = false
                    self.refreshControl.endRefreshing()
                }
                else
                {
                    if response != nil {
                        self.homeDataModel = response!
                        print("fgdgdfg",self.homeDataModel)

                        UserDefaults.standard.set(self.homeDataModel.welecome_area, forKey: "Welcome_Area")
                        UserDefaults.standard.set(self.homeDataModel.wallet_balance, forKey: "WALLET_AMT")
                        UserDefaults.standard.set(self.homeDataModel.liked_offer_count, forKey: "LIKED_OFFERS_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.follow_business_count, forKey: "FOLLOWED_STORES_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.user_used_coupon_count, forKey: "USER_COUPON_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.user_reminder_count, forKey: "UserReminderCount")
                        UserDefaults.standard.set(self.homeDataModel.scanned_coupon_count, forKey: "Scanned_Coupon_Count")
                        UserDefaults.standard.set(self.homeDataModel.notification, forKey: "Notification_Count")

                        UserDefaults.standard.synchronize()
                        self.location = nil
                        self.tableView.isHidden = false
                        self.noDataLabel.isHidden = true
                        if sharedInstance.fromNotification == false {
                            self.displayWelcomeToast()

                        }else {
                            DispatchQueue.main.async {
                                self.banner.hideToast()
                                self.banner.clearToastQueue()
                                self.view.hideToast(self.banner)
                            }
                        }

                        if let tabItems = self.tabBarController?.tabBar.items {
                            if UserDefaults.standard.value(forKey: UserId) != nil {
                                
                            if self.APPDELEGATE?.isNoted == false {
//                                if (response?.notification) != nil {
//                                    notificationCount = (response?.notification)!
//                                    let notifyCountInt: Int = Int(notificationCount) ?? 0
//                                    if notificationCount != "" {
//                                        if notificationCount == "0" {
//                                        } else {
//                                            if notifyCountInt <= 0 {
//                                            }
//                                            else {
//                                            let tabItem = tabItems[1]
//                                            tabItem.badgeValue = notificationCount
//                                            }
//                                        }
//                                    }
//                                    else {
//                                        let tabItem = tabItems[1]
//                                        tabItem.badgeValue = notificationCount
//                                    }
//                                }
                                self.APPDELEGATE?.isNoted = true
                                    }
                            }
                            
           /*                 // In this case we want to modify the badge number of the third tab:
                                  if UserDefaults.standard.bool(forKey: "isNewNotificationCome") == true {
                                 
//                                        let tabItem = tabItems[1]
//                                        tabItem.badgeValue = notificationCount
                                        if #available(iOS 10.0, *) {
//                                            tabItem.badgeColor = appThemeColor
//                                             tabItem.badgeValue = ""
//                                            YourTabBarController.addDotAtTabBarItemIndex(index: 0, radius: 7, color : UIColor.blue)
                                            self.tabBarController?.addDotAtTabBarItemIndex(index: 0)

                                         } else {
                                            // Fallback on earlier versions
                                        }
                                 }
                                else {
//                                    let tabItem = tabItems[1]
                                    self.tabBarController?.removeDotAtTabBarItemIndex(index: 0)

//                                    tabItem.badgeValue = nil
 
                                } */
                         }
                        self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
 
                        DispatchQueue.global(qos: .background).async {
                            // Call your background task
                            self.homePromotionsUpdate(limit: "1")
                            
                            DispatchQueue.main.async {
                                // UI Updates here for task complete.
//                                self.tableView.reloadData()
                                self.tableView.hideSkeleton()
                                self.view.hideSkeleton()
                                let indexPath0 = NSIndexPath(row: 0, section: 0)
                                let indexPath1 = NSIndexPath(row: 0, section: 1)
                                
                                self.tableView.reloadRows(at: [indexPath0 as IndexPath,indexPath1 as IndexPath], with: UITableViewRowAnimation.none)

                                self.refreshControl.endRefreshing()
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }
                        
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableView.hideSkeleton()
                        self.view.hideSkeleton()
                        self.tableView.isHidden = true
                        self.noDataLabel.isHidden = false
                    }
                }
            }
            FollowAdsServiceHandler.callHomeServiceCall(requestObject: self.followAdsRequestManager, homeCompletion)
        }
        else {
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
         }
    }
    
    /*
     This method is used to perform home Promotions service call.
     @param -.
     @return -.
     */
    func homePromotionsUpdate(limit: String) {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.homePromotionsRequestManager = FollowAdsHomePromotionsRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = ""
            }
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.device_name = kGetDeviceName
            if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                let userLatText = String()
                let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lat = userLat
            }
            if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                let userLngText = String()
                let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lng = userLng
            }
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.homePromotionsRequestManager?.lang_id = langId
            if UserDefaults.standard.value(forKey: City) != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.area = city
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.area = ""
                
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.limit = limit
            
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = ""
                
            }
            
            let homePromotionsCompletion: HomeCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
             //       FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                    self.refreshControl.endRefreshing()
                }
                else
                {
                    if response != nil {
 
                        print(self.homePromotionsDataModel)
                        
                        
                        self.tableView.isHidden = false
                        self.noDataLabel.isHidden = true
                        
                             for index3 in (response?.promotions.promotion3)! {
                                self.homeDataModel.promotions.promotion3.append(index3)
                            }
                            for index4 in (response?.promotions.promotion4)! {
                                self.homeDataModel.promotions.promotion4.append(index4)
                            }
                     
                        if self.homeDataModel.promotions.promotion3.isEmpty == true {
                        }
                        else {
                            self.Promotion3Name = self.homeDataModel.promotions.promotion3[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                        }
                        if self.homeDataModel.promotions.promotion4.isEmpty == true {
                        }
                        else {
                            self.Promotion4Name = self.homeDataModel.promotions.promotion4[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                        }
                        
                        
                        
                        DispatchQueue.global(qos: .background).async {
                            // Call your background task
                            
                            self.homePromotionsTwoUpdate(limit: "2")
                            
                            DispatchQueue.main.async {
                                // UI Updates here for task complete.
                                let indexPath2 = NSIndexPath(row: 0, section: 2)
                                let indexPath3 = NSIndexPath(row: 0, section: 3)
                                
                                self.tableView.reloadRows(at: [indexPath2 as IndexPath,indexPath3 as IndexPath], with: UITableViewRowAnimation.none)
                                self.refreshControl.endRefreshing()
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }
                        }
 
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableView.isHidden = true
                        self.noDataLabel.isHidden = false
                    }
                }
            }
            FollowAdsServiceHandler.callHomePromotionsServiceCall(requestObject: self.followAdsRequestManager, homePromotionsCompletion)
        }
        else {
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    /*
     This method is used to perform home Promotions 2 service call.
     @param -.
     @return -.
     */
    func homePromotionsTwoUpdate(limit: String) {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.homePromotionsRequestManager = FollowAdsHomePromotionsRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = ""
            }
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.device_name = kGetDeviceName
            if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                let userLatText = String()
                let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lat = userLat
            }
            if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                let userLngText = String()
                let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lng = userLng
            }
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.homePromotionsRequestManager?.lang_id = langId
            if UserDefaults.standard.value(forKey: City) != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.area = city
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.area = ""
                
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.limit = limit
            
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = ""
                
            }
            
            let homePromotionsCompletion: HomeCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
              //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                    self.refreshControl.endRefreshing()
                }
                else
                {
                    if response != nil {
 
                        print(self.homePromotionsDataModel)
 
                        
                             for index5 in (response?.promotions.promotion5)! {
                                self.homeDataModel.promotions.promotion5.append(index5)
                            }
                            for index6 in (response?.promotions.promotion6)! {
                                self.homeDataModel.promotions.promotion6.append(index6)
                            }
 
                         if self.homeDataModel.promotions.promotion5.isEmpty == true {
                        }
                        else {
                            self.Promotion5Name = self.homeDataModel.promotions.promotion5[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                         }
                        if self.homeDataModel.promotions.promotion6.isEmpty == true {
                        }
                        else {
                            self.Promotion6Name = self.homeDataModel.promotions.promotion6[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                         }
 
                        
                        
                        DispatchQueue.global(qos: .background).async {
                            // Call your background task
                            
                            self.homePromotionsThreeUpdate(limit: "3")
                            
                            DispatchQueue.main.async {
                                // UI Updates here for task complete.
                                let indexPath4 = NSIndexPath(row: 0, section: 4)
                                let indexPath5 = NSIndexPath(row: 0, section: 5)
                                
                                self.tableView.reloadRows(at: [indexPath4 as IndexPath,indexPath5 as IndexPath], with: UITableViewRowAnimation.none)
                                
                            }
                        }
                        
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableView.isHidden = true
                        self.noDataLabel.isHidden = false
                    }
                }
            }
            FollowAdsServiceHandler.callHomePromotionsServiceCall(requestObject: self.followAdsRequestManager, homePromotionsCompletion)
        }
        else {
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    /*
     This method is used to perform home Promotions 3 service call.
     @param -.
     @return -.
     */
    func homePromotionsThreeUpdate(limit: String) {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.homePromotionsRequestManager = FollowAdsHomePromotionsRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.user_id = ""
            }
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.device_name = kGetDeviceName
            if UserDefaults.standard.value(forKey: "User_Lat") != nil {
                let userLatText = String()
                let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lat = userLat
            }
            if UserDefaults.standard.value(forKey: "User_Lng") != nil {
                let userLngText = String()
                let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.user_lng = userLng
            }
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.homePromotionsRequestManager?.lang_id = langId
            if UserDefaults.standard.value(forKey: City) != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.area = city
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.area = ""
                
            }
            self.followAdsRequestManager.homePromotionsRequestManager?.limit = limit
            
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.homePromotionsRequestManager?.postal_code = ""
                
            }
            
            let homePromotionsCompletion: HomeCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
             //       FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                    self.refreshControl.endRefreshing()
                }
                else
                {
                    if response != nil {
 
                        print(self.homePromotionsDataModel)
                             for index7 in (response?.promotions.promotion7)! {
                                self.homeDataModel.promotions.promotion7.append(index7)
                            }
                            for index8 in (response?.promotions.promotion8)! {
                                self.homeDataModel.promotions.promotion8.append(index8)
                            }
                          if self.homeDataModel.promotions.promotion7.isEmpty == true {
                        }
                        else {
                            self.Promotion7Name = self.homeDataModel.promotions.promotion7[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                         }
                        if self.homeDataModel.promotions.promotion8.isEmpty == true {
                        }
                        else {
                            self.Promotion8Name = self.homeDataModel.promotions.promotion8[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                         }
                        
                        
                        
                        DispatchQueue.global(qos: .background).async {
                            // Call your background task
                            
                             DispatchQueue.main.async {
                                // UI Updates here for task complete.
                                  self.tableView.reloadData()
                             }
                        }
 
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableView.isHidden = true
                        self.noDataLabel.isHidden = false
                    }
                }
            }
            FollowAdsServiceHandler.callHomePromotionsServiceCall(requestObject: self.followAdsRequestManager, homePromotionsCompletion)
        }
        else {
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    
    /*
     This method is used to perform home service call.
     @param -.
     @return -.
     */
    func homeDefaultUpdate() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.homeRequestManager = FollowAdsHomeRequestManager()
            if UserDefaults.standard.value(forKey: UserId) != nil {
                let userIdText = String()
                let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                self.followAdsRequestManager.homeRequestManager?.user_id = userId
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.user_id = ""
            }
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.homeRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.homeRequestManager?.device_name = kGetDeviceName
            
            self.followAdsRequestManager.homeRequestManager?.user_lat = "13.0899778"
            
            self.followAdsRequestManager.homeRequestManager?.user_lng = "80.1901662"
            
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.homeRequestManager?.lang_id = langId
            if UserDefaults.standard.value(forKey: City) != nil{
                let cityName = String()
                let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
                self.followAdsRequestManager.homeRequestManager?.area = city
            }
            else {
                self.followAdsRequestManager.homeRequestManager?.area = ""
                
            }
            
            let homeCompletion: HomeCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
              //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                    self.refreshControl.endRefreshing()
                }
                else
                {
                    if response != nil {
                        self.homeDataModel = response!
                        print(self.homeDataModel)
                        UserDefaults.standard.set(self.homeDataModel.welecome_area, forKey: "Welcome_Area")
                        
                        UserDefaults.standard.set(self.homeDataModel.wallet_balance, forKey: "WALLET_AMT")
                        UserDefaults.standard.set(self.homeDataModel.liked_offer_count, forKey: "LIKED_OFFERS_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.follow_business_count, forKey: "FOLLOWED_STORES_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.user_used_coupon_count, forKey: "USER_COUPON_COUNT")
                        UserDefaults.standard.set(self.homeDataModel.user_reminder_count, forKey: "UserReminderCount")
                        UserDefaults.standard.set(self.homeDataModel.scanned_coupon_count, forKey: "Scanned_Coupon_Count")
                        UserDefaults.standard.synchronize()
                        if let tabItems = self.tabBarController?.tabBar.items {
                            // In this case we want to modify the badge number of the third tab:
                            if (response?.notification) != nil {
                                notificationCount = (response?.notification)!
                                if notificationCount != "" {
                                    if notificationCount == "0" {
                                    } else {
                                        let tabItem = tabItems[1]
                                        tabItem.badgeValue = notificationCount
                                    }
                                }
                                else {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = notificationCount
                                }
                            }
                        }
                        
                        //                        if self.homeDataModel.promotions.promotion1.isEmpty == true {
                        //                            self.tableView.isHidden = true
                        //                            self.noDataLabel.isHidden = false
                        //                        }
                        //                        else {
                        //                            self.Promotion1Name = self.homeDataModel.promotions.promotion1[0].promotion_name!
                        //                            self.promotionHeaders = ["", "", self.Promotion1Name!, self.Promotion2Name!, self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!]
                        //                            self.tableView.isHidden = false
                        //                        }
                        
                        if self.homeDataModel.promotions.promotion2.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion2Name = self.homeDataModel.promotions.promotion2[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //         self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion3.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion3Name = self.homeDataModel.promotions.promotion3[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //        self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion4.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion4Name = self.homeDataModel.promotions.promotion4[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //       self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion5.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion5Name = self.homeDataModel.promotions.promotion5[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //            self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion6.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion6Name = self.homeDataModel.promotions.promotion6[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //            self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion7.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion7Name = self.homeDataModel.promotions.promotion7[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //        self.tableView.isHidden = false
                        }
                        if self.homeDataModel.promotions.promotion8.isEmpty == true {
                            //                            self.tableView.isHidden = true
                            //                            self.noDataLabel.isHidden = false
                        }
                        else {
                            self.Promotion8Name = self.homeDataModel.promotions.promotion8[0].promotion_name!
                            self.promotionHeaders = ["", "", self.Promotion3Name!, self.Promotion4Name!, self.Promotion5Name!, self.Promotion6Name!, self.Promotion7Name!, self.Promotion8Name!]
                            //        self.tableView.isHidden = false
                        }
                        if self.homeDataModel.banner?.isEmpty == false {
                            imageUrl = self.homeDataModel.banner?[0].advertisment_img! ?? ""
                        }
                        //       self.tableView.isHidden = false
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                            self.refreshControl.endRefreshing()
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        //                        self.tableView.isHidden = true
                        //                        self.noDataLabel.isHidden = false
                    }
                }
            }
            FollowAdsServiceHandler.callHomeServiceCall(requestObject: self.followAdsRequestManager, homeCompletion)
        }
        else {
            self.presentLocationSettings()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func navigateToDefaultLocationAlert() {
        let alertController = UIAlertController(title: alertTitle.localized(),
                                                message: "The offers displayed are limited only to Chennai locality. We will be showing offers in your location very soon.".localized(),
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Switch Location".localized(), style: .default) { _ in
            self.addLoadingView()
            // Checking Reachability and doing Home service call.
            if Reachability.isConnectedToNetwork() == true {
                self.homeDefaultUpdate()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
        })
        
        present(alertController, animated: true)
    }
    
    /*
     This method is used to show the alert controller.
     @param -.
     @return -.
     */
    func presentLocationSettings() {
        let alertController = UIAlertController(title: "Oops!".localized(),
                                                message: "Location access is denied. Please enable it to proceed further.".localized(),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .default))
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                        // Handle
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        })
        
        present(alertController, animated: true)
    }
    
    /*
     This method is used to setup navigation bar buttons.
     @param ~~.
     @return ~~.
     */
  @objc func loactionaction(sender: UIButton!) {
        let btnsendtag: UIButton = sender
        if btnsendtag.tag == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
            
       
            self.navigationController?.pushViewController(vc, animated: true)
            
print("dkbsljbjslfblsf")
            
        }
    }
    func setUpNavigationBarButton()
    {
        if let navigationBar = self.navigationController?.navigationBar {
//            let btn1 = UIButton()
//          //  let image = UIImage()
//            let welcomlabel = UILabel()
//            let Selectlocationbutton = UIButton()
//            btn1.setImage(UIImage(named: "Geni"), for: .normal)
//            btn1.frame = CGRect(x: 0, y: 0, width: navigationBar.frame.width/2, height: 10)
//            welcomlabel.frame = CGRect(x: 0, y: 0, width: 5, height: 7)
//            Selectlocationbutton.frame = CGRect(x: 0, y: 3, width: 20, height: 10)
//            welcomlabel.text = "welcome to"
//            Selectlocationbutton.titleLabel?.text = "welcome to"
//            Selectlocationbutton.addTarget(self, action:  #selector(logoutUser), for: .touchUpInside)
//             self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: btn1), animated: true);
//             self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: Selectlocationbutton), animated: true);
//             self.navigationItem.setLeftBarButton(UIBarButtonItem(customView: welcomlabel), animated: true);
//            let firstFrame = CGRect(x: 50, y: 3, width: navigationBar.frame.width/2, height: 10)
//            let secondFrame = CGRect(x:50, y: 5, width: 100, height: navigationBar.frame.height)
//
//            Firstlabel = UILabel(frame: firstFrame)
//            locationbutton = UIButton(frame: secondFrame)
//            locationbutton.contentHorizontalAlignment = .left;
//            Firstlabel.text = "welcome to"
//            //locationbutton.backgroundColor =  UIColor.gray
//            locationbutton.addTarget(self, action: #selector(loactionaction), for: .touchUpInside)
//            locationbutton.tag = 1
//            Firstlabel.textColor =  UIColor.gray
//            Firstlabel.font = Firstlabel.font.withSize(10)
//            //secondLabel = UILabel(frame: secondFrame)
//            //    secondLabel.textColor =  UIColor.white
//
//            navigationBar.addSubview(Firstlabel)
//         //   navigationBar.addSubview(secondLabel)
//             navigationBar.addSubview(locationbutton)
        }
//        let navLabel = UILabel()
//
//
//        let navTitle = NSMutableAttributedString(string: "welcome", attributes:[
//            NSAttributedStringKey.foregroundColor: UIColor.white,
//            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 11)!])
//
////        navTitle.append(NSMutableAttributedString(string: "™", attributes:[
////            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!,
////            NSAttributedStringKey.foregroundColor: UIColor.white]))
//        navLabel.attributedText = navTitle
//        self.navigationItem.titleView = navLabel
//
////        self.navigationController?.navigationBar.titleTextAttributes = navTitle
//
////        self.navigationController?.navigationBar.titleTextAttributes =
////            [NSAttributedStringKey.foregroundColor: UIColor.white,
////             NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TABLEVIEW DELEGATES AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if homeDataModel.banner?.isEmpty == true {
                return 0.0
            }
            else {
                if FollowAdsDisplayType.typeIsLike == DisplayType.iphoneX || FollowAdsDisplayType.typeIsLike == DisplayType.unknown
                {
                    return tableView.bounds.size.height / 2.4
                }
                else if FollowAdsDisplayType.typeIsLike == DisplayType.iphoneXR {
                    return tableView.bounds.size.height / 2.8
                }
                else if FollowAdsDisplayType.typeIsLike == DisplayType.iphone7plus {
                    return tableView.bounds.size.height / 2.2
                }
                else {
                    return tableView.bounds.size.height / 2.0
                }
            }
        }
        else if indexPath.section == 1{
            if homeDataModel.promotions.promotion2.isEmpty == false {
                return 120
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 2 {
            //      if homeDataModel.promotions.promotion3.isEmpty == false {
            if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
                if (homeDataModel.promotions.promotion3.count) > 6 {
                    return tableView.bounds.size.width + 540
                }
                else if (homeDataModel.promotions.promotion3.count) > 4 {
                    //   return tableView.bounds.size.width + 216
                    return tableView.bounds.size.width + 324
                    
                }
                else if (homeDataModel.promotions.promotion3.count) > 2 {
                    return tableView.bounds.size.width + 108
                }
//                else if (homeDataModel.promotions.promotion3.count) > 0 {
//                    return (tableView.bounds.size.width + 108) / 2
//                }
                else {
                    return (tableView.bounds.size.width + 108) / 2
                }
            }
            else if FollowAdsDisplayType.typeIsLike == DisplayType.iphone7plus {
                if (homeDataModel.promotions.promotion3.count) > 6 {
                    return tableView.bounds.size.width + 630
                }
                else if (homeDataModel.promotions.promotion3.count) > 4 {
                    //   return tableView.bounds.size.width + 216
                    return tableView.bounds.size.width + 324
                    
                }
                else if (homeDataModel.promotions.promotion3.count) > 2 {
                    return tableView.bounds.size.width + 108
                }
//                else if (homeDataModel.promotions.promotion3.count) > 0 {
//                    return (tableView.bounds.size.width + 108) / 2
//                }
                else {
                    return (tableView.bounds.size.width + 108) / 2
                }
            }
            else {
                if (homeDataModel.promotions.promotion3.count) > 6 {
                 //   return tableView.bounds.size.width + 590
                    return tableView.bounds.size.width + 613

                }
                else if (homeDataModel.promotions.promotion3.count) > 4 {
                    //   return tableView.bounds.size.width + 216
                  //  return tableView.bounds.size.width + 324
                    return tableView.bounds.size.width + 366
                    
                }
                else if (homeDataModel.promotions.promotion3.count) > 2 {
                    return tableView.bounds.size.width + 108
                }
//                else if (homeDataModel.promotions.promotion3.count) > 0 {
//                    return (tableView.bounds.size.width + 108) / 2
//                }
                else {
                    return (tableView.bounds.size.width + 108) / 2
                }
            }
//                        }
//                        else {
//                        return (tableView.bounds.size.width + 108) / 2
//
//                        }
        }
        else if indexPath.section == 3 {
            if homeDataModel.promotions.promotion4.isEmpty == false {
                if (homeDataModel.promotions.promotion4.count) > 9 {
                    return 680.0
                }
                else if (homeDataModel.promotions.promotion4.count) > 6 {
                    return 510.0
                }
                else if (homeDataModel.promotions.promotion4.count) > 3 {
                    return 340.0
                }
                else {
                    return 170.0
                }
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 4 {
            if homeDataModel.promotions.promotion5.isEmpty == false {
                return tableView.bounds.size.width/1.6//280.0
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 5 {
            if homeDataModel.promotions.promotion6.isEmpty == false {
                return 181
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 6 {
            if homeDataModel.promotions.promotion7.isEmpty == false {
                return 191
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 7 {
            if homeDataModel.promotions.promotion8.isEmpty == false {
                switch homeDataModel.promotions.promotion8.count {
                case 0 :
                    return 0
                case 1:
                    return 165
                case 2:
                    return 330
                case 3:
                    return 495
                case 4:
                    return 660
                case 5:
                    return 825
                case 6:
                    return 990
                case 7:
                    return 1155
                case 8:
                    return 1320
                case 9:
                    return 1485
                default:
                    return 1485
                }
            }
            else {
                return 0
            }
        }
        else if indexPath.section == 8 {
            return 50
        }
        else {
            return 200.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: tableView.sectionHeaderHeight))
        view.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view.contentView.backgroundColor = UIColor.white
        return view
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        DispatchQueue.main.async {
            
            self.header = view as? UITableViewHeaderFooterView
            // create a label for section header.
            self.labelHeader = UILabel(frame: CGRect(x: 10, y: 22, width: self.view.frame.size.width - 70, height: 20));
            self.header?.addSubview(self.labelHeader!)
            
            self.labelHeader?.text = self.promotionHeaders?[section].capitalizingFirstLetter()
            self.labelHeader?.textColor = UIColor(red: 39.0/255.0, green: 31.0/255.0, blue: 32.0/255.0, alpha: 1.0)
            //        labelHeader?.font = UIFont(name: "System-Medium", size: 18)
            self.labelHeader?.font = UIFont.mediumSystemFont(ofSize: 19)
            
            let locStr = "See All".localized(using: buttonTitles)
            let stringsize = locStr.SizeOf(UIFont.mediumSystemFont(ofSize: 15))
            self.button  = UIButton(frame: CGRect(x: (self.header?.frame.size.width)! - stringsize.width - 15, y: 16, width: stringsize.width, height: 30))
            self.button.setTitle("See All".localized(using: buttonTitles), for: UIControlState.normal)
            print("******witdsads\(stringsize.width)")
            self.button.sizeToFit()
            self.button.setTitleColor(appThemeColor, for: UIControlState.normal)
            self.button.titleLabel?.font =  UIFont.mediumSystemFont(ofSize: 15)
            self.header?.addSubview(self.button)
            self.button.addTarget(self, action: #selector(self.SeeAllBtnClicked), for: UIControlEvents.touchUpInside)
            
            if section == 2 {
                self.button.tag = 0
            } else if section == 3 {
                self.button.tag = 1
            }
            else if section == 4 {
                self.button.tag = 2
            }
            else if section == 5 {
                self.button.tag = 3
            } else if section == 6 {
                self.button.tag = 4
            }
            else {
                self.button.tag = 5
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 0 {
            if section == 2 {
                if homeDataModel.promotions.promotion3.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else if section == 3 {
                if homeDataModel.promotions.promotion4.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else if section == 4 {
                if homeDataModel.promotions.promotion5.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else if section == 5 {
                if homeDataModel.promotions.promotion6.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else if section == 6 {
                if homeDataModel.promotions.promotion7.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else if section == 7 {  
                if homeDataModel.promotions.promotion8.isEmpty == false {
                    return 60.0
                }
                else {
                    return 0.0
                }
            }
            else {
                return 0.0
            }
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "offerPagingTableViewCell", for: indexPath) as? OffersPagingTableViewCell
            {
                cell.tableViewObj = self.tableView
                cell.homeDataModel = self.homeDataModel
                cell.delegate = self
                cell.itemsCount = self.homeDataModel.banner?.count
//                if self.homeDataModel.banner?.count ?? 0 >= 10 {
//                    cell.pageControls.numberOfPages = 10
//                }
//                else {
                    cell.pageControls.numberOfPages = self.homeDataModel.banner?.count ?? 0
//                }
                DispatchQueue.main.async {
                    cell.collectionView.reloadData()
                }
                selectedIndex = indexPath
                return cell
            }
            return UITableViewCell()
        }
        else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "superPremiumTableViewCell", for: indexPath) as? SuperPremiumTableViewCell
            {
                if homeDataModel.promotions.promotion2.isEmpty == false {
                    cell.homeDataModel = self.homeDataModel
                    DispatchQueue.main.async {
                        cell.collectionVw.isPagingEnabled = false
                        cell.collectionVw.reloadData()
                     }
                    cell.delegate = self
                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as? BigBagOfferTableViewCell
            {
          //           if homeDataModel.promotions.promotion3.isEmpty == false {
                cell.homeDataModel = self.homeDataModel

                DispatchQueue.main.async {
                    cell.hideAnimation()
                    cell.collectionView.reloadData()
                }
                cell.navigationDel = self
                cell.isEditing = false
                return cell
        //               }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 3 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "flashOffersTableViewCell", for: indexPath) as? FlashOffersTableViewCell
            {
                if homeDataModel.promotions.promotion4.isEmpty == false {
                    cell.homeDataModel = self.homeDataModel
                    DispatchQueue.main.async {

                    cell.collectionView.reloadData()
                    }
                    cell.delegate = self
                    cell.isEditing = false

                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 4 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "flatsNearByTableViewCell", for: indexPath) as? FlatsNearByTableViewCell
            {
                if homeDataModel.promotions.promotion5.isEmpty == false {
                    cell.homeDataModel = self.homeDataModel
                           DispatchQueue.main.async {
                    cell.collectionView.isPagingEnabled = false
                    cell.collectionView.reloadData()
                            }
                    cell.del = self
                    cell.isEditing = false

                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 5 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "percentOfferTableViewCell", for: indexPath) as? PercentOfferTableViewCell
            {
                if homeDataModel.promotions.promotion6.isEmpty == false {
                    cell.homeDataModel = self.homeDataModel
                    DispatchQueue.main.async {
                    cell.collectionView.isPagingEnabled = false
                    cell.collectionView.reloadData()
                    }
                    cell.delegate = self
                    cell.isEditing = false

                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 6 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "seasonalOffersTableViewCell", for: indexPath) as? SeasonalOffersTableViewCell
            {
                if homeDataModel.promotions.promotion7.isEmpty == false {
                    cell.homeDataModel = self.homeDataModel
                    DispatchQueue.main.async {
                    cell.collectionView.isPagingEnabled = false
                    cell.collectionView.reloadData()
                    }
                    cell.delegate = self
                    cell.isEditing = false

                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 7 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "brandOffersTableViewCell", for: indexPath) as? BrandOffersTableViewCell
            {
                if homeDataModel.promotions.promotion8.isEmpty == false {
//                    cell.homeDataModel?.promotions.promotion8 = []
                    cell.homeDataModel = self.homeDataModel
                    DispatchQueue.main.async {
                     cell.collectionView.reloadData()
                    }
                    cell.delegate = self
                    cell.isEditing = false

                    return cell
                }
            }
            return UITableViewCell()
        }
        else if indexPath.section == 8 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NearbyOffersCellId", for: indexPath) as? NearbyOffersTableViewCell
            {
                cell.nearbyOffersBtn.addTarget(self, action: #selector(nearbyOffersBtnPressed), for: .touchUpInside)
                return cell
            }
            return UITableViewCell()
        }
        else  {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as? BigBagOfferTableViewCell
            {
                return cell
            }
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    // MARK: Button Actions
    
    /*
     This method is used to perform gesture action.
     @param ~~.
     @return ~~.
     */
    @objc func tapEdit(recognizer: UITapGestureRecognizer)  {
        let selectedId = offersPagingTableViewCell?.pageControls?.currentPage
        self.tappedOfferId = Int(selectedId ?? 0)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        //   self.performSegue(withIdentifier: "OfferDetailViewId", sender: self)
    }
    
    /*
     This method is used to perform location button action.
     @param ~~.
     @return ~~.
     */
    @IBAction func locationBtnAction(_ sender: UIBarButtonItem) {
        Firstlabel.isHidden = true
              secondLabel.isHidden =  true
        self.performSegue(withIdentifier: "GotoCategory", sender: self)
    }
    
    /*
     This method is used to perform search button action.
     @param ~~.
     @return ~~.
     */
    @IBAction func searchBtnAction(_ sender: UIBarButtonItem) {
        Firstlabel.isHidden = true
        secondLabel.isHidden =  true
        self.performSegue(withIdentifier: "searchViewId", sender: self)
    }
    @IBAction func GeniAction(_ sender: UIBarButtonItem) {
          Firstlabel.isHidden = true
          secondLabel.isHidden =  true

      
      }
    func openGeni() {
        print("dgsfgdsfsf")
    }
    /*
     This method is used for see all button action.
     @param ~~.
     @return ~~.
     */
    @objc func SeeAllBtnClicked(sender: UIButton!){
        if sender.tag == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BigBagOffersViewController") as! BigBagOffersViewController
            vc.promotionId = homeDataModel.promotions.promotion3[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion3[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FlashOffersCollectionViewController") as! FlashOffersCollectionViewController
            vc.promotionId = homeDataModel.promotions.promotion4[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion4[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FlatSaleNearByCollectionViewController") as! FlatSaleNearByCollectionViewController
            vc.promotionId = homeDataModel.promotions.promotion5[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion5[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EightyPercentShoeOfferViewController") as! EightyPercentShoeOfferViewController
            vc.promotionId = homeDataModel.promotions.promotion6[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion6[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SeasonalOffersViewController") as! SeasonalOffersViewController
            vc.promotionId = homeDataModel.promotions.promotion7[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion7[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        } else if sender.tag == 5 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BrandOffersViewController") as! BrandOffersViewController
            vc.promotionId = homeDataModel.promotions.promotion8[0].promotion_id
            vc.navigationTitle = homeDataModel.promotions.promotion8[0].promotion_name
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*
     This method is used for nearby offers button action.
     @param ~~.
     @return ~~.
     */
    @objc func nearbyOffersBtnPressed() {
        self.performSegue(withIdentifier: "CurrentLocationIdentifier", sender: self)
    }
    
    /*
     This method is used for value parsing from one Vc to another Vc.
     @param ~~.
     @return ~~.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: OfferDetailViewController.self) {
            let vc = segue.destination as? OfferDetailViewController
            vc?.offerId = homeDataModel.banner?[self.tappedOfferId].off_id! ?? ""
        }
    }
    
    /*
     This method is used to determine current location.
     @param -.
     @return -.
     */
    
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: CLLOCATION MANAGER DELEGATES
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        let lat: Double = Double(userLocation.coordinate.latitude)
        let longt: Double = Double(userLocation.coordinate.longitude)
        
        loc_lat = String(describing: userLocation.coordinate.latitude)
        loc_lng = String(describing: userLocation.coordinate.longitude)
        
        UserDefaults.standard.set(loc_lat, forKey: "User_Lat")
        UserDefaults.standard.set(loc_lng, forKey: "User_Lng")
        
        let latString:String = String(format:"%3f", lat)
        print("latString: \(latString)") // b: 1.500000
        let lonString:String = String(format:"%3f", longt)
        print("lonString: \(lonString)") // b: 1.500000
        self.refreshControl.endRefreshing()

        let latestLocation = locations.last!
        // here check if no need to continue just return still in the same place
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        // if it location is nil or it has been moved
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in

                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need
                self.parsePlacemarks()
                if self.city != nil {
                }
            })
        }
        else {
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.subLocality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                    UserDefaults.standard.set(city, forKey: City)
                    UserDefaults.standard.synchronize()
                }
                if let subAdministrativeAre = placemark.name, !subAdministrativeAre.isEmpty {
                    self.subAdministrativeAre = subAdministrativeAre
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    self.country = country
                }
                if let postalCode = placemark.postalCode, !postalCode.isEmpty {
                    self.postalCode = postalCode
                    UserDefaults.standard.set(postalCode, forKey: "Postal_Code")
                    UserDefaults.standard.synchronize()
                    print(postalCode)
                    self.homeUpdate()
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    self.countryShortName = countryShortName
                }
                self.address = self.getAddressString(placemark: placemark)
            }
        } else {
            // add some more check's if for some reason location manager is nil
        }
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func getAddressString(placemark: CLPlacemark) -> String? {
        var originAddress : String?
        if let addrList = placemark.addressDictionary?[FormattedAddressLines] as? [String]
        {
            originAddress =  addrList.joined(separator: ", ")
        }
        return originAddress
    }
    
    
}
extension UIFont {
    class func mediumSystemFont(ofSize pointSize:CGFloat) -> UIFont {
        return self.systemFont(ofSize: pointSize, weight: .medium)
    }
}

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}



extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}
