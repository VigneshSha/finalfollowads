//
//  NearbyOffersTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 16/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

class NearbyOffersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nearbyOffersBtn: UIButton!
    @IBOutlet weak var locationImageVw: UIImageView!
    
    @IBOutlet weak var nearByOffersLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nearbyOffersBtn.layer.borderWidth = 1.5
        nearbyOffersBtn.layer.borderColor = appThemeColor.cgColor
    //    nearbyOffersBtn.setTitle("Explore Nearby Offers in Map".localized(using: buttonTitles), for: .normal)
        nearByOffersLbl.text = "Explore Nearby Offers in Map".localized()
        nearbyOffersBtn.layer.cornerRadius = 6.0
        nearbyOffersBtn.titleLabel?.textAlignment = NSTextAlignment.center

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
