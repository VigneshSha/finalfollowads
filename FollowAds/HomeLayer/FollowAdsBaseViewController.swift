//
//  FollowAdsBaseViewController.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 01/03/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class FollowAdsBaseViewController: UIViewController {

    @IBOutlet weak var noDataLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    //    noDataLabel.text = "No Data Available".localized()
    }
    
    func trackViewcontroller(name: String, screenClass: String) {
        Analytics.setScreenName(name, screenClass: screenClass)
    }

}
