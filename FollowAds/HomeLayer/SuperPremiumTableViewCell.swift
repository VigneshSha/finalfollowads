//
//  SuperPremiumTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

protocol PremiumDelegate {
    func navigateToSuperPremiumView(indexPath: IndexPath)
}

class SuperPremiumTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionVw: UICollectionView!
    var colVwCell: UICollectionViewCell?
    var delegate: PremiumDelegate?
    var homeDataModel: FollowAdsHomeDataModel?
    var selectedIndexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionVw.dataSource = self
        collectionVw.delegate = self
        collectionVw.register(UINib(nibName: "SuperPremiumCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "superPremiumCellId")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion2.count) != nil {
                if (homeDataModel?.promotions.promotion2.count ?? 0) >= 10 {
                    return 10
                }
                else {
                  
                return (homeDataModel?.promotions.promotion2.count)!
                }
            }
            else {
                return 0
            }
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : SuperPremiumCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "superPremiumCellId", for: indexPath) as! SuperPremiumCollectionViewCell

                cell.offerImageVw.layer.cornerRadius = cell.offerImageVw.frame.size.width / 2
                cell.offerImageVw.clipsToBounds = true
                
                if (homeDataModel?.promotions.promotion2.count) != nil {
                    cell.titleLabel.text = homeDataModel?.promotions.promotion2[indexPath.row].business_caption
                    let urlstring = self.homeDataModel?.promotions.promotion2[indexPath.row].business_image_icon_url
                    let urlstring2 = self.homeDataModel?.promotions.promotion2[indexPath.row].advertisment_img
                    if urlstring != nil {
                        if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                            cell.offerImageVw.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell.offerImageVw.image = UIImage(named: "home_ads_placeholder")
                    }
                    
                    if urlstring2 != nil {
                        if let encodedString  = urlstring2?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                            cell.anotherimage.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell.anotherimage.image = UIImage(named: "home_ads_placeholder")
                    }
                    
                    
                    
                    
                    
                }
                else {
                    cell.titleLabel.text = "Label"
                    cell.offerImageVw.image = UIImage(named: "home_ads_placeholder")
            }
                
                return cell
            }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            return CGSize(width: collectionView.bounds.size.width/3.20, height: collectionView.bounds.size.height)
        }
        else if FollowAdsDisplayType.typeIsLike == DisplayType.iphone7plus {
            return CGSize(width: collectionView.bounds.size.width/4.14, height: collectionView.bounds.size.height)
        }
        else {
        return CGSize(width: collectionView.bounds.size.width/3.75, height: collectionView.bounds.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell : SuperPremiumCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "superPremiumCellId", for: indexPath) as! SuperPremiumCollectionViewCell
//        cell.offerImageVw.layer.borderColor = greyColor.cgColor
        if delegate != nil {
            delegate?.navigateToSuperPremiumView(indexPath: indexPath)
        }
    
    }
    
}
