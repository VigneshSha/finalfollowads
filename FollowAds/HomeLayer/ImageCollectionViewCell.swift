 
 //  ImageCollectionViewCell.swift
 //  InfinitePagingCollectionView
 //
 //  Created by Sheereen Thowlath on 12/10/18.
 //  Copyright © 2018 com.openwave.app. All rights reserved.
 //
 
 import UIKit
 import FLAnimatedImage
 
 final class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: FLAnimatedImageView!
    static let identifier = "ImageCollectionViewCell"
    static let nib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
    
    func configure(indexPath: IndexPath) {
        setNeedsLayout()
        layoutIfNeeded()
    }
 }
