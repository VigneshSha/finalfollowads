//
//  HomeHeaderTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class HomeHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
