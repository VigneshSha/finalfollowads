//
//  SeasonalOffersTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol SixthPromotionDelegate {
    func navigateToSixthPromoView(indexPath: IndexPath)
}

class SeasonalOffersTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageArray = [String] ()
    var colVwCell: UICollectionViewCell?
    var homeDataModel: FollowAdsHomeDataModel?
    var delegate: SixthPromotionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "FifthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "FifthCellId")
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion7.count) != nil {
                if (homeDataModel?.promotions.promotion7.count ?? 0) >= 16 {
                    return 16
                }
                else {
                    return (homeDataModel?.promotions.promotion7.count)!
                }
            }
        }
        else {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : FifthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FifthCellId", for: indexPath) as! FifthTypeCollectionViewCell
            // show upto 16 datas, if the data exceeds 16
//            if (homeDataModel?.promotions.promotion7.count ?? 0) >= 16 {
//            if (homeDataModel?.promotions.promotion7.count) != nil {
//                for i in (0..<16) {
//
//                cell.titleLabel.text = homeDataModel?.promotions.promotion7[i].business_name
//                cell.descriptionLabel.text = homeDataModel?.promotions.promotion7[i].business_area
//                let urlstring = self.homeDataModel?.promotions.promotion7[i].advertisment_img
//
//                if urlstring != nil {
//                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
//                        cell.OfferImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
//                    }
//                }
//                else {
//                    cell.OfferImageView.image = UIImage(named: "home_ads_placeholder")
//                }
//                }
//            }
//            return cell
//            }
//
//                // if the data count is less than 16, show the entire datas.
//            else {
                if (homeDataModel?.promotions.promotion7.count) != nil {
                    cell.titleLabel.text = homeDataModel?.promotions.promotion7[indexPath.row].business_caption
                    cell.descriptionLabel.text = homeDataModel?.promotions.promotion7[indexPath.row].business_area
                    let urlstring = self.homeDataModel?.promotions.promotion7[indexPath.row].advertisment_img
                    
                    if urlstring != nil {
                        if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            cell.OfferImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell.OfferImageView.image = UIImage(named: "home_ads_placeholder")
                    }
                }
                return cell
            }
//        }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            return CGSize(width: collectionView.bounds.size.width/2.7, height: collectionView.bounds.size.height)

        }
        else {
            return CGSize(width: collectionView.bounds.size.width/3.2, height: collectionView.bounds.size.height)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToSixthPromoView(indexPath: indexPath)
        }
    }
    
}



