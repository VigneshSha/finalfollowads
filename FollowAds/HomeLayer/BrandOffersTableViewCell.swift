//
//  BrandOffersTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import FLAnimatedImage

protocol SeventhPromotionDelegate {
    func navigateToSeventhPromoView(indexPath: IndexPath)
}

class BrandOffersTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageArray = [String] ()
    var colVwCell: UICollectionViewCell?
    var homeDataModel: FollowAdsHomeDataModel?
    var delegate: SeventhPromotionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        collectionView.register(UINib(nibName: "SixthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SixthCellId")
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion8.count) != nil {
                if (homeDataModel?.promotions.promotion8.count ?? 0) >= 9 {
                    return 9
                }
                else {
                    return (homeDataModel?.promotions.promotion8.count)!
                }
            }
        }
        else {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : SixthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SixthCellId", for: indexPath) as! SixthTypeCollectionViewCell
            if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
                cell.offerImageView.contentMode = .scaleToFill
            }
            else {
                cell.offerImageView.contentMode = .scaleAspectFill

            }
            if (homeDataModel?.promotions.promotion8.count) != nil {
                cell.titleLabel.text = homeDataModel?.promotions.promotion8[indexPath.row].business_caption
                cell.descriptionLabel.text = homeDataModel?.promotions.promotion8[indexPath.row].business_area
                let urlstring = self.homeDataModel?.promotions.promotion8[indexPath.row].advertisment_img
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        
                        let imageExtensions = ["gif"]
                        let pathExtention = URL(string: encodedString)?.pathExtension

//                        if imageExtensions.contains(pathExtention ?? "") {
//                            cell.offerImageView.image = UIImage.gifImageWithURL(encodedString)
//                        }
//                        else {
//                            cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
//
//                        }
//
                        
                    //    DispatchQueue.global().async { [weak self] in
                        DispatchQueue.main.async {
                            do {
                                if imageExtensions.contains(pathExtention ?? "") {
                                    let gifData = try Data(contentsOf:  URL(string: encodedString)!)
                                     cell.offerImageView.animatedImage = FLAnimatedImage(gifData: gifData)
                                 }
                                else {
                                    cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
                                }
                                
                            } catch {
                                print(error)
                            }
                        }
                        
                    }
                }
                else {
                    cell.offerImageView.image = UIImage(named: "home_brand_placeholder")
                }
            }
            return cell
        }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: self.collectionView.frame.width , height: 165)
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToSeventhPromoView(indexPath: indexPath)
        }
    }
    
}
