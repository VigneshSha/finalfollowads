//
//  TableViewCell.swift
//  CollectionView
//
//  Created by    Ankit on 20/10/17.
//  Copyright © 2017 Ankit. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

protocol NavigationUpdateDelegate {
    func navigateToView(indexPath: IndexPath)
}

class BigBagOfferTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageArray = [String] ()
    var colVwCell: UICollectionViewCell?
    var followAdsRequestManager = FollowAdsRequestManager()
    var homeDataModel: FollowAdsHomeDataModel?
    var navigationDel: NavigationUpdateDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.showAnimatedGradientSkeleton()
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        collectionView.register(UINib(nibName: "FirstTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FirstCellId")
   //     collectionView.register(UINib(nibName: "SuperSqAnimationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SuperSqAnimationCellId")

    }
    
    
    func hideAnimation() {
       collectionView.hideSkeleton()
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion3.count) != nil {
                if (homeDataModel?.promotions.promotion3.count ?? 0) == 0 {
                    return 2
                }
                else if (homeDataModel?.promotions.promotion3.count ?? 0) > 8 {
                    return 8
                }
                else {
                    return (homeDataModel?.promotions.promotion3.count)!
                }
            }
        }
        else {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if (homeDataModel?.promotions.promotion3.count) != nil {
                if (homeDataModel?.promotions.promotion3.count ?? 0) == 0 {
                    let cell : FirstTypeCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstCellId", for: indexPath) as? FirstTypeCollectionViewCell
                    cell?.titleLabel.text = "Label"
                    cell?.descriptionLabel.text = "Label"
                    cell?.offerImageView.image = UIImage(named: "home_ads_placeholder")
                    return cell!

                }
                else if (homeDataModel?.promotions.promotion3.count ?? 0) > 8 {
                    let cell : FirstTypeCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstCellId", for: indexPath) as? FirstTypeCollectionViewCell
                    
                    cell?.hideGradientAnimation()
                    cell?.titleLabel.text = homeDataModel?.promotions.promotion3[indexPath.row].business_caption
                    cell?.descriptionLabel.text = homeDataModel?.promotions.promotion3[indexPath.row].business_area
                    let urlstring = self.homeDataModel?.promotions.promotion3[indexPath.row].advertisment_img
                    
                    if urlstring != nil {
                        if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                            cell?.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell?.offerImageView.image = UIImage(named: "home_ads_placeholder")
                    }
                    return cell!

                }
                else {
                    let cell : FirstTypeCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FirstCellId", for: indexPath) as? FirstTypeCollectionViewCell

                    cell?.hideGradientAnimation()
                    cell?.titleLabel.text = homeDataModel?.promotions.promotion3[indexPath.row].business_caption
                    cell?.descriptionLabel.text = homeDataModel?.promotions.promotion3[indexPath.row].business_area
                    let urlstring = self.homeDataModel?.promotions.promotion3[indexPath.row].advertisment_img
                    
                    if urlstring != nil {
                        if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                            cell?.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell?.offerImageView.image = UIImage(named: "home_ads_placeholder")
                    }
                    return cell!

                }
            }
            return colVwCell!

            //            else {
            //                cell?.titleLabel.text = "Label"
            //                cell?.descriptionLabel.text = "Label"
            //                cell?.offerImageView.image = UIImage(named: "home_ads_placeholder")
            //
            //            }
        }
        else {
//            let cell : SuperSqAnimationCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "SuperSqAnimationCellId", for: indexPath) as? SuperSqAnimationCollectionViewCell
//            return cell!

            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            if (homeDataModel?.promotions.promotion3.count) != nil {
                if (homeDataModel?.promotions.promotion3.count ?? 0) == 0 {
                    return CGSize(width: self.collectionView.frame.width/2, height: (self.collectionView.frame.width/2) + 54)
                }
                else {
            return CGSize(width: self.collectionView.frame.width/2, height: (self.collectionView.frame.width/2) + 54)
                }
        }
        else{
            return CGSize(width: 0, height: 0)
        }
        }
        return CGSize(width: 0, height: 0)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (homeDataModel?.promotions.promotion3.count ?? 0) == 0 {

        }else {
        if navigationDel != nil {
            navigationDel?.navigateToView(indexPath: indexPath)
        }
        }
    }
}

