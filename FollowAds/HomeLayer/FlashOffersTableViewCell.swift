//
//  FlashOffersTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol NavigateDetailDelegate {
    func navigateToDetailView(indexPath: IndexPath)
}

class FlashOffersTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var colVwCell: UICollectionViewCell?
    @IBOutlet weak var collectionView: UICollectionView!
    var followAdsRequestManager = FollowAdsRequestManager()
    var homeDataModel: FollowAdsHomeDataModel?
    var delegate: NavigateDetailDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
        collectionView.register(UINib(nibName: "SecondTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SecondCellId")
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion4.count) != nil {
                if (homeDataModel?.promotions.promotion4.count ?? 0) >= 12 {
                    return 12
                }
                else {
                    return (homeDataModel?.promotions.promotion4.count)!
                }
            }
        }
        else {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell : SecondTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondCellId", for: indexPath) as! SecondTypeCollectionViewCell
            if (homeDataModel?.promotions.promotion4.count) != nil {
                cell.titleLabel.text = homeDataModel?.promotions.promotion4[indexPath.row].business_caption
                cell.descriptionLabel.text = homeDataModel?.promotions.promotion4[indexPath.row].business_area
                let urlstring = self.homeDataModel?.promotions.promotion4[indexPath.row].advertisment_img
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        
                        cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                    }
                }
                else {
                    cell.offerImageView.image = UIImage(named: "home_ads_placeholder")
                }
            }
            return cell
        }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
                return CGSize(width: (self.collectionView.frame.width/3), height: 170)

            }
            else {
                return CGSize(width: self.collectionView.frame.width/3, height: 170)

            }
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToDetailView(indexPath: indexPath)
        }
    }
    
}
