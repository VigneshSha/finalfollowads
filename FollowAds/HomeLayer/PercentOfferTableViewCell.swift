//
//  PercentOfferTableViewCell.swift
//  FollowAds
//
//  Created by Sheereen Thowlath on 24/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol FivthPromotionDelegate {
    func navigateToFivthPromoView(indexPath: IndexPath)
}

class PercentOfferTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageArray = [String] ()
    var colVwCell: UICollectionViewCell?
    var homeDataModel: FollowAdsHomeDataModel?
    var delegate: FivthPromotionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "FourthTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "FourthCellId")
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if (homeDataModel?.promotions.promotion6.count) != nil {
                if (homeDataModel?.promotions.promotion6.count ?? 0) >= 10 {
                    return 10
                }
                else {
                    return (homeDataModel?.promotions.promotion6.count)!
                }
                
            }
        }
        else {
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            // show upto 10 datas, if the data exceeds 10
//            if (homeDataModel?.promotions.promotion6.count ?? 0) >= 10 {
//            if (homeDataModel?.promotions.promotion6.count) != nil {
//                for i in (0..<10) {
//                let cell : FourthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FourthCellId", for: indexPath) as! FourthTypeCollectionViewCell
//
//                cell.titleLabel.text = homeDataModel?.promotions.promotion6[i].business_caption
//                cell.descriptionLabel.text = homeDataModel?.promotions.promotion6[i].business_area
//                let urlstring = self.homeDataModel?.promotions.promotion6[i].advertisment_img
//                if urlstring != nil {
//                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
//
//                        cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
//                    }
//                }
//                else {
//                    cell.offerImageView.image = UIImage(named: "home_ads_placeholder")
//                }
//                    return cell
//                 }
//                return colVwCell!
//             }
//            else {
//                return colVwCell!
//
//                }
//            }
//
//                // if the data count is less than 10, show the entire datas.
//            else {
                let cell : FourthTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FourthCellId", for: indexPath) as! FourthTypeCollectionViewCell

                if (homeDataModel?.promotions.promotion6.count) != nil {
                    cell.titleLabel.text = homeDataModel?.promotions.promotion6[indexPath.row].business_caption
                    cell.descriptionLabel.text = homeDataModel?.promotions.promotion6[indexPath.row].business_area
                    let urlstring = self.homeDataModel?.promotions.promotion6[indexPath.row].advertisment_img
                    if urlstring != nil {
                        if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                            
                            cell.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                        }
                    }
                    else {
                        cell.offerImageView.image = UIImage(named: "home_ads_placeholder")
                    }
                }
                return cell
            }
//        }
        else {
            return colVwCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/2.3, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToFivthPromoView(indexPath: indexPath)
        }
    }
    
}


