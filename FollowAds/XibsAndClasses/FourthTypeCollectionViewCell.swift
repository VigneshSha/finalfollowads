//
//  FourthTypeCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 16/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import FLAnimatedImage
class FourthTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var offerImageView: FLAnimatedImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        offerImageView.layer.borderWidth = 1
        offerImageView.layer.cornerRadius = 10.0
//        offerImageView.layer.borderColor = UIColor.lightGray.cgColor
        offerImageView.clipsToBounds = true
        // Initialization code
    }
    
}
