//
//  SecondTypeCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class SecondTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var offerImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        offerImageView.layer.borderWidth = 1
        offerImageView.layer.cornerRadius = 10.0
//        offerImageView.layer.borderColor = UIColor.lightGray.cgColor
        offerImageView.clipsToBounds = true
        self.layer.cornerRadius = 10.0
    }
    
}
