//
//  FirstTypeCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 15/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class FirstTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var offerImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        offerImageView.showAnimatedGradientSkeleton()
        titleLabel.showAnimatedGradientSkeleton()
        descriptionLabel.showAnimatedGradientSkeleton()
//        offerImageView.layer.borderWidth = 1
        offerImageView.layer.cornerRadius = 10.0
//        offerImageView.layer.borderColor = UIColor.lightGray.cgColor
        offerImageView.clipsToBounds = true
        self.layer.cornerRadius = 10.0
        // Initialization code
    }
    
    func hideGradientAnimation() {
        offerImageView.hideSkeleton()
        titleLabel.hideSkeleton()
        descriptionLabel.hideSkeleton()
        offerImageView.layer.cornerRadius = 10.0
        offerImageView.clipsToBounds = true

    }
    
}
