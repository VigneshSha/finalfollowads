//
//  CustomMarkerView.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 20/02/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class CustomMarkerView: UIView {
    
    @IBOutlet weak var adImgVw: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var adCaptionLabel: UILabel!
    @IBOutlet weak var areaNameLabel: UILabel!
    @IBOutlet weak var btnMarkerAction: UIButton!
    @IBOutlet weak var innerVw: UIView!
    @IBOutlet weak var arrowImgVw: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomMarkerView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}
