//
//  SuperSqAnimationCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 03/07/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

class SuperSqAnimationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
