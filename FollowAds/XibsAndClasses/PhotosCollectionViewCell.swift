//
//  PhotosCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 19/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var shopImagesImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    //    shopImagesImgView.layer.borderWidth = 1
        shopImagesImgView.layer.cornerRadius = 10.0
   //     shopImagesImgView.layer.borderColor = UIColor.lightGray.cgColor
        shopImagesImgView.clipsToBounds = true
        // Initialization code
    }
    
}
