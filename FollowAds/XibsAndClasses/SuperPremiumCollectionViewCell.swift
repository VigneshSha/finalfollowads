//
//  SuperPremiumCollectionViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

class SuperPremiumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var offerImageVw: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
     @IBOutlet weak var anotherimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        offerImageVw.layer.borderWidth = 2
        offerImageVw.layer.borderColor = appThemeColor.cgColor
       titleLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
}
