//
//  SearchbarViewController.swift
//  FollowAds
//
//  Created by IOS Developer on 28/01/20.
//  Copyright © 2020 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import CoreLocation
import FirebaseDynamicLinks
import MBProgressHUD
import AppsFlyerLib

class SearchbarViewController: FollowAdsBaseViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate {
  
    var datavar = [""]
 var followAdsRequestManager = FollowAdsRequestManager()
      var locationManager = CLLocationManager()
    @IBOutlet weak var cureenbutton: UIButton!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
     var areaListDataModel = FollowAdsAreaListDataModel()
    var filteredData: [String] = []
    var location: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        let searchbar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        searchbar.placeholder = "Your placeholder"
        let leftNavBarButton = UIBarButtonItem(customView:searchbar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        cureenbutton.backgroundColor = .clear
        cureenbutton.layer.cornerRadius = 10
        cureenbutton.layer.borderWidth = 1
        cureenbutton.layer.borderColor = UIColor.gray.cgColor
        cureenbutton.layer.shadowColor =  UIColor.gray.cgColor
        cureenbutton.layer.shadowOpacity = 1.0
        
        if Reachability.isConnectedToNetwork() == true {
                         self.areaList()
                     }
                     else {
                         MBProgressHUD.hide(for: self.view, animated: true)
                         FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                     }
       
        tableview.dataSource =  self
        tableview.delegate =  self
        searchbar.delegate =  self
        filteredData =  datavar
     print("fged",filteredData)

        // Do any additional setup after loading the view.
    }
    func determineCurrentLocation()
     {
         locationManager = CLLocationManager()
         locationManager.delegate = self
         locationManager.desiredAccuracy = kCLLocationAccuracyBest
         locationManager.requestAlwaysAuthorization()
         if CLLocationManager.locationServicesEnabled() {
             //locationManager.startUpdatingHeading()
             locationManager.startUpdatingLocation()
             
         }
     }
    @IBAction func currentlocation(_ sender: Any) {
        self.location = nil
                   sharedInstance.switchLocEnabled = false
                   self.determineCurrentLocation()
    }
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork() == true {
                   self.areaList()
               }
               else {
                   MBProgressHUD.hide(for: self.view, animated: true)
                   FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
               }
        tableview.dataSource =  self
        tableview.delegate =  self
        searchbar.delegate =  self
        filteredData =  datavar
    }
    func areaList() {
        self.followAdsRequestManager.areaListRequestManager = FollowAdsAreaListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.areaListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.areaListRequestManager?.lang_id = ""
        }
        
        let areaListCompletion: AreaListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                //Navigate to Home View
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                       
                        self.areaListDataModel = response!
                        for data1 in self.areaListDataModel.area{
                            self.datavar.append(data1.area_lang_name!)
                        }
                        print("ddfgf",self.datavar)
                        self.filteredData =  self.datavar
                        print("jobhhioioe",self.filteredData)
                        sharedInstance.areaListDataModel = self.areaListDataModel
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableview.reloadData()
                    })
                }
               
            }
        }
        FollowAdsServiceHandler.callAreaListServiceCall(requestObject: self.followAdsRequestManager, areaListCompletion)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //    print("dgdfd",self.areaListDataModel.area.count)
        print("fdfe",self.datavar.count)
        return self.filteredData.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//        print("fedfsf",self.areaListDataModel.area[indexPath.row].area_lang_name)
                print("fdflhnine",self.filteredData[indexPath.row])
        cell.textLabel?.text = self.filteredData[indexPath.row]
        return cell
        
      }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? datavar : datavar.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })

        tableview.reloadData()
    }
    /*
     @IBAction func frwrw(_ sender: Any) {
     }
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
