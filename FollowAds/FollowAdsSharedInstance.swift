//
//  FollowAdsSharedInstance.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/11/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation

class FollowAdsSharedInstance {
    
    static let sharedInstance : FollowAdsSharedInstance = {
        let instance = FollowAdsSharedInstance(array: [])
        return instance
    }()
    
    //MARK: Local Variable
    
    var filterDataArrray : [String]
    
    //MARK: Init
    
    init( array : [String]) {
        filterDataArrray = array
    }
    
}
