//
//  OfferDetailShopRatingTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferDetailShopRatingTableViewCell: UITableViewCell {
    
    @IBOutlet var ratingValueLabel: UILabel!
    @IBOutlet var instructionLabel: UILabel!
    @IBOutlet var floatRatingView: FloatRatingView!
    var valueStr: NSAttributedString?
    var textStr: NSAttributedString?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
