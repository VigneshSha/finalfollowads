//
//  OfferDetailTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import BRYXBanner
import FirebaseAnalytics
import AppsFlyerLib
//import ExpandableLabel

protocol NavigationDelegate {
    func navigateToView(indexPath: IndexPath)
    func ReviewBtnAction()
    func ShareBtnAction()
    func MessageBtnAction()
    func CallBtnAction()
    func WhatsAppBtnAction()
    func LikeBtnAction()
    func LocationBtnAction()
    func SeeAllBtnPressed()
    func likeBtnNavigation()
    func likeBtnServiceCall()
    func offerDetailService()
}

let LikedImage = UIImage(named: "like_filled")! as UIImage
let UnLikedImage = UIImage(named: "like_unfilled")! as UIImage

class OfferDetailTableDatasource: NSObject, UITableViewDataSource, UITableViewDelegate,FloatRatingViewDelegate, ServiceCallDelegate, UIScrollViewDelegate,ExpandableLabelDelegate  {
    
    
    var offerCodeBttn : UIButton?
    var tableView: UITableView!
    var offerImage: String?
    var cell: UITableViewCell?
    var header: UITableViewHeaderFooterView? = nil
    var ratingsReviewsLabel: UILabel!
    var seeAllBtn: UIButton!
    var delegate: NavigationDelegate?
    var isAlreadySelected : Bool = false
    var appartmentString : String  = ""
    var offerDetailsDataModel: FollowAdsOfferDetailDataModel?
    var likeAdvertisementDataModel: FollowAdsLikeAdvertisementDataModel?
    var userRatingValue : Double?
    var offSet: CGFloat = 0
    var pageControl : UIPageControl?
    var imageUrl: URL?
    var offerDetailVc: OfferDetailViewController?
    var offerImgCell: OfferDetailImageTableViewCell?
    var autoScrollTimer = Timer()
    var selectedIndexPath: IndexPath? = nil
    var landLineNum: String?
    var mobNum: String?
    var states : Bool = true
    var banner = Banner()

    
    init(offerImageModel: String?, detailModel: FollowAdsOfferDetailDataModel?){
        self.offerImage = offerImageModel
        self.offerDetailsDataModel = detailModel
        super.init()
    }
    
    func doServiceCall() {
        if delegate != nil {
            delegate?.offerDetailService()
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews.isEmpty == false {
            return 8
        }
        else {
            return 7
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func willExpandLabel(_ label: ExpandableLabel) {
 
        tableView.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            states = false
            DispatchQueue.main.async { [weak self] in
//                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                self?.tableView.scrollToRow(at: indexPath, at: .none, animated: false)
            }
        }
        tableView.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableView.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            states = true
            DispatchQueue.main.async { [weak self] in
//                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableView.endUpdates()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews.isEmpty == false {
            if indexPath.section == 0 {
                let offerImageCell: OfferDetailImageTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferImgCellId") as?  OfferDetailImageTableViewCell
                offerImageCell.tableViewObj = self.tableView
                offerImageCell.offerDetailDataModel = self.offerDetailsDataModel
                offerImageCell.itemsCount = self.offerDetailsDataModel?.advertisement_detail.advertisement_img.count
                offerImageCell.pageContrl.numberOfPages = (self.offerDetailsDataModel?.advertisement_detail.advertisement_img.count)!
                offerImageCell.collectionView.reloadData()
                offerImageCell.delegate = self
                return offerImageCell
            }
            else if indexPath.section == 1 {
                let offerDescriptionCell: OfferDetailDescriptionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferDescriptionCellId") as? OfferDetailDescriptionTableViewCell
                

//                offerDescriptionCell.offerDescriptionLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_desc
                offerDescriptionCell.offerDescriptionLabel.delegate = self

                let currentSource = offerDetailsDataModel?.advertisement_detail.advertisement_desc

                
//                offerDescriptionCell.offerDescriptionLabel.setLessLinkWith(lessLink: "Close", attributes: [.foregroundColor:UIColor.red], position: .left)
                
                offerDescriptionCell.layoutIfNeeded()
                
                offerDescriptionCell.offerDescriptionLabel.shouldCollapse = true
                offerDescriptionCell.offerDescriptionLabel.textReplacementType = .word
                offerDescriptionCell.offerDescriptionLabel.numberOfLines = 3
                offerDescriptionCell.offerDescriptionLabel.collapsed = states
                offerDescriptionCell.offerDescriptionLabel.text = currentSource

//
//                let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11.0)
//                let readmoreFontColor = UIColor.blue
//                DispatchQueue.main.async {
//                    offerDescriptionCell.offerDescriptionLabel.addTrailing(with: "...", moreText: "Readmore", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
//                }
//
//                let ip = indexPath
//                if selectedIndexPath != nil {
//                    if ip == selectedIndexPath! {
//                        DispatchQueue.main.async {
//                            offerDescriptionCell.offerDescriptionLabel.addTrailing(with: "", moreText: "", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
//                        }
//                    }
//                    else {
//
//                    }
//
//                }
               /*     let ip = indexPath
                    if selectedIndexPath != nil {
                        if ip == selectedIndexPath! {kl
                            offerDescriptionCell.moreBtn.setTitle("Less".localized(using: buttonTitles), for: .normal)
                            
                        } else {
                            if offerDescriptionCell.offerDescriptionLabel.numberOfLines > 3 {
                                offerDescriptionCell.moreBtn.isHidden = true
                            }
                            else {
                                offerDescriptionCell.moreBtn.isHidden = false

                            }
                            offerDescriptionCell.moreBtn.setTitle("More".localized(using: buttonTitles), for: .normal)
                        }
                    } else {
                        if offerDescriptionCell.offerDescriptionLabel.numberOfLines > 3 {
                            offerDescriptionCell.moreBtn.isHidden = true
                        }
                        else {
                            offerDescriptionCell.moreBtn.isHidden = false
                            
                        }
                        offerDescriptionCell.moreBtn.setTitle("More".localized(using: buttonTitles), for: .normal)
                    } */
                
                return offerDescriptionCell
            }
            else if indexPath.section == 2 {
                let offerTitleCell: OfferDetailOfrTitleTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferTitleCellId") as? OfferDetailOfrTitleTableViewCell
                offerTitleCell.offerTitleLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_caption
                return offerTitleCell
            }
            else if indexPath.section == 3 {
                let offerReviewCell: OfferDetailReviewTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferReviewCellId") as? OfferDetailReviewTableViewCell
                offerReviewCell.reviewBtn.addTarget(self, action: #selector(reviewBtnAction), for: UIControlEvents.touchUpInside)
                
                if offerDetailsDataModel?.advertisement_detail.is_liked == "1" {
                    if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                        let statusText = String()
                        let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                        if status == "1" {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                        }
                        else {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                        }
                    }
                    else {
                        offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                    }
                }
                else  {
                    if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                        let statusText = String()
                        let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                        if status == "1" {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                        }
                        else {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                        }
                    }
                    else {
                        offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                    }
                }
                offerReviewCell.likeBtn.addTarget(self, action: #selector(likeBtnAction), for: UIControlEvents.touchUpInside)
                offerReviewCell.shareBtn.addTarget(self, action: #selector(shareBtnAction), for: UIControlEvents.touchUpInside)
                return offerReviewCell
            }
            else if indexPath.section == 4 {
                let ShopDetailCell: OfferDetailShopDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopDetailCellId") as? OfferDetailShopDetailTableViewCell
                ShopDetailCell.shopImageView.layer.cornerRadius = ShopDetailCell.shopImageView.frame.size.width / 2
                ShopDetailCell.shopImageView.clipsToBounds = true
                ShopDetailCell.shopNameLabel.text = offerDetailsDataModel?.advertisement_detail.business_name
                ShopDetailCell.shopAddrLabel.text = offerDetailsDataModel?.advertisement_detail.business_addr
                if Int(reviewsCount) ?? 0 > 1 && Int(interestCount) ?? 0 > 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Reviews | ".localized() + interestCount + " Interested".localized()
                }
                else if Int(reviewsCount) ?? 0 > 1 && Int(interestCount) ?? 0 <= 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Reviews | ".localized() + interestCount + " " + "Interested".localized()
                }
                else if Int(reviewsCount) ?? 0 <= 1 && Int(interestCount) ?? 0 > 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Review | ".localized() + interestCount + " Interested".localized()
                }
                else {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Review | ".localized() + interestCount + " " + "Interested".localized()
                }
                ShopDetailCell.shopDistanceLabel.text = distanceText + " away".localized()
                
                let urlstring = self.offerDetailsDataModel?.advertisement_detail.business_img
                
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        ShopDetailCell?.shopImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "business_Placeholder"))
                    }
                }
                else {
                    ShopDetailCell?.shopImageView.image = UIImage(named: "business_Placeholder")
                }
                return ShopDetailCell
            }
            else if indexPath.section == 5 {
                let shopContactCell : OfferDetailShopContactTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopContactCellId") as? OfferDetailShopContactTableViewCell
                if landLineNum == "" {
                    shopContactCell.callBtn.alpha = 0.5
                }
                else {
                    shopContactCell.callBtn.alpha = 1
                }
                if mobNum == "" {
                    shopContactCell.whatsappBtn.alpha = 0.5
                    shopContactCell.messageBtn.alpha = 0.5

                }
                else {
                    shopContactCell.whatsappBtn.alpha = 1
                    shopContactCell.messageBtn.alpha = 1
                }
                shopContactCell.locationBtn.addTarget(self, action: #selector(locationBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.whatsappBtn.addTarget(self, action: #selector(whatsAppBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.messageBtn.addTarget(self, action: #selector(messageBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.callBtn.addTarget(self, action: #selector(callBtnAction), for: UIControlEvents.touchUpInside)
                return shopContactCell
            }
            else if indexPath.section == 6 {
                let shopRatingCell: OfferDetailShopRatingTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopRatingCellId") as? OfferDetailShopRatingTableViewCell
                let valueAttribute = [ NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 24)) ]
                let rangeAttribute = [ NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17)) ]
                shopRatingCell.valueStr = NSAttributedString(string: rating, attributes: valueAttribute)
                shopRatingCell.textStr = NSAttributedString(string: " out of 5".localized(), attributes: rangeAttribute)
//                if UserDefaults.standard.value(forKey: "Language_Id") != nil {
//                let langidText = String()
//                let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
//                    if langId == "1" {
//                        let combination = NSMutableAttributedString()
//                        combination.append(shopRatingCell.valueStr!)
//                        combination.append(shopRatingCell.textStr!)
//                        shopRatingCell.ratingValueLabel.attributedText = combination
//                    }
//                    else {
//                        let combination = NSMutableAttributedString()
//                        combination.append(shopRatingCell.textStr!)
//                        combination.append(shopRatingCell.valueStr!)
//                        shopRatingCell.ratingValueLabel.attributedText = combination
//                    }
//                }
//                else {
                    let combination = NSMutableAttributedString()
                    combination.append(shopRatingCell.valueStr!)
                    combination.append(shopRatingCell.textStr!)
                    shopRatingCell.ratingValueLabel.attributedText = combination
//                }
               
                shopRatingCell.instructionLabel.text = "Tap to Rate".localized()
                shopRatingCell.floatRatingView.isUserInteractionEnabled = false
                var ratingStr = String()
                ratingStr = (offerDetailsDataModel?.advertisement_detail.rating_count)!
                var ratingValue = Double()
                ratingValue = Double(ratingStr)!
                shopRatingCell.floatRatingView.rating = ratingValue
                shopRatingCell.floatRatingView.delegate = self
                return shopRatingCell
            }
            else if indexPath.section == 7 {
                let shopReviewCell: OfferDetailShopReviewTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopReviewCellId") as? OfferDetailShopReviewTableViewCell
                var ratingStr = String()
                ratingStr = (offerDetailsDataModel?.advertisement_detail.advertisement_reviews[0].rating)!
                var ratingValue = Double()
                ratingValue = Double(ratingStr)!
                shopReviewCell.floatRatingView.rating = ratingValue
                shopReviewCell.commentTitleLabel.text = titleText
                shopReviewCell.commentsLabel.text = feedbackText
                shopReviewCell.userNameLabel.text = usernameText
                let dateStr = postedTime
                let dateString = currentTime
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let serverDate = dateFormatter.date(from: dateString)!
                
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let serverSeperatedDateString = dateFormatter.string(from: serverDate)
                
                dateFormatter.dateFormat = "HH:mm"
                let serverSeperatedTimeString = dateFormatter.string(from: serverDate)
                
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let postedDate = dateFormatter.date(from: dateStr)!
                
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let postedSeperatedDateString = dateFormatter.string(from: postedDate)
                
                dateFormatter.dateFormat = "HH:mm"
                let postedSeperatedTimeString = dateFormatter.string(from: postedDate)
                
                print("********serverSeperatedDateString****\n\(serverSeperatedDateString)")
                print("********serverSeperatedTimeString****\n\(serverSeperatedTimeString)")
                print("********postedSeperatedDateString****\n\(postedSeperatedDateString)")
                print("********postedSeperatedTimeString****\n\(postedSeperatedTimeString)")
                
                let timeAgo: String = ReviewsTimeAgo.timeAgoSinceDate(postedDate, currentDate: serverDate, numericDates: true)
                shopReviewCell.timeLabel.text = timeAgo
                return shopReviewCell
            }
            else {
                return cell!
            }
        }
        else {
            if indexPath.section == 0 {
                let offerImageCell: OfferDetailImageTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferImgCellId") as?  OfferDetailImageTableViewCell
                offerImageCell.offerDetailDataModel = self.offerDetailsDataModel
                offerImageCell.itemsCount = self.offerDetailsDataModel?.advertisement_detail.advertisement_img.count
                offerImageCell.pageContrl.numberOfPages = (self.offerDetailsDataModel?.advertisement_detail.advertisement_img.count)!
                offerImageCell.collectionView.reloadData()
                offerImageCell.delegate = self
                return offerImageCell
            }
            else if indexPath.section == 1 {
                let offerDescriptionCell: OfferDetailDescriptionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferDescriptionCellId") as? OfferDetailDescriptionTableViewCell


                let currentSource = offerDetailsDataModel?.advertisement_detail.advertisement_desc
                offerDescriptionCell.offerDescriptionLabel.delegate = self

                
//                offerDescriptionCell.offerDescriptionLabel.setLessLinkWith(lessLink: "Close", attributes: [.foregroundColor:UIColor.red], position: .left)
                
                
                offerDescriptionCell.offerDescriptionLabel.shouldCollapse = true
                offerDescriptionCell.offerDescriptionLabel.textReplacementType = .word
                offerDescriptionCell.offerDescriptionLabel.numberOfLines = 3
                offerDescriptionCell.offerDescriptionLabel.collapsed = states
                offerDescriptionCell.offerDescriptionLabel.text = currentSource
                offerDescriptionCell.layoutIfNeeded()

//                offerDescriptionCell.offerDescriptionLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_desc
//
//                let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11.0)
//                let readmoreFontColor = UIColor.blue
//                DispatchQueue.main.async {
//                    offerDescriptionCell.offerDescriptionLabel.addTrailing(with: "...", moreText: "Readmore", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
//                }
//
//                let ip = indexPath
//                if selectedIndexPath != nil {
//                    if ip == selectedIndexPath! {
//                        DispatchQueue.main.async {
//                            offerDescriptionCell.offerDescriptionLabel.addTrailing(with: "", moreText: "", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
//                        }
//                    }
//                    else {
//
//                    }
//                }
                
              /*  let ip = indexPath
                if selectedIndexPath != nil {
                    if ip == selectedIndexPath! {
                        offerDescriptionCell.moreBtn.setTitle("Less".localized(using: buttonTitles), for: .normal)
                        
                    } else {
                        if offerDescriptionCell.offerDescriptionLabel.numberOfLines > 3 {
                            offerDescriptionCell.moreBtn.isHidden = false
                        }
                        else {
                            offerDescriptionCell.moreBtn.isHidden = true
                            
                        }
                        offerDescriptionCell.moreBtn.setTitle("More".localized(using: buttonTitles), for: .normal)
                    }
                } else {
                    if offerDescriptionCell.offerDescriptionLabel.numberOfLines > 3 {
                        offerDescriptionCell.moreBtn.isHidden = true
                    }
                    else {
                        offerDescriptionCell.moreBtn.isHidden = false
                        
                    }
                    offerDescriptionCell.moreBtn.setTitle("More".localized(using: buttonTitles), for: .normal)
                } */
                
                return offerDescriptionCell
            }
            else if indexPath.section == 2 {
                let offerTitleCell: OfferDetailOfrTitleTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferTitleCellId") as? OfferDetailOfrTitleTableViewCell
                offerTitleCell.offerTitleLabel.text = offerDetailsDataModel?.advertisement_detail.advertisement_caption
                return offerTitleCell
            }
            else if indexPath.section == 3 {
                let offerReviewCell: OfferDetailReviewTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "OfferReviewCellId") as? OfferDetailReviewTableViewCell
                offerReviewCell.reviewBtn.addTarget(self, action: #selector(reviewBtnAction), for: UIControlEvents.touchUpInside)
                if offerDetailsDataModel?.advertisement_detail.is_liked == "1" {
                    if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                        let statusText = String()
                        let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                        if status == "1" {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                        }
                        else {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                        }
                    }
                    else {
                        offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                    }
                }
                else  {
                    if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                        let statusText = String()
                        let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                        if status == "1" {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_filled"), for: .normal)
                        }
                        else {
                            offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                            
                        }
                    }
                    else {
                        offerReviewCell.likeBtn.setImage(UIImage(named: "like_unfilled"), for: .normal)
                    }
                }
                offerReviewCell.likeBtn.addTarget(self, action: #selector(likeBtnAction), for: UIControlEvents.touchUpInside)
                offerReviewCell.shareBtn.addTarget(self, action: #selector(shareBtnAction), for: UIControlEvents.touchUpInside)
                return offerReviewCell
            }
            else if indexPath.section == 4 {
                let ShopDetailCell: OfferDetailShopDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopDetailCellId") as? OfferDetailShopDetailTableViewCell
                ShopDetailCell.shopImageView.layer.cornerRadius = ShopDetailCell.shopImageView.frame.size.width / 2
                ShopDetailCell.shopImageView.clipsToBounds = true
                ShopDetailCell.shopNameLabel.text = offerDetailsDataModel?.advertisement_detail.business_name
                ShopDetailCell.shopAddrLabel.text = offerDetailsDataModel?.advertisement_detail.business_addr
                
                if Int(reviewsCount) ?? 0 > 1 && Int(interestCount) ?? 0 > 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Reviews | ".localized() + interestCount + " Interested".localized()
                }
                else if Int(reviewsCount) ?? 0 > 1 && Int(interestCount) ?? 0 <= 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Reviews | ".localized() + interestCount + " " + "Interested".localized()
                }
                else if Int(reviewsCount) ?? 0 <= 1 && Int(interestCount) ?? 0 > 1 {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Review | ".localized() + interestCount + " Interested".localized()
                }
                else {
                    ShopDetailCell.shopReviewsLabel.text = reviewsCount + " Review | ".localized() + interestCount + " " + "Interested".localized()
                }
                
                ShopDetailCell.shopDistanceLabel.text = distanceText + " away".localized()
                
                let urlstring = self.offerDetailsDataModel?.advertisement_detail.business_img
                
                if urlstring != nil {
                    if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                        ShopDetailCell?.shopImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "Store_Placeholder"))
                    }
                }
                else {
                    ShopDetailCell?.shopImageView.image = UIImage(named: "Store_Placeholder")
                }
                return ShopDetailCell
            }
            else if indexPath.section == 5 {
                let shopContactCell : OfferDetailShopContactTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopContactCellId") as? OfferDetailShopContactTableViewCell
                if landLineNum == "" {
                    shopContactCell.callBtn.alpha = 0.5
                }
                else {
                    shopContactCell.callBtn.alpha = 1
                }
                if mobNum == "" {
                    shopContactCell.whatsappBtn.alpha = 0.5
                    shopContactCell.messageBtn.alpha = 0.5
                    
                }
                else {
                    shopContactCell.whatsappBtn.alpha = 1
                    shopContactCell.messageBtn.alpha = 1
                }
                shopContactCell.locationBtn.addTarget(self, action: #selector(locationBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.whatsappBtn.addTarget(self, action: #selector(whatsAppBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.messageBtn.addTarget(self, action: #selector(messageBtnAction), for: UIControlEvents.touchUpInside)
                shopContactCell.callBtn.addTarget(self, action: #selector(callBtnAction), for: UIControlEvents.touchUpInside)
                return shopContactCell
            }
            else if indexPath.section == 6 {
                let shopRatingCell: OfferDetailShopRatingTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShopRatingCellId") as? OfferDetailShopRatingTableViewCell
                let valueAttribute = [ NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 24)) ]
                let rangeAttribute = [ NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17)) ]
                shopRatingCell.valueStr = NSAttributedString(string: rating, attributes: valueAttribute)
                shopRatingCell.textStr = NSAttributedString(string: " out of 5".localized(), attributes: rangeAttribute)
                let combination = NSMutableAttributedString()
                combination.append(shopRatingCell.valueStr!)
                combination.append(shopRatingCell.textStr!)
                
                shopRatingCell.ratingValueLabel.attributedText = combination
                shopRatingCell.instructionLabel.text = "Tap to Rate".localized()
                shopRatingCell.floatRatingView.delegate = self
                return shopRatingCell
            }
            else {
                return cell!
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if offerDetailsDataModel?.advertisement_detail.advertisement_reviews.isEmpty == false {
            if indexPath.section == 0 {
                if FollowAdsDisplayType.typeIsLike == DisplayType.iphoneX || FollowAdsDisplayType.typeIsLike == DisplayType.unknown {
                    if self.offerCodeBttn?.isHidden == true {
                        return tableView.bounds.size.height / 2.6
                    }
                    else {
                        return tableView.bounds.size.height / 2.4
                    }
                }
                else {
                    if self.offerCodeBttn?.isHidden == true {
                        return tableView.bounds.size.height / 2.3
                    }
                    else {
                        return tableView.bounds.size.height / 2.3
                        
                    }
                }
            }
            else if indexPath.section == 1 {
                return UITableViewAutomaticDimension
                
//                let smallHeight: CGFloat = 100.0
//                let expandedHeight: CGFloat = UITableViewAutomaticDimension
//                let ip = indexPath
//                if selectedIndexPath != nil {
//                    if ip == selectedIndexPath! {
//                        return expandedHeight
//                    } else {
//                        return smallHeight
//                    }
//                } else {
//                    return smallHeight
//                }
            }
            else if indexPath.section == 2 {
                return UITableViewAutomaticDimension
            }
            else if indexPath.section == 3 {
                return 80
            }
            else if indexPath.section == 4 {
              //  return 150
                return UITableViewAutomaticDimension

            }
            else if indexPath.section == 5 {
                return 90
            }
            else if indexPath.section == 6 {
                return 85
            }
            else if indexPath.section == 7 {
                return UITableViewAutomaticDimension
            }
            else {
                return 0
            }
        }
        else {
            if indexPath.section == 0 {
                if FollowAdsDisplayType.typeIsLike == DisplayType.iphoneX || FollowAdsDisplayType.typeIsLike == DisplayType.unknown {
                    if self.offerCodeBttn?.isHidden == true {
                        return tableView.bounds.size.height / 2.6
                    }
                    else {
                        return tableView.bounds.size.height / 2.4
                    }
                }
                else {
                    if self.offerCodeBttn?.isHidden == true {
                        return tableView.bounds.size.height / 2.3
                    }
                    else {
                        return tableView.bounds.size.height / 2.3
                        
                    }
                }
            }
            else if indexPath.section == 1 {
                Analytics.setScreenName("View_more_storeoffers", screenClass: "OfferDetailTableDatasource")
                AppsFlyerTracker.shared().trackEvent("View_more_storeoffers",
                                                     withValues: [
                                                        AFEventAdView: "View_more_storeoffers",
                                                        ]);
                return UITableViewAutomaticDimension
//                let smallHeight: CGFloat = 100.0
//                let expandedHeight: CGFloat = UITableViewAutomaticDimension
//                let ip = indexPath
//                if selectedIndexPath != nil {
//                    if ip == selectedIndexPath! {
//                        return expandedHeight
//                    } else {
//                        return smallHeight
//                    }
//                } else {
//                    return smallHeight
//                }
            }
            else if indexPath.section == 2 {
                return UITableViewAutomaticDimension
            }
            else if indexPath.section == 3 {
                return 80
            }
            else if indexPath.section == 4 {
             //   return 150
                return UITableViewAutomaticDimension
            }
            else if indexPath.section == 5 {
                return 70
            }
            else if indexPath.section == 6 {
                return 85
            }
            else {
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 6 {
            return 50
        }
        else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 6 {
            header = view as? UITableViewHeaderFooterView
            // create a label for section header.
            ratingsReviewsLabel = UILabel(frame: CGRect(x: 15, y: 15, width: 180, height: 20))
            header?.addSubview(ratingsReviewsLabel!)
            if self.offerDetailsDataModel?.advertisement_detail.advertisement_reviews.count ?? 0 > 0 {
                let locStr = "See All".localized(using: buttonTitles)
                let stringsize = locStr.SizeOf(UIFont.mediumSystemFont(ofSize: 15))
                seeAllBtn  = UIButton(frame: CGRect(x: (self.header?.frame.size.width)! - stringsize.width - 15, y: 7, width: stringsize.width, height: 30))
                seeAllBtn.setTitle("See All".localized(using: buttonTitles), for: UIControlState.normal)
                print("******witdsads\(stringsize.width)")
                seeAllBtn.sizeToFit()
                seeAllBtn.setTitleColor(UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0), for: UIControlState.normal)
                seeAllBtn.addTarget(self, action: #selector(seeAllBtnPressed), for: UIControlEvents.touchUpInside)
                seeAllBtn?.titleLabel?.textColor = appThemeColor
                header?.addSubview(seeAllBtn)
            }
            
            else {
                
            }
            
            ratingsReviewsLabel?.text = "Ratings & Reviews".localized()
            ratingsReviewsLabel?.textColor = UIColor.darkGray
            ratingsReviewsLabel?.font = (UIFont.mediumSystemFont(ofSize: 17))
            
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: tableView.sectionHeaderHeight))
        view.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view.contentView.backgroundColor = UIColor.white
        return view
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
    //        let cell = tableView.cellForRow(at: indexPath) as! OfferDetailDescriptionTableViewCell

//            cell.offerDescriptionLabel.numberOfLines = 0
//            cell.offerDescriptionLabel.lineBreakMode = .byWordWrapping
//            DispatchQueue.main.async {
//
//                switch self.selectedIndexPath {
//            case nil:
//                self.selectedIndexPath = indexPath
//
//            default:
//                if self.selectedIndexPath! == indexPath {
//                    self.selectedIndexPath = nil
//
//                } else {
//                    self.selectedIndexPath = indexPath
//                }
//            }
//                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
//            }
            
        }
        else if indexPath.section == 6 {
            if delegate != nil {
                delegate?.navigateToView(indexPath: indexPath)
            }
        }
        else if indexPath.section == 4 {
            if delegate != nil {
                delegate?.navigateToView(indexPath: indexPath)
            }
        }
    }
    
    @objc func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double)
    {
        self.userRatingValue = rating
    }
    
    @objc func reviewBtnAction() {
        delegate?.ReviewBtnAction()
    }
    
    @objc func likeBtnAction(sender: UIButton!) {
        if UserDefaults.standard.value(forKey: "User_Id") != nil {
            if offerDetailsDataModel?.advertisement_detail.is_liked == "1" {
                if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                    if status == "1" {
                        likeStatus = "0"
                    }
                    else {
                        likeStatus = "1"
                    }
                }
                else {
                    likeStatus = "0"
                }
            }
            else  {
                if (UserDefaults.standard.value(forKey: "Liked_Status_Value") != nil) {
                    let statusText = String()
                    let status: String = statusText.passedString((UserDefaults.standard.object(forKey: "Liked_Status_Value") as? String))
                    if status == "1" {
                        likeStatus = "0"
                    }
                    else {
                        likeStatus = "1"
                    }
                }else {
                    likeStatus = "1"
                }
            }
            // COMMENTED FOR TEMPORARY
            if delegate != nil {
                delegate?.likeBtnServiceCall()
            }
//            self.banner = Banner(title: "Ads not mapped".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//            self.banner.dismissesOnTap = true
//            self.banner.show(duration: 3.0)
        }
        else {
            // COMMENTED FOR TEMPORARY
            if delegate != nil {
                delegate?.likeBtnNavigation()
            }
//            self.banner = Banner(title: "Please Login to Like this Advertisement".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//            self.banner.dismissesOnTap = true
//            self.banner.show(duration: 3.0)
            
        }
    }
    
    @objc func shareBtnAction() {
        delegate?.ShareBtnAction()
    }
    
    @objc func seeAllBtnPressed() {
        delegate?.SeeAllBtnPressed()
    }
    
    @objc func locationBtnAction() {
        delegate?.LocationBtnAction()
    }
    
    @objc func whatsAppBtnAction() {
        delegate?.WhatsAppBtnAction()
    }
    
    @objc func messageBtnAction() {
        delegate?.MessageBtnAction()
    }
    
    @objc func callBtnAction() {
        delegate?.CallBtnAction()
    }
    
}

extension UILabel{
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        
        let readMoreText: String = trailingText + moreText
        
        if self.visibleTextLength == 0 { return }
        
        let lengthForVisibleString: Int = self.visibleTextLength
        
        if let myText = self.text {
            
            let mutableString: String = myText
            
            let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: myText.count - lengthForVisibleString), with: "")
            
            let readMoreLength: Int = (readMoreText.count)
            
            guard let safeTrimmedString = trimmedString else { return }
            
            if safeTrimmedString.count <= readMoreLength { return }
            
            print("this number \(safeTrimmedString.count) should never be less\n")
            print("then this number \(readMoreLength)")
            
            // "safeTrimmedString.count - readMoreLength" should never be less then the readMoreLength because it'll be a negative value and will crash
            let trimmedForReadMore: String = (safeTrimmedString as NSString).replacingCharacters(in: NSRange(location: safeTrimmedString.count - readMoreLength, length: readMoreLength), with: "") + trailingText
            
            let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
            let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
            answerAttributed.append(readMoreAttributed)
            self.attributedText = answerAttributed
        }
    }
    
    var visibleTextLength: Int {
        
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        if let myText = self.text {
            
            let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
            let attributedText = NSAttributedString(string: myText, attributes: attributes as? [NSAttributedString.Key : Any])
            let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
            
            if boundingRect.size.height > labelHeight {
                var index: Int = 0
                var prev: Int = 0
                let characterSet = CharacterSet.whitespacesAndNewlines
                repeat {
                    prev = index
                    if mode == NSLineBreakMode.byCharWrapping {
                        index += 1
                    } else {
                        index = (myText as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: myText.count - index - 1)).location
                    }
                } while index != NSNotFound && index < myText.count && (myText as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
                return prev
            }
        }
        
        if self.text == nil {
            return 0
        } else {
            return self.text!.count
        }
    }
}


extension String {
    
    func specialPriceAttributedStringWith(_ color: UIColor) -> NSMutableAttributedString {
        let attributes = [NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue as Int),
                          .foregroundColor: color, .font: fontForPrice()]
        return NSMutableAttributedString(attributedString: NSAttributedString(string: self, attributes: attributes))
    }
    
    func priceAttributedStringWith(_ color: UIColor) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.foregroundColor: color, .font: fontForPrice()]
        
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    func priceAttributedString(_ color: UIColor) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.foregroundColor: color]
        
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    fileprivate func fontForPrice() -> UIFont {
        return UIFont(name: "Helvetica-Neue", size: 13) ?? UIFont()
    }
}
