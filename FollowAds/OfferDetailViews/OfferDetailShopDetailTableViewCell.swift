//
//  OfferDetailShopDetailTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferDetailShopDetailTableViewCell: UITableViewCell {

    @IBOutlet var shopImageView: UIImageView!
    @IBOutlet var shopNameLabel: UILabel!
    @IBOutlet var shopAddrLabel: UILabel!
    @IBOutlet var shopReviewsLabel: UILabel!
    @IBOutlet var shopDistanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
