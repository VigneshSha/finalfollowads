//
//  OfferDetailReviewTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferDetailReviewTableViewCell: UITableViewCell {

    @IBOutlet var reviewBtn: UIButton!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var shareBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

