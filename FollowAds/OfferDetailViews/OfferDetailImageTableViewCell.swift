//
//  OfferDetailImageTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol ServiceCallDelegate {
    func doServiceCall()
}

class OfferDetailImageTableViewCell: UITableViewCell, UIScrollViewDelegate {
    
    var pageControl : UIPageControl?
    var tapGesture = UITapGestureRecognizer()
    var timer: Timer?
    var offSet: CGFloat = 0
    var followAdsRequestManager = FollowAdsRequestManager()
    var urlString: String?
    var tableViewObj : UITableView?
    var delegate: ServiceCallDelegate?
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var pageContrl: UIPageControl! {
        didSet {
            pageContrl.numberOfPages = itemsCount ?? 0
        }
    }
    
    @IBOutlet weak var collectionView: InfiniteCollectionView! {
        didSet {
            collectionView.infiniteDataSource = self
            collectionView.infiniteDelegate = self
            collectionView.register(ImageCollectionViewCell.nib, forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
        }
    }
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout! {
        didSet {
            layout.itemSize = UIScreen.main.bounds.size
        }
    }
    
    var offerDetailDataModel: FollowAdsOfferDetailDataModel?
    var autoScrollTimer : Timer?
    var itemsCount: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if delegate != nil {
            delegate?.doServiceCall()
        }
    }
    
    // Use this method for autoscroll initiation.
    func autoScrollHandler() {
        if autoScrollTimer != nil
        {
            if (autoScrollTimer?.isValid)!{
                autoScrollTimer?.invalidate()
                autoScrollTimer = nil
                self.startTimer()
            }
            else {
                self.startTimer()
            }
        }
        else {
            self.startTimer()
        }
    }
    func startTimer() {
        autoScrollTimer =  Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    // This method is used for collectionview scroll automatically.
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = collectionView  {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if (indexPath?.row) != nil {
                    
                    if ((indexPath?.row)!  <  collectionView.numberOfItems(inSection: 0) - 1){
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                        pageContrl.currentPage = (indexPath1?.row)!
                    }
                    else{
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                        pageContrl.currentPage = (indexPath1?.row)!
                    }
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

// MARK: - InfiniteCollectionViewDataSource, InfiniteCollectionViewDelegate
extension OfferDetailImageTableViewCell: InfiniteCollectionViewDataSource, InfiniteCollectionViewDelegate {
    
    func number(ofItems collectionView: UICollectionView) -> Int {
        return itemsCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, dequeueForItemAt dequeueIndexPath: IndexPath, cellForItemAt usableIndexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: dequeueIndexPath) as! ImageCollectionViewCell
        if self.offerDetailDataModel?.advertisement_detail.advertisement_img.count ?? 0 > 1 {
            self.collectionView.isScrollEnabled = true
            self.autoScrollHandler()
            self.pageContrl.isHidden = false
        }
        else {
            self.collectionView.isScrollEnabled = false
            self.pageContrl.isHidden = true
        }
        
        let urlstring = self.offerDetailDataModel?.advertisement_detail.advertisement_img[usableIndexPath.row].advertisement_img
        
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                
                let imageExtensions = ["gif"]
                let pathExtention = URL(string: encodedString)?.pathExtension
                
                if imageExtensions.contains(pathExtention ?? "") {
                    cell.imageView.image = UIImage.gifImageWithURL(encodedString)
                }
                else {
                    cell.imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))

                }
            }
        }
        else {
            cell.imageView.image = UIImage(named: "home_banner_placeholder")
        }
      //  let image = cell.imageView.image
        return cell
    }
    func infiniteCollectionView(_ collectionView: UICollectionView, didSelectItemAt usableIndexPath: IndexPath) {
        // Here you will get the page control current page, pass this to next page id.
        print("didSelectItemAt: \(usableIndexPath.item)")
        print("current Page:\(pageContrl.currentPage)")
    }
    
    // MARK: - ScrollView Delegate
    func scrollView(_ scrollView: UIScrollView, pageIndex: Int) {
        pageContrl.currentPage = pageIndex // This is for page control update
    }
}
