//
//  OfferDetailShopReviewTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferDetailShopReviewTableViewCell: UITableViewCell {
    
    @IBOutlet var reviewView: UIView!
    @IBOutlet var commentTitleLabel: UILabel!
    @IBOutlet var ratingImgView: UIImageView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var commentsLabel: UILabel!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
