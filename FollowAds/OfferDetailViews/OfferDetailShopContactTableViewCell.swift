//
//  OfferDetailShopContactTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class OfferDetailShopContactTableViewCell: UITableViewCell {

    @IBOutlet var stackView: UIStackView!
    @IBOutlet var locationBtn: UIButton!
    @IBOutlet var whatsappBtn: UIButton!
    @IBOutlet var messageBtn: UIButton!
    @IBOutlet var callBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
