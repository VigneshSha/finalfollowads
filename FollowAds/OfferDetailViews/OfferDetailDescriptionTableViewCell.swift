//
//  OfferDetailDescriptionTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 21/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
//import ExpandableLabel

class OfferDetailDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var offerDescriptionLabel: ExpandableLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        offerDescriptionLabel.collapsed = true
//        offerDescriptionLabel.text = nil
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
