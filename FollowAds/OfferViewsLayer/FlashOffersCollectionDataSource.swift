//
//  FlashOffersCollectionDataSource.swift
//  FollowAds
//
//  Created by openwave on 25/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol ValueParsingPromotion2Delegate {
    func navigateToView(indexPath: IndexPath)
}

class FlashOffersCollectionDataSource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var collectionView: UICollectionView!
    var ShopImageData: [String?]
    var ShopTitleData: [String?]
    var ShopAddressData: [String?]
    var allPromotionsDataModel = FollowAdsAllPromotionsDataModel()
    var delegate: ValueParsingPromotion2Delegate?
    
    init(data1: [String?], data2: [String?], data3: [String?]) {
        self.ShopImageData = data1
        self.ShopTitleData = data2
        self.ShopAddressData = data3
        super.init()
    }
    
    // MARK : COLLECTION VIEW DELEGATES AND DATASOURCE
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPromotionsDataModel.promotions.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondCellId", for: indexPath) as? SecondTypeCollectionViewCell
        cell?.titleLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_name
        cell?.descriptionLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_area
        let urlstring = allPromotionsDataModel.promotions[indexPath.row].advertisment_img
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                
                cell?.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            cell?.offerImageView.image = UIImage(named: "home_ads_placeholder")
        }
        
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            return CGSize(width: self.collectionView.frame.width/3, height: 170)
        }
        else {
        return CGSize(width: self.collectionView.frame.width/3, height: 170)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}


