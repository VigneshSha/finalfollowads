//
//  BrandOffersCollectionDataSource.swift
//  FollowAds
//
//  Created by openwave on 25/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import FLAnimatedImage

protocol ValueParsingPromotion6Delegate {
    func navigateToView(indexPath: IndexPath)
}

class BrandOffersCollectionDataSource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    var ShopImageData: [String?]
    var ShopTitleData: [String?]
    var ShopAddressData: [String?]
    var delegate: ValueParsingPromotion6Delegate?
    var allPromotionsDataModel = FollowAdsAllPromotionsDataModel()
    
    init(data1: [String?], data2: [String?], data3: [String?]) {
        self.ShopImageData = data1
        self.ShopTitleData = data2
        self.ShopAddressData = data3
        super.init()
    }
    
    // MARK: - COLLECTIONVIEW DATASOURCES AND DELEGATES
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPromotionsDataModel.promotions.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SixthCellId", for: indexPath) as? SixthTypeCollectionViewCell
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            cell?.offerImageView.contentMode = .scaleToFill
        }
        else {
            cell?.offerImageView.contentMode = .scaleAspectFill
            
        }
        cell?.titleLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_name
        cell?.descriptionLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_area
        let urlstring = allPromotionsDataModel.promotions[indexPath.row].advertisment_img
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                
                let imageExtensions = ["gif"]
                let pathExtention = URL(string: encodedString)?.pathExtension
                
//                if imageExtensions.contains(pathExtention ?? "") {
//                    cell?.offerImageView.image = UIImage.gifImageWithURL(encodedString)
//                }
//                else {
//                    cell?.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
//
//                }
                
                
                DispatchQueue.global().async { [weak self] in
                    do {
                        if imageExtensions.contains(pathExtention ?? "") {
                            let gifData = try Data(contentsOf:  URL(string: encodedString)!)
                            cell?.offerImageView.animatedImage = FLAnimatedImage(gifData: gifData)
                        }
                        else {
                            cell?.offerImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_brand_placeholder"))
                        }
                        
                    } catch {
                        print(error)
                    }
                }
                
            }
        }
        else {
            cell?.offerImageView.image = UIImage(named: "home_brand_placeholder")
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width , height: 165)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
