//
//  SeasonalOffersCollectionDataSource.swift
//  FollowAds
//
//  Created by openwave on 25/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

protocol ValueParsingPromotion5Delegate {
    func navigateToView(indexPath: IndexPath)
}

class SeasonalOffersCollectionDataSource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    var ShopImageData: [String?]
    var ShopTitleData: [String?]
    var ShopAddressData: [String?]
    var delegate: ValueParsingPromotion5Delegate?
    var allPromotionsDataModel = FollowAdsAllPromotionsDataModel()
    
    init(data1: [String?], data2: [String?], data3: [String?]) {
        self.ShopImageData = data1
        self.ShopTitleData = data2
        self.ShopAddressData = data3
        super.init()
    }
    
    // MARK: COLLECTIONVIEW DATASOURCES AND DELEGATES
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPromotionsDataModel.promotions.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FifthCellId", for: indexPath) as? FifthTypeCollectionViewCell
        cell?.titleLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_name
        cell?.descriptionLabel.text = allPromotionsDataModel.promotions[indexPath.row].business_area
        let urlstring = allPromotionsDataModel.promotions[indexPath.row].advertisment_img
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                
                cell?.OfferImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
            }
        }
        else {
            cell?.OfferImageView.image = UIImage(named: "home_ads_placeholder")
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FollowAdsDisplayType.typeIsLike == DisplayType.iphone5 {
            let itemsPerRow:CGFloat = 2.7
            let itemWidth = (collectionView.bounds.width / itemsPerRow)
            return CGSize(width: itemWidth, height: 191)
        }
        else {
            let itemsPerRow:CGFloat = 3.2
            let itemWidth = (collectionView.bounds.width / itemsPerRow)
            return CGSize(width: itemWidth, height: 191)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
}
