//
//  FlatSaleNearByCollectionViewController.swift
//  FollowAds
//
//  Created by openwave on 25/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class FlatSaleNearByCollectionViewController: FollowAdsBaseViewController, UICollectionViewDelegateFlowLayout, ValueParsingPromotion3Delegate {
    
    let offerImage =  ["flat1", "flat2", "flat3", "flat4", "flat5"]
    let shopTitle = ["Mathew Garments & Clothes", "Jora Men Garments", "Westside Whole Sales", "Mathew Garments & Clothes", "Jora Men Garments"]
    let shopAddress = ["South Usman Road, T.nagar, Chennai", "Govindha St, Velachery, Chennai", "Nathan St, Nungambakkam, Chennai", "South Usman Road, T.nagar, Chennai", "Govindha St, Velachery, Chennai"]
    @IBOutlet weak var collectionView: UICollectionView!
    var FlatSaleNearByCollectionViewDataSource: FlatSaleNearByCollectionDataSource!
    var promotionId: String?
    var followAdsRequestManager = FollowAdsRequestManager()
    var allPromotionsDataModel = FollowAdsAllPromotionsDataModel()
    var selectedIndex: IndexPath?
    var navigationTitle: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addLoadingView()
        setupCollectionViewDataSource()
        collectionView.register(UINib(nibName: "ThirdTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "ThirdCellId")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.noDataLabel.text = "No Data Available".localized()
        self.noDataLabel.isHidden = true
        self.setUpNavigationBarButton()
        self.trackViewcontroller(name: "View_more_slim", screenClass: "FlatSaleNearByCollectionViewController")
        AppsFlyerTracker.shared().trackEvent("View_more_slim",
                                             withValues: [
                                                AFEventAdView: "View_more_slim",
                                                ]);

        if Reachability.isConnectedToNetwork() == true {
            self.promotionUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }

    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to add the activity indicator.
     @param -.
     @return -.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to navigate to the promotion view.
     @param -.
     @return -.
     */
    func navigateToView(indexPath: IndexPath) {
        selectedIndex = indexPath
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        vc.offerId = allPromotionsDataModel.promotions[(selectedIndex?.row)!].advertisement_id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to perform shop detail service call.
     @param -.
     @return -.
     */
    func promotionUpdate() {
        self.followAdsRequestManager.promotionSectionRequestManager = FollowAdsPromotionSectionRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.promotionSectionRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.promotionSectionRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.promotionSectionRequestManager?.promotion_id = promotionId
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.promotionSectionRequestManager?.lang_id = langId
        
        if UserDefaults.standard.value(forKey: "User_Lat") != nil {
            let userLatText = String()
            let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
            self.followAdsRequestManager.promotionSectionRequestManager?.user_lat = userLat
        }
        if UserDefaults.standard.value(forKey: "User_Lng") != nil {
            let userLngText = String()
            let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
            self.followAdsRequestManager.promotionSectionRequestManager?.user_lng = userLng
        }
        
        if UserDefaults.standard.value(forKey: City) != nil{
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
            self.followAdsRequestManager.promotionSectionRequestManager?.area = city
        }
        else {
            self.followAdsRequestManager.promotionSectionRequestManager?.area = ""
            
        }
        if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
            let postalCodeName = String()
            let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
            self.followAdsRequestManager.promotionSectionRequestManager?.postal_code = postalCode
        }
        else {
            self.followAdsRequestManager.promotionSectionRequestManager?.postal_code = ""
            
        }
        
        let allPromotionsCompletion: AllPromotionsCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
           //     FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                self.collectionView.isHidden = true
                self.noDataLabel.isHidden = false
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.allPromotionsDataModel = response!
                        print(self.allPromotionsDataModel)
                        self.collectionView.isHidden = false
                        self.noDataLabel.isHidden = true
                        self.FlatSaleNearByCollectionViewDataSource.allPromotionsDataModel = self.allPromotionsDataModel
                        self.collectionView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callAllPromotionsServiceCall(requestObject: self.followAdsRequestManager, allPromotionsCompletion)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.navigationItem.title = navigationTitle?.capitalizingFirstLetter()
    }
    
    /*
     This method is used to setup collection view datasource.
     @param -.
     @return -.
     */
    func setupCollectionViewDataSource() {
        FlatSaleNearByCollectionViewDataSource = FlatSaleNearByCollectionDataSource(data1: offerImage, data2: shopTitle, data3: shopAddress)
        self.collectionView.dataSource = FlatSaleNearByCollectionViewDataSource
        self.collectionView.delegate = FlatSaleNearByCollectionViewDataSource
        FlatSaleNearByCollectionViewDataSource?.collectionView = self.collectionView
        FlatSaleNearByCollectionViewDataSource.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used for value parsing from one Vc to another Vc.
     @param ~~.
     @return ~~.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
    }
}
