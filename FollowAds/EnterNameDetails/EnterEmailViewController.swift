//
//  EnterEmailViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 24/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
import AppsFlyerLib

protocol updateEmail {
    func EmailData(string: String)
}

class EnterEmailViewController: FollowAdsBaseViewController, UITextFieldDelegate {
    
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailIdTextField: UITextField!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var btnView: UIView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    var del: updateEmail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.trackViewcontroller(name: "Update_Email", screenClass: "EnterEmailViewController")
        AppsFlyerTracker.shared().trackEvent("Update_Email",
                                             withValues: [
                                                AFEventUpdate: "Update_Email",
                                                ]);

        emailIdTextField.becomeFirstResponder()
        self.setUpNavigationBarButton()
        emailLabel.text = "What's your Email Id?".localized()
        nextBtn.setTitle("Update Email".localized(using: buttonTitles), for: UIControlState.normal)
        let emailText = String()
        let email: String = emailText.passedString((UserDefaults.standard.object(forKey: "EmailId") as? String))
        emailIdTextField.text = email
    }
    
    /*
     This method is used to update the vieWDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
        self.emailIdTextField.delegate = self
        self.btnView.layer.cornerRadius = 30.0
        self.btnView.backgroundColor = .white
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        self.emailIdTextField.attributedPlaceholder = NSAttributedString(string: "Email".localized(),
                                                                         attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 241.0 / 255.0, green: 169.0 / 240.0, blue: 166.0 / 255.0, alpha: 1.0)])
        self.tabBarController?.tabBar.isHidden = true
    }
    
    /*
     This method is used to trigger notifications for keyboard show or keyboard hide.
     @param --.
     @return --.
     */
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    /*
     This method is used to show the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        bottomConstraint.constant = keyboardHeight + 20.0
        
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to hide the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomConstraint.constant = 20.0
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        // Set constraints for the navigation bar button. Fix for iOS 11 issue
        let widthConstraintBack = backBtn.widthAnchor.constraint(equalToConstant: 25)
        let heightConstraintBack = backBtn.heightAnchor.constraint(equalToConstant: 25)
        heightConstraintBack.isActive = true
        widthConstraintBack.isActive = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - TEXTFIELD DELEGATES
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    /*
     This method is used to upload the Image.
     @param -.
     @return -.
     */
    func uploadImage(image:UIImage)
    {
        let userIdText = String()
        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
        let userNameText = String()
        let userName: String = userNameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
        // User "authentication":
        let parameters = ["user_id":userId, "user_name":userName,"email_id":emailIdTextField.text!]
        let username = kUserName
        let password = kPassword
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedData(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-type": "multipart/form-data"]
        
        // Image to upload:
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        
        // Server address (replace this with the address of your own server):
        let url = kBaseURL + "/profile"
        
        // Use Alamofire to upload the image
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
                multipartFormData.append(imageData!, withName: "prof_img", fileName: "prof_img.jpeg", mimeType: "image/jpeg")
                for (key, val) in parameters {
                    multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                }
        },  usingThreshold: UInt64.init(), to: url, method: .post, headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let jsonResponse = response.result.value as? [String: Any] {
                            print(jsonResponse)
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let informationResponse = jsonResponse["information"] as? [String: Any] {
                                print(informationResponse["email_id"]!)
                                print(informationResponse["profile_img"]!)
                                UserDefaults.standard.set(informationResponse["profile_img"], forKey: ProfImg)
                                //UserDefaults.standard.synchronize()
                              
                                if let vcs = self.navigationController?.viewControllers {
                                    let previousVC = vcs[vcs.count - 2]
                                    if previousVC is EnterVerificationCodeViewController {
                                        // ... and so on
                                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                                        nextViewController.modalPresentationStyle = .fullScreen
                                        self.present(nextViewController, animated: true, completion: nil)
                                        nextViewController.selectedViewController = nextViewController.viewControllers?[4]
                                    }
                                    else {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
    // MARK : - BUTTON ACTIONS
    
    @IBAction func NextBtnPressed(_ sender: UIButton) {
        if emailIdTextField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: "Please enter your Email Id".localized())
        }
        else {
            if (emailIdTextField.text?.isEmail())! {
                UserDefaults.standard.set(emailIdTextField.text, forKey: "EmailId")
                
        //        self.uploadImage(image: imageViewProfilePic.image!)

                if del != nil {
                    del?.EmailData(string: emailIdTextField.text!)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: AlertName, message: InvalidEmailIdAlert)
            }
        }
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension EnterEmailViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch emailIdTextField {
        case emailIdTextField:
            if ((emailIdTextField.text?.characters.count)! + (string.characters.count - range.length)) > 30 {
                return false
            }
        default:
            if ((emailIdTextField.text?.characters.count)! + (string.characters.count - range.length)) > 30 {
                return false
            }
        }
        return true
    }
}
