//
//  PhoneNumberViewController.swift
//  FollowAdsNewProfile
//
//  Created by openwave on 17/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import AppsFlyerLib

class PhoneNumberViewController: FollowAdsBaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var RememberLabel: UILabel!
    @IBOutlet weak var IdentifyLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var BtnView: UIView!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    var followAdsRequestManager = FollowAdsRequestManager()
    var registerDataModel = FollowAdsRegisterDataModel()
    var user_IdStr: String?
    var otpValue: String?
    var otpCode: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var mobileNumAlert = "Please Enter Your Mobile Number"
    var validMobileNumAlert = "Please Enter Valid Mobile Number"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewWillAppearUpdate()
        
    }
    
    /*
     This method is used to update the vieWDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
    }
    
    /*
     This method is used to update the viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {
        self.navigationController?.isNavigationBarHidden = false
        self.textField.becomeFirstResponder()
        self.textField.delegate = self
        self.IdentifyLabel.text = identifyCommentText
     //   self.RememberLabel.text = rememberCommentText
        textField.attributedPlaceholder = NSAttributedString(string: phoneNumberPlaceholderText,
                                                             attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 241.0 / 255.0, green: 169.0 / 240.0, blue: 166.0 / 255.0, alpha: 1.0)])
        let attributedString = NSMutableAttributedString(string: IdentifyLabel.text!)
        attributedString.addAttribute(NSAttributedStringKey.kern, value: CGFloat(0.5), range: NSRange(location: 0, length: attributedString.length))
        IdentifyLabel.attributedText = attributedString
        self.BtnView.layer.cornerRadius = 30.0
        self.BtnView.backgroundColor = .white
        self.textField.keyboardType = UIKeyboardType.decimalPad
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        self.setText()
        self.setUpNavigationBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
        textField.placeholder = phoneNumberPlaceholderText.localized()
        IdentifyLabel.text = identifyCommentText.localized()
   //     RememberLabel.text = rememberCommentText.localized()
        nextBtn.setTitle(NextText.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to add the activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = emptyString
    }
    
    /*
     This method is used to trigger notifications for keyboard show or keyboard hide.
     @param --.
     @return --.
     */
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    /*
     This method is used to show the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        BottomConstraint.constant = keyboardHeight + 20.0
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to hide the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillHide(_ notification: Notification) {
        BottomConstraint.constant = 20.0
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        
    }
    
    /*
     This method is used to perform register service call.
     @param -.
     @return -.
     */
    func register() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.registerRequestManager = FollowAdsRegisterRequestManager()
            let usernameText = String()
            let username: String = usernameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
            self.followAdsRequestManager.registerRequestManager?.full_name = username
            self.followAdsRequestManager.registerRequestManager?.phone_number = textField.text
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.registerRequestManager?.device_token = deviceToken
                
            }
            else {
                self.followAdsRequestManager.registerRequestManager?.device_token = ""
                
            }
            self.followAdsRequestManager.registerRequestManager?.device_name = kGetDeviceName
            self.followAdsRequestManager.registerRequestManager?.email_id = ""
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.registerRequestManager?.lang = langId
            
            let locationidText = String()
            let locationId: String = locationidText.passedString((UserDefaults.standard.object(forKey: "City") as? String))
            self.followAdsRequestManager.registerRequestManager?.default_location = locationId
            self.followAdsRequestManager.registerRequestManager?.default_language = "1"
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.registerRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.registerRequestManager?.postal_code = ""
                
            }
            
            let registerCompletion: RegisterCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                }
                else
                {
                    if response != nil {
                        DispatchQueue.main.async(execute: {
                            self.registerDataModel = response!
                            print(self.registerDataModel)
                            self.user_IdStr = self.registerDataModel.information![0].user_id
                            self.otpCode = self.registerDataModel.information![0].otp
                            self.otpValue = (response?.message)!
                            self.performSegue(withIdentifier: verificationViewId, sender: self)
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    }
                }
            }
            FollowAdsServiceHandler.callRegisterServiceCall(requestObject: self.followAdsRequestManager, registerCompletion)
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.locationAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    /*
     This method is used to pass the value to destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: EnterVerificationCodeViewController.self) {
            let vc = segue.destination as? EnterVerificationCodeViewController
            vc?.userIdValue = self.user_IdStr
            vc?.phoneNumValue = textField.text
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - TEXTFIELD DELEGATES
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK : - BUTTON ACTIONS
    
    /*
     This method is used to perform next button action.
     @param --.
     @return --.
     */
    @IBAction func nextBtn(_ sender: UIButton) {
        self.addLoadingView()
        if textField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: mobileNumAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        else if (textField.text?.characters.count)! <= 9 {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: validMobileNumAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        else {
            if Reachability.isConnectedToNetwork() == true {
                UserDefaults.standard.set(textField.text, forKey: "PHONE_NUM")
                //UserDefaults.standard.synchronize()
                self.register()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
            textField.resignFirstResponder()
            self.view.endEditing(true)
        }
    }
    
    /*
     This method is used to perform back button action.
     @param --.
     @return --.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        UserDefaults.standard.removeObject(forKey: UserId)
        //UserDefaults.standard.synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PhoneNumberViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case textField:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
            
        default:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 10 {
                return false
            }
        }
        return true
    }
}

