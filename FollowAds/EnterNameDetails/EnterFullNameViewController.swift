//
//  EnterFullNameViewController.swift
//  FollowAdsNewProfile
//
//  Created by openwave on 17/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import AppsFlyerLib

class EnterFullNameViewController: FollowAdsBaseViewController, UITextFieldDelegate{
    
    @IBOutlet weak var BtnView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var fullNameLabel: UILabel!
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var nameAlert = "Please Enter Your Full Name"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textField.becomeFirstResponder()
        self.textField.delegate = self
        self.BtnView.layer.cornerRadius = 30.0
        self.BtnView.backgroundColor = .white
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        self.textField.attributedPlaceholder = NSAttributedString(string: FullNamePlaceholderText,
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 241.0 / 255.0, green: 169.0 / 240.0, blue: 166.0 / 255.0, alpha: 1.0)])
        self.tabBarController?.tabBar.isHidden = true
        self.setText()
        self.setUpNavigationBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
       
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     This method is used to update the vieWDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
    }
    
    @objc func setText(){
        textField.placeholder = NamePlaceholder.localized()
        fullNameLabel.text = fullNameText.localized()
        nextBtn.setTitle(NextText.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to trigger notifications for keyboard show or keyboard hide.
     @param --.
     @return --.
     */
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    /*
     This method is used to show the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        BottomConstraint.constant = keyboardHeight + 20.0
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to hide the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillHide(_ notification: Notification) {
        BottomConstraint.constant = 20.0
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    // MARK : - TEXTFIELD DELEGATES
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK : - Button Actions
    
    /*
     This method is used to perform next button action.
     @param -.
     @return -.
     */
    @IBAction func nextBtn(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        if textField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: nameAlert.localized())
        }
        else {
            UserDefaults.standard.set(textField.text, forKey: UserName)
            //UserDefaults.standard.synchronize()
            self.view.endEditing(true)
            textField.resignFirstResponder()
            self.performSegue(withIdentifier: PhoneNumberViewId, sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     This method is used to perform back button action.
     @param -.
     @return -.
     */
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension EnterFullNameViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case textField:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 30 {
                return false
            }
            
        default:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 30 {
                return false
            }
        }
        return true
    }
}


