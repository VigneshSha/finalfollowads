//
//  EnterVerificationCodeViewController.swift
//  FollowAdsNewProfile
//
//  Created by openwave on 17/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import AppsFlyerLib

class EnterVerificationCodeViewController: FollowAdsBaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var ButtonView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var CodeInstructionLabel: UILabel!
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet var newCodeView: UIView!
    @IBOutlet var getOtpLabel: UILabel!
    @IBOutlet var sendNewCodeBtn: UIButton!
    @IBOutlet var sentToLabel: UILabel!
    var profImg : UIImage?
    var profImgView: UIImageView?
    var userIdValue: String?
    var seconds = 60
    var timer = Timer()
    var isTimerRunning = false
    var followAdsRequestManager = FollowAdsRequestManager()
    var otpDataModel = FollowAdsOtpVerificationDataModel()
    var registerDataModel = FollowAdsRegisterDataModel()
    var user_IdStr: String?
    var otpValue: String?
    var otpCode: String?
    var phoneNumValue: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var otpValidationAlert = "Please Enter Your OTP Code"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewWillAppearUpdate()
       
    }
    
    /*
     This method is used to update the vieWDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
       
    }
    
    /*
     This method is used to update the viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {
        self.textField.delegate = self
        self.textField.attributedPlaceholder = NSAttributedString(string: CodePlaceholderText,
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 241.0 / 255.0, green: 169.0 / 240.0, blue: 166.0 / 255.0, alpha: 1.0)])
        self.CodeInstructionLabel.text = codeInstructionText
        self.ButtonView.layer.cornerRadius = 30.0
        self.ButtonView.backgroundColor = .white
        self.textField.keyboardType = UIKeyboardType.decimalPad
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        runTimer()
        timerLabel.isHidden = false
        newCodeView.isHidden = true
        self.setText()
        
        getOtpLabel.text = "Didn't get it?".localized()
        textField.becomeFirstResponder()
        let phoneNumText = String()
        let phoneNum: String = phoneNumText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
        sentToLabel.text = "Sent to ".localized() + phoneNum
        self.navigationController?.isNavigationBarHidden = false
        self.setUpNavigationBarButton()
        textField.defaultTextAttributes.updateValue(10.0, forKey: NSAttributedStringKey.kern.rawValue)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
        textField.placeholder = CodePlaceholderText.localized()
        CodeInstructionLabel.text = codeInstructionText.localized()
        let phoneNumText = String()
        let phoneNum: String = phoneNumText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
        sentToLabel.text = sentTo.localized() + phoneNum
        sendNewCodeBtn.setTitle(sendNewCode.localized(using: buttonTitles), for: UIControlState.normal)
        timerLabel.text = PleaseWaitText.localized()
        nextBtn.setTitle(NextText.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to add the activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    /*
     This method is used to perform register service call.
     @param -.
     @return -.
     */
    func register() {
        if UserDefaults.standard.value(forKey: City) != nil{
            self.followAdsRequestManager.registerRequestManager = FollowAdsRegisterRequestManager()
            let usernameText = String()
            let username: String = usernameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
            self.followAdsRequestManager.registerRequestManager?.full_name = username
            self.followAdsRequestManager.registerRequestManager?.phone_number = phoneNumValue
            if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                let deviceTokenText = String()
                let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                self.followAdsRequestManager.registerRequestManager?.device_token = deviceToken
            }
            else {
                self.followAdsRequestManager.registerRequestManager?.device_token = ""
            }
            self.followAdsRequestManager.registerRequestManager?.device_name = kGetDeviceName
            self.followAdsRequestManager.registerRequestManager?.email_id = ""
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.registerRequestManager?.lang = langId
            let locationidText = String()
            let locationId: String = locationidText.passedString((UserDefaults.standard.object(forKey: "City") as? String))
            self.followAdsRequestManager.registerRequestManager?.default_location = locationId
            self.followAdsRequestManager.registerRequestManager?.default_language = "1"
            if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
                let postalCodeName = String()
                let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
                self.followAdsRequestManager.registerRequestManager?.postal_code = postalCode
            }
            else {
                self.followAdsRequestManager.registerRequestManager?.postal_code = ""
                
            }
            let registerCompletion: RegisterCompletionBlock = {(response, error) in
                if let _ = error {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                }
                else
                {
                    if response != nil {
                        DispatchQueue.main.async(execute: {
                            self.registerDataModel = response!
                            print(self.registerDataModel)
                            self.user_IdStr = self.registerDataModel.information![0].user_id
                            self.otpCode = self.registerDataModel.information![0].otp
                            self.otpValue = OtpMsg
                            self.viewWillAppearUpdate()
                            self.seconds = 60
//                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.otpValue!)
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    }
                }
            }
            FollowAdsServiceHandler.callRegisterServiceCall(requestObject: self.followAdsRequestManager, registerCompletion)
        }
        else {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.locationAlert.localized())
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    /*
     This method is used to perform the otp service call.
     @param --.
     @return --.
     */
    func otpVerification() {
        self.followAdsRequestManager.otpVerificationRequestManager = FollowAdsOtpVerificationRequestManager()
        self.followAdsRequestManager.otpVerificationRequestManager?.user_id = userIdValue
        self.followAdsRequestManager.otpVerificationRequestManager?.otp = textField.text
        let otpVerificationCompletion: OtpVerificationCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.otpDataModel = response!
                        print(self.otpDataModel)
                        if self.otpDataModel.information != nil {
                            UserDefaults.standard.set(self.otpDataModel.information![0].user_id, forKey: "User_Id")
                            UserDefaults.standard.set(self.otpDataModel.information![0].user_phone_no, forKey: "PHONE_NUM")
                            UserDefaults.standard.set(self.otpDataModel.information![0].email_id, forKey: "EmailId")
                            UserDefaults.standard.set(self.otpDataModel.information![0].used_coupon_count, forKey: "USER_COUPON_COUNT")
                            UserDefaults.standard.set(self.otpDataModel.information![0].wallet_amt, forKey: "WALLET_AMT")
                            UserDefaults.standard.set(self.otpDataModel.information![0].profile_img, forKey: "PROF_IMG")
                            UserDefaults.standard.set(self.otpDataModel.information![0].liked_offers_count, forKey: "LIKED_OFFERS_COUNT")
                            UserDefaults.standard.set(self.otpDataModel.information![0].followed_stores_count, forKey: "FOLLOWED_STORES_COUNT")
                           UserDefaults.standard.set(self.otpDataModel.information![0].scanned_coupon_count, forKey: "Scanned_Coupon_Count")
                            UserDefaults.standard.set(self.otpDataModel.user_reminder_count, forKey: "UserReminderCount")
                            
                            UserDefaults.standard.set(self.otpDataModel.information![0].notification, forKey: "Notification_Count")
                            
                            self.performSegue(withIdentifier: ProfileViewId, sender: self)
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if response?.response_code == "027" {
                                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Verification code is incorrect".localized())
                            }
                        }
                    })
                }
            }
        }
        FollowAdsServiceHandler.callOtpVerificationServiceCall(requestObject: self.followAdsRequestManager, otpVerificationCompletion)
    }
    
    
    /*
     This method is used to run the timer.
     @param --.
     @return --.
     */
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    /*
     This method is used to perform the actions when timer stops and the timer running.
     @param --.
     @return --.
     */
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            timerLabel.isHidden = true
            newCodeView.isHidden = false
            //Send alert to indicate "time's up!"
        } else {
            timerLabel.isHidden = false
            newCodeView.isHidden = true
             seconds -= 1
            timerLabel.text = PleaseWaitText.localized() + timeString(time: TimeInterval(seconds))
        }
    }
    
    /*
     This method is used to return the minutes and seconds timer format.
     @param --.
     @return --.
     */
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    /*
     This method is used to trigger notifications for keyboard show or keyboard hide.
     @param --.
     @return --.
     */
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    /*
     This method is used to show the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        BottomConstraint.constant = keyboardHeight + 20.0
        
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to hide the keyboard with animation.
     @param --.
     @return --.
     */
    @objc func keyboardWillHide(_ notification: Notification) {
        BottomConstraint.constant = 20.0
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    // MARK : - TEXTFIELD DELEGATES
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK : - BUTTON ACTIONS
    
    /*
     This method is used to perform next button action.
     @param -.
     @return -.
     */
    @IBAction func nextBtn(_ sender: UIButton) {
        if textField.text == emptyString {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: otpValidationAlert.localized())
        }
        else {
            self.addLoadingView()
            if Reachability.isConnectedToNetwork() == true {
                self.otpVerification()
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
            self.view.endEditing(true)
            textField.resignFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform back button action.
     @param -.
     @return -.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used to perform send new button action.
     @param -.
     @return -.
     */
    @IBAction func sendNewButtonPressed(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            self.register()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
    }
    
    /*
     This method is used for value parsing from one Vc to another Vc.
     @param ~~.
     @return ~~.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: AddProfilePictureViewController.self) {
            let vc = segue.destination as? AddProfilePictureViewController
            if self.otpDataModel.information?[0].profile_img != nil {
            vc?.profUrl = self.otpDataModel.information?[0].profile_img
            }
        }
    }
    
}

extension EnterVerificationCodeViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case textField:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 4 {
                textField.resignFirstResponder()
                return false
            }
            
        default:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 4 {
                return false
            }
        }
        return true
    }
}




