//
//  EditViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 01/08/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import Alamofire
import FirebaseDynamicLinks
import AppsFlyerLib

protocol customProfileViewUpdate {
    func updateInfo(string: String,alertString: String)
    func EmailData(string: String)
    
}
class EditViewController: FollowAdsBaseViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet var imageViewProfilePic: UIImageView!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var choosePhotoButton: UIButton!
    @IBOutlet var TakePhotoButton: UIButton!
    @IBOutlet var SaveButton: UIButton!
    var imagePicker = UIImagePickerController()
    var delegate : customProfileViewUpdate?
    var nameStr: String?
    var emailStr: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var emailValidationAlert = "Please Enter Correct Email Address"
    var navigationTitle = "Edit Profile"
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewWillAppearUpdate()
       
    }
    
    /*
     This method is used to update viewDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
        self.imageViewProfilePic.layer.cornerRadius = imageViewProfilePic.frame.size.width / 2
        imageViewProfilePic.clipsToBounds = true
        self.imageViewProfilePic.layer.borderWidth = 1.0
        self.imageViewProfilePic.layer.borderColor = UIColor.white.cgColor
        self.choosePhotoButton.layer.cornerRadius = 25.0
        self.choosePhotoButton.layer.borderWidth = 1.0
        self.choosePhotoButton.layer.borderColor = UIColor.white.cgColor
        self.TakePhotoButton.layer.cornerRadius = 25.0
        self.TakePhotoButton.layer.borderWidth = 1.0
        self.TakePhotoButton.layer.borderColor = UIColor.white.cgColor
        self.SaveButton.layer.cornerRadius = 25.0
        self.SaveButton.layer.borderWidth = 1.0
        self.SaveButton.layer.borderColor = UIColor.white.cgColor
        nameTextField.attributedPlaceholder = NSAttributedString(string: "Full Name".localized(),
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email".localized(),
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        self.setText()
    }
    
    /*
     This method is used to update viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {
        emailTextField.delegate = self
        nameTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        let NameText = String()
        let Names: String = NameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
        nameTextField.text = Names
        let emailText = String()
        let email: String = emailText.passedString((UserDefaults.standard.object(forKey: "EmailId") as? String))
        emailTextField.text = email
        self.setUpNavigationBarButton()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is ProfileViewController {
                // ... and so on
                self.convertToImgFromUrl()
            }
            else {
            }
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        
        if let aps = notification.userInfo?["aps"] as? [AnyHashable:String]{
            if let ad_id = aps["advertisement_id"] {
                vc.offerId = ad_id
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    
    @objc func setText(){
        choosePhotoButton.setTitle(chooseAPhoto.localized(using: buttonTitles), for: UIControlState.normal)
        TakePhotoButton.setTitle(takeAPhoto.localized(using: buttonTitles), for: UIControlState.normal)
        SaveButton.setTitle(saveBtnTitle.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
        if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }

    /*
     This method is used to add activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = emptyString
    }
    
    /*
     This method is used to convert the url into image.
     @param --.
     @return --.
     */
    func convertToImgFromUrl() {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let urlstring = UserDefaults.standard.value(forKey: ProfImg) as? String
            
            if urlstring != nil {
                self.imageViewProfilePic.sd_setImage(with: URL(string: urlstring!), placeholderImage: UIImage(named: "Man_Avatar"))
            }
            else {
                self.imageViewProfilePic.image = UIImage(named: "Man_Avatar")
            }
        }
        else {
        }
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle.localized()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to end the view editing when tap on the view.
     @param --.
     @return --.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*
     This method is used to perform Choose button action.
     @param --.
     @return --.
     */
    @IBAction func choosePhotoBtnPressed(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let imgValue = UserDefaults.standard.value(forKey: ProfImg)
            UserDefaults.standard.set(imgValue, forKey: ImgUrl)
            UserDefaults.standard.removeObject(forKey: ProfImg)
        }
        self.openGallary()
    }
    
    /*
     This method is used to select photo from mobile photo gallery.
     @param --.
     @return --.
     */
    func openGallary(){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func takePhotoBtnPressed(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let imgValue = UserDefaults.standard.value(forKey: ProfImg)
            UserDefaults.standard.set(imgValue, forKey: ImgUrl)
            UserDefaults.standard.removeObject(forKey: ProfImg)
        }
        self.openCamera()
    }
    
    /*
     This method is used to take a photo and update in the image picker.
     @param --.
     @return --.
     */
    func openCamera(){
        self.navigationController?.navigationBar.isTranslucent = false
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: WarningAlertTitle, message: WarningMsg)
        }
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        self.addLoadingView()
        if Reachability.isConnectedToNetwork() == true {
            if emailTextField.text != emptyString {
                if (emailTextField.text?.isEmail())! {
                    UserDefaults.standard.set(emailTextField.text, forKey: "EmailId")
                    if delegate != nil {
                        delegate?.EmailData(string: emailTextField.text!)
                        UserDefaults.standard.set(nameTextField.text, forKey: UserName)
                        self.uploadImage(image: imageViewProfilePic.image!)
                    }
                }
                else {
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: emailValidationAlert.localized())
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                UserDefaults.standard.set(nameTextField.text, forKey: UserName)
                self.uploadImage(image: imageViewProfilePic.image!)
            }
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
    }
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    /*
     This method is used to upload the Image.
     @param -.
     @return -.
     */
    func uploadImage(image:UIImage)
    {
        let userIdText = String()
        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
        let userNameText = String()
        let userName: String = userNameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
        // User "authentication":
        let parameters = ["user_id":userId, "user_name":userName,"email_id":emailTextField.text!]
        let username = kUserName
        let password = kPassword
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedData(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-type": "multipart/form-data"]
        
        // Image to upload:
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        
        // Server address (replace this with the address of your own server):
        let url = kBaseURL + "/profile"
        
        // Use Alamofire to upload the image
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
                multipartFormData.append(imageData!, withName: "prof_img", fileName: "prof_img.jpeg", mimeType: "image/jpeg")
                for (key, val) in parameters {
                    multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                }
        },  usingThreshold: UInt64.init(), to: url, method: .post, headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let jsonResponse = response.result.value as? [String: Any] {
                            print(jsonResponse)
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let informationResponse = jsonResponse["information"] as? [String: Any] {
                                print(informationResponse["email_id"]!)
                                print(informationResponse["profile_img"]!)
                                UserDefaults.standard.set(informationResponse["profile_img"], forKey: ProfImg)
                                //UserDefaults.standard.synchronize()
                                if let deleggate = self.delegate {
                                    if jsonResponse["response_code"] as? String == "023" {
                                    deleggate.updateInfo(string: self.nameTextField.text!, alertString: "User profile has been updated successfully".localized())
                                    }
                                    else if jsonResponse["response_code"] as? String == "024" {
                                        deleggate.updateInfo(string: self.nameTextField.text!, alertString: "User profile has not been updated successfully".localized())

                                    }
                                }
                                if let vcs = self.navigationController?.viewControllers {
                                    let previousVC = vcs[vcs.count - 2]
                                    if previousVC is EnterVerificationCodeViewController {
                                        // ... and so on
                                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                                        nextViewController.modalPresentationStyle = .fullScreen
                                        self.present(nextViewController, animated: true, completion: nil)
                                        nextViewController.selectedViewController = nextViewController.viewControllers?[4]
                                    }
                                    else {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: encodingError as! String)
                }
        }
        )
    }
    
    /*
     This method is used to pass values to the destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let tabbarview: UITabBarController = (segue.destination as? UITabBarController)!
        let destinationVC : ProfileViewController = tabbarview.viewControllers?[4] as! ProfileViewController
        destinationVC.editBtn.setTitle(saveName, for: .normal)
        destinationVC.isSelected = true
    }
    
    /*
     This method is used to setup the parameters.
     @param -.
     @return -.
     */
    func createBodyWithParameters(parameters: [String:String], filePathKey: String?, imageDataKey: NSData?, boundary: String) -> NSData {
        var body = Data()
        let randomNumber = arc4random()
        let filename = "user-profile\(randomNumber).jpg"
        let mimetype = "image/jpg"
        body.append(Data("foo".utf8))
        body.append(string: "--\(boundary)\r\n")
        body.append(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.append(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(string: "\r\n")
        body.append(string: "--\(boundary)--\r\n")
        if imageDataKey != nil {
            body.append(imageDataKey! as Data)
        }
        return body as NSData
    }
    
    /*
     This method is used to generate Boundary String.
     @param -.
     @return -.
     */
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    // MARK : - TEXTFIELD DELEGATES
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension EditViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            DispatchQueue.main.async {
            self.imageViewProfilePic.image = editedImage
            }
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension EditViewController  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch emailTextField {
        case emailTextField:
            if ((emailTextField.text?.count)! + (string.count - range.length)) > 30 {
                return false
            }
        case nameTextField:
            if ((nameTextField.text?.count)! + (string.count - range.length)) > 25 {
                return false
            }
        default:
            if ((emailTextField.text?.characters.count)! + (string.characters.count - range.length)) > 30 {
                return false
            }
        }
        return true
    }
}
