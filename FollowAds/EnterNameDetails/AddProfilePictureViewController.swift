//
//  AddProfilePictureViewController.swift
//  FollowAdsNewProfile
//
//  Created by openwave on 17/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import AppsFlyerLib

protocol customNavigationViewUpdate {
    func updateUI()
}

class AddProfilePictureViewController: FollowAdsBaseViewController {
    
    var delegate : customNavigationViewUpdate?
    @IBOutlet weak var ProfilePicInstructionLabel: UILabel!
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var choosePhoto: UIButton!
    @IBOutlet weak var takePhoto: UIButton!
    var imagePicker = UIImagePickerController()
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var doneButton: UIButton!
    var followAdsRequestManager = FollowAdsRequestManager()
    var profileDataModel = FollowAdsProfileDataModel()
    var str: String?
    var user_img: UIImage?
    var imageBool: Bool? = false
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var setProfilePicAlert = "Please set your profile picture"
    @IBOutlet weak var addProfilePicLabel: UILabel!
    var profUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadUpdate()
        self.doneButton.setTitle("Done".localized(using: buttonTitles), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewWillAppearUpdate()
    }
    
    /*
     This method is used to update viewDidLoad method.
     @param --.
     @return --.
     */
    func viewDidLoadUpdate() {
        self.ProfilePic.layer.cornerRadius = ProfilePic.frame.size.width / 2
        ProfilePic.clipsToBounds = true
        self.ProfilePic.layer.borderWidth = 1.0
        self.ProfilePic.layer.borderColor = UIColor.white.cgColor
        self.choosePhoto.layer.cornerRadius = 25.0
        self.choosePhoto.layer.borderWidth = 1.0
        self.choosePhoto.layer.borderColor = UIColor.white.cgColor
        self.takePhoto.layer.cornerRadius = 25.0
        self.takePhoto.layer.borderWidth = 1.0
        self.takePhoto.layer.borderColor = UIColor.white.cgColor
    //    self.ProfilePicInstructionLabel.text = addPhotoCommentText
        self.setText()
    }
    
    /*
     This method is used to update viewWillAppear method.
     @param --.
     @return --.
     */
    func viewWillAppearUpdate() {
        self.setUpNavigationBarButton()
        self.tabBarController?.tabBar.isHidden = true
        self.convertToProfImgFromUrl()
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is ProfileViewController {
                // ... and so on
                skipButton.isHidden = true
                self.convertToImgFromUrl()
            }
            else {
                skipButton.isHidden = false
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
    //    ProfilePicInstructionLabel.text = addPhotoCommentText.localized()
        choosePhoto.setTitle(chooseAPhoto.localized(using: buttonTitles), for: UIControlState.normal)
        takePhoto.setTitle(takeAPhoto.localized(using: buttonTitles), for: UIControlState.normal)
        addProfilePicLabel.text = addProfilePic.localized()
        skipButton.setTitle(skip.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to add activity indicator.
     @param --.
     @return --.
     */
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = emptyString
    }
    
    /*
     This method is used to convert the url into image.
     @param --.
     @return --.
     */
    func convertToImgFromUrl() {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let urlstring = UserDefaults.standard.value(forKey: ProfImg) as? String
            if urlstring != nil {
                if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    self.ProfilePic.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "Man_Avatar"))
                }
            }
            else {
                self.ProfilePic.image = UIImage(named: "Man_Avatar")
            }
        }
        else {
        }
    }
    
    /*
     This method is used to convert the url into image.
     @param --.
     @return --.
     */
    func convertToProfImgFromUrl() {
        if self.profUrl != nil {
                if let encodedString  = profUrl?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    
                    self.ProfilePic.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "Man_Avatar"))
                }
        }
        else {
            self.ProfilePic.image = UIImage(named: "Man_Avatar")
        }
        
    }
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to perform back button action.
     @param --.
     @return --.
     */
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     This method is used to perform TakePhoto button action.
     @param --.
     @return --.
     */
    @IBAction func TakeAPhoto(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let imgValue = UserDefaults.standard.value(forKey: ProfImg)
            UserDefaults.standard.set(imgValue, forKey: ImgUrl)
            UserDefaults.standard.removeObject(forKey: ProfImg)
        }
        imageBool = true
        self.openCamera()
    }
    
    /*
     This method is used to take a photo and update in the image picker.
     @param --.
     @return --.
     */
    func openCamera(){
        self.navigationController?.navigationBar.isTranslucent = false
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: WarningAlertTitle, message: WarningMsg)
        }
    }
    
    /*
     This method is used to perform Choose button action.
     @param --.
     @return --.
     */
    @IBAction func ChooseAPhoto(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: ProfImg) != nil {
            let imgValue = UserDefaults.standard.value(forKey: ProfImg)
            UserDefaults.standard.set(imgValue, forKey: ImgUrl)
            UserDefaults.standard.removeObject(forKey: ProfImg)
        }
        imageBool = true
        self.openGallary()
    }
    
    /*
     This method is used to select photo from mobile photo gallery.
     @param --.
     @return --.
     */
    func openGallary(){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /*
     This method is used to perform skip button action.
     @param --.
     @return --.
     */
    @IBAction func skipButtonPressed(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
     This method is used to perform done button action.
     @param --.
     @return --.
     */
    @IBAction func doneButtonPressed(_ sender: UIButton) {
      //  if imageBool == false {
        if ProfilePic.image == UIImage(named: "Man_Avatar") {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: setProfilePicAlert.localized())
        }
        else {
            self.addLoadingView()
            if Reachability.isConnectedToNetwork() == true {
                self.uploadImage(image: ProfilePic.image!)
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
            }
        }
    }
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    /*
     This method is used to upload the Image.
     @param -.
     @return -.
     */
    func uploadImage(image:UIImage)
    {
        let userIdText = String()
        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
        let userNameText = String()
        let userName: String = userNameText.passedString((UserDefaults.standard.object(forKey: UserName) as? String))
        // User "authentication":
        let parameters = ["user_id":userId, "user_name":userName,"email_id":""]
        let username = kUserName
        let password = kPassword
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedData(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)", "Content-type": "multipart/form-data"]
        
        // Image to upload:
        let imageData = UIImageJPEGRepresentation(image, 0.2)
        
        // Server address (replace this with the address of your own server):
        let url = kBaseURL + "/profile"
        
        // Use Alamofire to upload the image
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
                multipartFormData.append(imageData!, withName: "prof_img", fileName: "prof_img.jpeg", mimeType: "image/jpeg")
                for (key, val) in parameters {
                    multipartFormData.append(val.data(using: String.Encoding.utf8)!, withName: key)
                }
        },  usingThreshold: UInt64.init(), to: url, method: .post, headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let jsonResponse = response.result.value as? [String: Any] {
                            print(jsonResponse)
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let informationResponse = jsonResponse["information"] as? [String: Any] {
                                print(informationResponse["email_id"]!)
                                print(informationResponse["profile_img"]!)
                                UserDefaults.standard.set(informationResponse["profile_img"], forKey: ProfImg)
                                //UserDefaults.standard.synchronize()
                                if let deleggate = self.delegate {
                                    deleggate.updateUI()
                                }
                                if let vcs = self.navigationController?.viewControllers {
                                    let previousVC = vcs[vcs.count - 2]
                                    if previousVC is EnterVerificationCodeViewController {
                                        // ... and so on
                                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                                        nextViewController.modalPresentationStyle = .fullScreen
                                        self.present(nextViewController, animated: true, completion: nil)
                                        nextViewController.selectedViewController = nextViewController.viewControllers?[4]
                                    }
                                    else {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: encodingError as! String)
                }
        }
        )
    }
    
    /*
     This method is used to pass values to the destination viewcontroller.
     @param --.
     @return --.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let tabbarview: UITabBarController = (segue.destination as? UITabBarController)!
        let destinationVC : ProfileViewController = tabbarview.viewControllers?[4] as! ProfileViewController
        destinationVC.editBtn.setTitle(saveName, for: .normal)
        destinationVC.isSelected = true
    }
    
    /*
     This method is used to setup the parameters.
     @param -.
     @return -.
     */
    func createBodyWithParameters(parameters: [String:String], filePathKey: String?, imageDataKey: NSData?, boundary: String) -> NSData {
        var body = Data()
        let randomNumber = arc4random()
        let filename = "user-profile\(randomNumber).jpg"
        let mimetype = "image/jpg"
        body.append(Data("foo".utf8))
        body.append(string: "--\(boundary)\r\n")
        body.append(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.append(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(string: "\r\n")
        body.append(string: "--\(boundary)--\r\n")
        if imageDataKey != nil {
            body.append(imageDataKey! as Data)
        }
        return body as NSData
    }
    
    /*
     This method is used to generate Boundary String.
     @param -.
     @return -.
     */
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}

extension AddProfilePictureViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            DispatchQueue.main.async {
                self.ProfilePic.image = editedImage
            }
            
        }
        else {
            print("Something went wrong")
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


