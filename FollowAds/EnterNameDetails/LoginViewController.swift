//
//  LoginViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 27/08/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import AppsFlyerLib

class LoginViewController: FollowAdsBaseViewController {
    
    @IBOutlet var LoginView: UIView!
    @IBOutlet var logoBtn: UIButton!
    @IBOutlet var signInBtn: UIButton!
    @IBOutlet var commentsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.signInBtn.layer.cornerRadius = 10.0
        logoBtn.layer.cornerRadius = logoBtn.frame.size.width / 2
        logoBtn.clipsToBounds = true
        logoBtn.contentMode = .scaleAspectFill
        self.setUpNavigationBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func setText(){
        commentsLabel.text = signInInstructionText.localized()
        signInBtn.setTitle(signInText.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    @IBAction func logoBtnPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EnterFullNameViewController") as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func SignInBtnPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EnterFullNameViewController") as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
