//
//  SuperPremiumDetailVC.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 17/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import UIKit

class SuperPremiumDetailVC: UIViewController, SegmentedProgressBarDelegate {
    
    @IBOutlet weak var navigationVw: UIView!
    @IBOutlet weak var shopImgVw: UIImageView!
    @IBOutlet weak var shopTitleLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var logoImgVw: UIImageView!
    @IBOutlet weak var bannerImgVw: UIImageView!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    private var spb: SegmentedProgressBar!
    private let images = [#imageLiteral(resourceName: "vanHeusen"), #imageLiteral(resourceName: "Pepsi"), #imageLiteral(resourceName: "GRT")]
    var indexValue: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        self.view.backgroundColor = UIColor.white
        bannerImgVw.contentMode = .scaleToFill
        updateImage(index: 0)
        spb = SegmentedProgressBar(numberOfSegments: offerImgs.count, duration: 5)
        spb.frame = CGRect(x: 15, y: self.navigationVw.frame.height + 2, width: view.frame.width - 30, height: 4)
        spb.delegate = self
        spb.indexVal = indexValue
        spb.topColor = UIColor.white
        spb.bottomColor = UIColor.white.withAlphaComponent(0.25)
        spb.padding = 2
        view.addSubview(spb)
        spb.startAnimation()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedView)))
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func segmentedProgressBarChangedIndex(index: Int) {
        print("Now showing index: \(index)")
        updateImage(index: index)
    }
    
    func segmentedProgressBarFinished() {
        print("Finished!")
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedView() {
        spb.isPaused = !spb.isPaused
    }
    
    private func updateImage(index: Int) {
        bannerImgVw.image = UIImage(named: offerImgs[index])
    }
    
    @objc func respondToSwipeGesture() {
    }
    
    @IBAction func rightBtnPressed(_ sender: UIButton) {
        spb.skip()
    }
    
    @IBAction func leftBtnPressed(_ sender: UIButton) {
        spb.rewind()
    }
    
}
