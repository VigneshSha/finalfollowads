//
//  CategoryViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 10/01/19.
//  Copyright © 2019 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

class CategoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    var categoryTableDatasource: CategoryTableDatasource?
    let categoryListArray = ["Apparel & Accessories", "Electronics & HomeAppliances", "Events & Activities", "Food & Beverages", "Gym & Fitness", "Mobile & Gadgets", "Provisions & Groceries", "Salon & Spa", "Services", "Others"]
    let categoryImgsArray = ["google-maps (4)", "google-maps (4)", "google-maps (4)", "google-maps (4)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)", "edit-draw-pencil (2)"]
    var navigationTitle = "Categories".localized()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableDatasource()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: fontHelveticaNeueMedium, size: 17)!]
        self.navigationItem.title = navigationTitle
    }
    
    /*
     This method is used to setup TableView Datasource.
     @param ~~.
     @return ~~.
     */
    func setupTableDatasource() {
        categoryTableDatasource = CategoryTableDatasource(catImgModel: categoryImgsArray, catNameModel: categoryListArray)
        self.tableView.delegate = categoryTableDatasource
        self.tableView.dataSource = categoryTableDatasource
        categoryTableDatasource?.tableView = self.tableView
        self.tableView.tableFooterView = UIView()
    }
    
    /*
     This method is used for back button action.
     @param ~~.
     @return ~~.
     */
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
