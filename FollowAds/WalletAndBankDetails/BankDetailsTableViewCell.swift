//
//  BankDetailsTableViewCell.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 18/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit

class BankDetailsTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.delegate = self
        textField.attributedPlaceholder = NSAttributedString(string: "placeholder text",
                                                             attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)])
        // Initialization code
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textField.endEditing(true)
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
