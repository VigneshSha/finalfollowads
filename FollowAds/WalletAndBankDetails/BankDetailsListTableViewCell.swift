//
//  BankDetailsListTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 11/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit

class BankDetailsListTableViewCell: UITableViewCell {
    
    @IBOutlet var labelBankName: UILabel!
    @IBOutlet var labelAccName: UILabel!
    @IBOutlet var labelAccNum: UILabel!
    @IBOutlet var labelIfscCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
