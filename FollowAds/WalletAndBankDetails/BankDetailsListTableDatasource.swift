//
//  BankDetailsListTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 11/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol BankListNavigationDelegate {
    func navigateToView(indexPath: IndexPath)
}

class BankDetailsListTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableview: UITableView!
    var bankListDataModel: FollowAdsBankListDataModel?
    var delegate: BankListNavigationDelegate?
    var bankListVC: BankDetailsListViewController?
    
    init(bankListData: FollowAdsBankListDataModel?) {
        self.bankListDataModel = bankListData
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview?.dequeueReusableCell(withIdentifier: "BankDetailsListCellId", for: indexPath) as! BankDetailsListTableViewCell
        cell.labelBankName.text = bankListDataModel?.bank_detail[0].bank_name
        cell.labelAccName.text = bankListDataModel?.bank_detail[0].acc_name
        cell.labelAccNum.text = bankListDataModel?.bank_detail[0].acc_no
        cell.labelIfscCode.text = bankListDataModel?.bank_detail[0].ifsc_code
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
//            if cell.accessoryType == .none {
//                cell.accessoryType = .checkmark
//
//            }
//            else {
//                cell.accessoryType = .none
//            }
            cell.accessoryType = .checkmark
        }
        else {
            
        }
        if delegate != nil {
            delegate?.navigateToView(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .none
        }
    }
    
}
