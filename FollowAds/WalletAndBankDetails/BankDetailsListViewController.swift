//
//  BankDetailsListViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 11/09/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import AppsFlyerLib

class BankDetailsListViewController: FollowAdsBaseViewController, BankListNavigationDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addBarButton: UIBarButtonItem!
    var bankDetailsListDatasource: BankDetailsListTableDatasource?
    var bankListDataModel: FollowAdsBankListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var transferMoneyDataModel: FollowAdsTransferMoneyDataModel?
    var nodataView: NoDataView?
    var selectedIndex: IndexPath?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var bankIdStr = String()
    @IBOutlet weak var addBankDetailsLbl: UILabel!
    @IBOutlet weak var editButton: UIBarButtonItem!
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoadingView()
        self.setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        addBankDetailsLbl.text = "Please Add Bank Details".localized()
        self.SetUpTableDataSource()
        self.setUpNavigationBarButton()
        if Reachability.isConnectedToNetwork() == true {
            self.bankListUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
     //   if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }
    
    func navigateToView(indexPath: IndexPath) {
        selectedIndex = indexPath
        if bankListDataModel?.bank_detail[(selectedIndex?.row)!].bank_id != nil {
            bankIdStr = bankListDataModel?.bank_detail[(selectedIndex?.row)!].bank_id ?? ""
        }
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is RedeemMoneyViewController {
                
            }
            else {
                if Reachability.isConnectedToNetwork() == true {
                    self.transferMoneyUpdate()
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
                }
            }
        }
    }
    
    /*
     This method is used to set the text for localization.
     @param -.
     @return -.
     */
    @objc func setText(){
        editButton.title = "Edit".localized()
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!]
    }
    
    /*
     This method is used to display the nodataView if no records are available.
     @param --.
     @return --.
     */
    func setupNoDataView() {
        self.navigationController?.isNavigationBarHidden = false
        let noDataViewFrame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        nodataView = NoDataView.init(frame: noDataViewFrame)
        nodataView?.isHidden = false
        self.view.addSubview(nodataView!)
    }
    
    
    /*
     This method is used to perform bank list service call.
     @param -.
     @return -.
     */
    func bankListUpdate() {
        self.followAdsRequestManager.bankDetailsListRequestManager = FollowAdsBankDetailsListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.bankDetailsListRequestManager?.lang_id = langId
        
        let bankListCompletion: BankListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.tableView.isHidden = true
            }
            else
            {
                if response != nil {
                    if response?.bank_detail.isEmpty == false {
                        if (response?.bank_detail.count)! > 0{
                            
                            DispatchQueue.main.async(execute: {
                                self.bankListDataModel = response!
                                print(self.bankListDataModel as Any)
                                self.bankDetailsListDatasource?.bankListDataModel = self.bankListDataModel
                                self.tableView.reloadData()
                                self.tableView.isHidden = false
                                self.nodataView?.isHidden = true
                                self.nodataView?.removeFromSuperview()
                                MBProgressHUD.hide(for: self.view, animated: true)
                            })
                        }
                        else {
                            if (self.bankListDataModel?.bank_detail.count)! < 0 {
                                DispatchQueue.main.async(execute: {
                                    self.tableView.isHidden = true
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                })
                            }
                            else {
                                self.tableView.isHidden = true
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }
                    }
                    else {
                        self.tableView.isHidden = true
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            }
        }
        FollowAdsServiceHandler.callBankListServiceCall(requestObject: self.followAdsRequestManager, bankListCompletion)
    }
    
    /*
     This method is used to perform transfer money service call.
     @param -.
     @return -.
     */
    func transferMoneyUpdate() {
        self.followAdsRequestManager.transferMoneyRequestManager = FollowAdsTransferMoneyRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = ""
        }
        self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = bankIdStr
        
        let transferMoneyCompletion: TransferMoneyCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.transferMoneyDataModel = response!
                        if response?.response_code == "015" {
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Money will be transferred to your bank account within 5 working days".localized())

                        }
                        print(self.transferMoneyDataModel as Any)
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                    
                    UserDefaults.standard.set(response?.wallet_amount, forKey: "WALLET_AMT")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callTransferMoneyServiceCall(requestObject: self.followAdsRequestManager, transferMoneyCompletion)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetUpTableDataSource(){
        bankDetailsListDatasource = BankDetailsListTableDatasource(bankListData: bankListDataModel)
        self.tableView.dataSource = bankDetailsListDatasource
        self.tableView.delegate = bankDetailsListDatasource
        bankDetailsListDatasource?.tableview = self.tableView
        bankDetailsListDatasource?.delegate = self
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addBtnPressed(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "bankDetailsViewId", sender: self)
    }
    
    @IBAction func editButtonAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankDetailsViewController") as! BankDetailsViewController
        vc.bankListDataModel = self.bankListDataModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
