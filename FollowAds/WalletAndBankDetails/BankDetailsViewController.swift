//
//  BankDetailsViewController.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 18/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import FirebaseDynamicLinks
import BRYXBanner
import AppsFlyerLib

class BankDetailsViewController: FollowAdsBaseViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, ShowAlertDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet weak var MoneyTransferInstructionLabel: UILabel!
    var BankDetailsDataSource: BankDetailsTableDataSource!
    var followAdsRequestManager = FollowAdsRequestManager()
    var navigationTitle = "Bank Details".localized()
    var instructionText = "Money will be transfered to your \n bank account within 3 working days \n after requesting".localized()
    var text = ["Account Name".localized(), "Account No.".localized(), "Bank Name".localized(), "IFSC Code".localized()]
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var emptyFieldAlert = "Please enter your bank details to proceed"
    var bankListDataModel: FollowAdsBankListDataModel?
    var editBankDetailsDataModel: FollowAdsEditBankDetailsDataModel?
    var bankId: String?
    var isAlerdayNav : Bool = false
    var banner = Banner()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SetUpTableDataSource()
        self.setText()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.navigationItem.title = navigationTitle
        self.MoneyTransferInstructionLabel.text = "Money will be transfered to your \n bank account within 3 working days \n after requesting.".localized()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        isAlerdayNav = false
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromNotifi = false
        isAlerdayNav = false
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    func showAlertForAccNo() {
        FollowAdsUtils.showAlertOnVC(targetVC: self, title: "Alert".localized(), message: "Acc No should not be exceed 16 digits.".localized())
    }
    
    func showAlertForIfsc() {
        FollowAdsUtils.showAlertOnVC(targetVC: self, title: "Alert".localized(), message: "IFSC code should not be exceed 11 digits.".localized())
    }
    
    // MARK: Localized Text
    
    @objc func setText(){
        cancelBtn.setTitle(cancelBtnTitle.localized(using: buttonTitles), for: UIControlState.normal)
        saveBtn.setTitle(saveBtnTitle.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    func hideKeyboardWhenTapped() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardView))
        tap.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboardView() {
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.endEditing(true)
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
   //     }
        
    }

    func SetUpTableDataSource(){
        BankDetailsDataSource = BankDetailsTableDataSource(texts: text)
        self.tableView.dataSource = BankDetailsDataSource
        self.tableView.delegate = BankDetailsDataSource
        BankDetailsDataSource.tableview = self.tableView
        BankDetailsDataSource.bankListDataModel = self.bankListDataModel
        self.tableView.tableFooterView = UIView()
        BankDetailsDataSource.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        self.BankDetailsDataSource.textfields?.resignFirstResponder()
        
        
        if self.BankDetailsDataSource.accountNameString == "" {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please enter Account Name".localized())
         }
        else if self.BankDetailsDataSource.accountNumberString == "" {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please enter Account No".localized())

        }
        else if self.BankDetailsDataSource.bankNameString == "" {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please enter Bank Name".localized())

        }
        else if self.BankDetailsDataSource.ifscCodeString  == "" {
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: "Please enter IFSC Code".localized())

        }
//
//
//
//        if self.BankDetailsDataSource.textfields?.text == "" {
//            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: emptyFieldAlert.localized())
//        }
        else {
            
            if let vcs = self.navigationController?.viewControllers {
                let previousVC = vcs[vcs.count - 2]
                if previousVC is BankDetailsListViewController {
                    // ... and so on
                    self.addLoadingView()
                    var arrayValue = [String : Array<[String:String]>]()
                    self.followAdsRequestManager.editBankDetailsRequestManager = FollowAdsEditBankDetailsRequestManager()
                    if bankListDataModel != nil {
                        bankId = bankListDataModel?.bank_detail[0].bank_id
                    }
                    if self.BankDetailsDataSource.accountNameString != nil {
                        if self.BankDetailsDataSource.bankNameString != nil {
                            if self.BankDetailsDataSource.accountNumberString != nil {
                                if self.BankDetailsDataSource.ifscCodeString != nil {
                                    arrayValue = ["bank_details" : [["detail_key":"Bank Id","detail_value":bankId ?? ""],["detail_key":"Account Name","detail_value":self.BankDetailsDataSource.accountNameString],["detail_key":"Bank Name","detail_value":self.BankDetailsDataSource.bankNameString],["detail_key":"Account No","detail_value":self.BankDetailsDataSource.accountNumberString],["detail_key":"Ifsc Code","detail_value":self.BankDetailsDataSource.ifscCodeString]]]
                                }
                            }
                        }
                    }
                    
                    if UserDefaults.standard.value(forKey: UserId) != nil {
                        let userIdText = String()
                        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                        self.followAdsRequestManager.editBankDetailsRequestManager?.user_id = userId
                    }
                    else {
                        self.followAdsRequestManager.editBankDetailsRequestManager?.user_id = ""
                    }
                    self.followAdsRequestManager.editBankDetailsRequestManager?.channel_code = "bank_details"
                    let langidText = String()

                    let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))

                    self.followAdsRequestManager.editBankDetailsRequestManager?.lang_id = langId

                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: arrayValue,
                        options: []) {
                        let theJSONText = String(data: theJSONData,
                                                 encoding: .ascii)
                        print("JSON string = \(theJSONText!)")
                        self.followAdsRequestManager.editBankDetailsRequestManager?.bank_details = theJSONText!
                    }
                    else {
                        self.followAdsRequestManager.editBankDetailsRequestManager?.bank_details = ""
                    }

                    let editbankCompletion: EditBankDetailsCompletionBlock = {(response, error) in
                        if let _ = error {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                        }
                        else
                        {
                            if response != nil {
                                DispatchQueue.main.async(execute: {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                   
//                                    self.banner = Banner(title: "Your Bank details has been saved".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                                    self.banner.dismissesOnTap = true
//                                    self.banner.show(duration: 3.0)
                                    
                                    self.view.makeToast("Your Bank details has been saved".localized(), duration: 3.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                                        self.navigationController?.popViewController(animated: true)

                                    })

                                })
                            }
                        }
                    }
                    FollowAdsServiceHandler.callEditBankDetailsServiceCall(requestObject: self.followAdsRequestManager, editbankCompletion)
                }
                else {
                    self.addLoadingView()
                    var arrayValue = [String : Array<[String:String]>]()
                    self.followAdsRequestManager.saveBankDetailsRequestManager = FollowAdsSaveBankDetailsRequestManager()
                    if self.BankDetailsDataSource.accountNameString != nil {
                        if self.BankDetailsDataSource.bankNameString != nil {
                            if self.BankDetailsDataSource.accountNumberString != nil {
                                if self.BankDetailsDataSource.ifscCodeString != nil {
                                    arrayValue = ["bank_details" : [["detail_key":"Account Name","detail_value":self.BankDetailsDataSource.accountNameString],["detail_key":"Bank Name","detail_value":self.BankDetailsDataSource.bankNameString],["detail_key":"Account No","detail_value":self.BankDetailsDataSource.accountNumberString],["detail_key":"Ifsc Code","detail_value":self.BankDetailsDataSource.ifscCodeString]]]
                                }
                            }
                        }
                    }
                    if UserDefaults.standard.value(forKey: UserId) != nil {
                        let userIdText = String()
                        let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
                        self.followAdsRequestManager.saveBankDetailsRequestManager?.user_id = userId
                    }
                    else {
                        self.followAdsRequestManager.saveBankDetailsRequestManager?.user_id = ""
                    }
                    self.followAdsRequestManager.saveBankDetailsRequestManager?.channel_code = "bank_details"
                    let langidText = String()
                    
                    let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
                    
                    self.followAdsRequestManager.saveBankDetailsRequestManager?.lang_id = langId


                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: arrayValue,
                        options: []) {
                        let theJSONText = String(data: theJSONData,
                                                 encoding: .ascii)
                        print("JSON string = \(theJSONText!)")
                        self.followAdsRequestManager.saveBankDetailsRequestManager?.bank_details = theJSONText!
                    }
                    else {
                        self.followAdsRequestManager.saveBankDetailsRequestManager?.bank_details = ""
                    }
                    
                    let savebankCompletion: SaveBankDetailsCompletionBlock = {(response, error) in
                        if let _ = error {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
                        }
                        else
                        {
                            if response != nil {
                                DispatchQueue.main.async(execute: {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    
//                                    self.banner = Banner(title: "Your Bank details has been saved".localized(), image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                                    self.banner.dismissesOnTap = true
//                                    self.banner.show(duration: 3.0)
                                    self.view.makeToast("Your Bank details has been saved".localized(), duration: 3.0, position: .center)

                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                    }
                    FollowAdsServiceHandler.callSaveBankDetailsServiceCall(requestObject: self.followAdsRequestManager, savebankCompletion)
                }
            }
        }
    }
    
}
