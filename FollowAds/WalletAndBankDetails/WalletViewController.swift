//
//  WalletViewController.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 18/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseAnalytics
import FirebaseDynamicLinks
import AppsFlyerLib

class WalletViewController: FollowAdsBaseViewController {
    
    @IBOutlet weak var RedeemMoneyBtn: UIButton!
    @IBOutlet weak var WalletMoneyLbl: UILabel!
    @IBOutlet weak var MoneyTransferInstructionLabel: UILabel!
    @IBOutlet weak var AmountInformationLabel: UILabel!
    var walletAmount = Int()
    var walletTitle = "Wallet".localized()
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var moneyTransferAlert = "Earn ₹1000 through FollowAds Coupons & Transfer to your Bank Account!"
    var bankListDataModel: FollowAdsBankListDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    private var loginWindow = LoginView()
    var transferMoneyDataModel: FollowAdsTransferMoneyDataModel?
    var isAlerdayNav : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        loginWindow = loadNiB()
        self.RedeemMoneyBtn.layer.cornerRadius = 10.0
        self.MoneyTransferInstructionLabel.text = "Money will be transfered to your \n bank account within 3 working days \n after requesting."
        self.AmountInformationLabel.text = "is in your wallet. You can transfer \n this to your account to get this money."
        self.setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isAlerdayNav = false

        if UserDefaults.standard.value(forKey: UserId) != nil {
            if UserDefaults.standard.value(forKey: "WALLET_AMT") != nil {
                let walletAmtText = String()
                var walletMoney: String = walletAmtText.passedString((UserDefaults.standard.object(forKey: "WALLET_AMT") as? String))
                if walletMoney == ""  {
                    walletMoney = "0"
                    walletAmount = Int(walletMoney) ?? 0
                }
                else {
                    // ADDED WALLET MONEY AS STATIC - FOR TEMPORARY
                 //   walletMoney = "0"
                    walletAmount = Int(walletMoney) ?? 0
                    
                }
                WalletMoneyLbl.text = "\u{20B9}" + walletMoney
            }
            else {
                WalletMoneyLbl.text = "\u{20B9}" + "0"
            }
        }
        self.navigationItem.title = walletTitle
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        tabBarController?.tabBar.isHidden = false
        if UserDefaults.standard.value(forKey: "User_Id") != nil {
            loginWindow.removeFromSuperview()
            loginWindow.isHidden = true
            
//            if let tabItems = self.tabBarController?.tabBar.items {
//                if UserDefaults.standard.value(forKey: UserId) != nil {
//
//                    if UserDefaults.standard.value(forKey: "Notification_Count") != nil {
//                        let notificationCountText = String()
//                        let notificationCount: String = notificationCountText.passedString((UserDefaults.standard.object(forKey: "Notification_Count") as? String))
//                        let notifyCountInt: Int = Int(notificationCount) ?? 0
//                        if notificationCount != "" {
//                            if notificationCount == "0" {
//                            } else {
//                                if notifyCountInt <= 0 {
//                                }
//                                else {
//                                    let tabItem = tabItems[1]
//                                    tabItem.badgeValue = notificationCount
//                                }
//                            }
//                        }
//                        else {
//                            if notifyCountInt <= 0 {
//                            }
//                            else {
//                                let tabItem = tabItems[1]
//                                tabItem.badgeValue = notificationCount
//                            }
//                        }
//                    }
//                }
//            }
        }
        else { 
            if let signInView = Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)?.first as? LoginView {
                self.view.addSubview(signInView)
                loginWindow = signInView
                signInView.isHidden = false
                signInView.frame = self.view.bounds
                signInView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                
           //     signInView.logoBtn.addTarget(self, action: #selector(logoBtnPressed), for: UIControlEvents.touchUpInside)
                signInView.signInBtn.addTarget(self, action: #selector(signInPressed), for: UIControlEvents.touchUpInside)
                signInView.signInBtn.setTitle("Sign In".localized(using: buttonTitles), for: .normal)
                signInView.commentsLabel.text = "To access all features of the app".localized()

            }
        }
        
        // Navigates to offerDetailView by tapping on push notification
        if isFromNotifi == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            vc.offerId = off_Notifi_Id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
            self.trackViewcontroller(name: "View_Wallet", screenClass: "WalletViewController")
        AppsFlyerTracker.shared().trackEvent("View_Wallet",
                                             withValues: [
                                                AFEventAdView: "View_Wallet",
                                                ]);
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
        if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> LoginView{
        let loginWindow = LoginView.instanceFromNib() as! LoginView
        return loginWindow
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
                
                if let pushId = aps["pushnotification_id"] as? Int {
                    vc.pushnotificationId = String(pushId)
                    vc.notifiId = "1"
                }
            }
            else {
                vc.offerId = "0"
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        loginWindow.removeFromSuperview()

        isFromNotifi = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        isAlerdayNav = false

    }
    
    // MARK: Localized Text
    
    @objc func setText(){
        AmountInformationLabel.text = WalletText.localized()
        MoneyTransferInstructionLabel.text = TransferText.localized()
        RedeemMoneyBtn.setTitle(RedeemMoney.localized(using: buttonTitles), for: UIControlState.normal)
    }
    
    /*
     This method is used to perform bank list service call.
     @param -.
     @return -.
     */
    func bankListUpdate() {
        self.followAdsRequestManager.bankDetailsListRequestManager = FollowAdsBankDetailsListRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.bankDetailsListRequestManager?.user_id = ""
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.bankDetailsListRequestManager?.lang_id = langId
        
        let bankListCompletion: BankListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            else
            {
                if response != nil {
                    self.bankListDataModel = response!
                    if response?.bank_detail.isEmpty == false {
                        
                        if (self.bankListDataModel?.bank_detail.count)! > 0 {
                            self.performSegue(withIdentifier: "bankListViewId", sender: self)
                        }
                        else {
                            self.performSegue(withIdentifier: "bankDetailsViewId", sender: self)
                        }
                    }
                    else {
                        self.performSegue(withIdentifier: "bankDetailsViewId", sender: self)
                    }
                }
            }
        }
        FollowAdsServiceHandler.callBankListServiceCall(requestObject: self.followAdsRequestManager, bankListCompletion)
    }
    
    /*
     This method is used to perform transfer money service call.
     @param -.
     @return -.
     */
    func transferMoneyUpdate() {
        self.followAdsRequestManager.transferMoneyRequestManager = FollowAdsTransferMoneyRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = ""
        }
        if UserDefaults.standard.value(forKey: "PaymentId") != nil {
            let paymentIdText = String()
            let paymentId: String = paymentIdText.passedString((UserDefaults.standard.object(forKey: "PaymentId") as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = paymentId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = ""
        }
        
        let transferMoneyCompletion: TransferMoneyCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.transferMoneyDataModel = response!
                        print(self.transferMoneyDataModel as Any)
                        if response?.response_code == "011" {
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Money will be transferred to your digital account within 5 working days".localized())
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let amount = response?.wallet_amount {
                            
                            // ADDED WALLET MONEY AS STATIC - FOR TEMPORARY
                        self.WalletMoneyLbl.text = "\u{20B9}" + "0"
                        }
                    })
                    
                    UserDefaults.standard.set(response?.wallet_amount, forKey: "WALLET_AMT")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callTransferMoneyServiceCall(requestObject: self.followAdsRequestManager, transferMoneyCompletion)
    }
    
    /*
     This method is used to perform transfer money service call.
     @param -.
     @return -.
     */
    func transferMoneyPaytmUpdate() {
        self.followAdsRequestManager.transferMoneyRequestManager = FollowAdsTransferMoneyRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = ""
        }
        if UserDefaults.standard.value(forKey: "PaymentIdPaytm") != nil {
            let paymentIdText = String()
            let paymentId: String = paymentIdText.passedString((UserDefaults.standard.object(forKey: "PaymentIdPaytm") as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = paymentId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = ""
        }
        
        let transferMoneyCompletion: TransferMoneyCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.transferMoneyDataModel = response!
                        print(self.transferMoneyDataModel as Any)
                        if response?.response_code == "011" {
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Money will be transferred to your digital account within 5 working days".localized())
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let amount = response?.wallet_amount {
                            // ADDED WALLET MONEY AS STATIC - FOR TEMPORARY
                            self.WalletMoneyLbl.text = "\u{20B9}" + "0"
                        }
                    })
                    
                    UserDefaults.standard.set(response?.wallet_amount, forKey: "WALLET_AMT")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callTransferMoneyServiceCall(requestObject: self.followAdsRequestManager, transferMoneyCompletion)
    }
    
    /*
     This method is used to perform transfer money service call.
     @param -.
     @return -.
     */
    func transferMoneyUPIUpdate() {
        self.followAdsRequestManager.transferMoneyRequestManager = FollowAdsTransferMoneyRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = userId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_id = ""
        }
        if UserDefaults.standard.value(forKey: "PaymentIdUPI") != nil {
            let paymentIdText = String()
            let paymentId: String = paymentIdText.passedString((UserDefaults.standard.object(forKey: "PaymentIdUPI") as? String))
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = paymentId
            
        }
        else {
            self.followAdsRequestManager.transferMoneyRequestManager?.user_payment_method_id = ""
        }
        
        let transferMoneyCompletion: TransferMoneyCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.transferMoneyDataModel = response!
                        print(self.transferMoneyDataModel as Any)
                        if response?.response_code == "011" {
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: "Money will be transferred to your digital account within 5 working days".localized())
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let amount = response?.wallet_amount {
                            // ADDED WALLET MONEY AS STATIC - FOR TEMPORARY
                            self.WalletMoneyLbl.text = "\u{20B9}" + "0"
                        }
                        
                    })
                    
                    UserDefaults.standard.set(response?.wallet_amount, forKey: "WALLET_AMT")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callTransferMoneyServiceCall(requestObject: self.followAdsRequestManager, transferMoneyCompletion)
    }
    
    @IBAction func RedeemMoneyButton(_ sender: UIButton) {
        Analytics.logEvent("Redeem_Cash", parameters: [
            "name": "Redeem money" as NSObject
            ])
        
        AppsFlyerTracker.shared().trackEvent("Redeem_Cash",
                                             withValues: [
                                                AFEventAdClick: "Redeem money",
                                                ]);
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .cancel) { _ in
            print("Cancel")
        }
        cancelActionButton.setValue(appThemeColor, forKey: "titleTextColor")
        actionSheetController.addAction(cancelActionButton)
        

        let bankDetailsActionButton = UIAlertAction(title: "Bank Account".localized(using: buttonTitles), style: .default)
        { _ in
            
            if self.walletAmount >= 1000 {
                if Reachability.isConnectedToNetwork() == true {
                    self.bankListUpdate()
                }
                else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
                }
            }
            else {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.moneyTransferAlert.localized())
            }
            
            print("Bank Details")
        }
        let bankImg = UIImage(named: "bankDetail")
        bankDetailsActionButton.setValue(bankImg?.withRenderingMode(.alwaysOriginal), forKey: "image")
        actionSheetController.addAction(bankDetailsActionButton)
        
        let gPayActionButton = UIAlertAction(title: "Google Pay".localized(using: buttonTitles), style: .default)
        { _ in
            if self.walletAmount >= 1000 {

                if UserDefaults.standard.value(forKey: "GPAYMobNum") != nil {
                    let phoneNumberText = String()
                    let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "GPAYMobNum") as? String))
                    let alertController = UIAlertController(title: "Alert".localized(),
                                                            message: "Proceed to redeem money using Google Pay wallet with this number".localized() + "\n\(phoneNumberCount)",
                        preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .default))
                    alertController.addAction(UIAlertAction(title: "Confirm".localized(using: buttonTitles), style: .default) { _ in
                        if Reachability.isConnectedToNetwork() == true {
                            self.transferMoneyUpdate()
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
                        }
                    })
                    
                    self.present(alertController, animated: true)
                    
                }
                else {
                    
                    if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GPayViewId") as! GPayViewController
                        let phoneNumberText = String()
                        let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
                        vc.mobNum = phoneNumberCount
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }

            }
            else {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.moneyTransferAlert.localized())
            }
        }
        let gPayImg = UIImage(named: "gPay")
        gPayActionButton.setValue(gPayImg?.withRenderingMode(.alwaysOriginal), forKey: "image")
        actionSheetController.addAction(gPayActionButton)
        
        let payTMActionButton = UIAlertAction(title: "Paytm".localized(using: buttonTitles), style: .default)
        { _ in
            if self.walletAmount >= 1000 {

                if UserDefaults.standard.value(forKey: "PaytmMobNum") != nil {
                    let phoneNumberText = String()
                    let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PaytmMobNum") as? String))
                    let alertController = UIAlertController(title: "Alert".localized(),
                                                            message: "Proceed to redeem money using Paytm wallet with this number".localized() + "\n\(phoneNumberCount)",
                        preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .default))
                    alertController.addAction(UIAlertAction(title: "Confirm".localized(using: buttonTitles), style: .default) { _ in
                        if Reachability.isConnectedToNetwork() == true {
                            self.transferMoneyPaytmUpdate()
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
                        }
                    })
                    
                    self.present(alertController, animated: true)
                }
                else {
                    if UserDefaults.standard.value(forKey: "PHONE_NUM") != nil {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayTMViewId") as! PayTMViewController
                        let phoneNumberText = String()
                        let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "PHONE_NUM") as? String))
                        vc.mobNum = phoneNumberCount
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                

            }
            else {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.moneyTransferAlert.localized())
            }
            
            print("PayTM")
        }
        let paytmImg = UIImage(named: "payTM")
        payTMActionButton.setValue(paytmImg?.withRenderingMode(.alwaysOriginal), forKey: "image")
        actionSheetController.addAction(payTMActionButton)
        
        let upiPinActionButton = UIAlertAction(title: "UPI Pin".localized(using: buttonTitles), style: .default)
        { _ in
            if self.walletAmount >= 1000 {

                
                if UserDefaults.standard.value(forKey: "UPIPinValue") != nil {
                    let phoneNumberText = String()
                    let phoneNumberCount: String = phoneNumberText.passedString((UserDefaults.standard.object(forKey: "UPIPinValue") as? String))
                    let alertController = UIAlertController(title: "Alert".localized(),
                                                            message: "Proceed to redeem money using this UPI Pin".localized() + "\n\(phoneNumberCount)",
                        preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .default))
                    alertController.addAction(UIAlertAction(title: "Confirm".localized(using: buttonTitles), style: .default) { _ in
                        if Reachability.isConnectedToNetwork() == true {
                            self.transferMoneyUPIUpdate()
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.reachabilityAlert.localized())
                        }
                    })
                    
                    self.present(alertController, animated: true)
                }
                else {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "UPIPinViewId") as! UPIPinViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            else {
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.moneyTransferAlert.localized())
            }
            print("UPI Pin")
        }
        let upiImg = UIImage(named: "UPI")
        upiPinActionButton.setValue(upiImg?.withRenderingMode(.alwaysOriginal), forKey: "image")
        actionSheetController.addAction(upiPinActionButton)
        actionSheetController.view.tintColor = UIColor.black
        self.present(actionSheetController, animated: true, completion: nil)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used for logo button action.
     @param --.
     @return --.
     */
    @objc func logoBtnPressed() {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    /*
     This method is used for sign in button action.
     @param --.
     @return --.
     */
    @objc func signInPressed() {
        print("Hello")
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: EnterFullNameViewId) as! EnterFullNameViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
}
