//
//  BankDetailsTableDataSource.swift
//  FollowAdsWalletAndBankDetails
//
//  Created by openwave on 18/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit

protocol ShowAlertDelegate {
    func showAlertForAccNo()
    func showAlertForIfsc()
}

class BankDetailsTableDataSource: NSObject, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
    
{
    var tableview: UITableView!
    var text : [String]!
    var accountNameString : String = ""
    var accountNumberString : String = ""
    var bankNameString : String = ""
    var ifscCodeString : String = ""
    var textfields : UITextField?
    var bankListDataModel: FollowAdsBankListDataModel?
    var editBankDetailsDataModel: FollowAdsEditBankDetailsDataModel?
    var delegate: ShowAlertDelegate?
    
    init(texts: [String]!) {
        self.text = texts
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return text.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview?.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BankDetailsTableViewCell
        cell.textField.placeholder = self.text[indexPath.row]
        cell.textField.tag = indexPath.row
        cell.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        cell.textField.delegate = self
        if bankListDataModel != nil {
            if cell.textField.tag == 0 {
                cell.textField.text = bankListDataModel?.bank_detail[0].acc_name
                accountNameString = cell.textField.text!
            }
            if cell.textField.tag == 1 {
                cell.textField.text = bankListDataModel?.bank_detail[0].acc_no
                accountNumberString = cell.textField.text!
            }
            if cell.textField.tag == 2 {
                cell.textField.text = bankListDataModel?.bank_detail[0].bank_name
                bankNameString = cell.textField.text!
            }
            if cell.textField.tag == 3 {
                cell.textField.text = bankListDataModel?.bank_detail[0].ifsc_code
                ifscCodeString = cell.textField.text!
            }
        }
        if cell.textField.tag == 0 {
            cell.textField.autocapitalizationType = .words
        }
        else if cell.textField.tag == 1 {
            cell.textField.keyboardType = UIKeyboardType.decimalPad
        }
        else if cell.textField.tag == 2 {
            cell.textField.autocapitalizationType = .words
        }
        else if cell.textField.tag == 3 {
//            cell.textField.autocapitalizationType = .allCharacters
            cell.textField.keyboardType = UIKeyboardType.namePhonePad
        }
        self.textfields = cell.textField
        return cell
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case 0:
            break
            
        case 1:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 16 {
                textField.resignFirstResponder()
//                if delegate != nil {
//                    delegate?.showAlertForAccNo()
//                }
                return false
            }
            else {
                return true

            }
        case 2:
            break
            
        case 3:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 11 {
                textField.resignFirstResponder()
//                if delegate != nil {
//                    delegate?.showAlertForIfsc()
//                }
                return false
            }
            else {
                return true

            }
        default:
            if ((textField.text?.characters.count)! + (string.characters.count - range.length)) > 4 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 0:
            accountNameString = textField.text!
            break
        case 1:
            accountNumberString = textField.text!
            break
        case 2:
            bankNameString = textField.text!
            break
        case 3:
            ifscCodeString = textField.text!
            break
        default:
            break
        }
        return true
    }
    
    @objc
    func textFieldDidChange(_ textField: UITextField){
        switch textField.tag {
        case 0:
            accountNameString = textField.text!
            break
        case 1:
            accountNumberString = textField.text!
            break
        case 2:
            bankNameString = textField.text!
            break
        case 3:
            ifscCodeString = textField.text!
            print("***********\(ifscCodeString)*********")
            break
        default:
            break
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            accountNameString = textField.text!
            break
        case 1:
            accountNumberString = textField.text!
            break
        case 2:
            bankNameString = textField.text!
            break
        case 3:
            ifscCodeString = textField.text!
            break
        default:
            break
        }
    }
    
}
