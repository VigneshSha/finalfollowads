//
//  NotificationsTableViewCell.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 23/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation   
import UIKit
import SDWebImageFLPlugin

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet var shopImageView: FLAnimatedImageView!
    @IBOutlet var shopTitleLabel: UILabel!
    @IBOutlet var shopDescLabel: UILabel!
    @IBOutlet weak var shopValidLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
