//
//  NotificationsTableDatasource.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 23/05/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit

protocol DidSelectDelegate {
    func navigateToNextView(indexPath: IndexPath)
}

class NotificationsTableDatasource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var cell: UITableViewCell?
    var shopImg : [String]?
    var shopTitle : [String]?
    var shopDesc : [String]?
    var delegate: DidSelectDelegate?
    var notificationListDataModel: FollowAdsNotificationListDataModel?
    
    init(shopImgModel: [String]?, shopTitleModel: [String]?, shopDescModel: [String]?){
        self.shopImg = shopImgModel
        self.shopTitle = shopTitleModel
        self.shopDesc = shopDescModel
        super.init()
    }
    
    // MARK: TableView Delegates and Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (notificationListDataModel?.information.count) != nil {
            return (notificationListDataModel?.information.count)!
        }
        else {
            return 1
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationsCell: NotificationsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationsCellId") as? NotificationsTableViewCell

        notificationsCell.shopImageView.layer.cornerRadius = 10
        notificationsCell.shopImageView.clipsToBounds = true
        
        notificationsCell.shopTitleLabel.text = notificationListDataModel?.information[indexPath.row].title
        notificationsCell.shopDescLabel.text = notificationListDataModel?.information[indexPath.row].ad_description
        DispatchQueue.main.async {
            if self.notificationListDataModel?.information[indexPath.row].read_unread != nil {
            if self.notificationListDataModel?.information[indexPath.row].read_unread == "1" {
                notificationsCell.shopTitleLabel.textColor = UIColor.gray
                notificationsCell.shopDescLabel.textColor = UIColor.gray
                notificationsCell.shopValidLabel.textColor = UIColor.gray
                
            }
            else {
                notificationsCell.shopTitleLabel.textColor = UIColor.black
                notificationsCell.shopDescLabel.textColor = UIColor.black
                notificationsCell.shopValidLabel.textColor = UIColor.black
            }
            }
        }
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if notificationListDataModel?.information[indexPath.row].advertisement_id != nil {
            
            if notificationListDataModel?.information[indexPath.row].valid_to != "" {
                let validDate = timeFormatter.date(from: notificationListDataModel?.information[indexPath.row].valid_to ?? "")
                
                let validDateFormat = DateFormatter()
                validDateFormat.dateFormat = "dd"
                let validDay = validDateFormat.string(from: validDate!)
                
                let validMonthFormat = DateFormatter()
                validMonthFormat.dateFormat = "MMM"
                if Localize.currentLanguage() == "ta-IN"{
                    validMonthFormat.locale = Locale(identifier: "ta")
                }
                else {
                    validMonthFormat.locale = Locale(identifier: "en")
                }
                let validMonth = validMonthFormat.string(from: validDate!)
                
                let validYearFormat = DateFormatter()
                validYearFormat.dateFormat = "yyyy"
                let validYear = validYearFormat.string(from: validDate!)
                
                if Localize.currentLanguage() == "ta-IN"{
                    notificationsCell.shopValidLabel.text = validMonth + " " + validDay + ", " + validYear + " வரை செல்லுபடியாகும்"
                }
                else {
                    notificationsCell.shopValidLabel.text = "Valid till ".localized() + validMonth + " " + validDay + ", " + validYear
                }
            }
            else {
                notificationsCell.shopValidLabel.text = ""
            }
         
            
        }
        else {
            notificationsCell.shopValidLabel.text = ""

        }
        
        let urlstring = self.notificationListDataModel?.information[indexPath.row].image_Url
        
        
        if urlstring != nil {
            if let encodedString  = urlstring?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                let imageExtensions = ["gif"]
                let pathExtention = URL(string: encodedString)?.pathExtension
                
                if imageExtensions.contains(pathExtention ?? "") {
                        if let url = URL(string: encodedString) {
                               notificationsCell?.shopImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "home_ads_placeholder"))
                         }
                        
                 }
                else {
                    notificationsCell?.shopImageView.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_ads_placeholder"))
                 }
            }
        }
        else {
            notificationsCell?.shopImageView.image = UIImage(named: "home_ads_placeholder")
        }
        return notificationsCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      //  return 110
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            if notificationListDataModel?.information[indexPath.row].advertisement_id != nil {
                if notificationListDataModel?.information[indexPath.row].advertisement_id != "" {
                    self.delegate?.navigateToNextView(indexPath: indexPath)
                }
            }
        }
    }
    
}

