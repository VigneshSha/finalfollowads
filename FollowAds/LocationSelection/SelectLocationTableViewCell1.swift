//
//  SelectLocationTableViewCell1.swift
//  FollowAdsCouponCode
//
//  Created by openwave on 21/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit

class SelectLocationTableViewCell1: UITableViewCell {
    
    @IBOutlet weak var LocationImageView: UIImageView!
    @IBOutlet weak var LocationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        LocationLabel.backgroundColor = .clear
             LocationLabel.layer.cornerRadius = 10
             LocationLabel.layer.borderWidth = 1
             LocationLabel.layer.borderColor = UIColor.gray.cgColor
             LocationLabel.layer.shadowColor =  UIColor.gray.cgColor
             LocationLabel.layer.shadowOpacity = 1.0
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

