//
//  DetectCurrentLocationViewController.swift
//  FollowAds
//
//  Created by Vijayarajan Suresh on 09/07/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import MapKit
import MBProgressHUD
import BRYXBanner
import FirebaseDynamicLinks
import AppsFlyerLib

struct State {
    let name: String
    let long: CLLocationDegrees
    let lat: CLLocationDegrees
}

class DetectCurrentLocationViewController: FollowAdsBaseViewController, CLLocationManagerDelegate, GMSMapViewDelegate,MakeFilterServiceCallDelegate {
    
    var filteredArray: [String] = []
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var filterBtn: UIButton!
    let marker = GMSMarker()
    fileprivate var customMarker : GMSMarker? = GMSMarker()
    var searchFilterDataModel: FollowAdsSearchFilterDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var nearbyBussinessDataModel : FollowAdsNearByBussinessDataModel?
    var IdValue: String?
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var businessId = String()
    let states = [
        State(name: "Loyola College", long: 80.2340, lat: 13.0634),
        State(name: "Dental Clinic", long: 80.2369, lat: 13.0647),
        State(name: "Ashife Briyani", long: 80.2362, lat: 13.0642),
        State(name: "Upper Crest", long: 80.2364, lat: 13.0644),
        State(name: "Medicals", long: 80.2366, lat: 13.0646),
        State(name: "CakeWalk", long: 80.2370, lat: 13.0650),
        State(name: "Apartments", long: 80.2375, lat: 13.0655),
        State(name: "Rajbhavan hotel", long: 80.2338, lat: 13.0632)
        // the other 51 states here...
    ]
    private let locationManager = CLLocationManager()
    
    @IBOutlet weak var currentLocIcon: UIImageView!
    var state_marker = GMSMarker()
    var location: CLLocation?
    var placemark: CLPlacemark?
    var city: String?
    var country: String?
    var countryShortName: String?
    var subAdministrativeAre : String?
    var address : String?
    var postalCode: String?
    var customLat: String?
    var customLng: String?
    let geocoder = CLGeocoder()
    var banner = Banner()
    var isAlerdayNav : Bool = false
    
    private var infoWindow = CustomMarkerView()
    fileprivate var googleMarker : GMSMarker? = GMSMarker()
    
    
    var userPinView: MKAnnotationView!
    var markerDict: [String: GMSMarker] = [:]
    var categoryListDataModel = FollowAdsCategoryListDataModel()
    var areaListDataModel = FollowAdsAreaListDataModel()
    var catVal: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBarButton()
        // Do any additional setup after loading the view.
        
        infoWindow = loadNiB() /// To register the nib file
        self.addLoadingView()
        
        DispatchQueue.main.async {
        self.filterBtn.isUserInteractionEnabled = false
        self.filterBtn.alpha = 0.5
        }
        self.catVal = ""
        self.state_marker.isDraggable = true

        self.nearbyBussinessDataModel  = FollowAdsNearByBussinessDataModel()
        self.nearbyBussinessDataModel?.business_list = [FollowAdsNearByBussinessDataModel.FollowAdsNearByBussinessList()]
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        locationManager.startUpdatingLocation()
        if Reachability.isConnectedToNetwork() == true {
            if sharedInstance.nearbyListDataModel.business_list.isEmpty == true {
                self.searchFilterUpdate(filterArray: filteredArray, limitVal: "1", cat_id: catVal ?? "")
            }
            else {
                
                DispatchQueue.main.async {
                    self.filterBtn.isUserInteractionEnabled = true
                    self.filterBtn.alpha = 1
                }
                
                self.allData()
            }
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        
     //   self.mapView.clear()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if sharedInstance.categoryListDataModel.categoryList.isEmpty == true {
            self.categoryList()
        }
        else {
            if sharedInstance.areaListDataModel.area.isEmpty == true {
                self.areaList()
            }
        }
        self.tabBarController?.tabBar.isHidden = true
        self.currentLocIcon.isHidden = true
        self.trackViewcontroller(name: "Explore_map", screenClass: "DetectCurrentLocationViewController")
        AppsFlyerTracker.shared().trackEvent("Explore_map",
                                             withValues: [
                                                AFEventAdView: "Explore_map",
                                                ]);

        let navLabel = UILabel()
        navLabel.frame = CGRect(x: 0, y: 0, width: 0, height: 44)

        let navTitle = NSMutableAttributedString(string: "FollowAds", attributes:[
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!])
        
//        navTitle.append(NSMutableAttributedString(string: "™", attributes:[
//            NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!,
//            NSAttributedStringKey.foregroundColor: UIColor.white]))
        
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        
//        self.navigationController?.navigationBar.titleTextAttributes =
//            [NSAttributedStringKey.foregroundColor: UIColor.white,
//             NSAttributedStringKey.font: UIFont(name: "FuturaDisplayBQ", size: 21)!]

        isAlerdayNav = false
        
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)

       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     //   self.mapView.clear()
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)

    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
        if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func allData() {
        
        self.locationManager.startUpdatingLocation()
        if sharedInstance.nearbyListDataModel.business_list.isEmpty == false {
           
            for state in (sharedInstance.nearbyListDataModel.business_list) {
                
                if let lattitude = state.business_lat, let longitude = state.business_lng {
                    if lattitude == "", longitude == "" {
                        
                    }
                    else {
                        let state_marker = GMSMarker()
                        
                        
                        state_marker.position = CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0)
                        
                        state_marker.icon = UIImage(named: "key_point_icon")
                        
                        state_marker.title = state.business_name ?? ""
                        state_marker.snippet = state.bus_area ?? ""
                        state_marker.accessibilityHint = state.advertisement_id ?? ""
                        state_marker.accessibilityLanguage = state.business_address_id ?? ""
                        state_marker.accessibilityLabel = state.advertisement_img ?? ""
                        state_marker.accessibilityValue = state.advertisement_caption ?? ""
                        
                        state_marker.map = self.mapView
                        
                        self.googleMarker = state_marker
                        if self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids == "" {
                        }
                        else {
                            self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0))
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)

                    }
                }
                
            }
    }
    }
   
    /*
     This method is used to perform search filter service call.
     @param -.
     @return -.
     */
    func searchFilterUpdate(filterArray: [String], limitVal: String, cat_id: String) {
        
       // self.nearbyBussinessDataModel?.business_list = []
        self.followAdsRequestManager.nearByBussinessRequestManager = FollowAdsNearBySearchRequestManager()
        
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = ""
        }
        
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lng = userLng
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.nearByBussinessRequestManager?.lang_id = langId
        if UserDefaults.standard.value(forKey: "Postal_Code") != nil{
            let postalCodeName = String()
            let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Postal_Code") as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.postal_code = postalCode
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.postal_code = ""
            
        }
        if UserDefaults.standard.value(forKey: City) != nil{
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: City) as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.area = city
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.area = ""

        }
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.limit = limitVal
        self.followAdsRequestManager.nearByBussinessRequestManager?.category_id = cat_id
        
//        if filterArray.count > 0 {
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: filterArray,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                print("JSON string = \(theJSONText!)")
                self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = theJSONText!
            }
            else {
                self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
            }
//        }
//        else{
//            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
//        }
//
        
        
//        if filterArray.count > 0 {
//            if let theJSONData = try? JSONSerialization.data(
//                withJSONObject: filterArray,
//                options: []) {
//                let theJSONText = String(data: theJSONData,
//                                         encoding: .ascii)
//                print("JSON string = \(theJSONText!)")
//                self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = theJSONText!
//            }
//            else {
//                self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
//            }
//        }
//        else{
//            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
//        }
//
        
        
        let nearbyBussinessCompletion: NearByBussinessCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
          //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    if (response?.business_list.count)! > 0 {
                        self.nearbyBussinessDataModel?.business_list = []
                         self.mapView.clear()
                        self.nearbyBussinessDataModel = response
                        for state in (self.nearbyBussinessDataModel?.business_list)! {
                            
                            if let lattitude = state.business_lat, let longitude = state.business_lng {
                                if lattitude == "", longitude == "" {
                                    
                                }
                                else {
                                    let state_marker = GMSMarker()
                                    
                                    
                                    state_marker.position = CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0)
                                    
                                    state_marker.icon = UIImage(named: "key_point_icon")
                                    
                                    state_marker.title = state.business_name ?? ""
                                    state_marker.snippet = state.bus_area ?? ""
                                    state_marker.accessibilityHint = state.advertisement_id ?? ""
                                    state_marker.accessibilityLanguage = state.business_address_id ?? ""
                                    state_marker.accessibilityLabel = state.advertisement_img ?? ""
                                    state_marker.accessibilityValue = state.advertisement_caption ?? ""
                                    
                                    state_marker.map = self.mapView
                                    self.infoWindow.removeFromSuperview()
                                    
                                    self.googleMarker = state_marker
                                    if self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids == "" {
                                    }
                                    else {
                                        if sharedInstance.switchLocEnabled == true && limitVal == "1" {
                                            UserDefaults.standard.set(lattitude, forKey: "updatedLat")
                                            UserDefaults.standard.set(longitude, forKey: "updatedLong")
                                            UserDefaults.standard.synchronize()

                                                 self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0))

                                        }
                                        //     self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0))
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                    else {
//                        self.banner = Banner(title: "Business are not available under this Category".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                        self.banner.dismissesOnTap = true
//                        self.banner.show(duration: 3.0)
                        self.view.makeToast("Business are not available under this Category".localized(), duration: 3.0, position: .center)

                    }
                    
                    if limitVal == "1" {
                        self.locationManager.startUpdatingLocation()
                        
                    }

                    if self.nearbyBussinessDataModel?.business_list != nil {
                        
                        
                        DispatchQueue.global(qos: .background).async {

                            if limitVal == "1" {
                                if self.catVal == "" {
                                    self.searchFilterUpdate(filterArray: self.filteredArray, limitVal: "2", cat_id: self.catVal ?? "")
                                 }
                                else {
                                    DispatchQueue.main.async {
                                        self.filterBtn.isUserInteractionEnabled = true
                                        self.filterBtn.alpha = 1
                                    }
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.filterBtn.isUserInteractionEnabled = true
                                    self.filterBtn.alpha = 1
                                }
                              
                                if self.nearbyBussinessDataModel != nil {
                                    sharedInstance.nearbyListDataModel = self.nearbyBussinessDataModel!
                                }
                            }
                            
                        }

                    //    MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callNearByBussinessServiceCall(requestObject: self.followAdsRequestManager, nearbyBussinessCompletion)
    }
    
    /*
     This method is used to perform search filter with particular service call.
     @param -.
     @return -.
     */
    func searchFilterLocationUpdate(filterArray: [String], limitVal: String, cat_id: String, postCode: String, area: String) {
        
        
       // self.nearbyBussinessDataModel?.business_list = []
        self.followAdsRequestManager.nearByBussinessRequestManager = FollowAdsNearBySearchRequestManager()
        
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = ""
        }
        
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lng = userLng
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.nearByBussinessRequestManager?.lang_id = langId
 
        self.followAdsRequestManager.nearByBussinessRequestManager?.postal_code = postCode
       
        self.followAdsRequestManager.nearByBussinessRequestManager?.area = area
            
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.limit = limitVal
        self.followAdsRequestManager.nearByBussinessRequestManager?.category_id = cat_id
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: filterArray,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = theJSONText!
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
        }
        
        
        let nearbyBussinessCompletion: NearByBussinessCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    
                    if (response?.business_list.count)! > 0 {
                        self.nearbyBussinessDataModel?.business_list = []
                        self.nearbyBussinessDataModel = response
                        self.mapView.clear()

                        for state in (self.nearbyBussinessDataModel?.business_list)! {
                            
                            if let lattitude = state.business_lat, let longitude = state.business_lng {
                                if lattitude == "", longitude == "" {
                                    
                                }
                                else {
                                    let state_marker = GMSMarker()
                                    
                                    
                                    state_marker.position = CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0)
                                    
                                    state_marker.icon = UIImage(named: "key_point_icon")
                                    
                                    state_marker.title = state.business_name ?? ""
                                    state_marker.snippet = state.bus_area ?? ""
                                    state_marker.accessibilityHint = state.advertisement_id ?? ""
                                    state_marker.accessibilityLanguage = state.business_address_id ?? ""
                                    state_marker.accessibilityLabel = state.advertisement_img ?? ""
                                    state_marker.accessibilityValue = state.advertisement_caption ?? ""
                                    
                                    state_marker.map = self.mapView
                                    self.infoWindow.removeFromSuperview()
                                    self.googleMarker = state_marker
                                    if self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids == "" {
                                    }
                                    else {
                                        self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0))
                                    }
                                    
                                    
                                }
                                
                            }
                            
                        }
                     }
                    else {
//                        self.banner = Banner(title: "Business are not available under this location".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                        self.banner.dismissesOnTap = true
//                        self.banner.show(duration: 3.0)
                        self.view.makeToast("Business are not available under this location".localized(), duration: 3.0, position: .center)

                    }
                  //  self.locationManager.startUpdatingLocation()
                    
//                    if self.nearbyBussinessDataModel?.business_list != nil {
//
//                        if self.nearbyBussinessDataModel?.business_list.isEmpty == true {
//                        self.banner = Banner(title: "Business are not available under this location".localized(), subtitle: "", image: UIImage(named: "Icon"), backgroundColor: UIColor(red:31.0/255.0, green:38.0/255.0, blue:42.0/255.0, alpha:0.80))
//                        self.banner.dismissesOnTap = true
//                        self.banner.show(duration: 3.0)
//                        }
//
//
//                        //    MBProgressHUD.hide(for: self.view, animated: true)
//                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
        FollowAdsServiceHandler.callNearByBussinessServiceCall(requestObject: self.followAdsRequestManager, nearbyBussinessCompletion)
    }
    /*
     This method is used to perform search filter service call.
     @param -.
     @return -.
     */
    func searchFilterChangeLocationUpdate(filterArray: [String]) {
        
        
        self.nearbyBussinessDataModel?.business_list = []
        self.followAdsRequestManager.nearByBussinessRequestManager = FollowAdsNearBySearchRequestManager()
        
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.user_id = ""
        }
        
        
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Custom_Lat") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Custom_Lng") as? String))
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.user_lng = userLng
        
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.nearByBussinessRequestManager?.lang_id = langId
        if UserDefaults.standard.value(forKey: "Post_Code") != nil{
            let postalCodeName = String()
            let postalCode: String = postalCodeName.passedString((UserDefaults.standard.object(forKey: "Post_Code") as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.postal_code = postalCode
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.postal_code = ""
            
        }
        if UserDefaults.standard.value(forKey: "CustomArea") != nil{
            let cityName = String()
            let city: String = cityName.passedString((UserDefaults.standard.object(forKey: "CustomArea") as? String))
            self.followAdsRequestManager.nearByBussinessRequestManager?.area = city
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.area = ""
            
        }
        
        self.followAdsRequestManager.nearByBussinessRequestManager?.limit = "2"
        self.followAdsRequestManager.nearByBussinessRequestManager?.category_id = ""
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: filterArray,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = theJSONText!
        }
        else {
            self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids = ""
        }
        
        let nearbyBussinessCompletion: NearByBussinessCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
         //       FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    
                    self.nearbyBussinessDataModel = response
                    
                    if self.nearbyBussinessDataModel?.business_list != nil {
                 //       DispatchQueue.main.async {
                            
                        for state in (self.nearbyBussinessDataModel?.business_list)! {
                            
                            if let lattitude = state.business_lat, let longitude = state.business_lng {
                                let state_marker = GMSMarker()
                                
                                
                                state_marker.position = CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0)
                                
                                state_marker.icon = UIImage(named: "key_point_icon")
                                
                                state_marker.title = state.business_name ?? ""
                                state_marker.snippet = state.bus_area ?? ""
                                state_marker.accessibilityHint = state.advertisement_id ?? ""
                                state_marker.accessibilityLanguage = state.business_address_id
                                state_marker.accessibilityLabel = state.advertisement_img ?? ""
                                state_marker.accessibilityValue = state.advertisement_caption ?? ""
                                
                                state_marker.map = self.mapView
                                self.infoWindow.removeFromSuperview()
                                self.googleMarker = state_marker
                                if self.followAdsRequestManager.nearByBussinessRequestManager?.ads_ids == "" {
                                }
                                else {
//                                    self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(lattitude) ?? 0.0, longitude: Double(longitude) ?? 0.0))
                                }
                            }
                            
                        }
                 //       }
                        //    MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                }
            }
        }
        FollowAdsServiceHandler.callNearByBussinessServiceCall(requestObject: self.followAdsRequestManager, nearbyBussinessCompletion)
    }
    
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.title == "Your Current Location" {
            return false
        }
        else {
            googleMarker = marker
            infoWindow.removeFromSuperview()
            infoWindow = loadNiB()
            
            guard let location = googleMarker?.position else {
                print("locationMarker is nil")
                return false
            }
            
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - sizeForOffset(view: infoWindow)
          //  infoWindow.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

            infoWindow.adImgVw.layer.cornerRadius = 10
            infoWindow.adImgVw.clipsToBounds = true
            infoWindow.adImgVw.contentMode = .scaleAspectFit
            infoWindow.businessName.text = marker.title
            infoWindow.adCaptionLabel.text = marker.snippet
            infoWindow.areaNameLabel.text = marker.accessibilityValue
         //   infoWindow.translatesAutoresizingMaskIntoConstraints = false
            
            let imgUrlStr = marker.accessibilityLabel
            if imgUrlStr != nil {
                if let encodedString  = imgUrlStr?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
                    infoWindow.adImgVw.sd_setImage(with: URL(string: encodedString), placeholderImage: UIImage(named: "home_banner_placeholder"))
                }
            }
            else {
                infoWindow.adImgVw.image = UIImage(named: "home_banner_placeholder")
            }
            
            infoWindow.btnMarkerAction.addTarget(self, action: #selector(shopDetails), for: .touchUpInside)
            
            infoWindow.btnMarkerAction.accessibilityHint = marker.accessibilityHint
            infoWindow.btnMarkerAction.accessibilityLanguage = marker.accessibilityLanguage
            self.view.addSubview(infoWindow)
            return false

        }
        
        
    }
    
    
    
    @objc func shopDetails(sender: UIButton) {
        print(sender.accessibilityHint ?? "")
        print(sender.accessibilityLanguage ?? "")

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        vc.offerId = sender.accessibilityHint ?? ""//(marker.userData as! String)
        vc.bus_addr_id = sender.accessibilityLanguage ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (googleMarker != nil){
            guard let location = googleMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - sizeForOffset(view: infoWindow)
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        
        print("Hello")
        
        
        //        if marker.title == "Your Current Location" {
        //        }
        //        else {
        //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetailId") as! ShopDetailViewController
        //            vc.businessIdValue = (marker.userData as! String)
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.subLocality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                    UserDefaults.standard.set(city, forKey: "CustomArea")
                    UserDefaults.standard.synchronize()
                    
                }
                if let subAdministrativeAre = placemark.name, !subAdministrativeAre.isEmpty {
                    self.subAdministrativeAre = subAdministrativeAre
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    self.country = country
                }
                if let postalCode = placemark.postalCode, !postalCode.isEmpty {
                    self.postalCode = postalCode
                    UserDefaults.standard.set(postalCode, forKey: "Post_Code")
                    UserDefaults.standard.synchronize()
                    print("Hey, its postal code, \(postalCode)")
                    
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    self.countryShortName = countryShortName
                }
                self.address = self.getAddressString(placemark: placemark)
                DispatchQueue.global(qos: .background).async {
             //       self.searchFilterChangeLocationUpdate(filterArray: self.filteredArray)
                    
                }
            }
        } else {
            // add some more check's if for some reason location manager is nil
        }
        
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func getAddressString(placemark: CLPlacemark) -> String? {
        var originAddress : String?
        if let addrList = placemark.addressDictionary?[FormattedAddressLines] as? [String]
        {
            originAddress =  addrList.joined(separator: ", ")
        }
       
        return originAddress
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            // 3
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
   
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
        print(position.target)
        let lat: String?
        let lng: String?
        lat = String(describing: position.target.latitude)
        lng = String(describing: position.target.longitude)
        UserDefaults.standard.set(lat, forKey: "User_Custom_Lat")
        UserDefaults.standard.set(lng, forKey: "User_Custom_Lng")
        UserDefaults.standard.synchronize()
        let latestLocation = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        
        // here check if no need to continue just return still in the same place
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        // if it location is nil or it has been moved
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need

                //***********************
                self.parsePlacemarks()

                //***************************
                if self.postalCode != nil {
                    if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {

                    }
                }
            })
        }
        else {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need
                
                //***********************
                self.parsePlacemarks()
                
                //***************************
                if self.postalCode != nil {
                    if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                        
                    }
                }
            })
        }
        
        
        

//        let coordinate = mapView.projection.coordinate(for: view.center)
//        print("latitude " + "\(coordinate.latitude)" + " longitude " + "\(coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        UIView.animate(withDuration: 3,
                       delay: 8,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                              longitude: location.coordinate.longitude,
                                                              zoom: 15
                        )
//                        self.customLat = String(describing: location.coordinate.latitude)
//                        self.customLng = String(describing: location.coordinate.longitude)
//
//                        UserDefaults.standard.set(self.customLat, forKey: "User_Custom_Lat")
//                        UserDefaults.standard.set(self.customLng, forKey: "User_Custom_Lng")
//                        UserDefaults.standard.synchronize()
                        
                        
            //            self.state_marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        
                        
                        
                 //       self.currentLocIcon.image = GMSMarker.markerImage(with: mapPointColor)
                 //       self.currentLocIcon.bringSubview(toFront: self.mapView)
                 //       self.state_marker.iconView?.addSubview(self.currentLocIcon)
                  
         //               self.state_marker.title = "Your Current Location"
                        
//                        self.state_marker.icon = UIImage(named: "LocationIcon")
//                        self.state_marker.icon = GMSMarker.markerImage(with: mapPointColor)

            //            self.state_marker.map = self.mapView
                        
                        self.mapView.isMyLocationEnabled = true
                        self.mapView.settings.myLocationButton = true
                        if self.mapView.isHidden {
                            self.mapView.isHidden = false
                            self.mapView.camera = camera
                        } else {
                            
                        // ************** This is to check whether if the user is in switched location or current location. If the user is in current location, animate to current location. Otherwise animate to changed location ****************
                            
                            if sharedInstance.switchLocEnabled == true {
                                let userLatText = String()
                                let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "updatedLat") as? String))
                                
                                let userLngText = String()
                                let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "updatedLong") as? String))
                                
                                let switchedLocCamera = GMSCameraPosition.camera(withLatitude: Double(userLat) ?? 0.0,
                                                                                 longitude: Double(userLng) ?? 0.0, zoom: 15)
                                    
                                    self.mapView.animate(to: switchedLocCamera)
                            }
                            else {
                            self.mapView.animate(to: camera)
                            }
                        }
        },
                       completion: { finished in
        })
        locationManager.stopUpdatingLocation()
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            let pin = mapView.view(for: annotation) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.image = UIImage(named: "LocationIcon")
            userPinView = pin
            return pin

        } else {
            // handle other annotations
        }
        return nil
    }
    
    func makeFilterServiceAfterAdd(withFilteredDate: [String]) {
        self.filteredArray = withFilteredDate
    }
    
    /*
     This method is used to perform category list service call.
     @param -.
     @return -.
     */
    func categoryList() {
        self.followAdsRequestManager.categoryListRequestManager = FollowAdsCategoryListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.categoryListRequestManager?.lang_id = ""
        }
        
        let categoryListCompletion: CategoryListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //     FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                self.areaList()
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.categoryListDataModel = response!
                        print(self.categoryListDataModel)
                        sharedInstance.categoryListDataModel = self.categoryListDataModel
                        self.areaList()
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callCategoryListServiceCall(requestObject: self.followAdsRequestManager, categoryListCompletion)
    }
    
    /*
     This method is used to perform area list service call.
     @param -.
     @return -.
     */
    func areaList() {
        self.followAdsRequestManager.areaListRequestManager = FollowAdsAreaListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.areaListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.areaListRequestManager?.lang_id = ""
        }
        
        let areaListCompletion: AreaListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                //Navigate to Home View
                let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                nextViewController.modalPresentationStyle = .fullScreen
                self.present(nextViewController, animated: true, completion: nil)
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.areaListDataModel = response!
                        print(self.areaListDataModel)
                        sharedInstance.areaListDataModel = self.areaListDataModel
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callAreaListServiceCall(requestObject: self.followAdsRequestManager, areaListCompletion)
    }
    
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterBtnAction(_ sender: UIButton) {
      /*  let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterTableViewController") as! FilterTableViewController
        vc.filterDelegate = self
        vc.searchFilterDataModel = self.searchFilterDataModel
        self.navigationController?.pushViewController(vc, animated: true) */
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .cancel) { _ in
            print("Cancel")
        }
        cancelActionButton.setValue(appThemeColor, forKey: "titleTextColor")
        actionSheetController.addAction(cancelActionButton)
        
        let allActionButton = UIAlertAction(title: "All".localized(using: buttonTitles), style: .default)
        { _ in
            self.addLoadingView()
            self.searchFilterUpdate(filterArray: self.filteredArray, limitVal: "2", cat_id: "")
            print("All")
        }
        actionSheetController.addAction(allActionButton)
        
        let categoriesActionButton = UIAlertAction(title: "Categories".localized(using: buttonTitles), style: .default)
        { _ in
            
            //Create the AlertController and add Its action like button in Actionsheet
            let categoriesActionSheetContr: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .cancel) { _ in
                print("Cancel")
            }
            cancelActionButton.setValue(appThemeColor, forKey: "titleTextColor")
            categoriesActionSheetContr.addAction(cancelActionButton)
            
            if sharedInstance.categoryListDataModel.categoryList.isEmpty == false {
            for name in sharedInstance.categoryListDataModel.categoryList {
            let catNameActionButton = UIAlertAction(title: name.category_name, style: .default)
            { _ in
//                self.mapView.clear()
                self.addLoadingView()
                self.catVal = name.category_id
                self.searchFilterUpdate(filterArray: self.filteredArray, limitVal: "1", cat_id: self.catVal ?? "")

                print("catName")
            }
            categoriesActionSheetContr.addAction(catNameActionButton)
            }
            }
            
            categoriesActionSheetContr.view.tintColor = UIColor.black
            self.present(categoriesActionSheetContr, animated: true, completion: nil)
            
            
            print("Categories")
        }
        actionSheetController.addAction(categoriesActionButton)
        
        let locationActionButton = UIAlertAction(title: "Location".localized(using: buttonTitles), style: .default)
        { _ in
            
            //Create the AlertController and fetch location area list in Actionsheet
            let locationActionSheetContr: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .cancel) { _ in
                print("Cancel")
            }
            cancelActionButton.setValue(appThemeColor, forKey: "titleTextColor")
            locationActionSheetContr.addAction(cancelActionButton)
            
            if sharedInstance.areaListDataModel.area.isEmpty == false {
                for name in sharedInstance.areaListDataModel.area {
                    let locationNameActionButton = UIAlertAction(title: name.area_lang_name, style: .default)
                    { _ in
                      //  self.mapView.clear()
                        self.addLoadingView()
                        
                        self.searchFilterLocationUpdate(filterArray: self.filteredArray, limitVal: "1", cat_id: "", postCode: name.PostalCode ?? "", area: name.area_name ?? "")

                        print("areaName")
                    }
                    locationActionSheetContr.addAction(locationNameActionButton)
                }
            }
            
            locationActionSheetContr.view.tintColor = UIColor.black
            self.present(locationActionSheetContr, animated: true, completion: nil)
            
            print("Location")
        }
        actionSheetController.addAction(locationActionButton)
        actionSheetController.view.tintColor = UIColor.black
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    
    
    ///Changes by kr
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> CustomMarkerView{
        let infoWindow = CustomMarkerView.instanceFromNib() as! CustomMarkerView
        return infoWindow
    }
    
    
    //Needed to create the custom info window (this is optional)
    func sizeForOffset(view: UIView) -> CGFloat {
        return 110
    }
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        
    }
    
    
}


