//
//  SelectLocationTableDataSource.swift
//  FollowAdsCouponCode
//
//  Created by openwave on 21/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import UIKit
protocol LocationPassing {
    func LocationPassing(indexPath: IndexPath)
}

class SelectLocationTableDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView : UITableView!
    var text : String!
    var imgArr: [String]?
    var locationArr: [String]?
    var Currentlocation: [String]?
    var delegate: LocationPassing!
    
    init(imgArrList: [String]?, locationArrList: [String]?,Currentloc: [String]?) {
        self.imgArr = imgArrList
        self.locationArr = locationArrList
        self.Currentlocation = Currentloc
        super.init()
    }
    
    // MARK :- TABLEVIEW DATASOURCE AND DELEGATE METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (imgArr?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SelectLocationTableViewCell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SelectLocationTableViewCell1
//         let cell2: SelectLocationTableViewCell1 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! SelectLocationTableViewCell1
        cell.LocationLabel.text = locationArr?[indexPath.row]
        cell.LocationImageView.image = UIImage(named: imgArr![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if delegate != nil {
                delegate?.LocationPassing(indexPath: indexPath)
            }
        }
        else if indexPath.row == 1 {
            if delegate != nil {
                delegate.LocationPassing(indexPath: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

