//
//  FilterTableViewDataSource.swift
//  FollowAds
//
//  Created by openwave on 26/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class FilterTableViewDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var data: [String?]
    var searchFilterDataModel: FollowAdsSearchFilterDataModel?
    var cell: UITableViewCell?
    var selectAllBool: Bool?
    var savedArray = [String]()
    
    init(data: [String?], searchFilterData: FollowAdsSearchFilterDataModel?) {
        self.data = data
        self.searchFilterDataModel = searchFilterData
        super.init()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchFilterDataModel?.filter_list.count != nil {
            return (searchFilterDataModel?.filter_list.count)!
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = searchFilterDataModel?.filter_list[indexPath.row].ads_caption
        if sharedInstance.filterDataArrray.count >  0 {
            let adId = searchFilterDataModel?.filter_list[indexPath.row].ads_id
            if sharedInstance.filterDataArrray.contains(adId ?? "") {
                cell?.accessoryType = .checkmark
            }
            else {
                cell?.accessoryType = .none
            }
        }
        else {
            cell?.accessoryType = .none
            
        }
        self.cell = cell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let adId = searchFilterDataModel?.filter_list[indexPath.row].ads_id
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                if sharedInstance.filterDataArrray.contains(adId!) {
                    let indexOfAdId = sharedInstance.filterDataArrray.lastIndex(of: adId!)
                    sharedInstance.filterDataArrray.remove(at: indexOfAdId!)
                }
                else {
                    sharedInstance.filterDataArrray.append(adId!)
                }
            }
            else{
                cell.accessoryType = .checkmark
                sharedInstance.filterDataArrray.append(adId!)
            }
        }
    }
}

