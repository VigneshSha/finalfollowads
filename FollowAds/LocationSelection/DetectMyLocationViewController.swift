//
//  DetectMyLocationViewController.swift
//  FollowAds
//
//  Created by openwave on 23/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import UIKit
import MapKit
let lattitude: CLLocationDegrees = 13.067439
let longitude: CLLocationDegrees = 80.237617
let latDelta: CLLocationDegrees = 0.01
let lonDelta: CLLocationDegrees = 0.01
let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(lattitude, longitude)
let span: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
let region: MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
class DetectMyLocationViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var UseLocationButton: UIButton!
    var circle: MKCircle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.updateViewDidLoad()
    }
    
    func updateViewDidLoad() {
        self.navigationItem.title = "Nungambakkam, Chennai"
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        UseLocationButton.layer.cornerRadius = 5.0
        UseLocationButton.backgroundColor = UIColor(red: 183.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
        let annotation: MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "India"
        annotation.subtitle = "Chennai"
        self.mapView.addAnnotation(annotation)
        loadOverlayForRegionWithLatitude(latitude: 13.067439, longitude: 80.237617)
        self.mapView.setRegion(region, animated: true)
        self.mapView.delegate = self
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        let reuseId = "cell"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil{
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        } else {
            anView?.annotation = annotation
        }
        anView?.canShowCallout = true
        mapView.delegate = self
        return anView
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        manager.startUpdatingLocation()
        manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
    }
    
    func loadOverlayForRegionWithLatitude(latitude: Double, longitude: Double) {
        let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let circle = MKCircle(center: coordinates, radius: 150)
        self.mapView.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)), animated: true)
        self.mapView.add(circle)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay.isKind(of: MKCircle.self){
            let circle = MKCircleRenderer(overlay: overlay)
            circle.fillColor = UIColor.blue.withAlphaComponent(0.1)
            circle.strokeColor = UIColor.blue
            circle.lineWidth = 1
            return circle
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    @IBAction func BackBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func FilterBtnAction(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterTableViewController") as! FilterTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func UseLocationBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
     }
    
}
