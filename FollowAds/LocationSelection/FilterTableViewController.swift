//
//  FilterTableViewController.swift
//  FollowAds
//
//  Created by openwave on 26/06/18.
//  Copyright © 2018 com.vijai.appname. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import AppsFlyerLib

protocol MakeFilterServiceCallDelegate {
    func makeFilterServiceAfterAdd(withFilteredDate : [String])
}

class FilterTableViewController: FollowAdsBaseViewController {
    
    var FilterTableDataSource: FilterTableViewDataSource!
    var searchFilterDataModel: FollowAdsSearchFilterDataModel?
    var followAdsRequestManager = FollowAdsRequestManager()
    var selectedIndex: IndexPath?
    var adIdStr: String?
    var arrayValue = [String]()
    var filterDelegate : MakeFilterServiceCallDelegate?
    var selectCellBool: Bool = true
    var navigationTitle = "Filter"
    @IBOutlet weak var tableView: UITableView!
    var data = ["Bags", "Caps", "Chappels","Caskets","Desks","Shirt for men","Shirt for women", "Shoe for men", "Shoe for women", "Mobiles", "Vadias", "Shoes foe kids", "Shirts for kids"]
    var alertTitle = "Alert"
    var reachabilityAlert = "Please check your Internet Connection"
    var errorMessage = "Sorry, something went wrong"
    var locationAlert = "Please allow access to your location to proceed further"
    var indexSave = [String]()
    var selectedArray = [String]()
    @IBOutlet weak var selectAllBtn: UIBarButtonItem!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableViewDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpNavigationBarButton()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
        self.navigationItem.title = navigationTitle.localized()
       
        if Reachability.isConnectedToNetwork() == true {
            self.searchFilterUpdate()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)

    }
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the LCLLanguageChangeNotification on viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)

    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        selectAllBtn.title = "Select All".localized()
        doneBtn.title = "Done".localized()
    }
    
    func setupTableViewDataSource() {
        FilterTableDataSource = FilterTableViewDataSource(data: data, searchFilterData: searchFilterDataModel)
        self.tableView.dataSource = FilterTableDataSource
        self.tableView.delegate = FilterTableDataSource
        FilterTableDataSource?.tableView = self.tableView
        self.tableView.tableFooterView =  UIView()
    }
    
    /*
     This method is used to perform search filter service call.
     @param -.
     @return -.
     */
    func searchFilterUpdate() {
        self.followAdsRequestManager.searchFiltersRequestManager = FollowAdsSearchFiltersRequestManager()
        if UserDefaults.standard.value(forKey: UserId) != nil {
            let userIdText = String()
            let userId: String = userIdText.passedString((UserDefaults.standard.object(forKey: UserId) as? String))
            self.followAdsRequestManager.searchFiltersRequestManager?.user_id = userId
        }
        else {
            self.followAdsRequestManager.searchFiltersRequestManager?.user_id = ""
        }
        let userLatText = String()
        let userLat: String = userLatText.passedString((UserDefaults.standard.object(forKey: "User_Lat") as? String))
        self.followAdsRequestManager.searchFiltersRequestManager?.user_lat = userLat
        
        let userLngText = String()
        let userLng: String = userLngText.passedString((UserDefaults.standard.object(forKey: "User_Lng") as? String))
        self.followAdsRequestManager.searchFiltersRequestManager?.user_lng = userLng
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: arrayValue,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            self.followAdsRequestManager.searchFiltersRequestManager?.ads_ids = theJSONText!
        }
        else {
            self.followAdsRequestManager.searchFiltersRequestManager?.ads_ids = ""
            
        }
        let langidText = String()
        let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
        self.followAdsRequestManager.searchFiltersRequestManager?.lang_id = langId
        
        
        let searchFilterCompletion: SearchFilterCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                FollowAdsUtils.showAlertOnVC(targetVC: self, title: self.alertTitle.localized(), message: self.errorMessage.localized())
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.searchFilterDataModel = response!
                        print(self.searchFilterDataModel as Any)
                        self.FilterTableDataSource.searchFilterDataModel = self.searchFilterDataModel
                        self.tableView.reloadData()
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callSearchFilterServiceCall(requestObject: self.followAdsRequestManager, searchFilterCompletion)
    }
    
    @IBAction func selectAllBtnAction(_ sender: UIBarButtonItem) {
        sharedInstance.filterDataArrray = []
        if selectCellBool == true {
            selectCellBool = false
            for add_Ids in (searchFilterDataModel?.filter_list.enumerated())! {
                sharedInstance.filterDataArrray.append(add_Ids.element.ads_id!)
            }
        }
        else {
            selectCellBool = true
        }
        self.FilterTableDataSource.selectAllBool = self.selectCellBool
        self.tableView.reloadData()
    }
    
    @IBAction func doneBtnAction(_ sender: UIBarButtonItem) {
        if let delegate = filterDelegate {
            delegate.makeFilterServiceAfterAdd(withFilteredDate: sharedInstance.filterDataArrray)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
