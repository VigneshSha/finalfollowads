//
//  SelectLocationViewController.swift
//  FollowAdsCouponCode
//
//  Created by openwave on 21/05/18.
//  Copyright © 2018 openwave. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import CoreLocation
import FirebaseDynamicLinks
import MBProgressHUD
import AppsFlyerLib
import MaterialComponents.MaterialActivityIndicator

class SelectLocationViewController: FollowAdsBaseViewController, UITextFieldDelegate, LocationPassing, UISearchBarDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableview2: UITableView!
    @IBOutlet weak var tableView: UITableView!
    var filteredData: [String] = []
    var vSpinner : UIView?
    var SelectLocationDataSource: SelectLocationTableDataSource!
    var imagesArray: [String]?
    var locationArray: [String]?
    var CurrentlocationArray = [""]
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var indicator = UIActivityIndicatorView()
    var searchBar: UISearchBar?
    var tableDataSource: GMSAutocompleteTableDataSource?
 //   let GoogleMapsAPIServerKey = "AIzaSyAw1EUIPb7qT-yvkzjEspIEiPStYpY4GlU"
    var navigationTitle = "Location".localized()
    var locationdata:String = ""
  
    var locationManager = CLLocationManager()
    var location: CLLocation?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var city: String?
    var country: String?
    var countryShortName: String?
    var subAdministrativeAre : String?
    var address : String?
    let GoogleMapsAPIServerKey = APIKey
    var loc_lat: String?
    var loc_lng: String?
    var isAlerdayNav : Bool = false
    var areaListDataModel = FollowAdsAreaListDataModel()
    var followAdsRequestManager = FollowAdsRequestManager()
    var latitude = String()
    var longitude = String()
    var postalCode: String?
    @IBOutlet weak var currentlocationbutton: UIButton!
    let activityIndicator = MDCActivityIndicator()
    
     let dispatchQueue = DispatchQueue(label: "Example Queue")
    override func viewDidLoad() {
        super.viewDidLoad()
        currentlocationbutton.layer.shadowOffset = CGSize(width: 0, height: 1)
        currentlocationbutton.layer.shadowColor = UIColor.lightGray.cgColor
        currentlocationbutton.layer.shadowOpacity = 1
        currentlocationbutton.layer.shadowRadius = 5
        currentlocationbutton.layer.masksToBounds = false
       // currentlocationbutton.layer.borderColor = UIColor.black.cgColor
        activityIndicator.sizeToFit()
             view.addSubview(activityIndicator)
          self.viewDidLoadUpdate()
             searchBar?.delegate =  self
             tableview2.dataSource =  self
             tableview2.delegate =  self
        if Reachability.isConnectedToNetwork() == true {
            self.areaList()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
         // tableview2.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

     
        
       let searchbar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 240, height: 20))
       searchbar.placeholder = "Search Location"
       let leftNavBarButton = UIBarButtonItem(customView:searchbar)
       self.navigationItem.rightBarButtonItem = leftNavBarButton
        self.viewDidLoadUpdate()
        self.setUpNavigationBarButton()
        self.trackViewcontroller(name: "View_Location", screenClass: "SelectLocationViewController")
        AppsFlyerTracker.shared().trackEvent("View_Location",
                                             withValues: [
                                                AFEventAdView: "View_Location",
                                                ]);
        isAlerdayNav = false
        
        if Reachability.isConnectedToNetwork() == true {
            self.areaList()
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: reachabilityAlert.localized())
        }
        if filteredData.isEmpty  {
           
                 self.showSpinner(onView: self.view)
          
                 tableview2.separatorStyle = .none

                 dispatchQueue.async {
                     Thread.sleep(forTimeInterval: 3)

                     OperationQueue.main.addOperation() {
                         if  self.filteredData.isEmpty{
                             MBProgressHUD.hide(for: self.view, animated: true)
                                                               self.tableview2.reloadData()
                         } else {
                             self.removeSpinner()

                                                    self.tableview2.separatorStyle = .singleLine
                                                    self.tableview2.reloadData()
                         }
                       

                         
                     }
                 }
             }
        // Local Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToOfferDetails(notification:)), name: NSNotification.Name( "Localnotification"), object: nil)
        
        // Push Notification addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(xmldataNotification), name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callDeepLinkService), name: NSNotification.Name(rawValue: kDeepLink), object: nil)
//  tableview2.dataSource =  self
//  tableview2.delegate =  self
  searchbar.delegate =  self
//  CurrentlocationArray =  filteredData
    }
    
    /*
     This method is used to update the viewDidLoad method.
     @param --.
     @return --.
     */
    
    func viewDidLoadUpdate() {
         //self.areaList()
        imagesArray = ["LocationIcon"]
        locationArray = ["Use Current Location".localized()]
        SetUpTableDataSource()
    }
    @IBAction func selectsearchbar(_ sender: Any) {
         
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        filteredData = searchText.isEmpty ? CurrentlocationArray : CurrentlocationArray.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })

        tableview2.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isFromNotifi = false
        isAlerdayNav = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Localnotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeepLink), object: nil)
        
    }
    
    
    /*
     This method is used to perform area list service call.
     @param -.
     @return -.
     */
    func areaList() {
        self.followAdsRequestManager.areaListRequestManager = FollowAdsAreaListRequestManager()
        if UserDefaults.standard.value(forKey: "Language_Id") != nil {
            let langidText = String()
            let langId: String = langidText.passedString((UserDefaults.standard.object(forKey: "Language_Id") as? String))
            self.followAdsRequestManager.areaListRequestManager?.lang_id = langId
        }
        else {
            self.followAdsRequestManager.areaListRequestManager?.lang_id = ""
        }
        
        let areaListCompletion: AreaListCompletionBlock = {(response, error) in
            if let _ = error {
                MBProgressHUD.hide(for: self.view, animated: true)
                //      FollowAdsUtils.showAlertOnVC(targetVC: self, title: alertTitle.localized(), message: errorMessage.localized())
                //Navigate to Home View
            }
            else
            {
                if response != nil {
                    DispatchQueue.main.async(execute: {
                        self.areaListDataModel = response!
                        print("FGFSD",self.areaListDataModel)
                        for name in self.areaListDataModel.area {
                            self.CurrentlocationArray.append(name.area_lang_name!)
                            
                        }
                        self.filteredData =  self.CurrentlocationArray
                       // print("feeer",self.filteredData)
                    //    self.tableview2.reloadData()
                        sharedInstance.areaListDataModel = self.areaListDataModel
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
        FollowAdsServiceHandler.callAreaListServiceCall(requestObject: self.followAdsRequestManager, areaListCompletion)
    }
    
    @objc func callDeepLinkService(notification:Notification) {
        
    //    if isAlerdayNav == false {
            isAlerdayNav = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
            let apss = notification.userInfo?["dynamicLink"] as? DynamicLink
            if let aps = apss {
                if let ad_id = aps.url  {
                    let off_Id = ad_id.valueOf("offerid")
                    vc.offerId = off_Id!
                }
                else {
                    vc.offerId = "0"
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
  //      }
        
    }
    
    
    
    /*
     This method is used to navigate the user to offer detail view.
     @param - .
     @return -.
     */
    @objc func navigateToOfferDetails(notification: NSNotification) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        if let id = notification.userInfo?["orderId"] as? String {
            vc.offerId = id
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     This method is used to get the ad_id from notification response and parse it to offerDetailViewController.
     @param - notification.
     @return -.
     */
    @objc func xmldataNotification(notification:Notification) -> Void
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailViewId") as! OfferDetailViewController
        let apss = notification.userInfo?["aps"] as? [AnyHashable : Any]
        if let aps = apss {
            if let ad_id = aps["advertisement_id"]  {
                let off_Id = ad_id as! Int
                vc.offerId = String(off_Id)
            }
            else {
                vc.offerId = "0"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
     This method is used to setup the tableview datasource.
     @param --.
     @return --.
     */
    func SetUpTableDataSource() {
        print("eteet",CurrentlocationArray)
        SelectLocationDataSource = SelectLocationTableDataSource(imgArrList: imagesArray, locationArrList: locationArray,Currentloc: CurrentlocationArray)
        self.tableView.dataSource = SelectLocationDataSource
        self.tableView.delegate = SelectLocationDataSource
        SelectLocationDataSource.tableView = self.tableView
        SelectLocationDataSource.delegate = self
        self.tableView.tableFooterView = UIView()
    }
    
    /*
     This method is used to setup navigation bar button item.
     @param -.
     @return -.
     */
    func setUpNavigationBarButton()
    {
        self.navigationItem.title = navigationTitle
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: (UIFont.mediumSystemFont(ofSize: 17))]
    }
    
    /*
     This method is used to perform back button action.
     @param --.
     @return --.
     */
    @IBAction func BackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addLoadingView() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = ""
    }
    
    @IBAction func Usecurrentlocationaction(_ sender: Any) {
        print("xlfmnmmxx")
        self.location = nil
                sharedInstance.switchLocEnabled = false
                self.determineCurrentLocation()
    }
    func LocationPassing(indexPath: IndexPath) {
        if indexPath.row == 0 {
        

            // To make the activity indicator appear:
    
//            self.location = nil
//            sharedInstance.switchLocEnabled = false
//            self.determineCurrentLocation()
           
         //   self.performSegue(withIdentifier: "CurrentLocationIdentifier", sender: self)
        }
        else {
            
            //Create the AlertController and fetch location area list in Actionsheet
            let locationActionSheetContr: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel".localized(using: buttonTitles), style: .cancel) { _ in
                print("Cancel")
            }
            cancelActionButton.setValue(appThemeColor, forKey: "titleTextColor")
            locationActionSheetContr.addAction(cancelActionButton)
            
            if self.areaListDataModel.area.isEmpty == false {
              
                for name in  self.areaListDataModel.area {
                    
                    let locationNameActionButton = UIAlertAction(title: name.area_lang_name, style: .default)
                        
                    { _ in
                        //  self.mapView.clear()
                        self.addLoadingView()
                        let geocoder = CLGeocoder()
                        print("sfwrwr",name.PostalCode)
                        geocoder.geocodeAddressString(name.PostalCode ?? "") {
                            placemarks, error in
                            print("sfsfs",placemarks)
                            let placemark = placemarks?.first
                            if let lat = placemark?.location?.coordinate.latitude {
                                if let lon = placemark?.location?.coordinate.longitude {
                            self.latitude = String(describing: lat)
                            self.longitude = String(describing: lon)
                            
                            UserDefaults.standard.set(self.latitude, forKey: "User_Lat")
                            UserDefaults.standard.set(self.longitude, forKey: "User_Lng")
                           
                                }
                            }
                        }
                        //  UserDefaults.standard.set(name.area_name, forKey: City)
                        UserDefaults.standard.set(name.PostalCode, forKey: "Postal_Code")
                        UserDefaults.standard.synchronize()
                        sharedInstance.switchLocEnabled = true
                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                        nextViewController.modalPresentationStyle = .fullScreen
                        self.present(nextViewController, animated: true, completion: nil)
                        
                        print("areaName")
                    }
                    locationActionSheetContr.addAction(locationNameActionButton)
                }
            }
            else if sharedInstance.areaListDataModel.area.isEmpty == false {
                for name in sharedInstance.areaListDataModel.area {
                    let locationNameActionButton = UIAlertAction(title: name.area_lang_name, style: .default)
                    { _ in
                        //  self.mapView.clear()
                        self.addLoadingView()
                        UserDefaults.standard.set(name.PostalCode, forKey: "Postal_Code")
                        UserDefaults.standard.synchronize()
                        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                        nextViewController.modalPresentationStyle = .fullScreen
                        self.present(nextViewController, animated: true, completion: nil)
                        
                        print("areaName")
                    }
                    locationActionSheetContr.addAction(locationNameActionButton)
                }
            }
            
            locationActionSheetContr.view.tintColor = UIColor.black
            self.present(locationActionSheetContr, animated: true, completion: nil)
            
            print("Location")
 
            //Commented google switch location.
            
         /*   let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
            // Adding color to Navigation Bar
            UINavigationBar.appearance().backgroundColor = appThemeColor
            resultsViewController = GMSAutocompleteResultsViewController()
            resultsViewController?.delegate = self
            searchController = UISearchController(searchResultsController: resultsViewController)
            searchController?.searchResultsUpdater = resultsViewController
            
            // Put the search bar in the navigation bar.
            searchController?.searchBar.sizeToFit()
            searchController?.navigationItem.titleView = searchController?.searchBar
            
            // When UISearchController presents the results view, present it in
            // this view controller, not one further up the chain.
            definesPresentationContext = true
            
            // Prevent the navigation bar from being hidden when searching.
            searchController?.hidesNavigationBarDuringPresentation = false */
 
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     This method is used to determine current location.
     @param -.
     @return -.
     */
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
            
        }
    }
    
    // MARK: CLLOCATION MANAGER DELEGATES
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.startUpdatingLocation()
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        loc_lat = String(describing: userLocation.coordinate.latitude)
        loc_lng = String(describing: userLocation.coordinate.longitude)
        UserDefaults.standard.set(loc_lat, forKey: "User_Lat")
        UserDefaults.standard.set(loc_lng, forKey: "User_Lng")
        //UserDefaults.standard.synchronize()
        
        let lat: Double = Double(userLocation.coordinate.latitude)
        let longt: Double = Double(userLocation.coordinate.longitude)
        let latString:String = String(format:"%3f", lat)
        print("latString: \(latString)") // b: 1.500000
        let lonString:String = String(format:"%3f", longt)
        print("lonString: \(lonString)") // b: 1.500000
        
        
        let latestLocation = locations.last!
        // here check if no need to continue just return still in the same place
        if latestLocation.horizontalAccuracy < 0 {
            return
        }
        // if it location is nil or it has been moved
        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            location = latestLocation
            // Here is the place you want to start reverseGeocoding
            geocoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) in
                // always good to check if no error
                // also we have to unwrap the placemark because it's optional
                // I have done all in a single if but you check them separately
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                // a new function where you start to parse placemarks to get the information you need
                self.parsePlacemarks()
                if self.city != nil {
                    if UserDefaults.standard.value(forKey: "KEY_DEVICE_TOKEN") != nil {
                        let deviceTokenText = String()
                        let deviceToken: String = deviceTokenText.passedString((UserDefaults.standard.object(forKey: "KEY_DEVICE_TOKEN") as? String))
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: deviceToken), object: nil, userInfo: nil)
                        
                    }
                }
            })
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func parsePlacemarks() {
        // here we check if location manager is not nil using a _ wild card
        if let _ = location {
            // unwrap the placemark
            if let placemark = placemark {
                // wow now you can get the city name. remember that apple refers to city name as locality not city
                // again we have to unwrap the locality remember optionalllls also some times there is no text so we check that it should not be empty
                if let city = placemark.subLocality, !city.isEmpty {
                    // here you have the city name
                    // assign city name to our iVar
                    self.city = city
                    UserDefaults.standard.set(city, forKey: City)
                    //UserDefaults.standard.synchronize()
                    print(city)
                }
                if let subAdministrativeAre = placemark.name, !subAdministrativeAre.isEmpty {
                    self.subAdministrativeAre = subAdministrativeAre
                }
                // the same story optionalllls also they are not empty
                if let country = placemark.country, !country.isEmpty {
                    self.country = country
                }
                if let postalCode = placemark.postalCode, !postalCode.isEmpty {
                    self.postalCode = postalCode
                    UserDefaults.standard.set(postalCode, forKey: "Postal_Code")
                    UserDefaults.standard.synchronize()
                    print(postalCode)
                    let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                    nextViewController.modalPresentationStyle = .fullScreen
                    self.present(nextViewController, animated: true, completion: nil)
                }
                // get the country short name which is called isoCountryCode
                if let countryShortName = placemark.isoCountryCode, !countryShortName.isEmpty {
                    self.countryShortName = countryShortName
                }
                
                //                if let addressLine = placemark.addressDictionary!["FormattedAddressLines"], placemark.addressDictionary!["FormattedAddressLines"] != nil {
                
                self.address = self.getAddressString(placemark: placemark)
                //                }
            }
        } else {
            // add some more check's if for some reason location manager is nil
        }
    }
    
    /*
     This method is used to get the placemarks.
     @param -.
     @return -.
     */
    func getAddressString(placemark: CLPlacemark) -> String? {
        var originAddress : String?
        if let addrList = placemark.addressDictionary?[FormattedAddressLines] as? [String]
        {
            originAddress =  addrList.joined(separator: ", ")
        }
        return originAddress
    }
    
    // MARK: - TextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    /*
     This method is used to perform back button action.
     @param --.
     @return --.
     */
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        print(#function)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SelectLocationViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place latitude: \(String(describing: place.coordinate.latitude))")
        print("Place longtitude: \(String(describing: place.coordinate.longitude))")
        
        var lat = String()
        var lng = String()
        lat = String(describing: place.coordinate.latitude)
        lng = String(describing: place.coordinate.longitude)
        
        UserDefaults.standard.set(lat, forKey: "User_Lat")
        UserDefaults.standard.set(lng, forKey: "User_Lng")
        
        UserDefaults.standard.set(place.name, forKey: "City")
        dismiss(animated: true, completion: nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBarId") as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

// Handle the user's selection.
extension SelectLocationViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        print("Place latitude: \(String(describing: place.coordinate.latitude))")
        print("Place longtitude: \(String(describing: place.coordinate.longitude))")
        
        
        //        print("Lat: \(String(describing: lattitude))")
        //        print("Lng: \(String(describing: longitude))")
        var lat = String()
        var lng = String()
        lat = String(describing: place.coordinate.latitude)
        lng = String(describing: place.coordinate.longitude)
        UserDefaults.standard.set(lat, forKey: "User_Lat")
        UserDefaults.standard.set(lng, forKey: "User_Lng")
        
        //UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(place.name, forKey: "City")
        //UserDefaults.standard.synchronize()
        dismiss(animated: true, completion: nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
//    func activityIndicator() {
//        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//        indicator.activityIndicatorViewStyle = UIActivityIndicatorView.Style.gray
//        indicator.center = self.view.center
//        self.view.addSubview(indicator)
//    }
}


extension SelectLocationViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("binfif",filteredData.count)

        return   filteredData.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableview2.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
        print("dvsfgsf",filteredData[indexPath.row])
        cell.textLabel!.text = filteredData[indexPath.row]
        return cell
         }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableview2.cellForRow(at: indexPath)
    if self.areaListDataModel.area.isEmpty == false {
                 self.addLoadingView()
                 let geocoder = CLGeocoder()

        var AreaName1:String? = nil
        geocoder.geocodeAddressString((cell?.textLabel!.text)! ) {
                     placemarks, error in
            let placemark = placemarks?.first
//            print("fuhsfihisfh\(placemark) \t \(PostalCode1) \t \(AreaName1)")
          
            AreaName1   = placemark?.name
            call()
                 
          
                     if let lat = placemark?.location?.coordinate.latitude {
                         if let lon = placemark?.location?.coordinate.longitude {
                     self.latitude = String(describing: lat)
                     self.longitude = String(describing: lon)
                     
                     UserDefaults.standard.set(self.latitude, forKey: "User_Lat")
                     UserDefaults.standard.set(self.longitude, forKey: "User_Lng")
                     print("lat \(String(describing: self.latitude)), long \(String(describing: self.longitude))")
                         }
                     }
                 }
        func call(){
     
            
            for myObj in self.areaListDataModel.area where myObj.area_lang_name == cell?.textLabel!.text {
             //object with name is foo
               let data  = myObj.PostalCode
              UserDefaults.standard.set(data, forKey: "Postal_Code")
            }
            
                            UserDefaults.standard.set(AreaName1 , forKey: City)

//            let str2 = String(samplestring!)
//            print("ggjgj",str2)
          
                      
                           
                            UserDefaults.standard.synchronize()
                            sharedInstance.switchLocEnabled = true
                            let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
            nextViewController.modalPresentationStyle = .fullScreen
                            self.present(nextViewController, animated: true, completion: nil)
                            
                            
        }
                 print("areaName")
             
           //  locationActionSheetContr.addAction(locationNameActionButton)
         
     }
     else if sharedInstance.areaListDataModel.area.isEmpty == false {
         for name in sharedInstance.areaListDataModel.area {
             let locationNameActionButton = UIAlertAction(title: name.area_lang_name, style: .default)
             { _ in
                 //  self.mapView.clear()
                 self.addLoadingView()
                 UserDefaults.standard.set(name.PostalCode, forKey: "Postal_Code")
                 UserDefaults.standard.synchronize()
                 let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardName, bundle:nil)
                 let nextViewController = storyBoard.instantiateViewController(withIdentifier: TabBarId) as! UITabBarController
                 self.present(nextViewController, animated: true, completion: nil)
                 
                 print("areaName")
             }
             //locationActionSheetContr.addAction(locationNameActionButton)
         }
     }
     
}
}
extension SelectLocationViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
